/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <surface_hive/surface_hive.pmh>

#define FE_STK_DEBUG		FALSE
#define FE_STK_TEXTURE_TEST	FALSE

using namespace fe;
using namespace fe::ext;

namespace hive
{

SurfaceTrack::SurfaceTrack(void)
{
#if	FE_STK_DEBUG
	feLog("SurfaceTrack::SurfaceTrack\n");
#endif

	setRadius(3.0);

	m_spDrawTexture=new DrawMode();
	m_spDrawTexture->setDrawStyle(DrawMode::e_solid);
//	m_spDrawTexture->setRefinement(1);
	m_spDrawTexture->setBackfaceCulling(FALSE);
}

SurfaceTrack::~SurfaceTrack(void)
{
#if	FE_STK_DEBUG
	feLog("SurfaceTrack::~SurfaceTrack\n");
#endif
}

void SurfaceTrack::initialize(void)
{
	m_spSurfaceFallback=registry()->create("*.SurfacePlane");

	sp<SurfacePlane> spSurfacePlane=m_spSurfaceFallback;
	if(spSurfacePlane.isValid())
	{
		spSurfacePlane->setRadius(300.0);
	}
}

Protectable* SurfaceTrack::clone(Protectable* pInstance)
{
#if	FE_STK_DEBUG
	feLog("SurfaceTrack::clone\n");
#endif
	return NULL;
}

void SurfaceTrack::cache(void)
{
#if	FE_STK_DEBUG
	feLog("SurfaceTrack::cache\n");
#endif

	m_spHiveHost=HiveHost::create();
	FEASSERT(m_spHiveHost.isValid());

	Result result=registry()->manage("feAutoLoadDL");
	if(fe::failure(result))
	{
		feLog("SurfaceTrack::cache failed to manage feAutoLoadDL\n");
	}

	result=registry()->manage("hiveApiaryDL");
	if(fe::failure(result))
	{
		feLog("SurfaceTrack::cache failed to manage hiveApiaryDL\n");
	}

	m_spStateCatalog=NULL;
	m_spTrackAccessible=NULL;
	m_surfaceArray.clear();
	m_colorArray.clear();
	m_nameArray.clear();
	m_bufferArray.clear();

	updateState();
}

void SurfaceTrack::updateState(void)
{
#if	FE_STK_DEBUG
	feLog("SurfaceTrack::updateState\n");
#endif

	if(m_spStateCatalog.isNull())
	{
		//* don't create a space here
		if(m_spHiveHost->hasSpace("world"))
		{
			m_spStateCatalog=m_spHiveHost->accessSpace("world");

			m_spStateCatalog->catalogDump();
		}
	}

	if(m_spStateCatalog.isValid() && !m_surfaceArray.size())
	{
		String trackFilename;
		Result result=m_spStateCatalog->getState<String>("track.filename",
				trackFilename);
		feLog("SurfaceTrack::updateState trackFilename \"%s\"\n",
				trackFilename.c_str());

		if(failure(result) || trackFilename.empty())
		{
			return;
		}

		const String format=
				String(strrchr(trackFilename.c_str(),'.')).prechop(".");

		m_spTrackAccessible=
				registry()->create("SurfaceAccessibleI.*.*."+format);
		if(m_spTrackAccessible.isNull())
		{
			return;
		}

		String paths;
		result=m_spStateCatalog->getState<String>("paths."+format,paths);

		String path=System::findFile(trackFilename,paths);
		if(path.empty())
		{
			feLog("SurfaceTrack::updateState"
					" failed to find \"%s\" using paths \"%s\"\n",
					trackFilename.c_str(),paths.c_str());
			return;
		}

		const String trackFullname=path+"/"+trackFilename;

		sp<Catalog> spSettings=
				m_spHiveHost->master()->createCatalog("settings");
		spSettings->catalog<String>("options")="ytoz";
		if(!m_spTrackAccessible->load(trackFullname,spSettings))
		{
			feLog("failed to load model \"%s\"\n",trackFullname.c_str());
			m_spTrackAccessible=NULL;
			return;
		}

		Array<String> meshNames;
		sp<SurfaceAccessorI> spNodeName=m_spTrackAccessible->accessor(
				SurfaceAccessibleI::e_primitive,"name");
		sp<SurfaceAccessorI> spNodeType=m_spTrackAccessible->accessor(
				SurfaceAccessibleI::e_primitive,"type");
		const I32 nodeCount=spNodeName->count();
		for(I32 nodeIndex=0;nodeIndex<nodeCount;nodeIndex++)
		{
			const String nodeName=
					spNodeName->string(nodeIndex);
			const String nodeType=
					spNodeType->string(nodeIndex);
			if(nodeType=="Mesh")
			{
				meshNames.push_back(nodeName);
			}
		}

		Regex patternOuter(".*Outer.*");
		Regex patternInner(".*Inner.*");
		Regex patternTrack(".*Track.*");
		Regex patternKerb(".*Kerb.*");

		const I32 meshCount=meshNames.size();
		m_surfaceArray.resize(meshCount);
		m_colorArray.resize(meshCount);
		m_nameArray.resize(meshCount);
		m_bufferArray.resize(meshCount);
		for(I32 meshIndex=0;meshIndex<meshCount;meshIndex++)
		{
			m_surfaceArray[meshIndex]=
					m_spTrackAccessible->surface(meshNames[meshIndex]);
			m_nameArray[meshIndex]=meshNames[meshIndex];

			Color& rColor=m_colorArray[meshIndex];

			if(patternOuter.match(meshNames[meshIndex].c_str()))
			{
				set(rColor,0.8,0.4,0.1);
			}
			else if(patternInner.match(meshNames[meshIndex].c_str()))
			{
				set(rColor,0.2,0.8,0.2);
			}
			else if(patternTrack.match(meshNames[meshIndex].c_str()))
			{
				set(rColor,0.5,0.5,0.5);
			}
			else if(patternKerb.match(meshNames[meshIndex].c_str()))
			{
				set(rColor,0.8,0.2,0.2);
			}
			else
			{
				set(rColor,1.0,0.0,1.0);
			}

			rColor*=0.7+0.1*(meshIndex%4);
			rColor[3]=1.0;

			//* combine bounding spheres
			const SpatialVector center=m_surfaceArray[meshIndex]->center();
			const Real radius=m_surfaceArray[meshIndex]->radius();

			if(!meshIndex)
			{
				m_center=center;
				m_radius=radius;
			}
			else
			{
				SpatialVector radial=center-m_center;
				const Real distance=magnitude(radial);
				if(distance>1e-6)
				{
					radial*=(1.0/distance);
				}
				else
				{
					set(radial,0,0,1);
				}
				const Real reach=distance+radius;
				if(reach>m_radius)
				{
					const SpatialVector p0=m_center-m_radius*radial;
					const SpatialVector p1=m_center+reach*radial;
					m_center=0.5*(p0+p1);
					m_radius=0.5*(m_radius+reach);
				}
			}
		}

#if FE_STK_TEXTURE_TEST
		if(m_spImageI.isNull())
		{
			m_spImageI=registry()->create("ImageI");
			if(m_spImageI.isValid())
			{
				//* HACK could be multiple paths; presumes image with USD files
				String usdPath;
				Result result=m_spStateCatalog->getState<String>(
						"paths.usd",usdPath);

				String texturefilename;
				result=m_spStateCatalog->getState<String>(
						"track.textures.0.filename",texturefilename);

				I32 imageID=m_spImageI->loadSelect(texturefilename);

				if(imageID<0)
				{
					const String fullName=usdPath+"/"+texturefilename;
					imageID=m_spImageI->loadSelect(fullName);
				}

#if FE_STK_DEBUG
				feLog("SurfaceTrack::handle imageID %d size %d %d\n",
						imageID,m_spImageI->width(),m_spImageI->height());
#endif

				if(imageID<0)
				{
					feLog("SurfaceTrack::handle"
							" failed to load texture \"%s\"\n",
							texturefilename.c_str());
				}
			}
		}

		if(m_spImageI.isNull())
		{
			feLog("SurfaceTrack::handle no texture\n");
		}

		m_spDrawTexture->setTextureImage(m_spImageI);
#endif
	}

	//* TEST edit texture
	if(m_spImageI.isValid())
	{
		static Real s_editRadius(0);
		static Real s_editAngle(0);
		const Real editX=0.5*m_spImageI->width()+s_editRadius*cos(s_editAngle);
		const Real editY=0.5*m_spImageI->height()+s_editRadius*sin(s_editAngle);
		s_editRadius+=0.1;
		s_editAngle+=0.1;
		const U32 data[1]={0};

		feLog("SurfaceTrack::handle replace %.3f %.3f size %d %d\n",
				editX,editY,m_spImageI->width(),m_spImageI->height());

		m_spImageI->replaceRegion(editX,editY,0,1,1,1,(void*)data);
	}
}

void SurfaceTrack::prepareForSearch(void)
{
#if	FE_STK_DEBUG
	feLog("SurfaceTrack::prepareForSearch\n");
#endif

	if(m_cached)
	{
		return;
	}

	checkCache();

	const I32 surfaceCount=m_surfaceArray.size();
	for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
	{
		m_surfaceArray[surfaceIndex]->prepareForSearch();
	}
}

sp<SurfaceI::ImpactI> SurfaceTrack::nearestPoint(
	const SpatialVector& a_origin,Real a_maxDistance) const
{
#if	FE_STK_DEBUG
	feLog("SurfaceTrack::nearestPoint to %s\n",c_print(a_origin));
#endif

	const I32 surfaceCount=m_surfaceArray.size();

	if(!surfaceCount && m_spSurfaceFallback.isValid())
	{
#if	FE_STK_DEBUG
		feLog("SurfaceTrack::nearestPoint fallback\n");
#endif

		return m_spSurfaceFallback->nearestPoint(a_origin,a_maxDistance);
	}

	sp<SurfaceI::ImpactI> spBestImpactI(NULL);
	Real bestDistance(-1.0);
	for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
	{
		sp<SurfaceI::ImpactI> spImpactI=
				m_surfaceArray[surfaceIndex]->nearestPoint(
				a_origin,a_maxDistance);
		if(spImpactI.isValid())
		{
			const Real distance=spImpactI->distance();
			if(bestDistance<0.0 || bestDistance>distance)
			{
				bestDistance=distance;
				spBestImpactI=spImpactI;
			}
		}
	}
#if	FE_STK_DEBUG
	if(spBestImpactI.isValid())
	{
		feLog("SurfaceTrack::nearestPoint is %s\n",
				c_print(spBestImpactI->intersection()));
	}
#endif
	return spBestImpactI;
}

sp<SurfaceI::ImpactI> SurfaceTrack::rayImpact(
	const SpatialVector& a_origin,const SpatialVector& a_direction,
	Real a_maxDistance,BWORD a_anyHit) const
{
#if	FE_STK_DEBUG
	feLog("SurfaceTrack::rayImpact to %s\n",c_print(a_origin));
#endif

	const I32 surfaceCount=m_surfaceArray.size();

	if(!surfaceCount && m_spSurfaceFallback.isValid())
	{
#if	FE_STK_DEBUG
		feLog("SurfaceTrack::rayImpact fallback\n");
#endif

		return m_spSurfaceFallback->rayImpact(a_origin,a_direction,
				a_maxDistance,a_anyHit);
	}

	sp<SurfaceI::ImpactI> spBestImpactI(NULL);
	Real bestDistance(-1.0);
	for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
	{
		sp<SurfaceI::ImpactI> spImpactI=
				m_surfaceArray[surfaceIndex]->rayImpact(
				a_origin,a_direction,a_maxDistance,a_anyHit);
		if(spImpactI.isValid())
		{
			const Real distance=spImpactI->distance();
			if(bestDistance<0.0 || bestDistance>distance)
			{
				bestDistance=distance;
				spBestImpactI=spImpactI;
			}
		}
	}
#if	FE_STK_DEBUG
	if(spBestImpactI.isValid())
	{
		feLog("SurfaceTrack::rayImpact is %s\n",
				c_print(spBestImpactI->intersection()));
	}
#endif
	return spBestImpactI;
}

void SurfaceTrack::draw(const SpatialTransform& a_rTransform,
	sp<DrawI> a_spDrawI,const fe::Color* a_color,
	sp<DrawBufferI> a_spDrawBuffer,sp<PartitionI> a_spPartition) const
{
#if	FE_STK_DEBUG
	feLog("SurfaceTrack::draw\n");
#endif

	const_cast<SurfaceTrack*>(this)->updateState();

	const I32 surfaceCount=m_surfaceArray.size();

	if(!surfaceCount && m_spSurfaceFallback.isValid())
	{
		const Color green(0.0,0.5,0.0);
		a_spDrawI->draw(m_spSurfaceFallback,&green);
		return;
	}

	a_spDrawI->pushDrawMode(m_spDrawTexture);

	for(I32 surfaceIndex=0;surfaceIndex<surfaceCount;surfaceIndex++)
	{
		sp<DrawBufferI>& rspBuffer=
				const_cast< sp<DrawBufferI>& >(m_bufferArray[surfaceIndex]);
		if(rspBuffer.isNull())
		{
			rspBuffer=a_spDrawI->createBuffer();
			if(rspBuffer.isValid())
			{
				rspBuffer->setName(m_nameArray[surfaceIndex]);
			}
		}

		a_spDrawI->draw(m_surfaceArray[surfaceIndex],
				&m_colorArray[surfaceIndex],rspBuffer);
	}

	a_spDrawI->popDrawMode();

#if	FE_STK_DEBUG
	feLog("SurfaceTrack::draw done\n");
#endif
}

} /* namespace hive */
