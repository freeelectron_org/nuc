/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __surface_hive_SurfaceAccessorPuppet_h__
#define __surface_hive_SurfaceAccessorPuppet_h__

namespace hive
{

/**************************************************************************//**
    @brief Accessor for Puppet meshes

	@ingroup surface_hive
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessorPuppet:
	public fe::ext::SurfaceAccessorBase,
	public fe::CastableAs<SurfaceAccessorPuppet>
{
	public:
						SurfaceAccessorPuppet(void);
virtual					~SurfaceAccessorPuppet(void)						{}

						using fe::ext::SurfaceAccessorBase::set;
						using fe::ext::SurfaceAccessorBase::append;
						using fe::ext::SurfaceAccessorBase::spatialVector;

						//* as SurfaceAccessorI
		BWORD			bind(fe::ext::SurfaceAccessibleI::Element a_element,
							fe::ext::SurfaceAccessibleI::Attribute
							a_attribute);
		BWORD			bind(fe::ext::SurfaceAccessibleI::Element a_element,
							const fe::String& a_name)
						{
							m_attribute=fe::ext::SurfaceAccessibleI::e_generic;
							return bindInternal(a_element,a_name);
						}
virtual	U32				count(void) const;
virtual	U32				subCount(U32 a_index) const;

virtual	void			set(U32 a_index,U32 a_subIndex,fe::String a_string)	{}
virtual	fe::String		string(U32 a_index,U32 a_subIndex=0);

virtual	void			set(U32 a_index,U32 a_subIndex,I32 a_integer)		{}
virtual	I32				integer(U32 a_index,U32 a_subIndex=0)	{ return 0; }

virtual	I32				append(fe::ext::SurfaceAccessibleI::Form a_form)
						{	return 0; }

virtual	void			append(U32 a_index,I32 a_integer)					{}

virtual	void			set(U32 a_index,U32 a_subIndex,fe::Real a_real)		{}

virtual	fe::Real		real(U32 a_index,U32 a_subIndex=0)	{ return 0.0; }

virtual	void			set(U32 a_index,U32 a_subIndex,
							const fe::SpatialVector& a_vector)				{}
virtual	fe::SpatialVector	spatialVector(U32 a_index,U32 a_subIndex=0)
						{	return fe::SpatialVector(0,0,0); }

virtual	void			setSurface(fe::sp<fe::ext::SurfaceI> a_spSurfaceI)
						{	m_spSurfaceI=a_spSurfaceI; }

	private:

virtual	BWORD			bindInternal(
							fe::ext::SurfaceAccessibleI::Element a_element,
							const fe::String& a_name);

		BWORD			isBound(void) const
						{	return TRUE; }

		fe::sp<fe::ext::SurfaceI>	m_spSurfaceI;
};

} /* namespace hive */

#endif /* __surface_hive_SurfaceAccessorPuppet_h__ */
