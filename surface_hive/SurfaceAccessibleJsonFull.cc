/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <surface_hive/surface_hive.pmh>

#define FE_SACG_DEBUG			FALSE

using namespace fe;
using namespace fe::ext;

namespace hive
{

//* TEMP copied here until we get the lla2enu module
struct lla2enu
{
  double    m_originLat;
  double    m_originLon;
  double    m_originAlt;
  double    m_originLambda;
  double    m_origPhi;
  double    m_originSinLambda;
  double    m_originCosLambda;
  double    m_origSinPhi;
  double    m_origCosPhi;
  double    x0;
  double    y0;
  double    z0;
  double    m_origin_s;
  double    m_origin_N;

  const double earth_a = 6378137.0;            // WGS-84 Earth semimajor axis (m)
  const double earth_b = 6356752.314245;          // Derived Earth semiminor axis (m)
  const double earth_f = (earth_a - earth_b) / earth_a;  // Ellipsoid Flatness
  const double earth_e_sq = earth_f * (2 - earth_f);    // Square of Eccentricity

  double DEG2RAD(double a){return a/(180.0/M_PI);}
  double RAD2DEG(double a){return a*(180.0/M_PI);}

  void init(double originLat, double originLon, double orignAltitude)
  {
    m_originLat = originLat;
    m_originLon = originLon;
    m_originAlt = orignAltitude;

    m_originLambda = DEG2RAD(originLat);
    m_origPhi = DEG2RAD(originLon);
    m_origin_s = std::sin(m_originLambda);
    m_origin_N = earth_a / std::sqrt(1 - earth_e_sq * m_origin_s * m_origin_s);

    m_originSinLambda = std::sin(m_originLambda);
    m_originCosLambda = std::cos(m_originLambda);
    m_origCosPhi = std::cos(m_origPhi);
    m_origSinPhi = std::sin(m_origPhi);

    x0 = (orignAltitude + m_origin_N) * m_originCosLambda * m_origCosPhi;
    y0 = (orignAltitude + m_origin_N) * m_originCosLambda * m_origSinPhi;
    z0 = (orignAltitude + (1 - earth_e_sq) * m_origin_N) * m_originSinLambda;

    //feLog("[lla2enu::init] x0: %lf; y0: %lf; z0: %lf\n",
    //  x0,y0,z0);
  };

  void convert(fe::Vector3f & Postion, double Lat, double Lon, double Alt)
  {
    double x, y, z;
    GeodeticToEcef(Lat, Lon, Alt, x, y, z);

    double xEast, yNorth, zUp;
    EcefToEnu(x, y, z, xEast, yNorth, zUp);

    Postion = fe::Vector3f(xEast, yNorth, zUp);
    //feLog("[lla2enu::convert]: %s\n", c_print(Postion));
  };
  void GeodeticToEcef(double lat, double lon, double h, double & x, double & y, double & z)
  {
    double lambda = DEG2RAD(lat);
    double phi = DEG2RAD(lon);
    double s = std::sin(lambda);
    double N = earth_a / std::sqrt(1 - earth_e_sq * s * s);

    double sin_lambda = std::sin(lambda);
    double cos_lambda = std::cos(lambda);
    double cos_phi = std::cos(phi);
    double sin_phi = std::sin(phi);

    x = (h + N) * cos_lambda * cos_phi;
    y = (h + N) * cos_lambda * sin_phi;
    z = (h + (1 - earth_e_sq) * N) * sin_lambda;
  }

  void EcefToEnu(double x, double y, double z, double & xEast,
          double & yNorth, double & zUp)
  {
    double xd, yd, zd;
    xd = x - x0;
    yd = y - y0;
    zd = z - z0;

    xEast = -m_origSinPhi*xd + m_origCosPhi * yd;

    yNorth = -m_origCosPhi*m_originSinLambda*xd -
        m_originSinLambda*m_origSinPhi*yd +
        m_originCosLambda*zd;

    zUp = m_originCosLambda*m_origCosPhi*xd +
      m_originCosLambda*m_origSinPhi*yd +
      m_originSinLambda*zd;
  }
};

void SurfaceAccessibleJsonFull::reset(void)
{
	if(m_spJsonRoot.isNull())
	{
		m_spJsonRoot=sp<JsonValue>(new JsonValue());
	}
}

sp<SurfaceAccessorI> SurfaceAccessibleJsonFull::accessor(
	String a_node,Element a_element,String a_name,Creation a_create)
{
	sp<SurfaceAccessorJsonFull> spAccessor(new SurfaceAccessorJsonFull);

	if(spAccessor.isValid())
	{
		spAccessor->setJsonRoot(m_spJsonRoot);
		spAccessor->setOriginOffset(m_originOffset);
		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

sp<SurfaceAccessorI> SurfaceAccessibleJsonFull::accessor(
	String a_node,Element a_element,Attribute a_attribute,Creation a_create)
{
	sp<SurfaceAccessorJsonFull> spAccessor(new SurfaceAccessorJsonFull);

	if(spAccessor.isValid())
	{
		spAccessor->setJsonRoot(m_spJsonRoot);
		spAccessor->setOriginOffset(m_originOffset);
		if(spAccessor->bind(a_element,a_attribute) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

BWORD SurfaceAccessibleJsonFull::load(String a_filename,
	sp<Catalog> a_spSettings)
{
#if FE_SACG_DEBUG
	feLog("SurfaceAccessibleJsonFull::load \"%s\"\n",a_filename.c_str());
#endif

	String options;

	if(a_spSettings.isValid())
	{
		options=a_spSettings->catalog<String>("options");
	}
	else
	{
		options="";
	}

	std::map<String,String> optionMap;

	String buffer=options;
	String option;
	while(!(option=buffer.parse()).empty())
	{
		const String property=option.parse("\"","=");
		const String value=option.parse("\"","=");

#if FE_SACG_DEBUG
		feLog("SurfaceAccessibleJsonFull::load parse \"%s\" \"%s\"\n",
				property.c_str(),value.c_str());
#endif

		optionMap[property]=value;
	}

	if(m_spJsonRoot.isNull())
	{
		m_spJsonRoot=sp<JsonValue>(new JsonValue());
	}
	m_spJsonRoot->value().clear();

	std::ifstream in(a_filename.c_str());
	if(!in)
	{
		feLog("SurfaceAccessibleJsonFull::load"
				" could not open \"%s\"\n",
				a_filename.c_str());
		return FALSE;
	}

	//* NOTE stackDepth_g is shared between readers so reentrant is risky
	//* TEMP try Json::CharReaderBuilder instead of Json::Reader
	SAFEGUARDCLASS;

	Json::Reader reader;
	const BWORD success=reader.parse(in,m_spJsonRoot->value());

	if(!success)
	{
		feLog("SurfaceAccessibleJsonFull::load parsing failed: %s\n",
				reader.getFormattedErrorMessages().c_str());
	}

	//* NOTE translate json_full curves to align with track
	//* track.coordinates are the LLA of track origin
	//* ReferencePoint is the LLA of the json_full origin
	if(optionMap.find("track_coordinates")!=optionMap.end())
	{
		Vector3d trackCoordinates(0,0,0);
		sscanf(optionMap["track_coordinates"].c_str(),"%lf %lf %lf",
				&trackCoordinates[0],&trackCoordinates[1],&trackCoordinates[2]);

		Json::Value referencePointNode=m_spJsonRoot->value()["ReferencePoint"];
		Vector3d referencePoint(
				referencePointNode[0].asFloat(),
				referencePointNode[1].asFloat(),
				referencePointNode[2].asFloat());

		lla2enu convertor;
		convertor.init(trackCoordinates[0],trackCoordinates[1],
				trackCoordinates[2]);

		convertor.convert(m_originOffset,
				referencePoint[0],referencePoint[1],referencePoint[2]);

#if !FE_JSON_FULL_USE_ALTITUDE
		m_originOffset[2]=0.0;
#endif

#if FE_SACG_DEBUG
			feLog("SurfaceAccessibleJsonFull::load"
					" trackCoordinates %s referencePoint %s enu %s\n",
					c_print(trackCoordinates),c_print(referencePoint),
					c_print(m_originOffset));
#endif

	}

	return success;
}

BWORD SurfaceAccessibleJsonFull::save(String a_filename,
	sp<Catalog> a_spSettings)
{
#if FE_SACG_DEBUG
	feLog("SurfaceAccessibleJsonFull::save \"%s\"\n",a_filename.c_str());
#endif

	if(m_spJsonRoot.isNull())
	{
		return FALSE;
	}

	std::ofstream out(a_filename.c_str());

	Json::StyledWriter writer;
	out << writer.write(m_spJsonRoot->value());

	return TRUE;
}

Protectable* SurfaceAccessibleJsonFull::clone(Protectable* pInstance)
{
#if FE_SACG_DEBUG
	feLog("SurfaceAccessibleJsonFull::clone\n");
#endif

	SurfaceAccessibleJsonFull* pSurfaceAccessibleJsonFull= pInstance?
			fe_cast<SurfaceAccessibleJsonFull>(pInstance):
			new SurfaceAccessibleJsonFull();

	pSurfaceAccessibleJsonFull->setLibrary(library());

	sp<JsonValue> spNewRoot=sp<JsonValue>(new JsonValue());
	if(m_spJsonRoot.isValid())
	{
		spNewRoot->value()=m_spJsonRoot->value();
	}
	pSurfaceAccessibleJsonFull->setJsonRoot(spNewRoot);
	pSurfaceAccessibleJsonFull->m_originOffset=m_originOffset;

	return pSurfaceAccessibleJsonFull;
}

void SurfaceAccessibleJsonFull::attributeSpecs(
	Array<SurfaceAccessibleI::Spec>& a_rSpecs,
	String a_node,
	SurfaceAccessibleI::Element a_element) const
{
#if FE_SACG_DEBUG
	feLog("SurfaceAccessibleJsonFull::attributeSpecs\n");
#endif

	a_rSpecs.clear();

	if(a_element==SurfaceAccessibleI::e_point)
	{
		SurfaceAccessibleI::Spec spec;

		spec.set("P","vector3");
		a_rSpecs.push_back(spec);

		spec.set("Cd","vector3");
		a_rSpecs.push_back(spec);

		spec.set("RacingPsi","real");
		a_rSpecs.push_back(spec);

		spec.set("RacingK","real");
		a_rSpecs.push_back(spec);

		spec.set("part","string");
		a_rSpecs.push_back(spec);
	}

	if(a_element==SurfaceAccessibleI::e_primitive)
	{
		SurfaceAccessibleI::Spec spec;

		spec.set("part","string");
		a_rSpecs.push_back(spec);
	}
}

} /* namespace hive */
