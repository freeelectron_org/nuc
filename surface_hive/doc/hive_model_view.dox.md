Using model_view in Hive {#hive_model_view}
============================

Building model_view {#hive_model_view_build}
======================

The `model_view.bash` script runs `model_view.py`.
Both files will be copied to the appropriate subdirectory under `lib`
when the `ext/opview` module is built.

The opview module will only build if a minimal set of prerequsites
are also built.

An appropriate set of other modules will need to be built
for the intended set of models to be loaded,
such as usd for USD support and fbx for FBX support.

Basic usage of model_view {#hive_model_view_use}
=========================

If the simplest case, you can just call `model_view` with the filenames
of some models you want to show together.

```sh
lib/x86_64_linux_optimize/model_view.bash model1.fbx model2.usd
```

A large number of file formats are potentially supported,
but the effective support is limited by the list of modules that you have built.

You are welcome to make an alias or caller script,
perhaps simply called `model_view`, that calls `model_view.bash`
from whatever location you generally build it to.

Calling model_view without args will give you the basic usage statement.

This viewer tool will default to pointing the y axis up,
given its movie production heritage.
Hive usage will generally need the `-zup` arg.

The `-options` arg can be used to pass surface options and key/value pairs
to models that follow on the command line.
Each instance of `-options` resets the options for any models that follow,
so each model can configured differently.

Racing cars in model_view {#hive_model_view_vehicles}
=========================

To view and operate vehicles in model_view,
some pseudo-surface types have provided in the surface_hive module.
The SurfaceTrack class supports a `.track` psuedo file type
that sets up and draws all the track,
using the canonical data for the filename.
The SurfacePuppet class supports a `.pup` psuedo file type
that sets up and draws all the vehicles and obstacles.
Aside from the suffixes,
the name of the track and puppet filenames does not matter;
they aren't really loaded.

Common usage currently runs the viewer on the two pseudo files
as well a json_full curve file, if desired.

```sh
lib/x86_64_linux_optimize/model_view.bash -zup -options config=model/client.yaml null.track null.pup model/ORN3_0_5m.json_full
```

They config option specifies a yaml file needed to make the network connection.
The canonical data of the scene itself is all provided by the server
over the connection.
