/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <surface_hive/surface_hive.pmh>

#define FE_SAP_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

namespace hive
{

SurfaceAccessorPuppet::SurfaceAccessorPuppet(void)
{
	setName("SurfaceAccessorPuppet");
}

BWORD SurfaceAccessorPuppet::	bind(SurfaceAccessibleI::Element a_element,
	SurfaceAccessibleI::Attribute a_attribute)
{
	m_attribute=a_attribute;

	String name;
	switch(a_attribute)
	{
		case fe::ext::SurfaceAccessibleI::e_generic:
		case fe::ext::SurfaceAccessibleI::e_position:
			name="P";
			break;
		case fe::ext::SurfaceAccessibleI::e_normal:
			name="N";
			break;
		case fe::ext::SurfaceAccessibleI::e_uv:
			name="uv";
			break;
		case fe::ext::SurfaceAccessibleI::e_color:
			name="Cd";
			break;
		case fe::ext::SurfaceAccessibleI::e_vertices:
			m_attrName="vertices";
			FEASSERT(a_element==SurfaceAccessibleI::e_primitive);
			break;
		case fe::ext::SurfaceAccessibleI::e_properties:
			m_attrName="properties";
			FEASSERT(a_element==SurfaceAccessibleI::e_primitive);
			break;
	}
	return bindInternal(a_element,name);
}

U32 SurfaceAccessorPuppet::count(void) const
{
	if(m_attribute!=SurfaceAccessibleI::e_generic &&
			m_attribute!=
			fe::ext::SurfaceAccessibleI::e_vertices &&
			m_attribute!=
			fe::ext::SurfaceAccessibleI::e_properties &&
			!isBound())
	{
		return 0;
	}

	switch(m_element)
	{
		case SurfaceAccessibleI::e_point:
		case SurfaceAccessibleI::e_vertex:
			return 0;
		case SurfaceAccessibleI::e_primitive:
		{
			sp<SurfacePuppet> spSurfacePuppet(m_spSurfaceI);
			if(spSurfacePuppet.isNull())
			{
				return 0;
			}
			return spSurfacePuppet->proxyCount()+1;
		}
		case SurfaceAccessibleI::e_detail:
			return 1;
		default:
			;
	}

	return 0;
}

U32 SurfaceAccessorPuppet::subCount(U32 a_index) const
{
	if(m_attribute!=SurfaceAccessibleI::e_generic &&
			m_attribute!=fe::ext::SurfaceAccessibleI::e_vertices &&
			m_attribute!=SurfaceAccessibleI::e_properties &&
			!isBound())
	{
		return 0;
	}

	return 1;
}

String SurfaceAccessorPuppet::string(U32 a_index,U32 a_subIndex)
{
	if(!isBound())
	{
		return 0;
	}

	if(m_element==SurfaceAccessibleI::e_primitive && m_attrName=="type")
	{
		return "Mesh";
	}

	if(m_element==SurfaceAccessibleI::e_primitive && m_attrName=="name")
	{
		if(!a_index)
		{
			return "";
		}
		sp<SurfacePuppet> spSurfacePuppet(m_spSurfaceI);
		if(spSurfacePuppet.isNull())
		{
			return String();
		}
		return spSurfacePuppet->proxyName(a_index-1);
	}

	return String();
}


BWORD SurfaceAccessorPuppet::bindInternal(
	SurfaceAccessibleI::Element a_element,const String& a_name)
{
#if FE_SAP_DEBUG
	feLog("SurfaceAccessorPuppet::bindInternal %s \"%s\" \"%s\"\n",
			SurfaceAccessibleBase::elementLayout(a_element).c_str(),
			SurfaceAccessibleBase::attributeString(m_attribute).c_str(),
			a_name.c_str());
#endif

	m_element=SurfaceAccessibleI::e_point;
	if(a_element<0 && a_element>5)
	{
		a_element=SurfaceAccessibleI::e_point;
	}

	m_element=a_element;
	m_attrName=a_name;

	if(m_element==SurfaceAccessibleI::e_primitive)
	{
		if(m_attrName=="type" || m_attrName=="name")
		{
			return TRUE;
		}
	}

	return FALSE;
}

} /* namespace hive */
