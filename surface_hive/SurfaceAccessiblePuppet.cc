/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <surface_hive/surface_hive.pmh>

#define FE_SAP_DEBUG	TRUE

using namespace fe;
using namespace fe::ext;

namespace hive
{

SurfaceAccessiblePuppet::SurfaceAccessiblePuppet(void)
{
#if FE_SAP_DEBUG
	feLog("SurfaceAccessiblePuppet::SurfaceAccessiblePuppet\n");
#endif
}

SurfaceAccessiblePuppet::~SurfaceAccessiblePuppet(void)
{
#if FE_SAP_DEBUG
	feLog("SurfaceAccessiblePuppet::~SurfaceAccessiblePuppet\n");
#endif
}

sp<SurfaceAccessorI> SurfaceAccessiblePuppet::accessor(
	String a_node,Element a_element,String a_name,Creation a_create)
{
	sp<SurfaceAccessorPuppet> spAccessor(new SurfaceAccessorPuppet);

	if(spAccessor.isValid())
	{
		if(spAccessor->bind(a_element,a_name) ||
				a_create==SurfaceAccessibleI::e_createMissing)
		{
			spAccessor->setSurface(m_spSurfaceI);
			spAccessor->setSurfaceAccessible(sp<SurfaceAccessibleI>(this));
			return spAccessor;
		}
	}

	return sp<SurfaceAccessorI>(NULL);
}

BWORD SurfaceAccessiblePuppet::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAP_DEBUG
	feLog("SurfaceAccessiblePuppet::load \"%s\"\n",a_filename.c_str());
#endif

	sp<HiveHost> spHiveHost=HiveHost::create();
	FEASSERT(spHiveHost.isValid());

	if(a_spSettings.isValid())
	{
		sp<Catalog> spPreCatalog=registry()->master()->createCatalog("yaml");

		String options=a_spSettings->catalog<String>("options");

		String option;
		while(!(option=options.parse()).empty())
		{
			const String property=option.parse("\"","=");
			const String value=option.parse("\"","=");

			if(property=="config")
			{
				const String configFilename=value;

				sp<CatalogReaderI> spCatalogReader=
						registry()->create("CatalogReaderI.*.*.yaml");
				if(spCatalogReader.isNull())
				{
					feLog("SurfaceAccessiblePuppet::load"
							" spCatalogReader invalid");
					continue;
				}

				spCatalogReader->load(configFilename,spPreCatalog);
			}
		}

		const String implementation=
				spPreCatalog->catalogOrDefault<String>(
				"net:implementation","ConnectedCatalog");
		if(!implementation.empty())
		{
			sp<StateCatalog> spStateCatalog=
					spHiveHost->accessSpace("world",implementation);
			if(spStateCatalog.isNull())
			{
				feLog("SurfaceAccessiblePuppet::load"
						" spStateCatalog invalid");
			}
			else
			{
				spStateCatalog->overlayState(spPreCatalog);
			}

//			spStateCatalog->catalogDump();
		}
	}

	m_spSurfaceI=registry()->create("*.SurfacePuppet");

#if FE_SAP_DEBUG
	feLog("SurfaceAccessiblePuppet::load valid %d\n",
			m_spSurfaceI.isValid());
#endif

	//* NOTE force cache
	m_spSurfaceI->prepareForSample();

	return TRUE;
}

sp<SurfaceI> SurfaceAccessiblePuppet::surface(String a_group)
{
#if FE_SAP_DEBUG
	feLog("SurfaceAccessiblePuppet::surface group \"%s\"\n",a_group.c_str());
#endif

	if(!a_group.empty())
	{
		sp<SurfacePuppet> spSurfacePuppet(m_spSurfaceI);
		if(spSurfacePuppet.isValid())
		{
			const I32 proxyCount=spSurfacePuppet->proxyCount();
			for(I32 proxyIndex=0;proxyIndex<proxyCount;proxyIndex++)
			{
				if(spSurfacePuppet->proxyName(proxyIndex)==a_group)
				{
					return spSurfacePuppet->proxySurface(proxyIndex);
				}
			}
		}
	}

	return m_spSurfaceI;
}

} /* namespace hive */
