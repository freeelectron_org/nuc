import sys
forge = sys.modules["forge"]

def prerequisites():
	return [ "guide", "json", "surface" ]

def setup(module):
	srcList = [	"SurfaceAccessibleJsonFull",
				"SurfaceAccessiblePuppet",
				"SurfaceAccessibleTrack",
				"SurfaceAccessorJsonFull",
				"SurfaceAccessorPuppet",
				"SurfacePuppet",
				"SurfaceTrack",
				"surface_hive.pmh",
				"surface_hiveDL" ]

	dll = module.DLL( "hiveSurfaceHiveDL", srcList )

	module.includemap = {}
	module.includemap["jsoncpp"] = "ext/json/jsoncpp/include"

	module.cppmap = {}
	module.cppmap["jsoncpp_import"] = " -D JSON_DLL"

	deplibs = forge.corelibs+ [
				"fexJsonDLLib",
				"fexOperateDLLib",
				"fexSurfaceDLLib",
				"hiveApiaryDLLib" ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [	"fexDataToolDLLib",
						"fexDrawDLLib",
						"fexGeometryDLLib",
						"fexThreadDLLib"]

	forge.deps( ["hiveSurfaceHiveDLLib"], deplibs )

	forge.tests += [
		("inspect.exe",		"hiveSurfaceHiveDL",			None,		None) ]

	module.Module('test')
