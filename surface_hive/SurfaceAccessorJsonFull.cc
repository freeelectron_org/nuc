/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <surface_hive/surface_hive.pmh>

#define FE_SAJF_DEBUG	FALSE

using namespace fe;
using namespace fe::ext;

namespace hive
{

/*
{
	"Name":"ORN3_0_5m",
	"Version":1,
	"Date":"18-Jan-2020",
	"ReferencePoint":[36.27482436, -115.016884, 607.59],
	"Direction":0,
	"Zone":[],
	"Inside":
	[
		[-73.905575586301353, -2.607621515302994, -0],
		... more ...
	],
	"Outside":
	[
		[-70.722622058625063, 14.099128665456877, -0],
		... more ...
	],
	"Centre":
	[
		[-68.800802953079142, 0.540986476756296],
		... more ...
	],
	"Racing":
	[
		[-65.32253481694724, 2.878418929549007],
		... more ...
	],
	"RacingPsi":
	[
		3.7321469209722471,
		... more ... (-2.5 to 4.8)
	],
	"RacingK":
	[
		-0.0010148497877282541,
		... more ... (within +-0.04)
	]
}
*/

SurfaceAccessorJsonFull::SurfaceAccessorJsonFull(void)
{
	setName("SurfaceAccessorJsonFull");
	for(I32 m=0;m<6;m++)
	{
		m_pElementValues[m]=NULL;
	}

	m_curveNames.resize(6);
	m_curveNames[0]="Inside";
	m_curveNames[1]="Outside";
	m_curveNames[2]="Centre";
	m_curveNames[3]="Racing";
	m_curveNames[4]="RacingPsi";
	m_curveNames[5]="RacingK";
}

BWORD SurfaceAccessorJsonFull::	bind(SurfaceAccessibleI::Element a_element,
	SurfaceAccessibleI::Attribute a_attribute)
{
	m_attribute=a_attribute;

	String name;
	switch(a_attribute)
	{
		case fe::ext::SurfaceAccessibleI::e_generic:
		case fe::ext::SurfaceAccessibleI::e_position:
			name="P";
			break;
		case fe::ext::SurfaceAccessibleI::e_normal:
			name="N";
			break;
		case fe::ext::SurfaceAccessibleI::e_uv:
			name="uv";
			break;
		case fe::ext::SurfaceAccessibleI::e_color:
			name="Cd";
			break;
		case fe::ext::SurfaceAccessibleI::e_vertices:
			m_attrName="vertices";
			FEASSERT(a_element==SurfaceAccessibleI::e_primitive);
			break;
		case fe::ext::SurfaceAccessibleI::e_properties:
			m_attrName="properties";
			FEASSERT(a_element==SurfaceAccessibleI::e_primitive);
			break;
	}
	return bindInternal(a_element,name);
}

U32 SurfaceAccessorJsonFull::count(void) const
{
	if(m_attribute!=SurfaceAccessibleI::e_generic &&
			m_attribute!=
			fe::ext::SurfaceAccessibleI::e_vertices &&
			m_attribute!=
			fe::ext::SurfaceAccessibleI::e_properties &&
			!isBound())
	{
		return 0;
	}

	switch(m_element)
	{
		case SurfaceAccessibleI::e_point:
		case SurfaceAccessibleI::e_vertex:
			return m_vertStarts[3]+m_vertCounts[3];
		case SurfaceAccessibleI::e_primitive:
			return 4;
		case SurfaceAccessibleI::e_detail:
			return 1;
		default:
			;
	}

	return 0;
}

U32 SurfaceAccessorJsonFull::subCount(U32 a_index) const
{
	if(m_attribute!=SurfaceAccessibleI::e_generic &&
			m_attribute!=fe::ext::SurfaceAccessibleI::e_vertices &&
			m_attribute!=SurfaceAccessibleI::e_properties &&
			!isBound())
	{
		return 0;
	}

	if(m_attribute==SurfaceAccessibleI::e_vertices)
	{
		FEASSERT(m_element==SurfaceAccessibleI::e_primitive);

		return m_vertCounts[a_index];
	}

	return 1;
}

void SurfaceAccessorJsonFull::computePrimitiveVertex(
	U32 a_index,U32 a_subIndex,
	I32& a_rPrimitiveIndex,I32& a_rVertexIndex) const
{
	if(m_element==SurfaceAccessibleI::e_primitiveGroup ||
			m_attribute==SurfaceAccessibleI::e_vertices)
	{
		FEASSERT(m_element==
				SurfaceAccessibleI::e_primitiveGroup
				|| m_element==
				SurfaceAccessibleI::e_primitive);

		a_rPrimitiveIndex=a_index;
		a_rVertexIndex=a_subIndex;
	}
	else
	{
		a_rPrimitiveIndex=0;
		a_rVertexIndex=a_index;
		for(I32 m=0;m<4;m++)
		{
			if(a_rVertexIndex<m_vertCounts[m])
			{
				break;
			}
			a_rVertexIndex-=m_vertCounts[m];
			a_rPrimitiveIndex++;
		}
	}
}

String SurfaceAccessorJsonFull::string(U32 a_index,U32 a_subIndex)
{
	if(!isBound())
	{
		return 0;
	}

	if(m_element==SurfaceAccessibleI::e_point && m_attrName=="part")
	{
		I32 primitiveIndex=0;
		I32 vertexIndex=0;
		computePrimitiveVertex(a_index,a_subIndex,primitiveIndex,vertexIndex);

		return (primitiveIndex<6)? m_curveNames[primitiveIndex]: String();
	}

	if(m_element==SurfaceAccessibleI::e_primitive && m_attrName=="part")
	{
		return (a_index<6)? m_curveNames[a_index]: String();
	}

	//* TODO header values
	return String();
}

I32 SurfaceAccessorJsonFull::integer(U32 a_index,U32 a_subIndex)
{
#if FE_SAJF_DEBUG
	feLog("SurfaceAccessorJsonFull::integer(%d,%d) %s \"%s\" \"%s\"\n",
			a_index,a_subIndex,
			SurfaceAccessibleBase::elementLayout(m_element).c_str(),
			SurfaceAccessibleBase::attributeString(m_attribute).c_str(),
			m_attrName.c_str());
#endif

	if(m_element==SurfaceAccessibleI::e_primitiveGroup ||
			m_attribute==SurfaceAccessibleI::e_vertices)
	{
		FEASSERT(m_element==SurfaceAccessibleI::e_primitiveGroup ||
				m_element==SurfaceAccessibleI::e_primitive);

		return m_vertStarts[a_index]+a_subIndex;
	}

	if(m_attribute==SurfaceAccessibleI::e_properties)
	{
		FEASSERT(m_element==SurfaceAccessibleI::e_primitiveGroup ||
				m_element==SurfaceAccessibleI::e_primitive);
		if(a_subIndex==SurfaceAccessibleI::e_openCurve)
		{
			return TRUE;
		}
		if(a_subIndex==SurfaceAccessibleI::e_countU ||
				a_subIndex==SurfaceAccessibleI::e_countV ||
				a_subIndex==SurfaceAccessibleI::e_wrappedU ||
				a_subIndex==SurfaceAccessibleI::e_wrappedV)
		{
			switch(a_subIndex)
			{
				case SurfaceAccessibleI::e_countU:
					return m_vertCounts[a_index];
				case SurfaceAccessibleI::e_countV:
					return 1;
				case SurfaceAccessibleI::e_wrappedU:
					return 0;
				case SurfaceAccessibleI::e_wrappedV:
					return 0;
				default:
					;
			}
		}
		return 0;
	}

	if(!isBound())
	{
		return 0;
	}

	//* TODO header values
	return 0;
}

Real SurfaceAccessorJsonFull::real(U32 a_index,U32 a_subIndex)
{
	if(!isBound())
	{
		return 0;
	}

	I32 primitiveIndex=0;
	I32 vertexIndex=0;
	computePrimitiveVertex(a_index,a_subIndex,primitiveIndex,vertexIndex);

	if(primitiveIndex!=3)
	{
		return 0;
	}

	if(m_attrName=="RacingPsi")
	{
		return (*m_pElementValues[4])[vertexIndex].asFloat();
	}

	if(m_attrName=="RacingK")
	{
		return (*m_pElementValues[5])[vertexIndex].asFloat();
	}

	return 0;
}

SpatialVector SurfaceAccessorJsonFull::spatialVector(U32 a_index,
	U32 a_subIndex)
{
#if FE_SAJF_DEBUG
	feLog("SurfaceAccessorJsonFull::spatialVector(%d,%d) %s \"%s\" \"%s\"\n",
			a_index,a_subIndex,
			SurfaceAccessibleBase::elementLayout(m_element).c_str(),
			SurfaceAccessibleBase::attributeString(m_attribute).c_str(),
			m_attrName.c_str());
#endif

	if(!isBound())
	{
		return SpatialVector(0.0,0.0,0.0);
	}

	I32 primitiveIndex=0;
	I32 vertexIndex=0;
	computePrimitiveVertex(a_index,a_subIndex,primitiveIndex,vertexIndex);

	if(primitiveIndex<4)
	{
		if(m_attrName=="Cd")
		{
			const SpatialVector colors[4]=
					{
					{1.0,0.8,0.5},
					{0.5,0.7,1.0},
					{1.0,0.5,0.5},
					{0.8,1.0,0.8}
					};
			return colors[primitiveIndex];
		}

		const Json::Value& rElementValue=
				(*m_pElementValues[primitiveIndex])[vertexIndex];

		if(m_attrName=="P")
		{
			SpatialVector point=SpatialVector(
					rElementValue[0].asFloat(),
					rElementValue[1].asFloat(),
					rElementValue[2].asFloat())+
					m_originOffset;

#if !FE_JSON_FULL_USE_ALTITUDE
					point[2]=0.0;
#endif

			return point;
		}

		return SpatialVector(
				rElementValue[0].asFloat(),
				rElementValue[1].asFloat(),
				rElementValue[2].asFloat());
	}

	return SpatialVector(0,0,0);
}

BWORD SurfaceAccessorJsonFull::bindInternal(
	SurfaceAccessibleI::Element a_element,const String& a_name)
{
#if FE_SAJF_DEBUG
	feLog("SurfaceAccessorJsonFull::bindInternal %s \"%s\" \"%s\"\n",
			SurfaceAccessibleBase::elementLayout(a_element).c_str(),
			SurfaceAccessibleBase::attributeString(m_attribute).c_str(),
			a_name.c_str());
#endif

	m_element=SurfaceAccessibleI::e_point;
	if(a_element<0 && a_element>5)
	{
		a_element=SurfaceAccessibleI::e_point;
	}

	m_element=a_element;
	m_attrName=a_name;

	if(m_attribute!=SurfaceAccessibleI::e_vertices &&
			m_attribute!=SurfaceAccessibleI::e_properties &&
			m_attrName!="P" &&
			m_attrName!="RacingPsi" &&
			m_attrName!="RacingK" &&
			m_attrName!="Cd" &&
			m_attrName!="part")
	{
		return FALSE;
	}

	for(I32 m=0;m<6;m++)
	{
		m_pElementValues[m]=
				&m_spJsonRoot->value()[m_curveNames[m].c_str()];

		if((!m_pElementValues[m] || m_pElementValues[m]->isNull()))
		{
#if FE_SAJF_DEBUG
			feLog("SurfaceAccessorJsonFull::bindInternal"
					" field %d not found\n",m);
#endif
			for(I32 m=0;m<6;m++)
			{
				m_pElementValues[m]=NULL;
			}
			return FALSE;
		}
	}

	m_vertCounts[0]=m_pElementValues[0]->size();
	m_vertCounts[1]=m_pElementValues[1]->size();
	m_vertCounts[2]=m_pElementValues[2]->size();
	m_vertCounts[3]=m_pElementValues[3]->size();

	m_vertStarts[0]=0;
	m_vertStarts[1]=m_vertStarts[0]+m_vertCounts[0];
	m_vertStarts[2]=m_vertStarts[1]+m_vertCounts[1];
	m_vertStarts[3]=m_vertStarts[2]+m_vertCounts[2];

//	feLog("size %d %d %d %d %d %d\n",
//			m_pElementValues[0]->size(),
//			m_pElementValues[1]->size(),
//			m_pElementValues[2]->size(),
//			m_pElementValues[3]->size(),
//			m_pElementValues[4]->size(),
//			m_pElementValues[5]->size());

	return TRUE;
}

} /* namespace hive */
