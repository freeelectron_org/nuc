/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <surface_hive/surface_hive.pmh>

#define FE_SPT_DEBUG		FALSE
#define FE_SPT_DISPLAY_INFO	FALSE
#define FE_SPT_AUDIO		TRUE
#define FE_SPT_CAM_HIGH		TRUE
#define FE_CAR_COUNT_LIMIT	32
#define FE_FOLLOW_CAR		0	// car index to watch

using namespace fe;
using namespace fe::ext;

namespace hive
{

SurfacePuppet::SurfacePuppet(void):
	m_throttle(0.0),
	m_brake(0.0),
	m_steeringAngle(0.0),
	m_horn(FALSE),
	m_adsTarget0(0,0,0),
	m_adsTarget1(0,0,0),
	m_velocity(0,0,0),
	m_historyIndex(0),
	m_nanoDelay(0),
	m_carSlipAngle(0)
{
#if	FE_SPT_DEBUG
	feLog("SurfacePuppet::SurfacePuppet\n");
#endif

	m_xformCar.resize(1);

	setRadius(3.0);
	setIdentity(m_xformCar[0]);
	setIdentity(m_xformCar0Prev);
	setIdentity(m_xformCam);

	const I32 historySize=32;
	m_history.resize(historySize);
	m_historyColor.resize(historySize);
	for(I32 m=0;m<historySize;m++)
	{
		set(m_history[m]);
		set(m_historyColor[m]);
	}

	m_spDrawPhantom=new DrawMode();
	m_spDrawPhantom->setDrawStyle(DrawMode::e_wireframe);
	m_spDrawPhantom->setZBuffering(FALSE);
	m_spDrawPhantom->setLineWidth(4.0);

	m_spDebugGroup=new DrawMode();
	m_spDebugGroup->setGroup("debug");
	m_spDebugGroup->setZBuffering(FALSE);

	SpatialTransform identity;
	setIdentity(identity);

	m_spCameraOverlay = new CameraEditable();
	m_spCameraOverlay->setName("ObjectViewer.CameraEditable overlay");
	m_spCameraOverlay->setCameraMatrix(identity);
	m_spCameraOverlay->setRasterSpace(TRUE);
}

SurfacePuppet::~SurfacePuppet(void)
{
#if	FE_SPT_DEBUG
	feLog("SurfacePuppet::~SurfacePuppet\n");
#endif
}

Protectable* SurfacePuppet::clone(Protectable* pInstance)
{
#if	FE_SPT_DEBUG
	feLog("SurfacePuppet::clone\n");
#endif
	return NULL;
}

void SurfacePuppet::cache(void)
{
#if	FE_SPT_DEBUG
	feLog("SurfacePuppet::cache\n");
#endif

	m_spHiveHost=HiveHost::create();
	FEASSERT(m_spHiveHost.isValid());

	sp<Registry> spRegistry=registry();

	m_spScope=spRegistry->create("Scope");
	m_asVehicle.bind(m_spScope);

#if FE_SPT_AUDIO
	m_spListenerI=spRegistry->create("ListenerI");
	m_spVoiceEngine=spRegistry->create("VoiceI");
	m_spVoiceTire=spRegistry->create("VoiceI");
	m_spVoiceHorn=spRegistry->create("VoiceI");
	m_spVoicePop=spRegistry->create("VoiceI");
	m_spAudioI=spRegistry->create("AudioI");
#endif

	if(m_spListenerI.isNull() || m_spAudioI.isNull() ||
			m_spVoiceEngine.isNull() || m_spVoiceTire.isNull() ||
			m_spVoiceHorn.isNull() || m_spVoicePop.isNull())
	{
		m_spListenerI=NULL;
		m_spVoiceEngine=NULL;
		m_spVoiceTire=NULL;
		m_spVoiceHorn=NULL;
		m_spVoicePop=NULL;
		m_spAudioI=NULL;
	}

	if(m_spAudioI.isValid())
	{
		String wavPath=
			m_spHiveHost->master()->catalog()->catalog<String>("path:media")+
			"/wav";

		String path;

//		path=wavPath+"/motor_a8.wav";
		path=wavPath+"/flatline2.wav";
		Result result=m_spAudioI->load("motor",path);
		if(fe::failure(result))
		{
			feLog("SurfacePuppet::cache failed to load motor sound\n");
		}

		path=wavPath+"/Bigskid_clip.wav";
		result=m_spAudioI->load("skid",path);
		if(fe::failure(result))
		{
			feLog("SurfacePuppet::cache failed to load motor sound\n");
		}

//		path=wavPath+"/truckhorn.wav";
		path=wavPath+"/vwhorn.wav";
		result=m_spAudioI->load("horn",path);
		if(fe::failure(result))
		{
			feLog("SurfacePuppet::cache failed to load horn sound\n");
		}

		path=wavPath+"/pop.wav";
		result=m_spAudioI->load("pop",path);
		if(fe::failure(result))
		{
			feLog("SurfacePuppet::cache failed to load pop sound\n");
		}
	}

	if(m_spVoiceEngine.isValid())
	{
		m_spVoiceEngine->queue("motor");
		m_spVoiceEngine->setVolume(0.0f);
		m_spVoiceEngine->setReferenceDistance(30.0f);
		m_spVoiceEngine->loop(TRUE);
		m_spVoiceEngine->play();
	}

	if(m_spVoiceTire.isValid())
	{
		m_spVoiceTire->queue("skid");
		m_spVoiceTire->setVolume(0.0f);
		m_spVoiceTire->setReferenceDistance(30.0f);
		m_spVoiceTire->loop(TRUE);
		m_spVoiceTire->play();
	}

	if(m_spVoiceHorn.isValid())
	{
		m_spVoiceHorn->setReferenceDistance(30.0f);
	}

	if(m_spVoicePop.isValid())
	{
		m_spVoicePop->setReferenceDistance(30.0f);
		m_spVoicePop->setVolume(0.5f);
	}

	m_spStateCatalog=m_spHiveHost->accessSpace("world");
	FEASSERT(m_spStateCatalog.isValid());

	Result result=m_spStateCatalog->start();
	if(fe::failure(result))
	{
		feLog("SurfacePuppet::cache failed to start networking\n");
	}

	m_spStateCatalog->setConnectionTimeout(10);

	m_spStateCatalog->waitForConnection();

	updateState();
	updateState();

	feLog("SurfacePuppet::cache catalogDump\n");
	m_spStateCatalog->catalogDump();
}

void SurfacePuppet::updateState(void)
{
	const Color red(0.5,0.0,0.0);
	const Color green(0.0,0.5,0.0);

	Color pointColor=green;

	static I32 flushCount(-1);
	static I32 lastFlushCount(-1);

	I32 spinCount(0);

	m_xformCar0Prev=m_xformCar[FE_FOLLOW_CAR];

	SystemTicker ticker;
	const U32 t0=ticker.timeMS();

	{
		StateCatalog::Atomic atomic(m_spStateCatalog,flushCount);
		spinCount=atomic.spinCount();

		I32 newFrame(0);
		atomic.getState<I32>("frame",newFrame);

		if(newFrame==m_frame)
		{
			feLog("frame %d repeated\n",m_frame);
		}
		m_frame=newFrame;

		atomic.getState<Real>("timeElapsed",m_timeElapsed);

		const String prefix=String("vehicles.")+FE_STRING(FE_FOLLOW_CAR)+".";

//~		atomic.getState<SpatialTransform>(
//~				prefix+m_asVehicle.chassisTransform.name(),
//~				m_xformCar[0]);
		atomic.getState<SpatialTransform>(
				prefix+m_asVehicle.wheelFLTransform.name(),
				m_xformWheel0[0]);
		atomic.getState<SpatialTransform>(
				prefix+m_asVehicle.wheelFRTransform.name(),
				m_xformWheel0[1]);
		atomic.getState<SpatialTransform>(
				prefix+m_asVehicle.wheelRLTransform.name(),
				m_xformWheel0[2]);
		atomic.getState<SpatialTransform>(
				prefix+m_asVehicle.wheelRRTransform.name(),
				m_xformWheel0[3]);
		atomic.getState<Real>(
				prefix+m_asVehicle.throttleInput.name(),
				m_throttle);
		atomic.getState<Real>(
				prefix+m_asVehicle.brakeInput.name(),
				m_brake);
		atomic.getState<I32>(
				prefix+m_asVehicle.gearModeInput.name(),
				m_gearMode);
		atomic.getState<BWORD>(
				prefix+m_asVehicle.hornInput.name(),
				m_horn);
		atomic.getState<SpatialVector>(
				prefix+m_asVehicle.adsTarget0.name(),
				m_adsTarget0);
		atomic.getState<SpatialVector>(
				prefix+m_asVehicle.adsTarget1.name(),
				m_adsTarget1);
		atomic.getState<SpatialVector>(
				prefix+"velocity",
				m_velocity);

		I32 carSurfaceCount=m_carSurface.size();
		I32 carCount=m_xformCar.size();
		I32 carIndex=0;
		while(carIndex<FE_CAR_COUNT_LIMIT)
		{
			String key;
			key.sPrintf("vehicles.%d.%s",
					carIndex,m_asVehicle.chassisTransform.name().c_str());

			SpatialTransform xform;
			if(failure(atomic.getState<SpatialTransform>(key,xform)))
			{
				break;
			}

			if(carIndex>=carCount)
			{
				carCount=carIndex+1;
				m_xformCar.resize(carCount);
			}
			m_xformCar[carIndex]=xform;

			I32 carSurfaceIndexCount=m_carSurfaceIndex.size();
			if(carSurfaceIndexCount<carCount)
			{
				m_carSurfaceIndex.resize(carCount);
				for(I32 index=carSurfaceIndexCount;index<carCount;index++)
				{
					m_carSurfaceIndex[index]= -1;
				}
			}

			I32& rSurfaceIndex=m_carSurfaceIndex[carIndex];
			if(rSurfaceIndex<0)
			{
				String key;
				key.sPrintf("vehicles.%d.surface.filename",carIndex);

				String filename;
				if(failure(atomic.getState<String>(key,filename)))
				{
					carIndex++;
					continue;
				}

				const String format=
						String(strrchr(filename.c_str(),'.')).prechop(".");

				String paths;
				atomic.getState<String>("paths."+format,paths);

				String path=System::findFile(filename,paths);
				if(path.empty())
				{
					feLog("SurfacePuppet::updateState"
							" failed to find \"%s\" using paths \"%s\"\n",
							filename.c_str(),paths.c_str());
					continue;
				}

				const String fullname=path+"/"+filename;

				feLog("state \"%s\" is \"%s\"\n",key.c_str(),fullname.c_str());

				key.sPrintf("vehicles.%d.surface.options",carIndex);

				String options;
				atomic.getState<String>(key,options);

				feLog("state \"%s\" is \"%s\"\n",key.c_str(),options.c_str());

				for(I32 index=0;index<carSurfaceCount;index++)
				{
					CarSurface& rCarSurface=m_carSurface[index];
					String& rSurfaceFilename=rCarSurface.m_filename;
					String& rSurfaceOptions=rCarSurface.m_options;

					if(fullname==rSurfaceFilename &&
							options==rSurfaceOptions)
					{
						rSurfaceIndex=index;
					}
				}
				if(rSurfaceIndex<0)
				{
					rSurfaceIndex=carSurfaceCount++;
					m_carSurface.resize(carSurfaceCount);
				}

				CarSurface& rCarSurface=m_carSurface[rSurfaceIndex];
				sp<SurfaceAccessibleI>& rspSurfaceAccessibleI=
						rCarSurface.m_spSurfaceAccessibleI;
				sp<SurfaceI>& rspSurfaceI=
						rCarSurface.m_spSurfaceI;
				String& rSurfaceFilename=rCarSurface.m_filename;
				String& rSurfaceOptions=rCarSurface.m_options;

				if(rspSurfaceI.isNull())
				{
					rSurfaceFilename=fullname;
					rSurfaceOptions=options;

					rspSurfaceAccessibleI=
							registry()->create("SurfaceAccessibleI.*.*.fbx");
					if(rspSurfaceAccessibleI.isValid())
					{
//~						sp<Scope> spScope=registry()->create("Scope");
//~						spScope->setLocking(FALSE);
//~						rspSurfaceAccessibleI->bind(spScope);

						sp<Catalog> spSettings=
								m_spHiveHost->master()->createCatalog(
								"settings");
						spSettings->catalog<String>("options")=rSurfaceOptions;
						if(!rspSurfaceAccessibleI->load(rSurfaceFilename,
								spSettings))
						{
							feLog("failed to load model \"%s\"\n",
									rSurfaceFilename.c_str());
							rspSurfaceAccessibleI=NULL;
						}
					}
					if(rspSurfaceAccessibleI.isValid())
					{
						rspSurfaceI=rspSurfaceAccessibleI->surface();
					}
				}
			}

			carIndex++;
		}

		Array<String> obstacleKeys;
		m_spStateCatalog->getStateKeys(obstacleKeys,
				"obstacles\\.[^.]*\\.transform$");

		const I32 obstacleCount=obstacleKeys.size();
		m_xformObstacle.resize(obstacleCount);

		for(I32 obstacleIndex=0;obstacleIndex<obstacleCount;obstacleIndex++)
		{
			atomic.getState<SpatialTransform>(obstacleKeys[obstacleIndex],
					m_xformObstacle[obstacleIndex]);
		}

		const I32 proxyCount=carCount+obstacleCount;
		m_proxySurface.resize(proxyCount);
		m_proxyName.resize(proxyCount);

		I32 proxyIndex(0);
		for(carIndex=0;carIndex<carCount;carIndex++)
		{
			m_proxyName[proxyIndex].sPrintf("v.%d",carIndex);

			sp<ProxySurface>& rspProxySurface=m_proxySurface[proxyIndex];
			if(rspProxySurface.isNull())
			{
				rspProxySurface=sp<ProxySurface>(new ProxySurface());
			}
			if(rspProxySurface.isValid())
			{
				rspProxySurface->setName(m_proxyName[proxyIndex]);
				rspProxySurface->setCenter(
						m_xformCar[carIndex].translation());
				rspProxySurface->setRadius(1.0);

				const I32 carSurfaceIndex=
						(carIndex<m_carSurfaceIndex.size())?
						m_carSurfaceIndex[carIndex]: -1;
				if(carSurfaceIndex>=0 &&
						carSurfaceIndex<m_carSurface.size())
				{
					sp<SurfaceI>& rspSurfaceI=
							m_carSurface[carSurfaceIndex].m_spSurfaceI;
					if(rspSurfaceI.isValid())
					{
						rspProxySurface->setRadius(rspSurfaceI->radius());
					}
				}
			}

			proxyIndex++;
		}
		for(I32 obstacleIndex=0;obstacleIndex<obstacleCount;obstacleIndex++)
		{
			m_proxyName[proxyIndex].sPrintf("o.%d",obstacleIndex);

			sp<ProxySurface>& rspProxySurface=m_proxySurface[proxyIndex];
			if(rspProxySurface.isNull())
			{
				rspProxySurface=sp<ProxySurface>(new ProxySurface());
			}
			if(rspProxySurface.isValid())
			{
				rspProxySurface->setName(m_proxyName[proxyIndex]);
				rspProxySurface->setCenter(
						m_xformObstacle[obstacleIndex].translation());
				rspProxySurface->setRadius(2.0);	//* NOTE a cylinder
			}

			proxyIndex++;
		}

		atomic.getState<Real>(
				prefix+m_asVehicle.longitudinalAcceleration.name(),
				m_longitudinalAcceleration);
		atomic.getState<Real>(
				prefix+m_asVehicle.lateralAcceleration.name(),
				m_lateralAcceleration);

		// Debug values
		atomic.getState<SpatialVector>(
				prefix+m_asVehicle.centerOfMassVelocity.name(),
				m_centerOfMassVelocity);
		atomic.getState<SpatialEuler>(
				prefix+m_asVehicle.centerOfMassAngularVelocity.name(),
				m_centerOfMassAngularVelocity);
		atomic.getState<SpatialTransform>(
				prefix+m_asVehicle.axlesRearTransform.name(),
				m_axlesRearTransform);
		atomic.getState<double>(
				prefix+m_asVehicle.latitude.name(),
				m_latitude);
		atomic.getState<double>(
				prefix+m_asVehicle.longitude.name(),
				m_longitude);
		atomic.getState<double>(
				prefix+m_asVehicle.altitude.name(),
				m_altitude);
		atomic.getState<double>(
				prefix+m_asVehicle.yaw.name(),
				m_yaw);
		atomic.getState<Real>(
				prefix+m_asVehicle.yawVelocity.name(),
				m_yawVelocity);
	}

	const U32 deltaMS=ticker.timeMS()-t0;
	const Real microseconds=1e3*deltaMS;

#if FALSE
	feLog("chassis\n%s\n",c_print(m_xformCar[0]));
	feLog("Wheel FL\n%s\n",c_print(m_xformWheel0[0]));
	feLog("Wheel FR\n%s\n",c_print(m_xformWheel0[1]));
	feLog("Wheel RL\n%s\n",c_print(m_xformWheel0[2]));
	feLog("Wheel RR\n%s\n",c_print(m_xformWheel0[3]));
#endif

//	feLog("spinCount %d\n",spinCount);

	if(flushCount!=lastFlushCount+1)
	{
		//* NOTE dropped data (way too fast)
		pointColor=red;
//		feLog("SurfacePuppet::updateState flushCount %d %d delta %d\n",
//				lastFlushCount,flushCount,flushCount-lastFlushCount);

		if(m_spVoicePop.isValid())
		{
			m_spVoicePop->queue("pop");
			m_spVoicePop->play();
		}

		m_nanoDelay+=200e3;
		m_spStateCatalog->setState<Real>("nanoDelay",m_nanoDelay);
		m_spStateCatalog->flush();
	}
	else if(spinCount<1)
	{
		//* NOTE didn't have to wait for new data (getting too fast)
//		feLog("SurfacePuppet::updateState spinCount %8d us %.1f\n",
//				spinCount,microseconds);

		m_nanoDelay+=10e3;
		m_spStateCatalog->setState<Real>("nanoDelay",m_nanoDelay);
		m_spStateCatalog->flush();
	}
	else
	{
		//* NOTE waited a while for new data (ask to speed up)
		if(microseconds>1000.0)
		{
//			feLog("SurfacePuppet::updateState spinCount %8d us %.1f\n",
//					spinCount,microseconds);

			m_nanoDelay-=2.0*microseconds;
			if(m_nanoDelay<1)
			{
				m_nanoDelay=1;
			}
			m_spStateCatalog->setState<Real>("nanoDelay",m_nanoDelay);
			m_spStateCatalog->flush();
		}
	}

	lastFlushCount=flushCount;

	m_history[m_historyIndex]=m_xformCar[FE_FOLLOW_CAR].translation();
	m_historyColor[m_historyIndex]=pointColor;
	if(++m_historyIndex>=(I32)m_history.size())
	{
		m_historyIndex=0;
	}

//	setCenter(m_xformCar[FE_FOLLOW_CAR].translation());

#if FE_SPT_CAM_HIGH
	SpatialVector offsetCam(-4.0,0.0,2.0);
#else
	SpatialVector offsetCam(0.0,0.0,1.0);
#endif

	//* reverse transform from -z_forward y_up default camera
	setIdentity(m_xformCam);
	rotate(m_xformCam,0.5*fe::pi,fe::e_yAxis);
	rotate(m_xformCam,-0.5*fe::pi,fe::e_xAxis);
	translate(m_xformCam,-offsetCam);

	SpatialTransform invCar;
	invert(invCar,m_xformCar[FE_FOLLOW_CAR]);

	m_xformCam=invCar*m_xformCam;

//	m_steeringAngle=
//			asin(cross(m_xformCar[0].direction(),m_xformWheel0[0].direction())[2]);
	m_steeringAngle=asin(cross(m_xformCar[FE_FOLLOW_CAR].left(),
			m_xformWheel0[0].left())[2]);

	const Real speed=magnitude(m_velocity);
	if(speed<1.0)
	{
		m_carSlipAngle=0.0;
		m_tireSlipAngle=m_steeringAngle;
	}
	else
	{
		const Real slipKeep=0.9;
		const SpatialVector travel=
				unitSafe(m_xformCar[FE_FOLLOW_CAR].translation()-
				m_xformCar0Prev.translation());
		const SpatialVector slipCross=
				cross(travel,m_xformCar[FE_FOLLOW_CAR].direction());
		const Real slipDot=
				0.999999*dot(travel,m_xformCar[FE_FOLLOW_CAR].direction());
		const Real slipAngle=
				fe::maximum(Real(0),Real(acos(fabs(slipDot))-0.004));
		m_carSlipAngle=slipKeep*m_carSlipAngle+(1.0-slipKeep)*
				copysign(slipAngle,slipCross[2]);
		m_tireSlipAngle=m_carSlipAngle+m_steeringAngle;
	}
}

void SurfacePuppet::draw(const SpatialTransform& a_rTransform,
	sp<DrawI> a_spDrawI,const fe::Color* a_color,
	sp<DrawBufferI> a_spDrawBuffer,sp<PartitionI> a_spPartition) const
{
	const_cast<SurfacePuppet*>(this)->updateState();

#if	FE_SPT_DEBUG
	feLog("SurfacePuppet::draw throttle %.3f brake %.3f velocity %s\n%s\n",
			m_throttle,m_brake,c_print(m_velocity),c_print(m_xformCar[0]));
#endif

	const Color white(1.0,1.0,1.0);
	const Color cyan(0.0,1.0,1.0);
	const Color green(0.0,1.0,0.0);
	const Color yellow(1.0,1.0,0.0);

	sp<ViewI> spViewI=a_spDrawI->view();
	sp<CameraI> spCameraI=spViewI->camera();
	const SpatialTransform& rCameraMatrix=spCameraI->cameraMatrix();
	SpatialTransform cameraTransform;
	invert(cameraTransform,rCameraMatrix);
	const SpatialVector cameraDir= -cameraTransform.column(2);
	const SpatialVector cameraUp=cameraTransform.column(1);
	const SpatialVector cameraLocation=cameraTransform.column(3);

	if(m_spListenerI.isValid())
	{
		m_spListenerI->setLocation(cameraLocation);
		m_spListenerI->setOrientation(cameraDir,cameraUp);
	}

	if(m_spVoiceEngine.isValid())
	{
		m_spVoiceEngine->setLocation(m_xformCar[FE_FOLLOW_CAR].translation());
		m_spVoiceEngine->setVelocity(m_velocity);
		m_spVoiceEngine->setPitch(0.7+0.02*magnitude(m_velocity));
		m_spVoiceEngine->setVolume(0.04+0.96*pow(fabs(m_throttle),3.0));
	}

	if(m_spVoiceTire.isValid())
	{
		const Real speedScale=
				fe::minimum(Real(1),Real(0.04*magnitude(m_velocity)));

		m_spVoiceTire->setLocation(m_xformCar[FE_FOLLOW_CAR].translation());
		m_spVoiceTire->setVelocity(m_velocity);
		m_spVoiceTire->setPitch(1.0);
		m_spVoiceTire->setVolume(0.3*fabs(m_carSlipAngle)*speedScale);
	}

	if(m_spVoiceHorn.isValid())
	{
		static BWORD lastHorn(FALSE);

		m_spVoiceHorn->setLocation(m_xformCar[FE_FOLLOW_CAR].translation());
		m_spVoiceHorn->setVelocity(m_velocity);

		if(!lastHorn && m_horn)
		{
			m_spVoiceHorn->queue("horn");
			m_spVoiceHorn->play();
		}

		lastHorn=m_horn;
	}

//	const Real radius(10);
//	a_spDrawI->drawTransformedMarker(m_xformCar[0],radius,
//			a_color? *a_color: white);

//	const Real radius(2);
//	for(I32 m=0;m<4;m++)
//	{
//		a_spDrawI->drawTransformedMarker(m_xformWheel0[m],radius,
//				a_color? *a_color: white);
//	}

	const Real radiusSphere=0.3f;
	const SpatialVector scaleSphere(radiusSphere,radiusSphere,radiusSphere);
	SpatialTransform transform;
	setIdentity(transform);
	transform.translation()=m_adsTarget0+SpatialVector(0,0,4);
	a_spDrawI->drawSphere(transform,&scaleSphere,green);
	transform.translation()=m_adsTarget1+SpatialVector(0,0,4);
	a_spDrawI->drawSphere(transform,&scaleSphere,yellow);

	a_spDrawI->drawPoints(&m_history[FE_FOLLOW_CAR],NULL,m_history.size(),
			TRUE,&m_historyColor[FE_FOLLOW_CAR]);

	const Color orange(1.0,0.5,0.2);
	const Color blue(0.0,0.5,1.0);
	const Color lightcyan(0.5,1.0,1.0);
	const Color violet(0.5,0.0,1.0);

	const I32 carCount=m_xformCar.size();
	for(I32 carIndex=0;carIndex<carCount;carIndex++)
	{
		const I32 carSurfaceIndex=(carIndex<m_carSurfaceIndex.size())?
				m_carSurfaceIndex[carIndex]: -1;
		if(carSurfaceIndex>=0 &&
				carSurfaceIndex<m_carSurface.size() &&
				m_carSurface[carSurfaceIndex].m_spSurfaceI.isValid())
		{
			const CarSurface& rCarSurface=m_carSurface[carSurfaceIndex];

			Color carColor;
			switch(carIndex%4)
			{
				case 0:
					carColor=orange;
					break;
				case 1:
					carColor=blue;
					break;
				case 2:
					carColor=lightcyan;
					break;
				default:
					carColor=violet;
					break;
			}

			a_spDrawI->drawTransformed(m_xformCar[carIndex],
					rCarSurface.m_spSurfaceI,
					&carColor,rCarSurface.m_spDrawBuffer);
		}
		else
		{
			SpatialVector tri[30];
			//* back
			set(tri[0],	-2.0,	1.0,	0.0);
			set(tri[1],	-2.0,	-1.0,	0.0);
			set(tri[2],	-2.0,	1.0,	1.0);
			set(tri[3],	-2.0,	-1.0,	1.0);
			tri[4]=tri[2];
			tri[5]=tri[1];

			//* front
			set(tri[6],	2.0,	-1.0,	0.0);
			set(tri[7],	2.0,	1.0,	0.0);
			set(tri[8],	1.6,	0.0,	0.7);

			//* bottom
			tri[9]=tri[0];
			tri[10]=tri[7];
			tri[11]=tri[1];
			tri[12]=tri[1];
			tri[13]=tri[7];
			tri[14]=tri[6];

			//* right
			tri[15]=tri[1];
			tri[16]=tri[6];
			tri[17]=tri[3];
			tri[18]=tri[3];
			tri[19]=tri[6];
			tri[20]=tri[8];

			//* left
			tri[21]=tri[2];
			tri[22]=tri[7];
			tri[23]=tri[0];
			tri[24]=tri[2];
			tri[25]=tri[8];
			tri[26]=tri[7];

			//* top
			tri[27]=tri[2];
			tri[28]=tri[3];
			tri[29]=tri[8];

			SpatialVector norm[30];
			for(I32 m=0;m<30;m++)
			{
				norm[m]=unitSafe(tri[m]);
			}

			Array<I32> hullPointMap;
			hullPointMap.resize(7);
			for(I32 m=0;m<4;m++)
			{
				hullPointMap[m]=m;
			}
			for(I32 m=4;m<7;m++)
			{
				hullPointMap[m]=m+2;
			}

			Array<Vector4i> hullFacePoint;
			hullFacePoint.resize(10);

			set(hullFacePoint[0],0,1,2,-1);
			set(hullFacePoint[1],3,2,1,-1);
			set(hullFacePoint[2],4,5,6,-1);
			set(hullFacePoint[3],0,5,1,-1);
			set(hullFacePoint[4],1,5,4,-1);
			set(hullFacePoint[5],1,4,3,-1);
			set(hullFacePoint[6],3,4,6,-1);
			set(hullFacePoint[7],2,5,0,-1);
			set(hullFacePoint[8],2,6,5,-1);
			set(hullFacePoint[9],2,3,6,-1);

			a_spDrawI->drawTransformedTriangles(m_xformCar[carIndex],NULL,
					tri,norm,NULL,30,
					DrawI::e_strip,FALSE,a_color? a_color: &white,
					NULL,&hullPointMap,&hullFacePoint,
//					a_spDrawBuffer);
					sp<DrawBufferI>(NULL));
		}
	}

	const I32 obstacleCount=m_xformObstacle.size();
	for(I32 obstacleIndex=0;obstacleIndex<obstacleCount;obstacleIndex++)
	{
		const U32 slices(16);
		const SpatialVector startpoint=
				m_xformObstacle[obstacleIndex].translation()+
				m_xformObstacle[obstacleIndex].direction();
		const SpatialVector endpoint=
				m_xformObstacle[obstacleIndex].translation()-
				m_xformObstacle[obstacleIndex].direction();
		a_spDrawI->drawCylinder(startpoint,endpoint,
				0.0,1.0,violet,slices);
	}

	const Real tireRadius(0.33);

	SpatialVector centerline[2];
	set(centerline[0],0,0,0);
	set(centerline[1],0,0.1,0);

	SpatialVector spoke[2];
	set(spoke[0],0,0,0);
	set(spoke[1],0,0,tireRadius);

	//* Draw the wheels as circles
	a_spDrawI->pushDrawMode(m_spDrawPhantom);
	const SpatialVector scaleTire(tireRadius,tireRadius,tireRadius);
	for(I32 m=0;m<4;m++)
	{
		SpatialTransform xform=m_xformWheel0[m];

		//* Draw line through the center of the wheels
		a_spDrawI->drawTransformedLines(xform,NULL,centerline,NULL,2,
				DrawI::e_discrete,FALSE,&cyan);

		//* Draw one wheel spoke
		a_spDrawI->drawTransformedLines(xform,NULL,spoke,NULL,2,
				DrawI::e_discrete,FALSE,&cyan);

		rotate(xform,90.0*fe::degToRad,e_xAxis);

		translate(xform,SpatialVector(0.0,0.0,0.1));
		a_spDrawI->drawCircle(xform,&scaleTire,cyan);

		translate(xform,SpatialVector(0.0,0.0,-0.2));
		a_spDrawI->drawCircle(xform,&scaleTire,cyan);
	}
	a_spDrawI->popDrawMode();

	drawDebugInfo(a_spDrawI);
}

void SurfacePuppet::drawDebugInfo(sp<DrawI> a_spDrawI) const
{
	const Color cyan(0.0,1.0,1.0);

	sp<ViewI> spViewI=a_spDrawI->view();
	sp<CameraEditable> spCameraEditable=spViewI->camera();

	a_spDrawI->pushDrawMode(m_spDebugGroup);

	sp<WindowI> spWindowI=spViewI->window();
	Box2i viewport=spWindowI->geometry();
	viewport[0]=0;
	viewport[1]=0;
	const I32 winx=width(viewport);
	const I32 winy=height(viewport);

	spViewI->setCamera(m_spCameraOverlay);
	spViewI->setViewport(viewport);
	spViewI->use(ViewI::e_ortho);

	sp<EventContextI> spEventContextI=spWindowI->getEventContextI();
	const Real multiplication=
			fe::maximum(Real(1),spEventContextI->multiplication());

	const Color purple(1.0,0.0,1.0);
	const Color brightRed(1.0,0.4,0.4);
	const Color dimRed(1.0,0.4,0.4,0.4);
	const Color black(0.0,0.0,0.0);
	const Color backColor(0.2,0.2,0.25);//* setable in SurfaceViewerOp

	Box2i box;

	const Real gKeep=0.7;	//* tweak

	I32 fontAscent=0;
	I32 fontDescent=0;
	a_spDrawI->font()->fontHeight(&fontAscent,&fontDescent);
	const I32 textHeight=fe::maximum(10,fontAscent+fontDescent);

	String text;
	text.sPrintf("Frame %d Time %6.2f\n",m_frame,m_timeElapsed);
	I32 textWidth=a_spDrawI->font()->pixelWidth(text);
	SpatialVector screen(0.5*(winx-textWidth),10);
	a_spDrawI->drawAlignedText(screen,text,purple);

	static Real gLateral=0.0;
	gLateral=gKeep*gLateral+(1.0-gKeep)*(-m_lateralAcceleration/9.81);
	set(box,0.75*winx,0.25*winy,0.2*winx,0.01*winy);
	OperateCommon::drawIndicator(a_spDrawI,box,FALSE,TRUE,gLateral,0.0,1.0,
			-1,5,5,multiplication,&brightRed,&dimRed,&backColor,&black);

	set(screen,box[0], box[1]+height(box)+10);
	a_spDrawI->drawAlignedText(screen,"Lateral G",brightRed);

	static Real gLongitudinal=0.0;
	gLongitudinal=gKeep*gLongitudinal+
			(1.0-gKeep)*(m_longitudinalAcceleration/9.81);
	set(box,0.75*winx,0.15*winy,0.2*winx,0.01*winy);
	OperateCommon::drawIndicator(a_spDrawI,box,FALSE,TRUE,gLongitudinal,0.0,1.0,
			-1,5,5,multiplication,&brightRed,&dimRed,&backColor,&black);

	set(screen,box[0], box[1]+height(box)+10);
	a_spDrawI->drawAlignedText(screen,"Longitudinal G",brightRed);

	box[1]=0.95*winy;
	OperateCommon::drawIndicator(a_spDrawI,box,FALSE,TRUE,
			m_carSlipAngle,0.0,0.1,
			-2,5,5,multiplication,&brightRed,&dimRed,&backColor,&black);

	set(screen,box[0], box[1]+height(box)+10);
	a_spDrawI->drawAlignedText(screen,"Car Slip Angle",brightRed);

	box[1]=0.8*winy;
	OperateCommon::drawIndicator(a_spDrawI,box,FALSE,TRUE,
			m_tireSlipAngle,0.0,0.1,
			-2,5,5,multiplication,&brightRed,&dimRed,&backColor,&black);

	set(screen,box[0], box[1]+height(box)+10);
	a_spDrawI->drawAlignedText(screen,"Tire Slip Angle",brightRed);

	set(box,0.1*winx,0.65*winy,0.01*winx,0.2*winy);
	OperateCommon::drawIndicator(a_spDrawI,box,TRUE,TRUE,
			m_throttle,0.0,0.1,
			-2,5,5,multiplication,&brightRed,&dimRed,&backColor,&black);

	textWidth=a_spDrawI->font()->pixelWidth("Throttle");
	set(screen,box[0]+width(box)-textWidth, box[1]+height(box)+10);
	a_spDrawI->drawAlignedText(screen,"Throttle",brightRed);

	SpatialVector line[2];
	const Real length=0.1*winy;
	set(screen,0.5*winx,0.65*winy);

	set(line[0],
			screen[0],
			screen[1]-0.5*length);
	set(line[1],
			screen[0],
			screen[1]+0.5*length);
	a_spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,FALSE,&black);

	Real drawAngle=m_carSlipAngle;
	Real sa=sin(drawAngle);
	Real ca=cos(drawAngle);
	line[0]=screen;
	set(line[1],
			screen[0]+0.5*length*sa,
			screen[1]+0.5*length*ca);
	a_spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,FALSE,&dimRed);

	drawAngle=-4.0*m_steeringAngle;
	sa=sin(drawAngle);
	ca=cos(drawAngle);
	set(line[0],
			screen[0]-0.5*length*sa,
			screen[1]-0.5*length*ca);
	set(line[1],
			screen[0]+0.5*length*sa,
			screen[1]+0.5*length*ca);
	a_spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,FALSE,&brightRed);

	set(line[0],
			screen[0]-0.1*length*ca,
			screen[1]+0.1*length*sa);
	set(line[1],
			screen[0]+0.1*length*ca,
			screen[1]-0.1*length*sa);
	a_spDrawI->drawLines(line,NULL,2,
			DrawI::e_discrete,FALSE,&black);

	const Real throttleLength=(m_throttle-m_brake)*0.5*length;
	screen+=SpatialVector(throttleLength*sa,throttleLength*ca);
	set(line[0],
			screen[0]-0.1*length*ca,
			screen[1]+0.1*length*sa);
	set(line[1],
			screen[0]+0.1*length*ca,
			screen[1]-0.1*length*sa);
	a_spDrawI->drawLines(line,NULL,2,
			DrawI::e_discrete,FALSE,&brightRed);

	set(screen,0.5*winx,0.8*winy);

	set(line[0],
			screen[0],
			screen[1]-0.5*length);
	set(line[1],
			line[0][0],
			screen[1]+0.5*length);
	a_spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,FALSE,&black);

	set(line[0],
			screen[0]+0.5*length*gLateral,
			screen[1]-0.5*length);
	set(line[1],
			line[0][0],
			screen[1]+0.5*length);
	a_spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,FALSE,&brightRed);

	set(line[0],
			screen[0]-0.5*length,
			screen[1]);
	set(line[1],
			screen[0]+0.5*length,
			line[0][1]);
	a_spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,FALSE,&black);

	set(line[0],
			screen[0]-0.5*length,
			screen[1]-0.5*length*gLongitudinal);
	set(line[1],
			screen[0]+0.5*length,
			line[0][1]);
	a_spDrawI->drawLines(line,NULL,2,DrawI::e_discrete,FALSE,&brightRed);

	set(screen,10.0, 0.65*winy);
	a_spDrawI->drawAlignedText(screen,"P",
			m_gearMode==AsVehicle::e_gearModePark? brightRed: dimRed);

	screen[1] -= textHeight;
	a_spDrawI->drawAlignedText(screen,"R",
			m_gearMode==AsVehicle::e_gearModeReverse? brightRed: dimRed);

	screen[1] -= textHeight;
	a_spDrawI->drawAlignedText(screen,"N",
			m_gearMode==AsVehicle::e_gearModeNeutral? brightRed: dimRed);

	screen[1] -= textHeight;
	a_spDrawI->drawAlignedText(screen,"D",
			m_gearMode==AsVehicle::e_gearModeDrive? brightRed: dimRed);

#if FE_SPT_DISPLAY_INFO
	text.sPrintf("LLA %.4f, %.4f, %.3f",
			m_latitude,
			m_longitude,
			m_altitude);
	set(screen,10.0, 0.8*winy);
	a_spDrawI->drawAlignedText(screen,text,cyan);

	text.sPrintf("Global %.2f, %.2f, %.2f",
			m_axlesRearTransform.column(3)[0],
			m_axlesRearTransform.column(3)[1],
			m_axlesRearTransform.column(3)[2]);
	screen[1] -= textHeight;
	a_spDrawI->drawAlignedText(screen,text,cyan);

	text.sPrintf("CoM Vel %.4G, %.4G, %.4G",
			m_centerOfMassVelocity[0],
			m_centerOfMassVelocity[1],
			m_centerOfMassVelocity[2]);
	screen[1] -= textHeight;
	a_spDrawI->drawAlignedText(screen,text,cyan);

	text.sPrintf("CoM Ang Vel %.4G, %.4G, %.4G",
			m_centerOfMassAngularVelocity[0],
			m_centerOfMassAngularVelocity[1],
			m_centerOfMassAngularVelocity[2]);
	screen[1] -= textHeight;
	a_spDrawI->drawAlignedText(screen,text,cyan);

	text.sPrintf("Long Acc %.3G",m_longitudinalAcceleration);
	screen[1] -= textHeight;
	a_spDrawI->drawAlignedText(screen,text,cyan);

	text.sPrintf("Lat Acc %.3G", m_lateralAcceleration);
	screen[1] -= textHeight;
	a_spDrawI->drawAlignedText(screen,text,cyan);

	text.sPrintf("Yaw %.4G", m_yaw);
	screen[1] -= textHeight;
	a_spDrawI->drawAlignedText(screen,text,cyan);

	text.sPrintf("Yaw Vel %.3G", m_yawVelocity);
	screen[1] -= textHeight;
	a_spDrawI->drawAlignedText(screen,text,cyan);
#endif

	a_spDrawI->popDrawMode();

	spViewI->setCamera(spCameraEditable);
	spViewI->use(ViewI::e_perspective);
}

} /* namespace hive */
