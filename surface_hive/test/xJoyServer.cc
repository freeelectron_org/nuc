/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "signal/signal.h"
#include "apiary/apiary.h"
#include "vehicle/vehicle.h"
#include "vehicle_driver/vehicle_driver.h"
#include "vehicle_dynamics/vehicle_dynamics.h"
#include "imu/ImuSensorI.h"

#define FE_XJS_OPDRIVER			TRUE
#define FE_XJS_DCU_LOGGER		FALSE
#define FE_XJS_FAKE_ADS			FALSE
#define FE_XJS_ADS_ON_HUMAN		TRUE
#define FE_XJS_DRONE			TRUE
#define FE_XJS_OSCILLOSCOPE		FALSE
#define FE_XJS_TRACK_SAMPLER	FALSE
#define FE_XJS_THREAD_SIGNALER	TRUE
#define FE_XJS_NOISY_TIME		FALSE
#define FE_XJS_BEACON			TRUE
#define FE_XJS_BEACON_SERVER	FE_XJS_BEACON

#if FE_XJS_BEACON
#include "guide/guide.h"
#endif

#if FE_XJS_DRONE
#include "flight/flight.h"
#endif

using namespace fe;
using namespace fe::ext;
using namespace hive;

Real smoothTimeStep(Real timeDelta)
{
	const I32 sampleCapacity(300);
	static Real history[sampleCapacity];

	static I32 sampleNext(0);
	static I32 sampleCount(0);

	history[sampleNext]=timeDelta;

	sampleNext++;
	if(sampleCount<sampleNext)
	{
		sampleCount=sampleNext;
	}
	if(sampleNext>=sampleCapacity)
	{
		sampleNext=0;
	}

	Real sumValue(0);
	Real sumWeight(0);
	for(I32 sampleIndex=0;sampleIndex<sampleCount;sampleIndex++)
	{
		const I32 reIndex=(sampleCount+sampleNext-1-sampleIndex)%sampleCount;

		//* NOTE similar to gaussian
		const Real weight=
				pow(sin(fe::pi*(sampleIndex+1)/Real(sampleCount+1)),4.0);

		sumValue+=weight*history[reIndex];
		sumWeight+=weight;

//		feLog("reIndex %d value %.3f weight %.3f\n",
//				reIndex,history[reIndex],weight);
	}

//	feLog("delta %.3f current %d/%d value %.3f/%.3f\n",
//			timeDelta,sampleNext,sampleCount,sumValue,sumWeight);

//	return 1.0/60.0;

	FEASSERT(sumWeight>0.0);
	return (sumWeight>0.0)? sumValue/sumWeight: 0.0;
}

class VehicleHandler:
	virtual public HandlerI
{
	public:
				VehicleHandler(void)		{ setName("VehicleHandler"); }

virtual	void	handle(Record &signal)
				{
					if(m_spSignalerI.isNull())
					{
						feLog("VehicleHandler::handle %p no signaler\n",this);
						return;
					}

//					feLog("VehicleHandler::handle %p\n",this);

					const I32 signalCount=m_signalArray.size();
					for(I32 signalIndex=0;signalIndex<signalCount;signalIndex++)
					{
//						feLog("VehicleHandler::handle %p signal %d/%d\n",
//								this,signalIndex,signalCount);
						m_spSignalerI->signal(m_signalArray[signalIndex]);
					}

//					feLog("VehicleHandler::handle %p done\n",this);
				}

		void	setSignaler(sp<SignalerI> a_spSignalerI)
				{	m_spSignalerI=a_spSignalerI; }

		void	addSignal(Record& a_signal)
				{	m_signalArray.push_back(a_signal); }

	private:
		sp<SignalerI>	m_spSignalerI;
		Array<Record>	m_signalArray;
};

int main(int argc,char** argv)
{
	UNIT_START();

	const BWORD networking=TRUE;

	I32 unitTestCount=36;
	BWORD complete=FALSE;

	feLog("Starting\n");
	try
	{
		{
			sp<HiveHost> spHiveHost(HiveHost::create());
			FEASSERT(spHiveHost.isValid());

			sp<Master> spMaster=spHiveHost->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			result=spRegistry->manage("fexSignalDL");
			UNIT_TEST(successful(result));

			result=spRegistry->manage("hiveApiaryDL");
			UNIT_TEST(successful(result));

			//* NOTE supposed to manage automatically with manifests
//			result=spRegistry->manage("hiveMoaDL");
//			result=spRegistry->manage("hiveTireHiveDL");

			sp<CatalogReaderI> spCatalogReader=
					spRegistry->create("CatalogReaderI.*.*.yaml");
			UNIT_TEST(spCatalogReader.isValid());
			if(spCatalogReader.isNull())
			{
				feLog("spCatalogReader invalid");
			}

			sp<Catalog> spPreCatalog=spMaster->createCatalog("yaml");
			if(spCatalogReader.isValid())
			{
				for(I32 arg=1;arg<argc;arg++)
				{
					spCatalogReader->load(argv[arg],spPreCatalog);
				}
			}
			const String implementation=
					spPreCatalog->catalogOrDefault<String>(
					"net:implementation","ConnectedCatalog");
			const String address=
					spPreCatalog->catalogOrDefault<String>(
					"net:address","127.0.0.1");
			const String transport=
					spPreCatalog->catalogOrDefault<String>(
					"net:transport","tcp");
			const I32 port=
					spPreCatalog->catalogOrDefault<I32>(
					"net:port",12345);

			//* make sure catalog is set, if defaulted
			spPreCatalog->catalog<String>("net:implementation")=implementation;
			spPreCatalog->catalog<String>("net:address")=address;
			spPreCatalog->catalog<String>("net:transport")=transport;
			spPreCatalog->catalog<I32>("net:port")=port;

			const String space("world");

#if FE_XJS_BEACON_SERVER
			sp<beacon::BeaconServerI> spBeaconServer(
					spRegistry->create("BeaconServerI"));

			BWORD started(FALSE);
			if(spBeaconServer.isValid())
			{
				const U16 beaconPort(5000);
				const U16 gdPort(5001);
				started=spBeaconServer->start(beaconPort, gdPort);
			}
			UNIT_TEST(started);
#endif

#if FE_XJS_BEACON

			sp<GuidePostI> spGuidePostI(spRegistry->create("GuidePostI"));

			if(!spGuidePostI->connect("localhost"))
			{
				feLog("failed to connect to beacon\n");
				spGuidePostI=NULL;
			}

			sp<StateCatalog> spStateCatalog=spPreCatalog;

			if(spGuidePostI.isValid())
			{
				spStateCatalog=spGuidePostI->createSpace(space,spPreCatalog);
			}

			if(spStateCatalog.isNull())
			{
				feX(e_invalidPointer,argv[0],"StateCatalog is invalid");
			}

			spStateCatalog->catalogDump();
#else
			sp<StateCatalog> spStateCatalog=
					spHiveHost->accessSpace(space,implementation);
			if(spStateCatalog.isNull())
			{
				feX(e_invalidPointer,argv[0],"StateCatalog is invalid");
			}

			spStateCatalog->overlayState(spPreCatalog);

			spStateCatalog->catalogDump();

			if(networking)
			{
				result=spStateCatalog->start();
				UNIT_TEST(successful(result));
			}
#endif

			sp<RecordGroup>& rDataSetRG=
					spMaster->catalog()->catalog< sp<RecordGroup> >("dataset");
			const Array<String>& rgFilenames=
					spStateCatalog->catalog< Array<String> >(
					"dataset.filenames");
			spHiveHost->loadDataSet(rDataSetRG,rgFilenames);

#if FE_XJS_DRONE
			sp<FlightMissionI> spFlightMissionI=
					spRegistry->create("*.DroneMission");
			spFlightMissionI->use(spStateCatalog);
#endif

			const String driving_test_component=
					spStateCatalog->catalogOrDefault<String>(
					"scene.driving_test:component","");
			feLog("scene.driving_test:component \"%s\"\n",
					driving_test_component.c_str());

			const String track_filename=
					spStateCatalog->catalog<String>("track.filename");
			feLog("track.filename \"%s\"\n",track_filename.c_str());

			sp<SurfaceI> spSurfaceTrack=spRegistry->create("*.SurfaceTrack");
			spSurfaceTrack->prepareForSearch();

			I32 vehicleCount(0);
			while(TRUE)
			{
				String key;
				key.sPrintf("vehicles.%d.mass",vehicleCount);

				if(!spStateCatalog->cataloged(key))
				{
					break;
				}
				vehicleCount++;
			}

			feLog("vehicleCount %d\n",vehicleCount);

//			const Vector3d track_coordinates=
//					spStateCatalog->catalog<Vector3d>(
//					"track.coordinates");
//			feLog("track.coordinates %s\n",
//					c_print(track_coordinates));

			sp<Scope> spScope=spRegistry->create("Scope");
			sp<Layout> spTickLayout=spScope->declare("tick");
			Record tickRecord=spScope->createRecord(spTickLayout);

#if FE_XJS_THREAD_SIGNALER
			sp<SignalerI> spVehicleSignaler=
					spRegistry->create("*.ThreadSignaler");
#else
			sp<SignalerI> spVehicleSignaler=
					spRegistry->create("*.ChainSignaler");
#endif

			Array< sp<Scope> > scopeArray;
			scopeArray.resize(vehicleCount);

			Array< sp<VehicleHandler> > vehicleHandlerArray;
			vehicleHandlerArray.resize(vehicleCount);

			Array< sp<SignalerI> > signalerArray;
			signalerArray.resize(vehicleCount);

			Array<Record> vehicleRecordArray;
			vehicleRecordArray.resize(vehicleCount);

			AsVehicle* asVehicleArray=new AsVehicle[vehicleCount];

			for(I32 vehicleIndex=0;vehicleIndex<vehicleCount;
					vehicleIndex++)
			{
				sp<Scope>& rspScope=scopeArray[vehicleIndex];
				rspScope=spRegistry->create("Scope");

				sp<Layout> spVehicleLayout=rspScope->declare("vehicle");

				AsVehicle& rAsVehicle=asVehicleArray[vehicleIndex];
				rAsVehicle.bind(rspScope);
				rAsVehicle.populate(spVehicleLayout);

				sp<Layout> spAdsSafetyStateLayout=
						rspScope->declare("ads_safety_state");
				AsAdsSafetyState asAdsSafetyState;
				asAdsSafetyState.bind(rspScope);
				asAdsSafetyState.populate(spAdsSafetyStateLayout);

				sp<Layout> spAdsSafety1Layout=rspScope->declare("ads_safety_1");
				AsAdsSafety1 asAdsSafety1;
				asAdsSafety1.bind(rspScope);
				asAdsSafety1.populate(spAdsSafety1Layout);

				sp<Layout> spAdsSafety2Layout=rspScope->declare("ads_safety_2");
				AsAdsSafety2 asAdsSafety2;
				asAdsSafety2.bind(rspScope);
				asAdsSafety2.populate(spAdsSafety2Layout);

				sp<Layout> spAdsSafety3Layout=rspScope->declare("ads_safety_3");
				AsAdsSafety3 asAdsSafety3;
				asAdsSafety3.bind(rspScope);
				asAdsSafety3.populate(spAdsSafety3Layout);

				Record adsSafety1Record=
						rspScope->createRecord(spAdsSafety1Layout);
				Record adsSafety2Record=
						rspScope->createRecord(spAdsSafety2Layout);
				Record adsSafety3Record=
						rspScope->createRecord(spAdsSafety3Layout);
				Record adsSafetyStateRecord=
						rspScope->createRecord(spAdsSafetyStateLayout);

				asAdsSafety1.gearRequest(adsSafety1Record)=
						AdsVehicleSafetyState::e_drive;

				Record& rVehicleRecord=vehicleRecordArray[vehicleIndex];

				rVehicleRecord=rspScope->createRecord(spVehicleLayout);

				rAsVehicle.surfaceTrack(rVehicleRecord)=spSurfaceTrack;

				String vehicleID;
				vehicleID.sPrintf("%d",vehicleIndex);

				rAsVehicle.vehicleID(rVehicleRecord)=vehicleID;
				rAsVehicle.maxSpeed(rVehicleRecord)=300e6;

				String prefix;
				prefix.sPrintf("vehicles.%d.",vehicleIndex);

				const String dynamicsComponent=
						spStateCatalog->catalogOrDefault<String>(
						prefix+"dynamics.component","Scooter");

				sp<HandlerI> spEventPoller;
				if(!vehicleIndex)
				{
					spEventPoller=spRegistry->create("*.EventPoller");
					UNIT_TEST(spEventPoller.isValid());
				}

				sp<HandlerI> spAdsController=
						spRegistry->create("*.AdsController");
				UNIT_TEST(spAdsController.isValid());

				BWORD quiet(TRUE);

				sp<HandlerI> spDrivingTest;
				if(!driving_test_component.empty())
				{
					spDrivingTest=
							spRegistry->create(driving_test_component,quiet);
					if(spDrivingTest.isNull())
					{
						spDrivingTest=spRegistry->create(
								"*."+driving_test_component);
					}
					UNIT_TEST(spDrivingTest.isValid());
					unitTestCount++;
				}

#if FE_XJS_OPDRIVER
				sp<HandlerI> spAdsDriver=spRegistry->create("*.OpDriver");
#else
				sp<HandlerI> spAdsDriver=spRegistry->create("*.AdsDriver");
#endif
				UNIT_TEST(spAdsDriver.isValid());

				sp<Catalog> spAdsDriverCatalog(spAdsDriver);
				if(spAdsDriverCatalog.isValid())
				{
					if(vehicleIndex==2)
					{
						spAdsDriverCatalog->catalog<Real>(
								"aggressionScale")=1.25;
					}
					else if(vehicleIndex==3)
					{
						spAdsDriverCatalog->catalog<Real>(
								"aggressionScale")=0.8;
					}
				}

				sp<HandlerI> spAdsStub=spRegistry->create("*.AdsStub");
				UNIT_TEST(spAdsStub.isValid());

				sp<HandlerI> spHumanDriver;
#if !FE_XJS_DRONE
				spHumanDriver=spRegistry->create("*.HumanDriver");
				UNIT_TEST(spHumanDriver.isValid());
#endif

				sp<HandlerI> spCollisionHandler=
						spRegistry->create("*.SimpleBump");
				UNIT_TEST(spCollisionHandler.isValid());

				sp<HandlerI> spTrackSampler;
#if FE_XJS_TRACK_SAMPLER
				spTrackSampler=spRegistry->create("*.TrackSampler");
				UNIT_TEST(spTrackSampler.isValid());
#endif

				sp<HandlerI> spUnearthHandler=
						spRegistry->create("*.Unearth");
				UNIT_TEST(spUnearthHandler.isValid());

				sp<HandlerI> spGovernor=spRegistry->create("*.Governor");
				UNIT_TEST(spGovernor.isValid());

				sp<HandlerI> spVehicleDynamics=
						spRegistry->create(dynamicsComponent,quiet);
				if(spVehicleDynamics.isNull())
				{
					spVehicleDynamics=
							spRegistry->create("*."+dynamicsComponent);
				}
				UNIT_TEST(spVehicleDynamics.isValid());

				sp<ImuSensorI> spImuSensorI = spRegistry->create("*.ImuSensor");
				UNIT_TEST(spImuSensorI.isValid());

				// Relative sensor offset in ISO coordinates
				Transform3x4F sensorRelativePosition;
				sensorRelativePosition.col[0] = { 1.0f, 0.0f, 0.0f };
				sensorRelativePosition.col[1] = { 0.0f, 1.0f, 0.0f };
				sensorRelativePosition.col[2] = { 0.0f, 0.0f, 1.0f };
				sensorRelativePosition.col[3] = { 1.387f, 0.0f, 0.0f };

				spImuSensorI->SetSensorRelativePositionISO(
						&sensorRelativePosition);
				spImuSensorI->Init("127.0.0.1", 8000,
									"0.0.0.0", 0,
									// Vegas lat,long,alt
									36.2748244, -115.016884, 607.59);

				sp<HandlerI> spDcuLogger;
#if FE_XJS_DCU_LOGGER
				if(vehicleIndex==0)
				{
					spDcuLogger=spRegistry->create("*.DcuLogger");
					UNIT_TEST(spDcuLogger.isValid());
				}
#endif

				sp<SignalerI>& rspSignalerI=signalerArray[vehicleIndex];
				rspSignalerI=spRegistry->create("SignalerI");

				sp<VehicleHandler>& rspVehicleHandler=
						vehicleHandlerArray[vehicleIndex];
				rspVehicleHandler=new(VehicleHandler);
				rspVehicleHandler->setSignaler(rspSignalerI);
				rspVehicleHandler->addSignal(adsSafety1Record);
				rspVehicleHandler->addSignal(adsSafety2Record);
				rspVehicleHandler->addSignal(adsSafety3Record);
				rspVehicleHandler->addSignal(rVehicleRecord);
				rspVehicleHandler->addSignal(adsSafetyStateRecord);

				spVehicleSignaler->insert(rspVehicleHandler,sp<Layout>(NULL));

				//* NOTE currently, all NULL filters go first
				//* otherwise, spEventPoller could be just spVehicleLayout
				if(spEventPoller.isValid())
				{
					rspSignalerI->insert(spEventPoller,sp<Layout>(NULL));
				}

				if(vehicleIndex)
				{
					rspSignalerI->insert(spAdsController,sp<Layout>(NULL));

					if(spDrivingTest.isValid())
					{
						rspSignalerI->insert(spDrivingTest,sp<Layout>(NULL));
					}

#if FE_XJS_FAKE_ADS
					rspSignalerI->insert(spAdsStub,sp<Layout>(NULL));
#endif

					rspSignalerI->insert(spAdsDriver,sp<Layout>(NULL));
				}
				else
				{
#if FE_XJS_ADS_ON_HUMAN
					rspSignalerI->insert(spAdsController,sp<Layout>(NULL));
					rspSignalerI->insert(spAdsDriver,sp<Layout>(NULL));
#endif
					rspSignalerI->insert(spHumanDriver,sp<Layout>(NULL));
				}

				if(spTrackSampler.isValid())
				{
					rspSignalerI->insert(spTrackSampler,spVehicleLayout);
				}

				rspSignalerI->insert(spUnearthHandler,spVehicleLayout);
				rspSignalerI->insert(spCollisionHandler,spVehicleLayout);
				rspSignalerI->insert(spGovernor,spVehicleLayout);
				rspSignalerI->insert(spVehicleDynamics,spVehicleLayout);
				rspSignalerI->insert(spImuSensorI,spVehicleLayout);
				rspSignalerI->insert(spDcuLogger,sp<Layout>(NULL));

#if FE_XJS_OSCILLOSCOPE
				sp<HandlerI> spVehicleOscilloscope;
				if(vehicleIndex==1)
				{
					spVehicleOscilloscope=
							spRegistry->create("*.VehicleOscilloscope");
					UNIT_TEST(spVehicleOscilloscope.isValid());

					if(spVehicleOscilloscope.isValid())
					{
						rspSignalerI->insert(spVehicleOscilloscope,
								spVehicleLayout);
					}
				}
#endif
			}

			sp<Scope> spObstacleScope=spRegistry->create("Scope");
			sp<Layout> spObstacleLayout=spObstacleScope->declare("obstacle");

			Record obstacleRecord=
					spObstacleScope->createRecord(spObstacleLayout);

			AsObstacle asObstacle;
			asObstacle.bind(spObstacleScope);
			asObstacle.populate(spObstacleLayout);

			sp<SignalerI> spObstacleSignaler=spRegistry->create("SignalerI");

			sp<HandlerI> spLocomotion=spRegistry->create("*.Locomotion");
			if(spLocomotion.isValid())
			{
				spObstacleSignaler->insert(spLocomotion,sp<Layout>(NULL));
			}

			if(spStateCatalog->started())
			{
				BWORD wasConnected(FALSE);
				I32 frame(0);
				Real timeElapsed(0.0);
				Real timeStep(1.0/60.0);
				Real nanoStep(0);
				Real nanoDelay(0);
				Real lastDelay(0);

				spStateCatalog->setState<bool>("running",true);
				spStateCatalog->flush();

				SystemTicker ticker;
				U32 lastTimeMS=ticker.timeMS();

				Array<SpatialTransform> transformArray;
				transformArray.resize(vehicleCount);

				std::map<String, sp<StateCatalog> > childStateArray;

				BWORD done=FALSE;
				while(!done)
				{
#if FALSE//FE_XJS_BEACON
					const U8 beaconID=spGuidePostI->getID();

					Array<String> spaceList;
					spGuidePostI->listSpaces(spaceList);
					const I32 spaceCount=spaceList.size();
					for(I32 spaceIndex=0;spaceIndex<spaceCount;spaceIndex++)
					{
						feLog("Space %d/%d \"%s\"\n",spaceIndex,spaceCount,
								spaceList[spaceIndex].c_str());

					}
#endif

					const U32 timeMS=ticker.timeMS();
					const U32 deltaMS=timeMS-lastTimeMS;
					lastTimeMS=timeMS;
					feLog("\nFRAME %d milliseconds %d\n",frame,deltaMS);

					if(nanoStep>0.0)
					{
						timeStep=nanoStep/1e9;
					}
					else
					{
						const Real timeDelta=
								1e-3*fe::maximum(Real(1),Real(deltaMS));

						timeStep=smoothTimeStep(timeDelta);
					}

#if FE_XJS_NOISY_TIME
					static I32 flip(0);
					if(flip=!flip)
					{
						timeStep+=0.01;
					}
#endif

					feLog("timeStep %.5f frequency %.1f\n",timeStep,
							timeStep>0.0? 1.0/timeStep: 0.0);

					SpatialTransform xform;
					SpatialTransform wheelTransform[4];

					if(vehicleCount>1)
					{
						done=TRUE;
					}

					for(I32 vehicleIndex=0;vehicleIndex<vehicleCount;
							vehicleIndex++)
					{
						AsVehicle& rAsVehicle=asVehicleArray[vehicleIndex];
						Record& rVehicleRecord=vehicleRecordArray[vehicleIndex];

						rAsVehicle.deltaTime(rVehicleRecord)=timeStep;
					}

					asObstacle.deltaTime(obstacleRecord)=timeStep;

					spVehicleSignaler->signal(tickRecord);

					spObstacleSignaler->signal(obstacleRecord);

					//* TODO
//					I64 ms: m_vehicleTag + ".sim.timestamp"
//					I32 serial: "global.sim.counter"
//					String vid's (space delim): "vehicles.active"

//					timestamp=
//						std::chrono::duration_cast<std::chrono::milliseconds>
//						(std::chrono::steady_clock::now()
//						.time_since_epoch()).count();

					for(I32 vehicleIndex=0;vehicleIndex<vehicleCount;
							vehicleIndex++)
					{
						SpatialTransform& rLastTransform=
								transformArray[vehicleIndex];
						AsVehicle& rAsVehicle=asVehicleArray[vehicleIndex];
						Record& rVehicleRecord=vehicleRecordArray[vehicleIndex];

						String prefix;
						prefix.sPrintf("vehicles.%d.",vehicleIndex);

//						feLog("gearModeInput %d %d\n",vehicleIndex,
//								rAsVehicle.gearModeInput(rVehicleRecord));

						//* NOTE not checking human car
						if(vehicleIndex &&
								rAsVehicle.gearModeInput(rVehicleRecord)!=
								AsVehicle::e_gearModePark)
						{
							done=FALSE;
						}

//						const Real steering=rAsVehicle.steering(rVehicleRecord);
						const Real throttle=
								rAsVehicle.throttleInput(rVehicleRecord);
						const Real brake=rAsVehicle.brakeInput(rVehicleRecord);
						const I32 gearMode=
								rAsVehicle.gearModeInput(rVehicleRecord);
						const BWORD horn=rAsVehicle.hornInput(rVehicleRecord);

						const SpatialVector adsTarget0=
								rAsVehicle.adsTarget0(rVehicleRecord);
						const SpatialVector adsTarget1=
								rAsVehicle.adsTarget1(rVehicleRecord);

						xform=rAsVehicle.chassisTransform(rVehicleRecord);
						wheelTransform[0]=
								rAsVehicle.wheelFLTransform(rVehicleRecord);
						wheelTransform[1]=
								rAsVehicle.wheelFRTransform(rVehicleRecord);
						wheelTransform[2]=
								rAsVehicle.wheelRLTransform(rVehicleRecord);
						wheelTransform[3]=
								rAsVehicle.wheelRRTransform(rVehicleRecord);

						const SpatialVector delta=
								xform.translation()-
								rLastTransform.translation();
						const Real speed=dot(delta,xform.direction())/timeStep;
						rLastTransform=xform;

#if FALSE
						feLog("transform \"%s\":\n%s\n",
								(prefix+
								rAsVehicle.chassisTransform.name()).c_str(),
								c_print(xform));
#endif

						spStateCatalog->setState<SpatialTransform>(prefix+
								rAsVehicle.chassisTransform.name(),
								xform);
						spStateCatalog->setState<SpatialTransform>(prefix+
								rAsVehicle.wheelFLTransform.name(),
								wheelTransform[0]);
						spStateCatalog->setState<SpatialTransform>(prefix+
								rAsVehicle.wheelFRTransform.name(),
								wheelTransform[1]);
						spStateCatalog->setState<SpatialTransform>(prefix+
								rAsVehicle.wheelRLTransform.name(),
								wheelTransform[2]);
						spStateCatalog->setState<SpatialTransform>(prefix+
								rAsVehicle.wheelRRTransform.name(),
								wheelTransform[3]);
						spStateCatalog->setState<Real>(prefix+
								rAsVehicle.throttleInput.name(),
								throttle);
						spStateCatalog->setState<Real>(prefix+
								rAsVehicle.brakeInput.name(),
								brake);
						spStateCatalog->setState<I32>(prefix+
								rAsVehicle.gearModeInput.name(),
								gearMode);
						spStateCatalog->setState<BWORD>(prefix+
								rAsVehicle.hornInput.name(),
								horn);

						//* show vehicle 0 target0 as target0
						if(vehicleIndex==0)
						{
							spStateCatalog->setState<SpatialVector>(prefix+
									rAsVehicle.adsTarget0.name(),
									adsTarget0);
						}
#if TRUE
						//* show vehicle 0 target1 as target1
						if(vehicleIndex==0)
						{
							spStateCatalog->setState<SpatialVector>(prefix+
									rAsVehicle.adsTarget1.name(),
									adsTarget1);
						}
#else
						//* show vehicle 1 target0 as target1
						if(vehicleIndex==1)
						{
							spStateCatalog->setState<SpatialVector>(
									"vehicles.0."+
									rAsVehicle.adsTarget1.name(),
									adsTarget0);
						}
#endif

						spStateCatalog->setState<SpatialVector>(prefix+
								"velocity",
								speed*xform.direction());

						Real lateralAcceleration=
								rAsVehicle.lateralAcceleration(rVehicleRecord);
						Real longitudinalAcceleration=
								rAsVehicle.longitudinalAcceleration(
								rVehicleRecord);

						spStateCatalog->setState<Real>(prefix+
								rAsVehicle.longitudinalAcceleration.name(),
								longitudinalAcceleration);
						spStateCatalog->setState<Real>(prefix+
								rAsVehicle.lateralAcceleration.name(),
								lateralAcceleration);
#if FALSE
						// Debug values
						SpatialVector centerOfMassVelocity=
								rAsVehicle.centerOfMassVelocity(rVehicleRecord);
						SpatialVector centerOfMassAngularVelocity=
								rAsVehicle.centerOfMassAngularVelocity(
								rVehicleRecord);
						SpatialTransform axlesRearTransform=
								rAsVehicle.axlesRearTransform(rVehicleRecord);
						Real yawVelocity=
								rAsVehicle.yawVelocity(rVehicleRecord);
						double yaw=
								rAsVehicle.yaw(rVehicleRecord);
						double longitude=
								rAsVehicle.longitude(rVehicleRecord);
						double latitude=
								rAsVehicle.latitude(rVehicleRecord);
						double altitude=
								rAsVehicle.altitude(rVehicleRecord);

						spStateCatalog->setState<SpatialVector>(prefix+
								rAsVehicle.centerOfMassVelocity.name(),
								centerOfMassVelocity);
						spStateCatalog->setState<SpatialEuler>(prefix+
								rAsVehicle.centerOfMassAngularVelocity.name(),
								centerOfMassAngularVelocity);
						spStateCatalog->setState<SpatialTransform>(prefix+
								rAsVehicle.axlesRearTransform.name(),
								axlesRearTransform);
						spStateCatalog->setState<double>(prefix+
								rAsVehicle.latitude.name(),
								latitude);
						spStateCatalog->setState<double>(prefix+
								rAsVehicle.longitude.name(),
								longitude);
						spStateCatalog->setState<double>(prefix+
								rAsVehicle.altitude.name(),
								altitude);
						spStateCatalog->setState<Real>(prefix+
								rAsVehicle.yawVelocity.name(),
								yawVelocity);
						spStateCatalog->setState<double>(prefix+
								rAsVehicle.yaw.name(),
								yaw);
#endif
					}

#if FE_XJS_DRONE
					spFlightMissionI->step(timeStep);
#endif

					timeElapsed+=timeStep;

					spStateCatalog->setState<I32>("frame",frame++);
					spStateCatalog->setState<Real>("timeElapsed",timeElapsed);
					spStateCatalog->flush();

					result=spStateCatalog->getState<Real>(
							"nanoStep",nanoStep);

					result=spStateCatalog->getState<Real>(
							"nanoDelay",nanoDelay);
					if(lastDelay!=nanoDelay)
					{
//						feLog("delay ms %.3f\n",1e-6*nanoDelay);
						lastDelay=nanoDelay;
					}
					if(nanoDelay>0.0)
					{
						nanoSpin(I32(nanoDelay));
					}

#if FALSE
					//* HACK early exit
					if(frame>400)
					{
						done=TRUE;
					}
#endif

					const BWORD connected=spStateCatalog->connected();
					if(wasConnected && !connected)
					{
						done=TRUE;
					}
					wasConnected=connected;
				}

				spStateCatalog->setState<bool>("running",false);
				spStateCatalog->flush();

				if(networking)
				{
					spStateCatalog->stop();
				}
			}

			delete[] asVehicleArray;


#if FE_XJS_BEACON
			if(spGuidePostI.isValid())
			{
				feLog("GuidePostI shutdown\n");
				spGuidePostI->shutdown();
				feLog("GuidePostI -> NULL\n");
				spGuidePostI=NULL;
				feLog("GuidePostI is NULL\n");
			}
#endif

#if FE_XJS_BEACON_SERVER
			if(spBeaconServer.isValid())
			{
				feLog("BeaconServer stop\n");
				spBeaconServer->stop();
				feLog("BeaconServer -> NULL\n");
				spBeaconServer=NULL;
				feLog("BeaconServer is NULL\n");
			}
#endif

			feLog("HiveHost -> NULL\n");
			spHiveHost=NULL;
			feLog("HiveHost is NULL\n");
		}

		complete=TRUE;
	}
	catch(Exception &e)			{ e.log(); }
	catch(std::exception &e)	{ feLog("std::exception %s\n", e.what()); }
	catch(...)					{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(unitTestCount);
	UNIT_RETURN();
}
