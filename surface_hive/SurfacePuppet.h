/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __surface_hive_SurfacePuppet_h__
#define __surface_hive_SurfacePuppet_h__

namespace hive
{

/**************************************************************************//**
	@brief HiveHost Driven Surface

	@ingroup surface_hive
*//***************************************************************************/
class FE_DL_EXPORT SurfacePuppet:
	public fe::ext::SurfaceSphere,
	virtual public fe::ext::CameraEditable,
	public fe::CastableAs<SurfacePuppet>
{
	public:
								SurfacePuppet(void);
virtual							~SurfacePuppet(void);

								//* As Protectable
virtual	fe::Protectable*		clone(fe::Protectable* pInstance=NULL);

								//* As SurfaceI

								using fe::ext::SurfaceSphere::draw;

virtual	void					draw(const fe::SpatialTransform&,
									fe::sp<fe::ext::DrawI> a_spDrawI,
									const fe::Color* a_color,
									fe::sp<fe::ext::DrawBufferI> a_spDrawBuffer,
									fe::sp<fe::ext::PartitionI> a_spPartition)
									const;

								//* As CameraEditable
virtual
const	fe::SpatialTransform&	cameraMatrix(void) const
								{	return m_xformCam; }

								/// @brief Field of View (degrees)
virtual	fe::Vector2				fov(void) const
								{	return fe::Vector2(90,90); }

	class ProxySurface: public fe::ext::SurfaceSphere
	{
		public:
			void	setCenter(fe::SpatialVector& a_center)
					{	m_center=a_center; }
			void	setRadius(fe::Real a_radius)
					{	m_radius=a_radius; }

					using fe::ext::SurfaceSphere::draw;

	virtual	void	draw(const fe::SpatialTransform&,
						fe::sp<fe::ext::DrawI> a_spDrawI,
						const fe::Color* a_color,
						fe::sp<fe::ext::DrawBufferI> a_spDrawBuffer,
						fe::sp<fe::ext::PartitionI> a_spPartition)
						const												{}
	};

		I32							proxyCount(void) const
									{	return m_proxyName.size(); }
		fe::String					proxyName(I32 a_index) const
									{	return m_proxyName[a_index]; }
		fe::sp<fe::ext::SurfaceI>	proxySurface(I32 a_index) const
									{	return m_proxySurface[a_index]; }

	protected:

virtual	void					cache(void);

	private:

	class CarSurface
	{
		public:
			fe::sp<fe::ext::SurfaceAccessibleI>	m_spSurfaceAccessibleI;
			fe::sp<fe::ext::SurfaceI>			m_spSurfaceI;
			fe::sp<fe::ext::DrawBufferI>		m_spDrawBuffer;
			fe::String							m_filename;
			fe::String							m_options;
	};

		void					updateState(void);
		void					drawDebugInfo(
									fe::sp<fe::ext::DrawI> a_spDrawI) const;

		fe::sp<fe::ext::DrawMode>			m_spDrawPhantom;
		fe::sp<fe::ext::DrawMode>			m_spDebugGroup;
		fe::sp<fe::ext::CameraEditable>		m_spCameraOverlay;

		fe::sp<HiveHost>					m_spHiveHost;
		fe::sp<fe::StateCatalog>			m_spStateCatalog;

		fe::sp<fe::Scope>					m_spScope;
		AsVehicle							m_asVehicle;

		I32									m_frame;
		fe::Real							m_timeElapsed;

		fe::Array<fe::SpatialTransform>		m_xformObstacle;
		fe::Array<fe::SpatialTransform>		m_xformCar;
		fe::SpatialTransform				m_xformCar0Prev;
		fe::SpatialTransform				m_xformWheel0[4];
		fe::SpatialTransform				m_xformCam;
		fe::Real							m_throttle;
		fe::Real							m_brake;
		I32									m_gearMode;
		fe::Real							m_steeringAngle;
		BWORD								m_horn;
		fe::SpatialVector					m_adsTarget0;
		fe::SpatialVector					m_adsTarget1;
		fe::SpatialVector					m_velocity;

		fe::Array<fe::SpatialVector>		m_history;
		fe::Array<fe::Color>				m_historyColor;
		I32									m_historyIndex;

		fe::Array< fe::sp<ProxySurface> >	m_proxySurface;
		fe::Array<fe::String>				m_proxyName;

		fe::Array<CarSurface>				m_carSurface;
		fe::Array<I32>						m_carSurfaceIndex;

		fe::sp<fe::ext::ListenerI>			m_spListenerI;
		fe::sp<fe::ext::VoiceI>				m_spVoiceEngine;
		fe::sp<fe::ext::VoiceI>				m_spVoiceTire;
		fe::sp<fe::ext::VoiceI>				m_spVoiceHorn;
		fe::sp<fe::ext::VoiceI>				m_spVoicePop;
		fe::sp<fe::ext::AudioI>				m_spAudioI;

		fe::Real							m_nanoDelay;

		// Debug values
		fe::SpatialVector					m_centerOfMassVelocity;
		fe::SpatialEuler					m_centerOfMassAngularVelocity;
		fe::SpatialTransform				m_axlesRearTransform;
		double								m_latitude;
		double								m_longitude;
		double								m_altitude;
		fe::Real							m_longitudinalAcceleration;
		fe::Real							m_lateralAcceleration;
		fe::Real							m_yawVelocity;
		double								m_yaw;
		fe::Real							m_carSlipAngle;
		fe::Real							m_tireSlipAngle;
};

} /* namespace hive */

#endif /* __surface_hive_SurfacePuppet_h__ */
