/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsAdsReferencePos_h__
#define __vehicle_AsAdsReferencePos_h__

namespace hive
{

class FE_DL_EXPORT AsAdsReferencePos:
	public fe::AccessorSet,
	public fe::Initialize<AsAdsReferencePos>
{
	public:
		void initialize(void)
		{
			add(adsReferenceLongitude,	FE_USE("track:referenceLongitude"));
			add(adsReferenceLatitude,	FE_USE("track:referenceLatitude"));
			add(adsReferenceAltitude,	FE_USE("track:adsReferenceAltitude"));
		}

		// RX_1 and 2
		fe::Accessor<double>	adsReferenceLongitude;
		fe::Accessor<double>	adsReferenceLatitude;
		fe::Accessor<double>	adsReferenceAltitude;
};

}
#endif
