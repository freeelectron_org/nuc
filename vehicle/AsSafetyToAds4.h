/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsSafetyToAds4_h__
#define __vehicle_AsSafetyToAds4_h__

namespace hive
{

class FE_DL_EXPORT AsSafetyToAds4:
	public fe::AccessorSet,
	public fe::Initialize<AsSafetyToAds4>
{
	public:
		void initialize(void)
		{
			add(longitudinalAccelerationPlus,
					FE_USE("ads:safety.acceleration.longitudinal.postive"));
			add(longitudinalAccelerationNeg,
					FE_USE("ads:safety.acceleration.longitudinal.negative"));
			add(curvature,
					FE_USE("ads:safety.curvature"));
		}

		fe::Accessor<F32>	longitudinalAccelerationPlus;
		fe::Accessor<F32>	longitudinalAccelerationNeg;
		fe::Accessor<F32>	curvature;
};

}
#endif
