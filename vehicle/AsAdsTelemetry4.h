/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsAdsTelemetry4_h__
#define __vehicle_AsAdsTelemetry4_h__

namespace hive
{

class FE_DL_EXPORT AsAdsTelemetry4:
	public fe::AccessorSet,
	public fe::Initialize<AsAdsTelemetry4>
{
	public:
		void initialize(void)
		{
			add(logByte1,	FE_USE("ads:telemetry.logByte1"));
			add(logByte2,	FE_USE("ads:telemetry.logByte2"));
			add(logByte3,	FE_USE("ads:telemetry.logByte3"));
			add(logByte4,	FE_USE("ads:telemetry.logByte4"));
			add(logByte5,	FE_USE("ads:telemetry.logByte5"));
			add(logByte6,	FE_USE("ads:telemetry.logByte6"));
			add(logByte7,	FE_USE("ads:telemetry.logByte7"));
			add(logByte8,	FE_USE("ads:telemetry.logByte8"));
		}

		fe::Accessor<U8>	logByte1;
		fe::Accessor<U8>	logByte2;
		fe::Accessor<U8>	logByte3;
		fe::Accessor<U8>	logByte4;
		fe::Accessor<U8>	logByte5;
		fe::Accessor<U8>	logByte6;
		fe::Accessor<U8>	logByte7;
		fe::Accessor<U8>	logByte8;
};

}
#endif
