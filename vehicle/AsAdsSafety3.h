/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsAdsSafety3_h__
#define __vehicle_AsAdsSafety3_h__

namespace hive
{

class FE_DL_EXPORT AsAdsSafety3:
	public fe::AccessorSet,
	public fe::Initialize<AsAdsSafety3>
{
	public:
		void initialize(void)
		{
			add(motorRLRequestNm,
					FE_USE("ads:safety.motors.RL.torqueRequest"));
			add(motorRRRequestNm,
					FE_USE("ads:safety.motors.RR.torqueRequest"));
			add(brakeRRequestBar,
					FE_USE("ads:safety.brakes.rear.request"));
			add(brakeFRequestBar,
					FE_USE("ads:safety.brakes.front.request"));
			add(steerFMeanRequestDeg,
					FE_USE("ads:safety.steer.front.request"));
		}

		fe::Accessor<F32>	motorRLRequestNm;
		fe::Accessor<F32>	motorRRRequestNm;
		fe::Accessor<F32>	brakeRRequestBar;
		fe::Accessor<F32>	brakeFRequestBar;
		fe::Accessor<F32>	steerFMeanRequestDeg;
};

}
#endif
