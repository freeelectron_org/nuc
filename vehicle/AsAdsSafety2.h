/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsAdsSafety2_h__
#define __vehicle_AsAdsSafety2_h__

namespace hive
{

class FE_DL_EXPORT AsAdsSafety2:
	public fe::AccessorSet,
	public fe::Initialize<AsAdsSafety2>
{
	public:
		void initialize(void)
		{
			add(curvatureRequest,
					FE_USE("ads:safety.curveRequest"));
			add(gLongRequest,
					FE_USE("ads:safety.longAccRequest"));
			add(motorFLRequestNm,
					FE_USE("ads:safety.motors.FL.torqueRequest"));
			add(motorFRRequestNm,
					FE_USE("ads:safety.motors.FR.torqueRequest"));
		}

		fe::Accessor<F32>	curvatureRequest;
		fe::Accessor<F32>	gLongRequest;
		fe::Accessor<F32>	motorFLRequestNm;
		fe::Accessor<F32>	motorFRRequestNm;
};

}
#endif
