/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsMeasurementToAds5_h__
#define __vehicle_AsMeasurementToAds5_h__

namespace hive
{

//-----------------------------------------------
// NOTE: All units in radians, meters, seconds, bar, Nw-m
//       Status = Good = 1;
//-----------------------------------------------

class FE_DL_EXPORT AsMeasurementToAds5:
	public fe::AccessorSet,
	public fe::Initialize<AsMeasurementToAds5>
{
	public:
		void initialize(void)
		{
			add(curvatureStatus,
				FE_USE("ads:measurement.curvature.status"));
			add(motorTorqueFLStatus,
				FE_USE("ads:measurement.motors.FL.status"));
			add(motorTorqueFRStatus,
				FE_USE("ads:measurement.motors.FR.status"));
			add(steerAngleStatus,
				FE_USE("ads:measurement.steer.status"));

			add(curvature,
				FE_USE("ads:measurement.curvature"));
			add(motorTorqueFL,
				FE_USE("ads:measurement.motors.FL.torque"));
			add(motorTorqueFR,
				FE_USE("ads:measurement.motors.FR.torque"));
			add(steerAngle,
				FE_USE("ads:measurement.steer.angle"));
		}

		fe::Accessor<U8>	curvatureStatus;
		fe::Accessor<U8>	motorTorqueFLStatus;
		fe::Accessor<U8>	motorTorqueFRStatus;
		fe::Accessor<U8>	steerAngleStatus;

		fe::Accessor<F32>	curvature;
		fe::Accessor<F32>	motorTorqueFL;
		fe::Accessor<F32>	motorTorqueFR;
		fe::Accessor<F32>	steerAngle;
};

}
#endif
