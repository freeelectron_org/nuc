/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsMetaverseControl_h__
#define __vehicle_AsMetaverseControl_h__

// This is mostly intended for Ghost Cars Virtual Vehicles
namespace hive
{

class FE_DL_EXPORT AsVehiclesSimControl:
	public fe::AccessorSet,
	public fe::Initialize<AsVehiclesSimControl>
{
	public:
		void initialize(void)
		{
			add(driveMode,		FE_USE("vehiclesManager:driveModeRequest"));
			add(maxPowerFront,	FE_USE("vehiclesManager:motorMaxPowerFront"));
			add(maxPowerRear,	FE_USE("vehiclesManager:motorMaxPowerRear"));
			add(maxSpeed,		FE_USE("vehiclesManager:maxSpeed"));
			add(vid,			FE_USE("vehiclesManager:vehicleID"));
		}

		fe::Accessor<I32>						driveMode;
		fe::Accessor<fe::Real>					maxPowerFront;
		fe::Accessor<fe::Real>					maxPowerRear;
		fe::Accessor<fe::Real>					maxSpeed;
		fe::Accessor<fe::String>				vid;
};

} //namespace
#endif
