/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __hive_vehicle_h__
#define __hive_vehicle_h__

#include "fe/data.h"
#include "math/math.h"

#include "vehicle/AdsVehicleSafetyState.h"

#include "vehicle/AsObstacle.h"

#include "vehicle/AsVehicle.h"
#include "vehicle/AsAdsSafetyState.h"

#include "vehicle/AsAdsSafety1.h"
#include "vehicle/AsAdsSafety2.h"
#include "vehicle/AsAdsSafety3.h"

#include "vehicle/AsSafetyToAds1.h"
#include "vehicle/AsSafetyToAds2.h"
#include "vehicle/AsSafetyToAds3.h"
#include "vehicle/AsSafetyToAds4.h"
#include "vehicle/AsSafetyToAds5.h"

#include "vehicle/AsAdsTelemetry1.h"
#include "vehicle/AsAdsTelemetry2.h"
#include "vehicle/AsAdsTelemetry3.h"
#include "vehicle/AsAdsTelemetry4.h"

#include "vehicle/AsMeasurementToAds1.h"
#include "vehicle/AsMeasurementToAds2.h"
#include "vehicle/AsMeasurementToAds3.h"
#include "vehicle/AsMeasurementToAds4.h"
#include "vehicle/AsMeasurementToAds5.h"

#include "vehicle/AsRaceControl.h"
#include "vehicle/AsAdsReferencePos.h"

#include "vehicle/AsSimTick.h"
#include "vehicle/AsVehiclesSimControl.h"
#endif
