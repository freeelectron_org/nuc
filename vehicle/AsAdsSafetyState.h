/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsAdsSafetyState_h__
#define __vehicle_AsAdsSafetyState_h__

namespace hive
{

class FE_DL_EXPORT AsAdsSafetyState:
	public fe::AccessorSet,
	public fe::Initialize<AsAdsSafetyState>
{
	public:
		void initialize(void)
		{
			add(driveMode,			FE_USE("ads:safety.driveMode"));
			add(controlMode,		FE_USE("ads:safety.controlMode"));
			add(safeStop,			FE_USE("ads:safety.safeStop"));
			add(gear,				FE_USE("ads:safety.gear"));
		}

		fe::Accessor<I32>	driveMode;
		fe::Accessor<I32>	controlMode;
		fe::Accessor<I32>	safeStop;
		fe::Accessor<I32>	gear;
};

}
#endif
