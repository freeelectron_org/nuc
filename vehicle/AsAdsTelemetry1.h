/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsAdsTelemetry1_h__
#define __vehicle_AsAdsTelemetry1_h__

namespace hive
{

class FE_DL_EXPORT AsAdsTelemetry1:
	public fe::AccessorSet,
	public fe::Initialize<AsAdsTelemetry1>
{
	public:
		void initialize(void)
		{
			add(positionNorthEstimate,
					FE_USE("ads:telemetry.position.north.estimate"));
			add(positionEastEstimate,
					FE_USE("ads:telemetry.position.east.estimate"));
			add(headingEstimate,
					FE_USE("ads:telemetry.heading.estimate"));
			add(teamId,
					FE_USE("ads:telemetry.teamId"));
		}

		fe::Accessor<F32>	positionNorthEstimate;		// meters from origin
		fe::Accessor<F32>	positionEastEstimate;		// meters from origin
		fe::Accessor<F32>	headingEstimate;			// radians
		fe::Accessor<U8>	teamId;						// 0-10
};

}
#endif
