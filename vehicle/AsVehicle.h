/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsVehicle_h__
#define __vehicle_AsVehicle_h__

namespace hive
{

//* HACK presumes four wheels
//* TODO should this be broken up? (input, render transforms, IMU data)
//* TODO should this be mostly references into Records/RGs in a Scope?

class FE_DL_EXPORT AsVehicle:
	public fe::AccessorSet,
	public fe::Initialize<AsVehicle>
{
	public:
		typedef enum
		{
			e_gearModeUnknown=	-1,
			e_gearModePark=		0,
			e_gearModeReverse,
			e_gearModeNeutral,
			e_gearModeDrive,
			e_gearModeManual
		} GearMode;

		void initialize(void)
		{
			add(vehicleID,			FE_USE("sim:vehicleID"));
			add(deltaTime,			FE_USE("sim:timestep"));
			add(integratedDeltaTime,FE_USE("sim.integratedDeltaTime"));
			add(vehicleSimCounter,	FE_USE("sim:vehicleSimCounter"));

			add(surfaceTrack,		FE_USE("surface.track"));

			add(throttleInput,		FE_USE("throttle.input"));
			add(brakeInput,			FE_USE("brake.input"));
			add(clutchInput,		FE_USE("clutch.input"));
			add(steeringFInput,		FE_USE("steerings.F.input"));
			add(hornInput,			FE_USE("horn.input"));
			add(gearModeInput,		FE_USE("gear.mode.input"));
			add(gearIndexInput,		FE_USE("gear.index.input"));

			add(centerOfMassOffset,	FE_USE("centerOfMass.offset"));
			add(centerOfMassVelocity,
									FE_USE("centerOfMass.velocity"));
			add(centerOfMassAcceleration,
									FE_USE("centerOfMass.acceleration"));
			add(centerOfMassAngularVelocity,
									FE_USE("centerOfMass.angularVelocity"));
			add(centerOfMassAngularAcceleration,
									FE_USE("centerOfMass.angularAcceleration"));
			add(axlesRearTransform,	FE_USE("axles.rear.transform"));

			add(chassisTransform,	FE_USE("chassis.transform"));
			add(wheelFLTransform,	FE_USE("wheels.FL.transform"));
			add(wheelFRTransform,	FE_USE("wheels.FR.transform"));
			add(wheelRLTransform,	FE_USE("wheels.RL.transform"));
			add(wheelRRTransform,	FE_USE("wheels.RR.transform"));

			add(speed,				FE_USE("speed"));
			add(wheelFLAngularVelocity,
									FE_USE("wheels.FL.angularVelocity"));
			add(wheelFRAngularVelocity,
									FE_USE("wheels.FR.angularVelocity"));
			add(wheelRLAngularVelocity,
									FE_USE("wheels.RL.angularVelocity"));
			add(wheelRRAngularVelocity,
									FE_USE("wheels.RR.angularVelocity"));
			add(motorTorqueRL,		FE_USE("motors.RL.torque"));
			add(motorTorqueRR,		FE_USE("motors.RR.torque"));
			add(motorTorqueMaxRL,	FE_USE("motors.RL.torqueMax"));
			add(motorTorqueMaxRR,	FE_USE("motors.RR.torqueMax"));

			add(powerScale,			FE_USE("dynamics.powerScale"));
			add(maxSpeed,			FE_USE("dynamics.maxSpeed"));

			add(frontSteerAngle,	FE_USE("steerAngle"));
			add(curvature,			FE_USE("curvature"));
			add(frontBrakesPressure,FE_USE("brakes.front.pressure.state"));
			add(rearBrakePressure,	FE_USE("brakes.rear.pressure.state"));
			add(longitudinalAcceleration,
									FE_USE("acceleration.longitudinal"));
			add(lateralAcceleration,FE_USE("acceleration.lateral"));
			add(yawVelocity,		FE_USE("yawVelocity"));
			add(latitude,			FE_USE("latitude"));
			add(longitude,			FE_USE("longitude"));
			add(altitude,			FE_USE("altitude"));
			add(yaw,				FE_USE("yaw"));
			add(heading,			FE_USE("heading"));
			add(slipAngle,			FE_USE("oss.slipAngle"));
			add(longitudinalVelocityOss,
									FE_USE("oss.velocity.longitudinal"));
			add(lateralVelocityOss,	FE_USE("oss.velocity.lateral"));

			add(pid_brake,			FE_USE("pid_brake"));
			add(pid_throttle,		FE_USE("pid_throttle"));
			add(pid_steering,		FE_USE("pid_steering"));
			add(pid_error,			FE_USE("pid_error"));

			add(adsTarget0,			FE_USE("ads.feedback.targets.0"));
			add(adsTarget1,			FE_USE("ads.feedback.targets.1"));

			add(impulse,			FE_USE("impulse"));
			add(angularImpulse,		FE_USE("angularImpulse"));

			add(probe,				FE_USE("probe"));
		}

		fe::Accessor<fe::String>				vehicleID;
		fe::Accessor<fe::Real>					deltaTime;
		fe::Accessor<I64>						vehicleSimCounter;
		fe::Accessor< fe::sp<fe::Component> >	surfaceTrack;

		fe::Accessor<fe::Real>					throttleInput;
		fe::Accessor<fe::Real>					brakeInput;
		fe::Accessor<fe::Real>					clutchInput;
		fe::Accessor<fe::Real>					steeringFInput;
		fe::Accessor<BWORD>						hornInput;
		fe::Accessor<I32>						gearModeInput;
		fe::Accessor<I32>						gearIndexInput;

		// Filled by Vehicle Dynamics
		fe::Accessor<fe::SpatialVector>		centerOfMassOffset;
		fe::Accessor<fe::SpatialVector>		centerOfMassVelocity;
		fe::Accessor<fe::SpatialVector>		centerOfMassAcceleration;
		fe::Accessor<fe::SpatialEuler>		centerOfMassAngularVelocity;
		fe::Accessor<fe::SpatialEuler>		centerOfMassAngularAcceleration;
		fe::Accessor<fe::SpatialTransform>	axlesRearTransform;

		fe::Accessor<fe::SpatialTransform>	chassisTransform;
		fe::Accessor<fe::SpatialTransform>	wheelFLTransform;
		fe::Accessor<fe::SpatialTransform>	wheelFRTransform;
		fe::Accessor<fe::SpatialTransform>	wheelRLTransform;
		fe::Accessor<fe::SpatialTransform>	wheelRRTransform;

		fe::Accessor<fe::Real>	speed;						// m/sec
		fe::Accessor<fe::Real>	wheelFLAngularVelocity;		// radians/sec
		fe::Accessor<fe::Real>	wheelFRAngularVelocity;
		fe::Accessor<fe::Real>	wheelRLAngularVelocity;
		fe::Accessor<fe::Real>	wheelRRAngularVelocity;
		fe::Accessor<fe::Real>	motorTorqueRL;				// Nw-m
		fe::Accessor<fe::Real>	motorTorqueRR;
		fe::Accessor<fe::Real>	motorTorqueMaxRL;
		fe::Accessor<fe::Real>	motorTorqueMaxRR;

		fe::Accessor<fe::Real>	powerScale;					// unit governor
		fe::Accessor<fe::Real>	maxSpeed;					// set by Server.

		//Filled by Steering Component
		fe::Accessor<fe::Real>	frontSteerAngle;			// radians
		fe::Accessor<fe::Real>	curvature;					// 1/m

		// Filled by Brake Component
		fe::Accessor<fe::Real>	frontBrakesPressure;		// bar
		fe::Accessor<fe::Real>	rearBrakePressure;			// bar

		// Filled by IMU
		fe::Accessor<fe::Real>	longitudinalAcceleration;	// g
		fe::Accessor<fe::Real>	lateralAcceleration;		// g
		fe::Accessor<fe::Real>	yawVelocity;				// radians/sec
		fe::Accessor<double>	latitude;					// latitude
		fe::Accessor<double>	longitude;					// longitude
		fe::Accessor<double>	altitude;					// altitude
		fe::Accessor<double>	yaw;						// degrees
		fe::Accessor<fe::Real>	heading;					// CCW rad from N

		// Filled by OSS Sensor
		fe::Accessor<fe::Real>	longitudinalVelocityOss;	// m/s
		fe::Accessor<fe::Real>	lateralVelocityOss;		// m/s
		fe::Accessor<fe::Real>	slipAngle;				// radians

		// PID Vehicle Controller
		fe::Accessor<fe::Real>	pid_brake;
		fe::Accessor<fe::Real>	pid_throttle;
		fe::Accessor<fe::Real>	pid_steering;
		fe::Accessor<fe::Real>	pid_error;

		// generic feedback from ADS
		fe::Accessor<fe::SpatialVector>	adsTarget0;
		fe::Accessor<fe::SpatialVector>	adsTarget1;

		//DEBUG TEST
		fe::Accessor<fe::Real>	integratedDeltaTime;

		// Simple Collisions
		fe::Accessor<fe::Vector3d>		impulse;
		fe::Accessor<fe::Quaterniond>	angularImpulse;

		// Generic test value
		fe::Accessor<fe::SpatialVector>	probe;
};

}
#endif
