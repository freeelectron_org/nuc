/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsAdsSafety1_h__
#define __vehicle_AsAdsSafety1_h__

namespace hive
{

class FE_DL_EXPORT AsAdsSafety1:
	public fe::AccessorSet,
	public fe::Initialize<AsAdsSafety1>
{
	public:
		void initialize(void)
		{
			add(adsStatus,			FE_USE("ads:safety.adsStatus"));
			add(adsDriveMode,		FE_USE("ads:safety.adsDriveMode"));
			add(gearRequest,		FE_USE("ads:safety.gearRequest"));
			add(watchdog,			FE_USE("ads:safety.watchdog"));
			add(mappingLocalisationStatus,
									FE_USE("ads:safety.slamStatus"));
			add(perceptionStatus,	FE_USE("ads:safety.perceptionStatus"));
			add(planningStatus,		FE_USE("ads:safety.planningStatus"));
			add(stabilisationStatus,FE_USE("ads:safety.stabilizationStatus"));
			add(motorTotalMax_kW,	FE_USE("ads:safety.maxMotorPower"));
		}

		fe::Accessor<I32>	adsStatus;
		fe::Accessor<I32>	adsDriveMode;
		fe::Accessor<I32>	gearRequest;
		fe::Accessor<I32>	watchdog;
		fe::Accessor<I32>	mappingLocalisationStatus;
		fe::Accessor<I32>	perceptionStatus;
		fe::Accessor<I32>	planningStatus;
		fe::Accessor<I32>	stabilisationStatus;
		fe::Accessor<F32>	motorTotalMax_kW;
};

}
#endif
