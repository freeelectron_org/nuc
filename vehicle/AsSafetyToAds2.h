/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsSafetyToAds2_h__
#define __vehicle_AsSafetyToAds2_h__

namespace hive
{

class FE_DL_EXPORT AsSafetyToAds2:
	public fe::AccessorSet,
	public fe::Initialize<AsSafetyToAds2>
{
	public:
		void initialize(void)
		{
			add(motorTorquePositiveRL,
				FE_USE("ads:safety.motors.RL.torque.positive"));
			add(motorTorquePositiveRR,
				FE_USE("ads:safety.motors.RR.torque.positive"));
			add(motorTorqueNegativeRL,
				FE_USE("ads:safety.motors.RL.torque.negative"));
			add(motorTorqueNegativeRR,
				FE_USE("ads:safety.motors.RR.torque.negative"));
			add(batteryPowerOutput,
				FE_USE("ads:safety.battery.powerOutput"));
		}

		fe::Accessor<F32>	motorTorquePositiveRL;
		fe::Accessor<F32>	motorTorquePositiveRR;
		fe::Accessor<F32>	motorTorqueNegativeRL;
		fe::Accessor<F32>	motorTorqueNegativeRR;
		fe::Accessor<F32>	batteryPowerOutput;
};
}
#endif
