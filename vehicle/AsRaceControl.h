/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsRaceControl_h__
#define __vehicle_AsRaceControl_h__

namespace hive
{

class FE_DL_EXPORT AsRaceControl:
	public fe::AccessorSet,
	public fe::Initialize<AsRaceControl>
{
	public:
		void initialize(void)
		{
			add(controlModeRequest,	FE_USE("raceCtl:controlModeRequest"));
			add(driveModeRequest,	FE_USE("raceCtl:driverModeRequest"));
			add(safeStopRequest,	FE_USE("raceCtl:safeStopStateRequest"));
			add(raceMode,			FE_USE("raceCtl:raceMode"));
			add(overtakingStatus,	FE_USE("raceCtl:overtakingState"));
			add(vehicleMaxSpeed, 	FE_USE("raceCtl:vehicleMaxSpeed"));
			add(racePosition,		FE_USE("raceCtl:racePosition"));
			add(motorMaxPower,		FE_USE("raceCtrl:motorMaxPower"));
			add(lapsRemaining,		FE_USE("raceCtl:lapsRemaining"));
			add(watchdogCtrRx,		FE_USE("raceCtl:watchdogRx"));

			add(rxPER,				FE_USE("raceCtl:rxPER"));
			add(rxRSSI,				FE_USE("raceCtl:rxRSSI"));
			add(txPER,				FE_USE("raceCtl:txPER"));
			add(txRSSI,				FE_USE("raceCtl:txRSSI"));
			add(feedback_1,			FE_USE("raceCtl:feedback_1"));
			add(feedback_2,			FE_USE("raceCtl:feedback_2"));
			add(feedback_3,			FE_USE("raceCtl:feedback_3"));
			add(feedback_4,			FE_USE("raceCtl:feedback_4"));

			add(controlModeState,	FE_USE("raceCtl:controlModeState"));
			add(driveModeState,		FE_USE("raceCtl:driveModeState"));
			add(adsState,			FE_USE("raceCtl:adsState"));
			add(gear,				FE_USE("raceCtl:gear"));
			add(cohdaSafestopEnabledState,FE_USE("raceCtl:safeStopState"));
			add(vehicleSpeed,		FE_USE("raceCtl:vehicleSpeed"));
			add(vehicleState,		FE_USE("raceCtl:vehicleState"));
			add(stateOfCharge,		FE_USE("raceCtl:stateOfCharge"));
			add(watchdogCtrTx,		FE_USE("raceCtl:watchdogCtrTx"));
			add(currentLap,			FE_USE("raceCtl:currentLap"));
			add(lastLapTime,		FE_USE("lastLapTime"));

			add(gLong,				FE_USE("raceCtl:gLong"));
			add(gLat,				FE_USE("raceCtl:gLat"));
			add(posErrorN,			FE_USE("raceCtl:positionNorthError"));
			add(posErrorE,			FE_USE("raceCtl:positionEastError"));
			add(headingError,		FE_USE("raceCtl:headingError"));

			add(brakeTemperatureFL,	FE_USE("raceCtl:brakes.FL.temperature"));
			add(brakeTemperatureFR,	FE_USE("raceCtl:brakes.FR.temperature"));
			add(brakeTemperatureRL,	FE_USE("raceCtl:brakes.RL.temperature"));
			add(brakeTemperatureRR,	FE_USE("raceCtl:brakes.RR.temperature"));
			add(understeer,			FE_USE("raceCtl:understeer"));
		}

		// RX_1
		fe::Accessor<U8>	controlModeRequest;
		fe::Accessor<U8>	driveModeRequest;
		fe::Accessor<U8>	safeStopRequest;
		fe::Accessor<U8>	raceMode;
		fe::Accessor<U8>	overtakingStatus;
		fe::Accessor<U32>	vehicleMaxSpeed;
		fe::Accessor<U8>	racePosition;
		fe::Accessor<U32>	motorMaxPower;
		fe::Accessor<U8>	lapsRemaining;
		fe::Accessor<U8>	watchdogCtrRx;

		// RX_2
		fe::Accessor<U32>	rxPER;
		fe::Accessor<I32>	rxRSSI;
		fe::Accessor<U32>	txPER;
		fe::Accessor<I32>	txRSSI;
		fe::Accessor<U32>	feedback_1;
		fe::Accessor<U32>	feedback_2;
		fe::Accessor<U32>	feedback_3;
		fe::Accessor<U32>	feedback_4;

		// TX_1
		fe::Accessor<U8>	controlModeState;
		fe::Accessor<U8>	driveModeState;
		fe::Accessor<U8>	adsState;
		fe::Accessor<I32>	gear;
		fe::Accessor<U8>	cohdaSafestopEnabledState;
		fe::Accessor<U32>	vehicleSpeed;
		fe::Accessor<U8>	vehicleState;
		fe::Accessor<U8>	stateOfCharge;
		fe::Accessor<U8>	watchdogCtrTx;
		fe::Accessor<U8>	currentLap;
		fe::Accessor<U32>	lastLapTime;

		// TX_2
		fe::Accessor<U32>	gLong;				// +- 0.1 m/s2
		fe::Accessor<U32>	gLat;				// +- 0.1 m/s2
		fe::Accessor<U32>	posErrorN;			// +- 0.01 m
		fe::Accessor<U32>	posErrorE;			// +- 0.01 m
		fe::Accessor<U32>	headingError;		// +- 0.1 deg

		// TX_3
		fe::Accessor<U32>	brakeTemperatureFL;	// +  1.0 deg
		fe::Accessor<U32>	brakeTemperatureFR;	// +  1.0 deg
		fe::Accessor<U32>	brakeTemperatureRL;	// +  1.0 deg
		fe::Accessor<U32>	brakeTemperatureRR;	// +  1.0 deg
		fe::Accessor<U32>	understeer;			// +- 0.01 deg

};

}
#endif
