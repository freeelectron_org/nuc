/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsAdsTelemetry3_h__
#define __vehicle_AsAdsTelemetry3_h__

namespace hive
{

class FE_DL_EXPORT AsAdsTelemetry3:
	public fe::AccessorSet,
	public fe::Initialize<AsAdsTelemetry3>
{
	public:
		void initialize(void)
		{
			add(adsVersion,		FE_USE("ads:telemetry.adsVersion"));
			add(emotionTrigger,	FE_USE("ads:telemetry.emotionTriggers"));
		}

		fe::Accessor<I32>	adsVersion;
		fe::Accessor<I32>	emotionTrigger;
};

}
#endif

