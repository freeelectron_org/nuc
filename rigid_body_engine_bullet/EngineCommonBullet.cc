/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine_bullet.pmh"

namespace hive {

	EngineCommonBullet::EngineCommonBullet()
	{

	}

	EngineCommonBullet::~EngineCommonBullet()
	{
		/*
		//remove the rigidbodies from the dynamics world and delete them
		for (int i = m_dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
		{
			btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
			btRigidBody* body = btRigidBody::upcast(obj);
			if (body && body->getMotionState())
			{
				delete body->getMotionState();
			}
			m_dynamicsWorld->removeCollisionObject(obj);
			delete obj;
		}

		//delete dynamics world
		delete m_dynamicsWorld;

		//delete solver
		delete m_solver;

		//delete broadphase
		delete m_overlappingPairCache;

		//delete dispatcher
		delete m_dispatcher;

		delete m_collisionConfiguration;
		*/
	}

	void EngineCommonBullet::initialize()
	{
		/*
		///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
		m_collisionConfiguration = new btDefaultCollisionConfiguration();
		//const btCollisionAlgorithmConstructionInfo btCollInfo;
		//m_collisionConfiguration = new btEmptyAlgorithm(btCollInfo);

		///use the default collision dispatcher. For parallel processing you can use a different dispatcher (see Extras/BulletMultiThreaded)
		//btCollisionDispatcher*
		m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);

		///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
		//btBroadphaseInterface*
		m_overlappingPairCache = new btDbvtBroadphase();

		///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
		//btSequentialImpulseConstraintSolver*
		m_solver = new btSequentialImpulseConstraintSolver;

		//btDiscreteDynamicsWorld*
		m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher, m_overlappingPairCache, m_solver, m_collisionConfiguration);

		m_dynamicsWorld->setGravity(btVector3(0, 0, 0));
		*/
	}

}
