/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_bullet_JointBullet_h__
#define __rigid_body_engine_bullet_JointBullet_h__

using namespace fe;

namespace hive
{

class FE_DL_EXPORT JointBullet : virtual public JointI, public fe::CastableAs<JointBullet>
{
public:
	JointBullet();
	~JointBullet();

	/// These take the positions (and orientation, for Hinge joint) in coordinates relative to each of the rigid bodies.
	void		initBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
						const SpatialVector pivotInA, const SpatialVector pivotInB);
	void		initHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
						const SpatialVector pivotInA, const SpatialVector pivotInB,
						const SpatialVector axisInA, const SpatialVector axisInB);

	/// These take the positions (and orientation, for Hinge joint) in world coordinates.
	void		initBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB, const SpatialVector pivotPos);
	void		initHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB, const SpatialVector pivotPos, const SpatialVector axisDir);

	void		limitRotation(Real angleLo, Real angleHi);
	void		setSpring(Real timestep, Real stiffness, Real damping);

	void		setCFM(Real cfm);
	void		setStopCFM(Real cfm);
	void		setStopERP(Real erp);
	void 		setParam(int param, Real value);

	int			m_jointID;
	int			m_worldID;

	String 		m_jointType;

	sp<EngineCommonBullet>	m_spCommon;

	btPoint2PointConstraint* m_btPoint2PointConstraint;
	btHingeConstraint* m_btHingeConstraint;
};

} /* namespace hive */

#endif /* __rigid_body_engine_bullet_JointBullet_h__ */
