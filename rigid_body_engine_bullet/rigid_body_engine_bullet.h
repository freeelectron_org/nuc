/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_bullet_h__
#define __rigid_body_engine_bullet_h__

#define dDOUBLE

#include "fe/plugin.h"
#include "fe/data.h"

#include "bullet/btBulletCollisionCommon.h"
#include "bullet/btBulletDynamicsCommon.h"
#include "bullet/LinearMath/btAlignedAllocator.h"
#include "bullet/BulletDynamics/ConstraintSolver/btContactSolverInfo.h"
#include "bullet/BulletCollision/CollisionDispatch/btEmptyCollisionAlgorithm.h"

#include "rigid_body_engine/rigid_body_engine.h"

#endif // __rigid_body_engine_bullet_h__
