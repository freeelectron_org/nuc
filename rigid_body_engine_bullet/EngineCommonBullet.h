/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_bullet_EngineCommonBullet_h__
#define __rigid_body_engine_bullet_EngineCommonBullet_h__

#include "rigid_body_engine_bullet.h"

using namespace fe;

namespace hive
{

class FE_DL_EXPORT EngineCommonBullet : virtual public EngineCommonI, public Initialize<EngineCommonBullet>
{
public:
	EngineCommonBullet();
	~EngineCommonBullet();

	void		initialize();

private:
	/// Moved to ConnectedSystemBullet
	//btDefaultCollisionConfiguration* m_collisionConfiguration;
	//btCollisionAlgorithm* m_btCollisionAlgorithm;
	//btCollisionDispatcher* m_dispatcher;
	//btBroadphaseInterface* m_overlappingPairCache;
	//btSequentialImpulseConstraintSolver* m_solver;
	//btDiscreteDynamicsWorld* m_dynamicsWorld;
};

} /* namespace hive */

#endif /* __rigid_body_engine_bullet_ConnectedSystemBullet_h__ */
