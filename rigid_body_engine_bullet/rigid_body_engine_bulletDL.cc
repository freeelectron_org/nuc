/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine_bullet/rigid_body_engine_bullet.pmh"
#include "platform/dlCore.cc"

using namespace fe;

extern "C"
{

    FE_DL_EXPORT void ListDependencies(List<String *> &list)
    {
    }

    FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
    {
        Library *pLibrary = new Library();

		pLibrary->addSingleton<hive::EngineCommonBullet>("EngineCommonI.EngineCommonBullet.hive.bullet");
		pLibrary->add<hive::ConnectedSystemBullet>("ConnectedSystemI.ConnectedSystemBullet.hive.bullet");
		pLibrary->add<hive::JointBullet>("JointI.JointBullet.hive.bullet");
		pLibrary->add<hive::RigidBodyBullet>("RigidBodyI.RigidBodyBullet.hive.bullet");

        return pLibrary;
    }

    //-----------------------------------------------------------------
    // InitializeLibrary()
    //-----------------------------------------------------------------

    FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
    {
    }
}
