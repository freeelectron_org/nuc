/** @file */

#include <rigid_body_engine/rigid_body_engine.h>

using namespace fe;
using namespace hive;

int main(int argc, char** argv)
{
	UNIT_START();

	BWORD complete = FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry = spMaster->registry();

		Result result = spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));

		{
			// Test creation
			sp<ConnectedSystemI> spConnectedSystemI(spRegistry->create("ConnectedSystemI.*.*.bullet"));

			sp<RigidBodyI> spRigidBodyI(spRegistry->create("RigidBodyI.*.*.bullet"));

			sp<JointI> spJointI(spRegistry->create("JointI.*.*.bullet"));

			if (!spJointI.isValid() || !spRigidBodyI.isValid() ||
				//!spEngineCommonI.isValid() ||
				!spConnectedSystemI.isValid())			{
				feX(argv[0], "couldn't create components");
			}

			// Test functionality
			SpatialVector spatialVector1 = { 1, 1, 1 };
			SpatialVector spatialVector2 = { 0, 0, 0 };
			SpatialMatrix spatialMatrix1;
			setIdentity(spatialMatrix1);

			SpatialMatrix spatialMatrix2;
			setIdentity(spatialMatrix2);

			sp<RigidBodyI> spRB1 = spConnectedSystemI->createRigidBody(1, spatialVector1, 1);
			sp<RigidBodyI> spRB2 = spConnectedSystemI->createThinRod(9, spatialVector1, spatialMatrix1, 0.5);
			sp<JointI> spJ = spConnectedSystemI->createBallJoint(spRB1, spRB2, spatialVector1, spatialVector2);

			feLog("done\n");
		}

		complete = TRUE;
	}
	catch (Exception & e) { e.log(); }
	catch (...) { feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
