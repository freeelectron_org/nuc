/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine_bullet.pmh"

namespace hive {

void ConnectedSystemBullet::initialize()
{
	m_spEngineCommon = registry()->create("EngineCommonI.*.*.bullet");

	///collision configuration contains default setup for memory,
	///collision setup. Advanced users can create their own configuration.
	m_collisionConfiguration = new btDefaultCollisionConfiguration();
	//const btCollisionAlgorithmConstructionInfo btCollInfo;
	//m_collisionConfiguration = new btEmptyAlgorithm(btCollInfo);

	///use the default collision dispatcher.
	///For parallel processing you can use a different dispatcher
	///(see Extras/BulletMultiThreaded)
	//btCollisionDispatcher*
	m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);

	///btDbvtBroadphase is a good general purpose broadphase.
	///You can also try out btAxis3Sweep.
	//btBroadphaseInterface*
	m_overlappingPairCache = new btDbvtBroadphase();

	///the default constraint solver. For parallel processing you can
	///use a different solver (see Extras/BulletMultiThreaded)
	//btSequentialImpulseConstraintSolver*
	m_solver = new btSequentialImpulseConstraintSolver;

	//btDiscreteDynamicsWorld*
	m_dynamicsWorld = new btDiscreteDynamicsWorld(
		m_dispatcher, m_overlappingPairCache,
		m_solver, m_collisionConfiguration);
	m_btContactSolverInfo = new btContactSolverInfo();

	// Initialize world constants
	SpatialVector gravity(0, 0, 0);
	setGravity(gravity);
	setGlobalERP(0.9);
	setGlobalCFM(0.000001);
	setGlobalLinearDamping(0.0005);
	setGlobalAngularDamping(0.0005);

}

/// General case - requires a rotation as well as inertia matrix
sp<RigidBodyI> ConnectedSystemBullet::createRigidBody(
	const Real mass, const SpatialVector& position,
	const SpatialMatrix& rotation, const SpatialMatrix& inertia,
	bool bDynamic)
{
	sp<RigidBodyBullet> spBodyBullet(
		registry()->create("RigidBodyI.*.*.bullet"));
	spBodyBullet->init(mass, inertia, position, rotation, this, bDynamic);
	spBodyBullet->setLinearDamping(m_linearDamping);
	spBodyBullet->setAngularDamping(m_angularDamping);

	sp<RigidBodyI> spBody = spBodyBullet;
	m_rigidBodyList.push_back(spBody);
	return spBody;
}

/// Special case for spherical objects
sp<RigidBodyI> ConnectedSystemBullet::createRigidBody(
	const Real mass, const SpatialVector& position,
	const Real radius, bool bDynamic)
{
	sp<RigidBodyBullet> spBodyBullet(
		registry()->create("RigidBodyI.*.*.bullet"));
	SpatialMatrix rotation;
	setIdentity(rotation);
	spBodyBullet->init(mass, radius, position, rotation, this, bDynamic);
	spBodyBullet->setLinearDamping(m_linearDamping);
	spBodyBullet->setAngularDamping(m_angularDamping);

	sp<RigidBodyI> spBody = spBodyBullet;
	m_rigidBodyList.push_back(spBody);
	return spBody;
}

sp<JointI> ConnectedSystemBullet::createBallJoint(
	const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector& pivotInA, const SpatialVector& pivotInB)
{
	sp<JointBullet> spJointBullet(registry()->create("JointI.*.*.bullet"));
	spJointBullet->initBallJoint(bodyA, bodyB, pivotInA, pivotInB);

	m_dynamicsWorld->addConstraint(spJointBullet->m_btPoint2PointConstraint,
		true);

	sp<JointI> spJoint = spJointBullet;
	m_jointList.push_back(spJoint);
	return spJoint;
}

sp<JointI> ConnectedSystemBullet::createHingeJoint(
	const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector& pivotInA, const SpatialVector& pivotInB,
	const SpatialVector& axisInA, const SpatialVector& axisInB)
{
	sp<JointBullet> spJointBullet(registry()->create("JointI.*.*.bullet"));
	spJointBullet->initHingeJoint(bodyA, bodyB, pivotInA, pivotInB,
												axisInA, axisInB);

	m_dynamicsWorld->addConstraint(spJointBullet->m_btHingeConstraint, true);

	sp<JointI> spJoint = spJointBullet;
	m_jointList.push_back(spJoint);
	return spJoint;
}

sp<JointI> ConnectedSystemBullet::createBallJoint(
	const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector& pivotPos)
{
	sp<JointBullet> spJointBullet(registry()->create("JointI.*.*.bullet"));
	spJointBullet->initBallJoint(bodyA, bodyB, pivotPos);

	m_dynamicsWorld->addConstraint(spJointBullet->m_btPoint2PointConstraint,
																		true);

	sp<JointI> spJoint = spJointBullet;
	m_jointList.push_back(spJoint);
	return spJoint;
}

sp<JointI> ConnectedSystemBullet::createHingeJoint(
	const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector& pivotPos, const SpatialVector& axisDir)
{
	sp<JointBullet> spJointBullet(registry()->create("JointI.*.*.bullet"));
	spJointBullet->initHingeJoint(bodyA, bodyB, pivotPos, axisDir);

	m_dynamicsWorld->addConstraint(spJointBullet->m_btHingeConstraint, true);

	sp<JointI> spJoint = spJointBullet;
	m_jointList.push_back(spJoint);
	return spJoint;
}

sp<SpringDamperI> ConnectedSystemBullet::createSpringDamper(
	const sp<RigidBodyI> bodyA, const SpatialVector pointA,
	const sp<RigidBodyI> bodyB, const SpatialVector pointB)
{

	sp<SpringDamperI> spSpringDamper(
		registry()->create("SpringDamperI.SpringDamperTest.hive"));
	spSpringDamper->setUp(bodyA, pointA, bodyB, pointB);

	m_springDamperList.push_back(spSpringDamper);
	return spSpringDamper;
}

sp<SpringDamperI> ConnectedSystemBullet::createSpringDamper(
	const sp<RigidBodyI> bodyA, const SpatialVector pointA,
	const sp<RigidBodyI> bodyB, const SpatialVector pointB,
	const Real a_springRate, const Real a_dampingConstant,
	const Real a_restLength)
{
	sp<SpringDamperTest> spSpringDamperTest(
		registry()->create("SpringDamperI.SpringDamperTest.hive"));
	spSpringDamperTest->setUp(
		bodyA, pointA, bodyB, pointB,
		a_springRate, a_dampingConstant, a_restLength);

	sp<SpringDamperI> spSpringDamper = spSpringDamperTest;
	m_springDamperList.push_back(spSpringDamper);
	return spSpringDamper;
}

void ConnectedSystemBullet::step(const fe::Real dt){
	for (uint32_t i = 0; i < m_springDamperList.size(); i++)
	{
		m_springDamperList[i]->step();
	}

	btScalar timeStep(dt);
	this->m_dynamicsWorld->stepSimulation(timeStep);
}

void ConnectedSystemBullet::setGravity(const SpatialVector& gravity)
{
	btVector3 btGravity(gravity[0], gravity[1], gravity[2]);
	m_dynamicsWorld->setGravity(btGravity);
}

void ConnectedSystemBullet::setGlobalCFM(const Real cfm)
{
	btContactSolverInfo &solverInfo = m_dynamicsWorld->getSolverInfo();
	btScalar btCfm(cfm);
	solverInfo.m_globalCfm = btCfm;
  	for (uint32_t i = 0; i < m_jointList.size(); i++)
	{
		m_jointList[i]->setCFM(cfm);
	}
}

void ConnectedSystemBullet::setGlobalERP(const Real erp)
{
	btContactSolverInfo &solverInfo = m_dynamicsWorld->getSolverInfo();
	btScalar btErp(erp);
	solverInfo.m_erp = btErp;
  	for (uint32_t i = 0; i < m_jointList.size(); i++)
	{
		m_jointList[i]->setStopERP(erp);
	}
}

void ConnectedSystemBullet::setGlobalLinearDamping(const Real damping)
{
	m_linearDamping = damping;
	for (uint32_t i = 0; i < m_rigidBodyList.size(); i++)
	{
		m_rigidBodyList[i]->setLinearDamping(damping);
	}
}

void ConnectedSystemBullet::setGlobalAngularDamping(const Real damping)
{
	m_angularDamping = damping;
	for (uint32_t i = 0; i < m_rigidBodyList.size(); i++)
	{
		m_rigidBodyList[i]->setAngularDamping(damping);
	}
}

std::vector<sp<RigidBodyI>> ConnectedSystemBullet::getRigidBodies()
{
	return m_rigidBodyList;
}

}
