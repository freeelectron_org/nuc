import sys
import os.path
import re

forge = sys.modules["forge"]

def prerequisites():
	return ["rigid_body_engine"]

def setup(module):

	re_cpp = re.compile(r'.*\.cpp')

	for bulletLib in [ "LinearMath", "BulletCollision", "BulletDynamics" ]:
		srcListSub = []

		libDir = os.path.join("bullet", bulletLib)
		subPath = os.path.join(module.modPath, libDir)

		subSubDirs = [ "" ]
		if bulletLib == "BulletCollision":
			subSubDirs = [	"BroadphaseCollision",
							"CollisionDispatch",
							"CollisionShapes",
							"Gimpact",
							"NarrowPhaseCollision" ]
		elif bulletLib == "BulletDynamics":
			subSubDirs = [	"Character",
							"ConstraintSolver",
							"Dynamics",
							"Featherstone",
							"MLCPSolvers",
							"Vehicle" ]

		for subSubDir in subSubDirs:
			subSubPath = os.path.join(subPath, subSubDir)
			filenames = os.listdir(subSubPath)
			for filename in filenames:
				if re_cpp.match(filename):
					srcListSub += [ os.path.join(libDir,subSubDir, filename) ]

		dllSub = module.Lib( "hive" + bulletLib, srcListSub )

		for src in srcListSub:
			srcTarget = module.FindObjTargetForSrc(src)
			srcTarget.cppmap = { 'visibility' : "" }

	srcList = [	"rigid_body_engine_bullet.pmh",
				"EngineCommonBullet",
				"JointBullet",
				"RigidBodyBullet",
				"ConnectedSystemBullet",
				"rigid_body_engine_bulletDL" ]

	dll = module.DLL( "hiveRigidBodyEngineBulletDL", srcList )

	deplibs = forge.basiclibs + [	"fexDataToolLib",
									"feDataLib",
									"fePluginLib",
									"feMathLib",
									"hiveRigidBodyEngineDLLib",
									"hiveLinearMathLib",
									"hiveBulletCollisionLib",
									"hiveBulletDynamicsLib" ]

	forge.deps( ["hiveRigidBodyEngineBulletDLLib"], deplibs )

	module.includemap = { 'bullet' : os.path.join(module.modPath,'bullet') }

	module.Module('test')
