/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_bullet_RigidBodyBullet_h__
#define __rigid_body_engine_bullet_RigidBodyBullet_h__

namespace hive
{
class ConnectedSystemBullet;

class RigidBodyBullet :
	virtual public RigidBodyI,
	public fe::CastableAs<RigidBodyBullet>
{
public:
	RigidBodyBullet();
	~RigidBodyBullet();

	void	init(const fe::Real mass,
					const fe::Real radius,
					const fe::SpatialVector& position,
					const SpatialMatrix& rotation,
					const ConnectedSystemBullet* pConnectedSystem,
					bool bDynamic = true);
	void	init(const Real mass,
					const SpatialMatrix& inertia,
					const fe::SpatialVector& position,
					const SpatialMatrix& rotation,
					const ConnectedSystemBullet* pConnectedSystem,
					bool bDynamic = true);

	fe::Real				getMass(void) const;
	fe::SpatialVector		getPosition(void) const;
	fe::SpatialMatrix		getRotation(void) const;
	fe::SpatialTransform	getTransform(void) const;
	void					setMass(const fe::Real mass);
	void					setPosition(const fe::SpatialVector &pos);
	void					setRotation(const fe::SpatialMatrix &rotation);
	void					setTransform(const fe::SpatialTransform &transform);
	void					applyForce(const fe::SpatialVector &force,
									const fe::SpatialVector &pos);
	void					applyForceRelative(const fe::SpatialVector &force,
									const fe::SpatialVector &pos);
	void					applyTorque(const fe::SpatialVector &torque);
	void 					setVelocity(const fe::SpatialVector &vel);
	fe::SpatialVector		getVelocity(const fe::SpatialVector &pos)	const;
	void 					setAngularVelocity(const fe::SpatialVector &angVel);
	fe::SpatialVector		getAngularVelocity(void) const;
	void					translate(const fe::SpatialVector &displ);
	void					setLinearDamping(const fe::Real damping);
	void					setAngularDamping(const fe::Real damping);

//private:
	sp<EngineCommonI>		m_spCommon;
	btRigidBody*			m_btRigidBody;
};

} /* namespace hive */

#endif /* __rigid_body_engine_bullet_RigidBodyBullet_h__ */
