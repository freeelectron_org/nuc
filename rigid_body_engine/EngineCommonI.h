/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_EngineCommonI_h__
#define __rigid_body_engine_EngineCommonI_h__

namespace hive
{

class FE_DL_EXPORT EngineCommonI : virtual public fe::Component, public fe::CastableAs<EngineCommonI>
{
public:
	virtual void					initialize()			= 0;
	//virtual sp<ConnectedSystemI>	createConnectedSystem()	= 0;
};

} /* namespace hive */

#endif /* __rigid_body_engine_EngineCommonI_h__ */
