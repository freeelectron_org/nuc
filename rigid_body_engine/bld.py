import sys
import os.path

forge = sys.modules["forge"]

def prerequisites():
	return [ "datatool" ]

def setup(module):

	srcList = [	"rigid_body_engine.pmh",
				"ConnectedSystemBase.cc",
				"SpringDamperBase.cc",
				"SpringDamperTest.cc",
				"rigid_body_engineDL.cc"]

	dll = module.DLL( "hiveRigidBodyEngineDL", srcList )

	deplibs = forge.basiclibs + [ "fePluginLib",
								  "feMathLib"]

	forge.deps( ["hiveRigidBodyEngineDLLib"], deplibs )

	module.Module('test')
