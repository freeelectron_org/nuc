/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_SpringDamperI_h__
#define __rigid_body_engine_SpringDamperI_h__

namespace hive
{

/*
	TODO make this doxygen style
	SpringDamperI will track two points, each relative to one of two RigidBodyI's.
	The distance between the points may change,
	Forces should be calculable from just the positions of the two tracked points
*/

class FE_DL_EXPORT SpringDamperI :
	virtual public fe::Component,
	public fe::CastableAs<SpringDamperI>
{
public:
	// Implemented in SpringDamperBase ///////////////////////////
	// Points should be given in world coordinates
	virtual void setUp(sp<RigidBodyI> bodyA, SpatialVector pointA,
			sp<RigidBodyI> bodyB, SpatialVector pointB)			= 0;
	virtual SpatialVector getPointA() 							= 0;
	virtual SpatialVector getPointB() 							= 0;
	virtual Real distance() 									= 0;

	// Pointing in the direction of A
	virtual SpatialVector unitAxisA() 							= 0;
	// Pointing in the direction of B
	virtual SpatialVector unitAxisB() 							= 0;
	////////////////////////////////////////////////////////////////


	// To be implemented by child class, ///////////////////////////
	// inheriting from SpringDamperBase  ///////////////////////////
	virtual void step() = 0;
	////////////////////////////////////////////////////////////////
};

} /* namespace hive */

#endif /* __rigid_body_engine_SpringDamperI_h__ */
