/** @file */

#include "rigid_body_engine/rigid_body_engine.h"
#include "viewer/viewer.h"
#include "window/WindowEvent.h"
#include "vehicle_dynamics/vehicle_dynamics.h"
#include <vector>

using namespace fe;
using namespace fe::ext;
using namespace hive;

// Light wrapper to contain
//	-shape
//	-dimensions
//	-joint positions in body space
// so that the viewer can draw the bodies
class DrawableBody
{
public:
	DrawableBody() {}
	DrawableBody(sp<RigidBodyI> body, String shapeName, Real dim1, Real dim2 = -1, Real dim3 = -1)
	{
		m_spBody = body;
		m_shape = shapeName;
		m_dim1 = dim1;
		m_dim2 = dim2;
		m_dim3 = dim3;
		m_jointPositions = std::vector<SpatialVector>();
	}

	void						addJoint(SpatialVector& jointPosition)
	{
		m_jointPositions.push_back(jointPosition);
	}

	sp<RigidBodyI>				getInternalBody() { return m_spBody; }
	String 						getShape() { return m_shape; }
	Real 						getDim1() { return m_dim1; }
	Real 						getDim2() { return m_dim2; }
	Real 						getDim3() { return m_dim3; }
	std::vector<SpatialVector> 	getJointPositions() { return m_jointPositions; }

private:
	sp<RigidBodyI>				m_spBody;
	String						m_shape;
	Real 						m_dim1;
	Real 						m_dim2;
	Real 						m_dim3;
	std::vector<SpatialVector> 	m_jointPositions;
};


// Boilerplate is copied from xViewer. Additions here are:
class MyDraw : public HandlerI
{
public:
	MyDraw(sp<QuickViewerI> spQuickViewerI,
		sp<DrawI> spDrawI,
		sp<PointI> spPickPoint,
		sp<ImageI> spImageI) :
		m_spQuickViewerI(spQuickViewerI),
		m_spDrawI(spDrawI),
		m_spPickPoint(spPickPoint)
	{
		m_spSolid = new DrawMode();
		m_spSolid->setDrawStyle(DrawMode::e_solid);
		m_spSolid->setLayer(1);

		m_spEdged = new DrawMode();
		m_spEdged->setDrawStyle(DrawMode::e_edgedSolid);
		m_spEdged->setLayer(2);

		if (spImageI.isValid())		{}

		m_spWireframe = new DrawMode();
		m_spWireframe->setDrawStyle(DrawMode::e_wireframe);
		m_spWireframe->setLineWidth(1.5f);

		m_drawableBodies = std::vector<DrawableBody>();
	}

	virtual void		handle(Record& record);

	void				addBody(DrawableBody& body)
						{
							m_drawableBodies.push_back(body);
						}

	void				drawBody(DrawableBody& body);

	void				setConnectedSystem(
							sp<ConnectedSystemI> spConnectedSystemI)
						{
							m_spConnectedSystemI = spConnectedSystemI;
						}

	void				pointCameraAt(SpatialVector myCameraLocation,
							Real azimuth, Real downAngle, Real distance)
						{
							sp<CameraControllerI> spCameraControllerI =
									m_spQuickViewerI->getCameraControllerI();
							spCameraControllerI->setLookPoint(
									myCameraLocation);

							// Angles are in DEGREES
							spCameraControllerI->setLookAngle(
									azimuth, downAngle, distance);
						}

	void 				setSystemType(String type)
	{
		m_systemType = type;
	}

// These will be called in the handle loop depending on which system setup is used
void 					handleDoubleWishbone();
void 					handlePendulum();
void 					handleOscillator();
void 					handleSpringDamper();
// void 					handleBoxcar();

private:
	sp<DrawMode>		m_spEdged;
	sp<DrawMode>		m_spSolid;
	sp<DrawMode>		m_spWireframe;
	sp<QuickViewerI>	m_spQuickViewerI;
	sp<DrawI>			m_spDrawI;
	sp<PointI>			m_spPickPoint;
	sp<ViewI>			m_spViewI;
	AsViewer			m_asViewer;
	int					m_counter = 0;
	int					m_forceSign = 1;
	std::vector<DrawableBody>	m_drawableBodies;
	sp<ConnectedSystemI> m_spConnectedSystemI;
	String 				m_systemType;
};

// Create method here to create bodies you wish to render and hand them to the drawer
// To swap out, change the method in main.
void setupDoubleWishbone(sp<ConnectedSystemI> spConnectedSystemI, sp<MyDraw> spMyDraw);
void setupPendulum(sp<ConnectedSystemI> spConnectedSystemI, sp<MyDraw> spMyDraw);
void setupOscillator(sp<ConnectedSystemI> spConnectedSystemI, sp<MyDraw> spMyDraw);
void setupSpringDamper(sp<ConnectedSystemI> spConnectedSystemI, sp<MyDraw> spMyDraw);
// void setupBoxcar(sp<HandlerI> spVehicleDynamics, sp<MyDraw> spMyDraw);

static String engineType = "ode";
static String systemType = "DWB";//"Boxcar";
static float slowmoSpeed = 100.0f;

int main(int argc, char** argv)
{
	UNIT_START();

	BWORD complete = FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry = spMaster->registry();

		Result result = spRegistry->manage("fexViewerDL");
		UNIT_TEST(successful(result));

		result = spRegistry->manage("fexOpenILDL");
		if (!successful(result))
		{
			feLog("fexOpenILDL not found, trying fexSDLDL\n");
			result = spRegistry->manage("fexSDLDL");
		}

		const char* libname = "fexViewerDL";
		const char* viewername = "QuickViewerI";



		U32 frames = 0;// (argc > 2) ? atoi(argv[argc]) + 1 : 0;
		if(argc > 3)
		{
			engineType = argv[1];
			systemType = argv[2];
			slowmoSpeed = atof(argv[3]);
		}
		else if(argc>2)
		{
			engineType = argv[1];
			systemType = argv[2];
		}
		else if(argc>1)
		{
			engineType = argv[1];
		}

		feLog("libname=%s\n",libname);
		feLog("viewername=%s\n",viewername);
		feLog("frames=%d\n",frames);

		result = spRegistry->manage("feAutoLoadDL");

		result = spRegistry->manage(libname);

		result = spRegistry->manage(libname);

		UNIT_TEST(successful(result));

		{
			sp<QuickViewerI> spQuickViewerI(spRegistry->create(viewername));
			sp<PointI> spPickPoint(spRegistry->create("*.PickPoint"));
			sp<ImageI> spImageI(spRegistry->create("ImageI"));
			if (!spQuickViewerI.isValid())
				feX(argv[0], "couldn't create components");

			spQuickViewerI->open();

			sp<WindowI> spWindowI = spQuickViewerI->getWindowI();
			if (spWindowI.isValid())
			{
				sp<EventContextI> spEventContextI =
					spWindowI->getEventContextI();
				spEventContextI->setPointerMotion(TRUE);
			}

			sp<DrawI> spDrawI = spQuickViewerI->getDrawI();
			UNIT_TEST(spDrawI.isValid());

			if (spPickPoint.isValid())
			{
				spQuickViewerI->insertEventHandler(spPickPoint);
			}

			if (spImageI.isValid())
			{
				String longname = spMaster->catalog()->catalog<String>(
					"path:media") + "/image/FreeElectron64.bmp";
				spImageI->loadSelect(longname);
			}

			sp<MyDraw> spMyDraw(new MyDraw(spQuickViewerI, spDrawI,
				spPickPoint, spImageI));
			spQuickViewerI->insertEventHandler(spMyDraw);
			spQuickViewerI->insertDrawHandler(spMyDraw);

			//////////////////////////////////////////
			// Creates rigid body system and initializes engine
			sp<ConnectedSystemI> spConnectedSystemI(spRegistry->create("ConnectedSystemI.*.*." + engineType));
			spMyDraw->setConnectedSystem(spConnectedSystemI);

			if (systemType == "DWB")
			{
				setupDoubleWishbone(spConnectedSystemI, spMyDraw);
			}
			else if (systemType == "Pend")
			{
				setupPendulum(spConnectedSystemI, spMyDraw);
			}
			else if(systemType == "Osc")
			{
				setupOscillator(spConnectedSystemI, spMyDraw);
			}
			else if (systemType == "SpringDamper")
			{
				setupSpringDamper(spConnectedSystemI, spMyDraw);
			}
			// else if (systemType == "Boxcar")
			// {
			// 	sp<HandlerI> spVehicleDynamics=
			// 			spRegistry->create("*.Boxcar.hive");
			// 	feLog("Boxcar created\n");
			// 	setupBoxcar(spVehicleDynamics, spMyDraw);
			// }

			spQuickViewerI->run(frames);
		}

		complete = TRUE;
	}
	catch (Exception& e) { e.log(); }
	catch (...) { feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}


void MyDraw::handle(Record& render)
{
	if (!m_asViewer.scope().isValid())
	{
		m_asViewer.bind(render.layout()->scope());
	}

	if (m_asViewer.viewer_spatial.check(render) &&
		m_asViewer.viewer_layer.check(render) &&
		m_asViewer.viewer_layer(render))
	{
		////// From xViewer /////////////////////////////
		const Color white(1.0f, 1.0f, 1.0f, 1.0f);
		const Color yellow(1.0f, 1.0f, 0.0f, 1.0f);
		const Color pink(1.0f, 0.5f, 0.5f, 1.0f);
		const Color cyan(0.0f, 1.0f, 1.0f, 1.0f);
		const Color green(0.0f, 1.0f, 0.0f, 1.0f);
		const Color blue(0.0f, 0.0f, 1.0f, 1.0f);

		const SpatialVector origin(0.0f, 0.0f, 0.0f);
		const SpatialVector corner(10.0f, 20.0f, 0.0f);
		const SpatialVector point(0.0f, 2.0f, 3.0f);

		m_spViewI = m_spDrawI->view();
		m_spDrawI->drawAxes(1.0f);
		m_spDrawI->drawAlignedText(origin, "origin", yellow);

		m_spDrawI->setDrawMode(m_spSolid);

		SpatialVector vector[2];
		vector[0] = point;
		set(vector[1], 1.0f, 4.0f, 6.0f);
		//////////////////////////////////////////////////


		m_counter++;

		// Apply forces, move camera, display additional info depending on which
		// system we've set up the viewer to render

		//m_systemType = "Pendulum";
		//m_systemType = "DoubleWishbone";
		//m_systemType = "Oscillator";
		if (m_counter % (int)(100.0f / slowmoSpeed) == 0)
		{
		/// TODO make this switching command-line based
		if (systemType == "DWB")
		{
			handleDoubleWishbone();
		}
		else if (systemType == "Pend")
		{
			handlePendulum();
		}
		else if (systemType == "Osc")
		{
			handleOscillator();
		}
		else if (systemType == "SpringDamper")
		{
			handleSpringDamper();
		}
		// else if (systemType == "Boxcar")
		// {
		// 	//handleBoxcar();
		// }

		m_spConnectedSystemI->step(0.0166);
		}

		for (uint32_t i = 0; i < m_drawableBodies.size(); i++)
		{
			DrawableBody body = m_drawableBodies.at(i);
			drawBody(body);
		}
	}
}

void MyDraw::drawBody(DrawableBody& body)
{
	const Color cyan(0.0f, 1.0f, 1.0f, 1.0f);
	const Color green(0.0f, 1.0f, 0.0f, 1.0f);
	const Color blue(0.0f, 0.0f, 1.0f, 1.0f);

	sp<RigidBodyI> internalBody = body.getInternalBody();
	String shape = body.getShape();

	// Transform is declared in this scope because it is later used to draw
	// attached joints relative to the body's frame of reference
	SpatialTransform transform;

	if (shape == "bar")
	{
		// Calculate position of box
		// 'Box' objects are specified by corner and span
		// getPosition gives us the center, so we must convert
		SpatialVector position = internalBody->getPosition();
		Real dim1 = body.getDim1();
		Real dim2 = body.getDim2();
		Real dim3 = body.getDim3();
		SpatialVector offset(-dim1 / 2.0, -dim2 / 2.0, -dim3 / 2.0);
		SpatialVector span(dim1, dim2, dim3);
		Box3 box = Box3(offset, span);

		// Now we get the rotation and place it in a SpatialTransform
		setIdentity(transform);
		setRotation(transform, internalBody->getRotation());
		setTranslation(transform, position);
		m_spDrawI->drawTransformedBox(transform, box, green);

		// // TODO test and remove this once debugging is fixed
		// SpatialTransform transforms[1000];
		// SpatialVector scales[1000];
		// Color colors[1000];

		// for (uint32_t i = 0; i < 1000; i++)
		// {
		// 	rotate(transform, M_PI / 3, e_xAxis);
		// 	transforms[i] = transform;
		// 	scales[i] = span;
		// 	colors[i] = cyan;
		// }
		// clock1 = clock();
		// m_spDrawI->drawTransformedBoxes(transforms, scales, 1000, true, colors);
		// elapsed = clock() - clock1;
	}
	else if (shape == "thinRod")
	{
		// By convention, rods within rigid bodies are initially
		// centered at the origin and aligned along the y-axis.
		// This block of code moves the position and rotation so that it's
		// located correctly per that convention.
		setIdentity(transform);
		SpatialMatrix rotation = internalBody->getRotation();
		SpatialVector center = internalBody->getPosition();
		setTranslation(transform, center);
		setRotation(transform, rotation);

		// The drawCylinder method of DrawCommon draws cylinders with their
		// base at the origin, along the positive z-axis.
		// With the position and rotation computed above, we now offset by
		// half the length and then rotate 90 degrees about the x axis.
		Real length = body.getDim1();
		SpatialVector offset(0, length / 2, 0);
		translate(transform, offset);
		rotate(transform, M_PI / 2.0, e_xAxis);

		// Finally, we draw!
		Real radius = 0.05f;
		SpatialVector scale(radius, radius, length);
		U32 slices = 16;
		m_spDrawI->drawCylinder(transform, &scale, 1.0f, blue, slices);

	}
	else if (shape == "sphere")
	{
		// This one is easy
		setIdentity(transform);
		SpatialVector center = internalBody->getPosition();
		setTranslation(transform, center);
		Real radius = body.getDim1();
		SpatialVector scale(radius, radius, radius);
		m_spDrawI->drawSphere(transform, &scale, blue);
	}
	else
	{
		feLog("%s is not a supported rigid body shape.",
				body.getShape().c_str());
	}
}

void setupDoubleWishbone(sp<ConnectedSystemI> spConnectedSystemI, sp<MyDraw> spMyDraw)
{
	spMyDraw->setSystemType("DWB");

	// Set up double wishbone suspension //////////////////////////////////////////////////////////

	// Vertical attachment (first, since this is where we apply forces)
	SpatialVector vPosition = { 0, -1.4, 0 };
	SpatialMatrix rotation;
	setIdentity(rotation);
	rotateMatrix(rotation, M_PI / 2.0, e_xAxis);
	sp<RigidBodyI> spVertical = spConnectedSystemI->createBar(0.25, vPosition, rotation, 1.2, 1.2, 0.1);
	DrawableBody dVertical = DrawableBody(spVertical, "bar", 1.2, 1.2, 0.1);
	spMyDraw->addBody(dVertical);

	// Control arm
	// TODO make spring
	// SpatialVector controlArmPosition = { 0, -0.5, -0.3 };
	// setIdentity(rotation);
	// rotateMatrix(rotation, 0.54042, e_xAxis);
	// sp<RigidBodyI> spControlArm = spConnectedSystemI->createThinRod(0.25, controlArmPosition, rotation, 1.16619);
	// DrawableBody dControlArm = DrawableBody(spControlArm, "thinRod", 1.16619);
	// spMyDraw->addBody(dControlArm);

	// Chassis (as kinematic box)
	SpatialVector chassisPosition = { 0, 0, 0 };
	sp<RigidBodyI> spChassis = spConnectedSystemI->createRigidBody(0, chassisPosition, 0, false);
	DrawableBody dChassis = DrawableBody(spChassis, "bar", 1.2, 1.2, 1.2);
	spMyDraw->addBody(dChassis);

	// Top wishbone
	// Left
	SpatialVector tlPosition = { -0.3, -1.0, 0.6 };
	setIdentity(rotation);
	rotateMatrix(rotation, M_PI / 6.0, e_zAxis);
	sp<RigidBodyI> spTopLeftWishbone = spConnectedSystemI->createThinRod(0.1, tlPosition, rotation, 1);
	DrawableBody dTopLeftWishbone = DrawableBody(spTopLeftWishbone, "thinRod", 1);
	spMyDraw->addBody(dTopLeftWishbone);

	// Right
	SpatialVector trPosition = { 0.3, -1, 0.6 };
	setIdentity(rotation);
	rotateMatrix(rotation, -M_PI / 6.0, e_zAxis);
	sp<RigidBodyI> spTopRightWishbone = spConnectedSystemI->createThinRod(0.1, trPosition, rotation, 1);
	DrawableBody dTopRightWishbone = DrawableBody(spTopRightWishbone, "thinRod", 1);
	spMyDraw->addBody(dTopRightWishbone);

	// Bottom wishbone
	// Left
	SpatialVector blPosition = { -0.3, -1, -0.6 };
	setIdentity(rotation);
	rotateMatrix(rotation, M_PI / 6.0, e_zAxis);
	sp<RigidBodyI> spBottomLeftWishbone = spConnectedSystemI->createThinRod(0.1, blPosition, rotation, 1);
	DrawableBody dBottomLeftWishbone = DrawableBody(spBottomLeftWishbone, "thinRod", 1);
	spMyDraw->addBody(dBottomLeftWishbone);

	// Right
	SpatialVector brPosition = { 0.3, -1, -0.6 };
	setIdentity(rotation);
	rotateMatrix(rotation, -M_PI / 6.0, e_zAxis);
	sp<RigidBodyI> spBottomRightWishbone = spConnectedSystemI->createThinRod(0.1, brPosition, rotation, 1);
	DrawableBody dBottomRightWishbone = DrawableBody(spBottomRightWishbone, "thinRod", 1);
	spMyDraw->addBody(dBottomRightWishbone);

	// Joints ///////////////////////////////////////////////////
	SpatialVector jointPointTLChassis = 	{ -0.6,-0.6,   0.6 };
	SpatialVector jointPointTRChassis = 	{  0.6,-0.6,   0.6 };
	SpatialVector jointPointBLChassis = 	{ -0.6,-0.6,  -0.6 };
	SpatialVector jointPointBRChassis = 	{  0.6,-0.6,  -0.6 };

	// About x-axis
	SpatialVector xAxis(1,0,0);
	sp<JointI> spTopLeftJoint = spConnectedSystemI->createHingeJoint(
		spChassis, spTopLeftWishbone, jointPointTLChassis, xAxis);

	sp<JointI> spTopRightJoint = spConnectedSystemI->createHingeJoint(
		spChassis, spTopRightWishbone, jointPointTRChassis, xAxis);

	sp<JointI> spBottomLeftJoint = spConnectedSystemI->createHingeJoint(
		spChassis, spBottomLeftWishbone, jointPointBLChassis, xAxis);

	sp<JointI> spBottomRightJoint = spConnectedSystemI->createHingeJoint(
		spChassis, spBottomRightWishbone, jointPointBRChassis, xAxis);

	SpatialVector jointPointCenterTop(0, -1.4, 0.6);
	sp<JointI> spTopCenterJoint1 = spConnectedSystemI->createBallJoint(
		spTopLeftWishbone, spTopRightWishbone, jointPointCenterTop);

	sp<JointI> spTopCenterJoint2 = spConnectedSystemI->createBallJoint(
		spTopLeftWishbone, spVertical, jointPointCenterTop);

	sp<JointI> spTopCenterJoint3 = spConnectedSystemI->createBallJoint(
		spTopRightWishbone, spVertical, jointPointCenterTop);

	SpatialVector jointPointCenterBottom(0, -1.4, -0.6);
	sp<JointI> spBottomCenterJoint1 = spConnectedSystemI->createBallJoint(
		spBottomLeftWishbone, spBottomRightWishbone, jointPointCenterBottom);

	sp<JointI> spBottomCenterJoint2 = spConnectedSystemI->createBallJoint(
		spBottomLeftWishbone, spVertical, jointPointCenterBottom);

	sp<JointI> spBottomCenterJoint3 = spConnectedSystemI->createBallJoint(
		spBottomRightWishbone, spVertical, jointPointCenterBottom);

	// Set up camera location
	SpatialVector lookPoint(0, -1, 0);
	spMyDraw->pointCameraAt(lookPoint, -55, 30, 5);
}

// TODO make this more realistic
void MyDraw::handleDoubleWishbone()
{
	// Apply gravity
	for (uint32_t i = 0; i < m_drawableBodies.size(); i++)
	{
		sp<RigidBodyI> internalBody = m_drawableBodies[i].getInternalBody();
		float gravity = -9.8 * internalBody->getMass();
		internalBody->applyForce({ 0, 0, gravity }, internalBody->getPosition());
	}

	// Magnitude of upward force varies sinusoidally with period about 2.5 seconds (assuming 60 fps)
	float forceMagnitude = sin(2 * M_PI * m_counter / 150);
	if (m_counter % 150 == 0)
	{
		m_forceSign *= -1;
	}
	SpatialVector force(0, 0, forceMagnitude);
	sp<RigidBodyI> spVertical = m_drawableBodies[0].getInternalBody();
	//spVertical->applyForce({ 0, 0, forceMagnitude }, spVertical->getPosition());
}

void setupPendulum(sp<ConnectedSystemI> spConnectedSystemI, sp<MyDraw> spMyDraw)
{
	spMyDraw->setSystemType("Pendulum");

	// Chain of two cylindrical bodies supended from an anchor
	SpatialVector anchorPosition(0, 0, 0);
	sp<RigidBodyI> spAnchor = spConnectedSystemI->createRigidBody(0, anchorPosition, 0, false);

	SpatialMatrix rotation;
	setIdentity(rotation);

	SpatialVector bodyPos1(0, -0.5, 0);
	sp<RigidBodyI> spRB1 = spConnectedSystemI->createThinRod(0.5, bodyPos1, rotation, 1);

	SpatialVector bodyPos2(0, -1.5, 0);
	sp<RigidBodyI> spRB2 = spConnectedSystemI->createThinRod(0.5, bodyPos2, rotation, 1);

	SpatialVector bodyPos3(0, -2.5, 0);
	sp<RigidBodyI> spRB3 = spConnectedSystemI->createThinRod(0.5, bodyPos3, rotation, 1);

	SpatialVector bodyPos4(0, -3.5, 0);
	sp<RigidBodyI> spRB4 = spConnectedSystemI->createThinRod(0.5, bodyPos4, rotation, 1);

	SpatialVector bodyPos5(0, -4.5, 0);
	sp<RigidBodyI> spRB5 = spConnectedSystemI->createThinRod(0.5, bodyPos5, rotation, 1);

	SpatialVector bodyPos6(0, -5.5, 0);
	sp<RigidBodyI> spRB6 = spConnectedSystemI->createThinRod(0.5, bodyPos6, rotation, 1);

	SpatialVector bodyPos7(0, -6.5, 0);
	sp<RigidBodyI> spRB7 = spConnectedSystemI->createThinRod(0.5, bodyPos7, rotation, 1);

	SpatialVector bodyPos8(0, -7.5, 0);
	sp<RigidBodyI> spRB8 = spConnectedSystemI->createThinRod(0.5, bodyPos8, rotation, 1);

	SpatialVector bodyPos9(0, -8.5, 0);
	sp<RigidBodyI> spRB9 = spConnectedSystemI->createBall(10, bodyPos9, 1.0);

	sp<JointI> spJ01 = spConnectedSystemI->createBallJoint(spAnchor, spRB1, anchorPosition);
	sp<JointI> spJ12 = spConnectedSystemI->createBallJoint(spRB1, spRB2, { 0, -1, 0 });
	sp<JointI> spJ23 = spConnectedSystemI->createBallJoint(spRB2, spRB3, { 0, -2, 0 });
	sp<JointI> spJ34 = spConnectedSystemI->createBallJoint(spRB3, spRB4, { 0, -3, 0 });
	sp<JointI> spJ45 = spConnectedSystemI->createBallJoint(spRB4, spRB5, { 0, -4, 0 });
	sp<JointI> spJ56 = spConnectedSystemI->createBallJoint(spRB5, spRB6, { 0, -5, 0 });
	sp<JointI> spJ67 = spConnectedSystemI->createBallJoint(spRB6, spRB7, { 0, -6, 0 });
	sp<JointI> spJ78 = spConnectedSystemI->createBallJoint(spRB7, spRB8, { 0, -7, 0 });
	sp<JointI> spJ89 = spConnectedSystemI->createBallJoint(spRB8, spRB9, { 0, -8, 0 });

	DrawableBody dRB1 = DrawableBody(spRB1, "thinRod", 1);
	spMyDraw->addBody(dRB1);
	DrawableBody dRB2 = DrawableBody(spRB2, "thinRod", 1);
	spMyDraw->addBody(dRB2);
	DrawableBody dRB3 = DrawableBody(spRB3, "thinRod", 1);
	spMyDraw->addBody(dRB3);
	DrawableBody dRB4 = DrawableBody(spRB4, "thinRod", 1);
	spMyDraw->addBody(dRB4);
	DrawableBody dRB5 = DrawableBody(spRB5, "thinRod", 1);
	spMyDraw->addBody(dRB5);
	DrawableBody dRB6 = DrawableBody(spRB6, "thinRod", 1);
	spMyDraw->addBody(dRB6);
	DrawableBody dRB7 = DrawableBody(spRB7, "thinRod", 1);
	spMyDraw->addBody(dRB7);
	DrawableBody dRB8 = DrawableBody(spRB8, "thinRod", 1);
	spMyDraw->addBody(dRB8);
	DrawableBody dRB9 = DrawableBody(spRB9, "sphere", 1);
	spMyDraw->addBody(dRB9);

	// 30 degree offset start position
	// 		SpatialVector bodyPos1(0, -0.4, -0.3);
	// sp<RigidBodyI> spRB1 = spConnectedSystemI->createThinRod(0.5, bodyPos1, rotation, 1);
	// DrawableBody dRB1 = DrawableBody(spRB1, "thinRod", 1);
	// spMyDraw->addBody(dRB1);

	// SpatialVector bodyPos2(0, -1.2, -0.9);
	// sp<RigidBodyI> spRB2 = spConnectedSystemI->createThinRod(0.5, bodyPos2, rotation, 1);
	// DrawableBody dRB2 = DrawableBody(spRB2, "thinRod", 1);
	// spMyDraw->addBody(dRB2);

	// SpatialVector bodyPos3(0, -2.0, -1.5);
	// sp<RigidBodyI> spRB3 = spConnectedSystemI->createThinRod(0.5, bodyPos3, rotation, 1);
	// DrawableBody dRB3 = DrawableBody(spRB3, "thinRod", 1);
	// spMyDraw->addBody(dRB3);

	// SpatialVector bodyPos4(0, -2.8, -2.1);
	// sp<RigidBodyI> spRB4 = spConnectedSystemI->createThinRod(0.5, bodyPos4, rotation, 1);
	// DrawableBody dRB4 = DrawableBody(spRB4, "thinRod", 1);
	// spMyDraw->addBody(dRB4);

	// SpatialVector bodyPos5(0, -3.6, -2.7);
	// sp<RigidBodyI> spRB5 = spConnectedSystemI->createThinRod(0.5, bodyPos5, rotation, 1);
	// DrawableBody dRB5 = DrawableBody(spRB5, "thinRod", 1);
	// spMyDraw->addBody(dRB5);

	// SpatialVector bodyPos6(0, -4.4, -3.3);
	// sp<RigidBodyI> spRB6 = spConnectedSystemI->createThinRod(0.5, bodyPos6, rotation, 1);
	// DrawableBody dRB6 = DrawableBody(spRB6, "thinRod", 1);
	// spMyDraw->addBody(dRB6);

	// SpatialVector bodyPos7(0, -5.2, -3.9);
	// sp<RigidBodyI> spRB7 = spConnectedSystemI->createThinRod(0.5, bodyPos7, rotation, 1);
	// DrawableBody dRB7 = DrawableBody(spRB7, "thinRod", 1);
	// spMyDraw->addBody(dRB7);

	// SpatialVector bodyPos8(0, -6.0, -4.5);
	// sp<RigidBodyI> spRB8 = spConnectedSystemI->createThinRod(0.5, bodyPos8, rotation, 1);
	// DrawableBody dRB8 = DrawableBody(spRB8, "thinRod", 1);
	// spMyDraw->addBody(dRB8);

	// SpatialVector bodyPos9(0, -6.8, -5.1);
	// sp<RigidBodyI> spRB9 = spConnectedSystemI->createBall(8, bodyPos9, 1.0);
	// DrawableBody dRB9 = DrawableBody(spRB9, "sphere", 1);
	// spMyDraw->addBody(dRB9);

	// sp<JointI> spJ01 = spConnectedSystemI->createBallJoint(spAnchor, spRB1, anchorPosition);
	// sp<JointI> spJ12 = spConnectedSystemI->createBallJoint(spRB1, spRB2, {0, -0.8, -0.6});
	// sp<JointI> spJ23 = spConnectedSystemI->createBallJoint(spRB2, spRB3, {0, -1.6, -1.2});
	// sp<JointI> spJ34 = spConnectedSystemI->createBallJoint(spRB3, spRB4, {0, -2.4, -1.8});
	// sp<JointI> spJ45 = spConnectedSystemI->createBallJoint(spRB4, spRB5, {0, -3.2, -2.4});
	// sp<JointI> spJ56 = spConnectedSystemI->createBallJoint(spRB5, spRB6, {0, -4.0, -3.0});
	// sp<JointI> spJ67 = spConnectedSystemI->createBallJoint(spRB6, spRB7, {0, -4.8, -3.6});
	// sp<JointI> spJ78 = spConnectedSystemI->createBallJoint(spRB7, spRB8, {0, -5.6, -4.2});
	// sp<JointI> spJ89 = spConnectedSystemI->createBallJoint(spRB8, spRB9, {0, -6.4, -4.8});

	// Set up camera location
	SpatialVector lookPoint(0, 0, -5);
	spMyDraw->pointCameraAt(lookPoint, 0, 0, 20);
}

void MyDraw::handlePendulum()
{
	// Apply gravity
	for (uint32_t i = 0; i < m_drawableBodies.size(); i++)
	{
		sp<RigidBodyI> internalBody = m_drawableBodies[i].getInternalBody();
		//if (m_counter > 200)
		{
			Real mass = internalBody->getMass();
			Real weight = 9.8 * mass;
			SpatialVector gravity = { 0, 0, -weight };
			internalBody->applyForce(gravity, internalBody->getPosition());
		}
	}
}

SpatialMatrix makeRotation(Real radians, Axis axis)
{
	SpatialMatrix rotation;
	setIdentity(rotation);
	rotateMatrix(rotation, radians, axis);
	return rotation;
}

void setupOscillator(sp<ConnectedSystemI> spConnectedSystemI, sp<MyDraw> spMyDraw)
{
	spMyDraw->setSystemType("Oscillator");

	// Copy of xRigidBodyEngine
	SpatialVector bodyPos1 = { 0, 0, 0 };
	SpatialVector bodyPos2 = { 2, 0, 0 };
	SpatialVector joint1Pos = { 2, 0, 0 };/// absolute position of joint in world coordinates

	SpatialMatrix spatialMatrix1;
	setIdentity(spatialMatrix1);

	SpatialMatrix spatialMatrix2;
	setIdentity(spatialMatrix2);;

	/// Hardcoded pair of spheres and a ball joint
	sp<RigidBodyI> spGround = spConnectedSystemI->createRigidBody(0, bodyPos1, 0, false);
	sp<RigidBodyI> spRB1 = spConnectedSystemI->createBall(8, bodyPos1, 2);
	sp<JointI> spJ0 = spConnectedSystemI->createBallJoint(spGround, spRB1, bodyPos1);

	sp<RigidBodyI> spRB2 = spConnectedSystemI->createBall(1, bodyPos2, 1);
	sp<JointI> spJ1 = spConnectedSystemI->createBallJoint(spRB1, spRB2, joint1Pos);

	DrawableBody dRB1 = DrawableBody(spRB1, "sphere", 2);
	spMyDraw->addBody(dRB1);
	DrawableBody dRB2 = DrawableBody(spRB2, "sphere", 1);
	spMyDraw->addBody(dRB2);

	/// Rotate sphere2
	/// e_zAxis is some global define of sorts
	SpatialMatrix rotate = makeRotation(0.1, e_zAxis);
	spRB2->setRotation(rotate);

	// Set up camera location
	SpatialVector lookPoint(0, 0, 0);
	spMyDraw->pointCameraAt(lookPoint, -90, 0, 20);
}

void MyDraw::handleOscillator()
{
	SpatialVector pos2 = m_drawableBodies[1].getInternalBody()->getPosition();

	m_drawableBodies[1].getInternalBody()->applyForce({ 0,0,-10 }, pos2);
}

void setupSpringDamper(sp<ConnectedSystemI> spConnectedSystemI, sp<MyDraw> spMyDraw)
{
	spMyDraw->setSystemType("SpringDamper");

	SpatialVector bodyPos1 = { 0, 0, 0 };
	SpatialVector bodyPos2 = { 2, 0, 0 };

	SpatialMatrix spatialMatrix;
	setIdentity(spatialMatrix);

	// sp<RigidBodyI> spGround = spConnectedSystemI->createRigidBody(0, bodyPos1, 0, false);
	sp<RigidBodyI> spRB1 = spConnectedSystemI->createBall(2, bodyPos1, 0.25);
	sp<RigidBodyI> spRB2 = spConnectedSystemI->createBall(2, bodyPos2, 0.25);

	DrawableBody dRB1 = DrawableBody(spRB1, "sphere", 0.25);
	spMyDraw->addBody(dRB1);
	DrawableBody dRB2 = DrawableBody(spRB2, "sphere", 0.25);
	spMyDraw->addBody(dRB2);

	sp<SpringDamperI> spSD = spConnectedSystemI->createSpringDamper(spRB1, bodyPos1, spRB2, bodyPos2, 10, 0.5, 1);

	// Set up camera location
	SpatialVector lookPoint(0, 0, 0);
	spMyDraw->pointCameraAt(lookPoint, -90, 0, 5);
}

// Optional, since forces are calculated internally
// External forces are applied to demonstrate axis tracking for the spring/damper component
void MyDraw::handleSpringDamper()
{
	if (m_counter % 150 == 0)
	{
		m_forceSign *= -1;
	}

	// Magnitude of vertical force varies sinusoidally with period about 2.5 seconds (assuming 60 fps)
	float sinusoidalPart = m_forceSign * sin(2 * M_PI * m_counter / 150);

	for (uint32_t i = 0; i < m_drawableBodies.size(); i++)
	{
		sp<RigidBodyI> spBody = m_drawableBodies[i].getInternalBody();
		float forceMagnitude = spBody->getMass() * sinusoidalPart;
		SpatialVector force(0, 0, forceMagnitude);
		spBody->applyForce(force, spBody->getPosition());
	}
}

// void setupBoxcar(sp<HandlerI> spVehicleDynamics, sp<MyDraw> spMyDraw)
// {
// 	sp<Boxcar> spBoxcar = spVehicleDynamics;
// 	sp<RigidBodyI> spTheBox = spBoxcar->getRigidBody();

// 	DrawableBody dTheBox = DrawableBody(spTheBox, "bar", 5, 2, 1.5);
// 	spMyDraw->addBody(dTheBox);
// }

// void MyDraw::handleBoxcar()
// {
	// if (m_counter % 150 == 0)
	// {
	// 	m_forceSign *= -1;
	// }

	// // Magnitude of vertical force varies sinusoidally with period about 2.5 seconds (assuming 60 fps)
	// float sinusoidalPart = m_forceSign * sin(2 * M_PI * m_counter / 150);

	// for (uint32_t i = 0; i < m_drawableBodies.size(); i++)
	// {
	// 	sp<RigidBodyI> spBody = m_drawableBodies[i].getInternalBody();
	// 	float forceMagnitude = spBody->getMass() * sinusoidalPart;
	// 	SpatialVector force(0, 0, forceMagnitude);
	// 	spBody->applyForce(force, spBody->getPosition());
	// }
// }
