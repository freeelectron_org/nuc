import sys
forge = sys.modules["forge"]

def setup(module):
	deplibs = forge.corelibs + ["hiveRigidBodyEngineDLLib",
								"fexDataToolDLLib" ]

	tests = [ 'xRigidBodyEngine' ]

	for t in tests:
		exe = module.Exe(t)
		forge.deps([t + "Exe"], deplibs)

	deplibs += forge.corelibs + [	"fexDrawDLLib",
									'fexViewerDLLib',
									'fexWindowDLLib' ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [	"fexThreadDLLib" ]

	if 'viewer' in forge.modules_confirmed:
		viewer_tests = [ 'xDoubleWishbone' ]

		for t in viewer_tests:
			exe = module.Exe(t)
			forge.deps([t + "Exe"], deplibs)
