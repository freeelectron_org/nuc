/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_ConnectedSystemBase_h__
#define __rigid_body_engine_ConnectedSystemBase_h__

namespace hive
{

	class FE_DL_EXPORT ConnectedSystemBase :
	virtual public ConnectedSystemI,
	public fe::CastableAs<ConnectedSystemBase>
	{
	public:
		virtual sp<RigidBodyI>	createBall(const Real mass, const SpatialVector& position,
										const Real radius, bool bDynamic = true);
		virtual sp<RigidBodyI>	createThinRod(const Real mass, const SpatialVector& position,
										const SpatialMatrix& rotation, const Real length,
										bool bDynamic = true);
		virtual sp<RigidBodyI>	createCylinder(const Real mass, const SpatialVector& position,
										const SpatialMatrix& rotation, const Real length,
										const Real radius, bool bDynamic = true);
		virtual sp<RigidBodyI>	createTube(const Real mass, const SpatialVector& position,
										const SpatialMatrix& rotation,
										const Real length, const Real innerRadius,
										const Real outerRadius, bool bDynamic = true);
		virtual sp<RigidBodyI>	createBar(const Real mass, const SpatialVector& position,
										const SpatialMatrix& rotation, const Real widthX,
										const Real depthY, const Real heightZ,
										bool bDynamic = true);

		virtual sp<RigidBodyI>	createRigidBody(const Real mass, const SpatialVector& position,
										const Real radius, bool bDynamic = true) 						= 0;
		virtual sp<RigidBodyI>	createRigidBody(const Real mass, const SpatialVector& position,
										const SpatialMatrix& rotation, const SpatialMatrix& inertia,
										bool bDynamic = true)											= 0;
	};

} /* namespace hive */

#endif /* __rigid_body_engine_ConnectedSystemBase_h__ */
