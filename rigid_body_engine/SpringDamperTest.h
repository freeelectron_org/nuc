/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_SpringDamperTest_h__
#define __rigid_body_engine_SpringDamperTest_h__

namespace hive
{

class FE_DL_EXPORT SpringDamperTest : virtual public SpringDamperBase, public fe::CastableAs<SpringDamperTest>
{
public:
	SpringDamperTest();
	~SpringDamperTest();

	using SpringDamperBase::setUp;

	void setUp(const sp<RigidBodyI> bodyA, const SpatialVector pointA,
			const sp<RigidBodyI> bodyB, const SpatialVector pointB,
			const Real springK, const Real dampingC,
			const Real restLength);

	// Points should be given in world coordinates
	void step();

// Protected methods and variables from SpringDamperBase
// protected:
	// SpatialVector getPointA();
	// SpatialVector getPointB();

	// sp<RigidBodyI>	m_bodyA;
	// SpatialVector	m_pointAInBodyCoordinates;
	// sp<RigidBodyI>	m_bodyB;
	// SpatialVector	m_pointBInBodyCoordinates;

private:
	Real 			m_kSpring = 10;
	Real 			m_cDamping = 0.1;
	Real 			m_restLength = 1.5;
};

} /* namespace hive */

#endif /* __rigid_body_engine_SpringDamperTest_h__ */
