/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine.pmh"

namespace hive {

SpringDamperTest::SpringDamperTest()
{
}

SpringDamperTest::~SpringDamperTest()
{
}

void SpringDamperTest::setUp(const sp<RigidBodyI> bodyA, const SpatialVector pointA,
		const sp<RigidBodyI> bodyB, const SpatialVector pointB,
		const Real springK, const Real dampingC,
		const Real restLength)
{
	m_bodyA = bodyA;
	m_bodyB = bodyB;
	m_kSpring = springK;
	m_cDamping = dampingC;
	m_restLength = restLength;

	// Convert world coordinates to body coordinates for bodyA, pointA
	SpatialVector position = bodyA->getPosition();
	SpatialMatrix inverseRotation;
	invert(inverseRotation, bodyA->getRotation());
	SpatialVector offset = pointA - position;
	// TODO test
	multiply(m_pointAInBodyCoordinates, inverseRotation, offset);

	// Do the same for bodyB, pointB
	position = bodyB->getPosition();
	invert(inverseRotation, bodyB->getRotation());
	offset = pointB - position;
	multiply(m_pointBInBodyCoordinates, inverseRotation, offset);
}


void SpringDamperTest::step()
{
	Real dist = distance();
	Real compression = m_restLength - dist;

	// Unit vector along axis pointing toward each body
	SpatialVector forceVectorA = unitAxisA(); // pointing toward A
	SpatialVector forceVectorB = unitAxisB(); // pointing toward B

	// force = (unit axis) * (compression * K + velocity in direction of axis * C)
	// dot(unit axis, velocity) is the projection of velocity in the direction of the axis
	Real dampingForceA = -m_cDamping * dot(forceVectorA, m_bodyA->getVelocity(getPointA()));
	Real dampingForceB = -m_cDamping * dot(forceVectorB, m_bodyB->getVelocity(getPointB()));
	Real springForce = m_kSpring * compression;

	Real magnitudeA = springForce + dampingForceA;
	Real magnitudeB = springForce + dampingForceB;

	forceVectorA = forceVectorA * magnitudeA;
	forceVectorB = forceVectorB * magnitudeB;

	m_bodyA->applyForce(forceVectorA, getPointA());
	m_bodyB->applyForce(forceVectorB, getPointB());
}

}
