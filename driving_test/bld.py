import os
import sys
forge = sys.modules["forge"]

def prerequisites():
	return [ "signal", "lua", "apiary" ]

def setup(module):
	srcList = [	"driving_test.pmh",
				"DrivingTestLua",
				"SimpleLap",
				"drivingTestDL"]

	dll = module.DLL( "hiveDrivingTestDL", srcList )

	deplibs = forge.corelibs+ [
				"fexSignalLib",
				"hiveApiaryDLLib" ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [	"fexThreadDLLib" ]

	forge.deps( ["hiveDrivingTestDLLib"], deplibs )

	forge.tests += [("inspect.exe",	"hiveDrivingTestDL",	None,	None) ]

#	module.Module('test')
