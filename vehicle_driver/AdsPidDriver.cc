#include "vehicle_driver/vehicle_driver.pmh"
//#include <bits/stdc++.h>
#include <algorithm>

using namespace fe;
using namespace fe::ext;

namespace hive
{

///
/// Constructor
///
AdsPidDriver::AdsPidDriver(void):
	m_adsStatus(0),
	m_torqueNmFL(0.0),
	m_torqueNmFR(0.0),
	m_torqueNmRL(0.0),
	m_torqueNmRR(0.0),
	m_brakeBar(0.0),
	m_steeringAngle(0.0)
{
	m_pidController.e_integral = 0.0;
	m_pidController.e_derivative = 0.0;
	m_pidController.e_t = 0.0;
	m_pidController.e_t_1 = 0.0;
	m_pidController.e_integralBrakes = 0.0;

	m_pidController.kp = 0.2;
	m_pidController.kd = 0.2;
	m_pidController.ki = 0.05;
	m_pidController.kp_br = 0.2;
	m_pidController.kd_br = 0.2;
	m_pidController.ki_br = 0.05;
};

///
///
///
AdsPidDriver::~AdsPidDriver(void)
{
}

///
/// Convert CAN input data to throttle, brake, steer
///
void AdsPidDriver::handle(Record& a_signal)
{
	/*
	feLog("AdsDriver::handle layout \"%s\"\n",
			a_signal.layout()->name().c_str());
	*/
	sp<Scope> spScope=a_signal.layout()->scope();
	if(m_spScope!=spScope)
	{
		m_asAdsSafety1.bind(spScope);
		m_asAdsSafety2.bind(spScope);
		m_asAdsSafety3.bind(spScope);
		m_asVehicle.bind(spScope);
		m_asAdsSafetyState.bind(spScope);
		m_spScope=spScope;
	}

	// Gather inputs
	if (CheckAdsModes(a_signal))	{ return; }
	if (CheckSafety1(a_signal))		{ return; }
	if (CheckSafety2(a_signal))		{ return; }
	if (CheckSafety3(a_signal))		{ return; }

	// Compute Throttle, Brake, Steer
	if(m_asVehicle.check(a_signal))
	{
		switch(m_safetyGear)
		{
			case AdsVehicleSafetyState::e_neutral:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeNeutral;
				break;
			case AdsVehicleSafetyState::e_drive:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeDrive;
				break;
			case AdsVehicleSafetyState::e_reverse:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeReverse;
				break;
			case AdsVehicleSafetyState::e_park:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModePark;
				break;
			default:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeUnknown;
				break;
		}

		m_asVehicle.gearIndexInput(a_signal)=1;	//* TODO manual gearing

		if(m_controlMode==AdsVehicleSafetyState::e_prohibited)
		{
			//* ADS prohibited (don't affect controls)
			return;
		}

		//* 0=simple 1=advanced
		if(m_adsDriveMode)
		{
			//* Advanced Mode

			//* NOTE back-compute pseudo-inputs

			//* torqueNm: -300 to 300 Nm
//			m_asVehicle.throttleInput(a_signal)=(1.0/300.0)*
//					0.25*(m_torqueNmFL+m_torqueNmFR+m_torqueNmRL+m_torqueNmRR);
			fe::Real max_torque=0.5*(m_asVehicle.motorTorqueMaxRL(a_signal)+
									m_asVehicle.motorTorqueMaxRR(a_signal));
			m_asVehicle.throttleInput(a_signal)=(1.0/max_torque)*
					0.5*(m_torqueNmRL+m_torqueNmRR);


			//* brakesBar: 0 to 60 bar
			//m_asVehicle.brakeInput(a_signal)=(1.0/60.0)*m_brakeBar;
			AdvancedBrakes(a_signal);

			//* steeringAngle: -20 to 20 degrees
			m_asVehicle.steeringFInput(a_signal)=(1.0/20.0)*m_steeringAngle;
			/*
			feLog("AdsDriver::handle AsVehicle\n");
			feLog("  throttle %.6G\n",m_asVehicle.throttleInput(a_signal));
			feLog("  brake    %.6G\n",m_asVehicle.brakeInput(a_signal));
			feLog("  steering %.6G\n",m_asVehicle.steeringFInput(a_signal));
			*/
		}
		else
		{
			// Throttle and Brakes.
			ThrottleBrake_Pid(a_signal);
			//OldThrottleBrake(a_signal);

			//* curvatureRequest: listed -1 to +1, actually +-0.13 (RC)
			Real steering = (1.0/0.13) * m_curvatureRequest;
			steering = fe::maximum(Real(-1),fe::minimum(Real(1),steering));
			m_asVehicle.steeringFInput(a_signal) = steering;

			/*
			feLog("gLong: %.6G\tThr: %.6G\tBr: %.6G\tGear: %d\tKph:  %.6G\n"
				,m_gLongRequest,
				m_asVehicle.throttleInput(a_signal),
				m_asVehicle.brakeInput(a_signal),
				m_safetyGear,
				m_asVehicle.speed(a_signal) * 3.6f);
			*/
		}
		return;
	}
}


///
/// AdsPidDriver::CheckAdsModes()
///
bool AdsPidDriver::CheckAdsModes(fe::Record& a_signal)
{
	if(m_asAdsSafetyState.check(a_signal))
	{
		m_controlMode = AdsVehicleSafetyState::ControlMode(
				m_asAdsSafetyState.controlMode(a_signal));

		m_safetyGear = m_asAdsSafetyState.gear(a_signal);

		return true;
	}
	else
	{
		return false;
	}

}

///
/// AdsPidDriver::CheckSafety1()
///
bool AdsPidDriver::CheckSafety1(fe::Record& a_signal)
{
	if(m_asAdsSafety1.check(a_signal))
	{
		//* 0=not ready 1=ready 2=active 3=error
		m_adsStatus = m_asAdsSafety1.adsStatus(a_signal);
		m_adsDriveMode = m_asAdsSafety1.adsDriveMode(a_signal);

		return true;
	}
	else
	{
		return false;
	}
}

///
/// AdsPidDriver::CheckSafety2()
///
bool AdsPidDriver::CheckSafety2(fe::Record& a_signal)
{
	if(m_asAdsSafety2.check(a_signal))
	{
		m_curvatureRequest = -m_asAdsSafety2.curvatureRequest(a_signal);
		// Apply LPF
		//m_curvatureRequest = smoothCurvatureStep(m_curvatureRequest);
		//m_curvatureRequest=
		//		m_curv_n1 + 0.4 *(m_curvatureRequest - m_curv_n1);

		m_gLongRequest = m_asAdsSafety2.gLongRequest(a_signal);
		m_torqueNmFL = m_asAdsSafety2.motorFLRequestNm(a_signal);
		m_torqueNmFR = m_asAdsSafety2.motorFRRequestNm(a_signal);
		return true;
	}
	else
	{
		return false;
	}


}

///
/// AdsPidDriver::CheckSafety3()
///
bool AdsPidDriver::CheckSafety3(fe::Record& a_signal)
{
	if(m_asAdsSafety3.check(a_signal))
	{
		m_torqueNmRL = m_asAdsSafety3.motorRLRequestNm(a_signal);
		m_torqueNmRR = m_asAdsSafety3.motorRRRequestNm(a_signal);

		m_brakeBar = 0.5*
				(m_asAdsSafety3.brakeFRequestBar(a_signal)+
				m_asAdsSafety3.brakeRRequestBar(a_signal));

		m_steeringAngle = m_asAdsSafety3.steerFMeanRequestDeg(a_signal);

		/*
		feLog("  motorRLRequestNm %.6G\n",m_torqueNmRL);
		feLog("  motorRRRequestNm %.6G\n",m_torqueNmRR);
		feLog("  brakeBar %.6G\n",m_brakeBar);
		feLog("  steerFMeanRequestDeg %.6G\n",m_steeringAngle);
		*/
		return true;
	}
	else
	{
		return false;
	}

}

//-----------------------------------------------------------------------------
void AdsPidDriver::ThrottleBrake_Pid(Record& a_signal)
{

	if(m_safetyGear != AdsVehicleSafetyState::e_drive)
		return;

	// error(t) = gLongReq(t) - gLong(t)
	// th = kp*e(t) + ki*integral{e(t)} + kd*derivative{e(t)}

	// at some point throttle woud try to go negative,
	// we engage the brakes then.

	//thr[n] = thr[n-1] + a(x[n] - thr[n-1])
	//

	m_pidController.gLongIn = m_gLongRequest;
	m_pidController.gLongOut = m_asVehicle.longitudinalAcceleration(a_signal)/9.81;



	if(m_gLongRequest >= 0)
	{
		/* The PID Controller is broken. Need to investigate.
		// Proportional
		m_pidController.e_t = m_pidController.gLongIn - m_pidController.gLongOut;
		float_t proportional = m_pidController.kp*m_pidController.e_t;

		// Integral
		m_pidController.e_integral+=
			(m_pidController.e_t * m_asVehicle.deltaTime(a_signal)) * 0.1;

		// Derivative
		m_pidController.e_derivative=(m_pidController.e_t-m_pidController.e_t_1)/
															m_asVehicle.deltaTime(a_signal);

		m_pidController.throttle =
				proportional +
				m_pidController.kd * m_pidController.e_derivative;
				m_pidController.ki * m_pidController.e_integral;
		*/
		// TODO: this fudge numbers need to be explained. There is evidence
		// that the numbers yield correct results by way of data analysis
		// and plots: see wiki issue:
		// id=issue:linked:task1144

		float thr_n1 = m_pidController.throttle;
		m_pidController.throttle = (1.0/0.5)*m_gLongRequest;
		// Apply Exponential Decay smoothing
		//thr[n] = thr[n-1] + a(x[n] - thr[n-1])
		m_pidController.throttle=
						thr_n1 + 0.4 *(m_pidController.throttle - thr_n1);

		// Cap throttle to 1.0
		if(m_pidController.throttle > 1.0)
		{
			m_pidController.throttle=1.0;
		}
		if(m_pidController.throttle < 0)
		{
			m_pidController.throttle = 0;
			m_pidController.e_integral = 0;
		}

		// We are accelerating => no breaks.
		m_pidController.brake = 0;
		m_pidController.e_integralBrakes = 0.0;
	}
	else
	{
		/* TODO: PID is not functional at this moment.
		// Negative Acceleration Request: Apply Brakes
		m_pidController.e_t = (m_pidController.gLongOut-m_pidController.gLongIn);

		// Proportional
		float_t proportional = m_pidController.kp_br*m_pidController.e_t;

		// Integral
		m_pidController.e_integralBrakes+=
			(m_pidController.e_t * m_asVehicle.deltaTime(a_signal)) * 0.1;

		// Derivative
		m_pidController.e_derivativeBrakes=(m_pidController.e_t-m_pidController.e_t_1)/
															m_asVehicle.deltaTime(a_signal);


		m_pidController.brake =
						proportional +
						m_pidController.ki_br*m_pidController.e_integralBrakes +
						m_pidController.kd_br*m_pidController.e_derivativeBrakes;
		*/
		float brk_n1 = m_pidController.brake;
		m_pidController.brake = (-1.0/1.22)*m_gLongRequest;
		// Apply Exponential Decay smoothing
		//thr[n] = thr[n-1] + a(x[n] - thr[n-1])
		m_pidController.brake=
						brk_n1 + 0.4 *(m_pidController.brake - brk_n1);

		// Cap brakes at 1.0
		if(m_pidController.brake > 1)
		{
			m_pidController.brake = 1.0;
		}
		if(m_pidController.brake < 0)
		{
			m_pidController.brake = 0.0;
		}
		// We are braking => no throttle
		m_pidController.throttle = 0;
		m_pidController.e_integral = 0;
	}

	// Update Vehicle Record.
	m_asVehicle.throttleInput(a_signal) = m_pidController.throttle;
	m_asVehicle.brakeInput(a_signal) = m_pidController.brake;

	m_asVehicle.pid_throttle(a_signal) = m_pidController.throttle;
	m_asVehicle.pid_brake(a_signal) = m_pidController.brake;
	m_asVehicle.pid_error(a_signal) = m_pidController.e_t;
	m_asVehicle.pid_steering(a_signal) = m_gLongRequest;
	m_pidController.e_t_1 = m_pidController.e_t;
}

//-----------------------------------------------------------------------------
void AdsPidDriver::OldThrottleBrake(Record& a_signal)
{
	//* Simple Mode
	//* Throttle and Break get calculated from gLongRequest: -5 to +5
	//F32 brakeNormalized;
	if(m_safetyGear == AdsVehicleSafetyState::e_drive)
	{
		if(m_gLongRequest >= 0.0)
		{
			m_asVehicle.brakeInput(a_signal) = 0;

			m_asVehicle.throttleInput(a_signal) = (1.0/0.8)*m_gLongRequest;
		}
		else
		{
			m_asVehicle.throttleInput(a_signal) = 0;

			F32 brakeTorque = -1.0*m_gLongRequest/1.1;/* * 1160.0 //m_asVehicle.vehicleMass(a_signal)
											* 0.508/2.0;//m_asVehicle.wheelRadius(a_signal);
			brakeNormalized = brakeTorque / 250.0;
			m_asVehicle.brakeInput(a_signal) =
				std::max((F32)0.0, std::min(brakeNormalized, (F32)1.0));*/
			m_asVehicle.brakeInput(a_signal) = brakeTorque;
		}
	}
	else if(m_safetyGear == AdsVehicleSafetyState::e_neutral)
	{

	}

}

//-----------------------------------------------------------------------------
void AdsPidDriver::AdvancedBrakes(Record& a_signal)
{

	//* brakesBar: 0 to 60 bar
	m_asVehicle.brakeInput(a_signal)=(1.0/60.0)*m_brakeBar/4.2f;
/*
	m_pidController.e_t = (m_pidController.gLongOut-m_pidController.gLongIn);

	// Proportional
	float_t proportional = m_pidController.kp_br*m_pidController.e_t;

	// Integral
	m_pidController.e_integralBrakes+=
		(m_pidController.e_t * m_asVehicle.deltaTime(a_signal)) * 0.1;

	// Derivative
	m_pidController.e_derivativeBrakes=(m_pidController.e_t-m_pidController.e_t_1)/
														m_asVehicle.deltaTime(a_signal);

	float brk_n1 = m_pidController.brake;
	m_pidController.brake =
					proportional +
					m_pidController.ki_br*m_pidController.e_integralBrakes +
					m_pidController.kd_br*m_pidController.e_derivativeBrakes;

	// Apply Exponential Decay smoothing
	//thr[n] = thr[n-1] + a(x[n] - thr[n-1])
	m_pidController.brake = (-1.0/1.2)*m_gLongRequest;// test
	m_pidController.brake=
					brk_n1 + 0.4 *(m_pidController.brake - brk_n1);

	if(m_pidController.brake > 1) {m_pidController.brake = 1.0;}
	if(m_pidController.brake < 0) {m_pidController.brake = 0.0;}

	m_asVehicle.throttleInput(a_signal) = m_pidController.throttle;
	m_asVehicle.brakeInput(a_signal) = m_pidController.brake;

	m_asVehicle.pid_throttle(a_signal) = m_pidController.throttle;
	m_asVehicle.pid_brake(a_signal) = m_pidController.brake;
	m_asVehicle.pid_error(a_signal) = m_pidController.e_t;
	m_asVehicle.pid_steering(a_signal) = m_gLongRequest;

	m_pidController.e_t_1 = m_pidController.e_t;
*/

}


}
