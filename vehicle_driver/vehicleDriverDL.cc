/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <vehicle_driver/vehicle_driver.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace hive;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSignalDL"));
}

FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = new Library();

	pLibrary->add<AdsDriver>("HandlerI.AdsDriver.hive");

	pLibrary->add<AdsPidDriver>("HandlerI.AdsPidDriver.hive");

	pLibrary->add<AdsStub>("HandlerI.AdsStub.hive");

	pLibrary->add<HumanDriver>("HandlerI.HumanDriver.hive");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}

