#ifndef hive_human_vehicle_driver_h
#define hive_human_vehicle_driver_h

namespace hive
{

class FE_DL_EXPORT HumanDriver:
	virtual public fe::ext::HandlerI
{
public:
				HumanDriver();
virtual			~HumanDriver(){}

virtual	void	handle(fe::Record& a_signal);

virtual fe::sp<fe::Catalog>&	settings(void)	{ return m_spSettings; }

private:
		void	handleEvent(fe::Record& a_signal);

	fe::ext::WindowEvent			m_event;
	fe::Real						m_incrementingThrottle;
	fe::Real						m_incrementingBrake;
	fe::Real						m_incrementingSteering;
	fe::Real						m_rampingThrottle;
	fe::Real						m_rampingBrake;
	fe::Real						m_rampingSteering;
	fe::Real						m_centeringThrottle;
	fe::Real						m_centeringBrake;
	fe::Real						m_centeringSteering;
	fe::Real						m_weightThrottle;
	fe::Real						m_weightBrake;
	fe::Real						m_weightSteering;
	fe::Real						m_throttle;
	fe::Real						m_brake;
	fe::Real						m_clutch;
	fe::Real						m_steering;
	BWORD							m_horn;
	BWORD							m_manualShifting;
	AsVehicle::GearMode				m_gearMode;
	I32								m_gearIndex;

	AsAdsSafetyState				m_asAdsSafetyState;
	AsVehicle						m_asVehicle;

	AdsVehicleSafetyState::ControlMode	m_controlMode;

	fe::sp<fe::Catalog>				m_spSettings;
	bool							m_blendHumanInputWithAds;
};

}
#endif
