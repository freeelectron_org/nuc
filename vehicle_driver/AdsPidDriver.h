#ifndef hive_vehicle_driver_vehicle_driver_h
#define hive_vehicle_driver_vehicle_driver_h

namespace hive
{

struct PidData
{
	// Throttle
	float_t	gLongIn;
	float_t	gLongOut;
	float_t	e_t;
	float_t	e_t_1;
	float_t e_integral;
	float_t e_derivative;
	float_t	kp;
	float_t	kd;
	float_t	ki;
	float_t throttle;

	// Brakes
	float_t	kp_br;
	float_t	kd_br;
	float_t	ki_br;
	float_t e_integralBrakes;
	float_t e_derivativeBrakes;
	float_t brake;
};

/// Converts CAN input to throttle, brakes, steer output.
class FE_DL_EXPORT AdsPidDriver:
	virtual public fe::ext::HandlerI
{
public:
					AdsPidDriver(void);
	virtual			~AdsPidDriver(void);

	virtual	void	handle(fe::Record& a_signal);

private:
			bool	CheckAdsModes(fe::Record& a_signal);
			bool	CheckSafety1(fe::Record& a_signal);
			bool	CheckSafety2(fe::Record& a_signal);
			bool	CheckSafety3(fe::Record& a_signal);
			void	ThrottleBrake_Pid(fe::Record& a_signal);
			void	OldThrottleBrake(fe::Record& a_signal);
			void	AdvancedBrakes(fe::Record& a_signal);

	fe::sp<fe::Scope>	m_spScope;
	AsAdsSafetyState	m_asAdsSafetyState;
	AsAdsSafety1		m_asAdsSafety1;
	AsAdsSafety2		m_asAdsSafety2;
	AsAdsSafety3		m_asAdsSafety3;
	AsVehicle			m_asVehicle;

	AdsVehicleSafetyState::ControlMode	m_controlMode;

	I32					m_adsStatus;
	I32					m_adsDriveMode;
	I32					m_safetyGear;

	fe::Real			m_curvatureRequest;
	fe::Real			m_curv_n1;
	fe::Real			m_gLongRequest;
	fe::Real			m_torqueNmFL;
	fe::Real			m_torqueNmFR;
	fe::Real			m_torqueNmRL;
	fe::Real			m_torqueNmRR;
	fe::Real			m_brakeBar;
	fe::Real			m_steeringAngle;
	fe::sp<fe::Catalog>	m_spSettings;

	PidData				m_pidController;
};

}
#endif
