#include "vehicle_driver/vehicle_driver.pmh"

using namespace fe;
using namespace fe::ext;

namespace hive
{

HumanDriver::HumanDriver():
	m_incrementingThrottle(0.0),
	m_incrementingBrake(0.0),
	m_incrementingSteering(0.0),
	m_rampingThrottle(0.0),
	m_rampingBrake(0.0),
	m_rampingSteering(0.0),
	m_centeringThrottle(0.0),
	m_centeringBrake(0.0),
	m_centeringSteering(0.0),
	m_weightThrottle(0.0),
	m_weightBrake(0.0),
	m_weightSteering(0.0),
	m_throttle(0.0),
	m_brake(0.0),
	m_clutch(0.0),
	m_steering(0.0),
	m_horn(FALSE),
	m_manualShifting(FALSE),
	m_gearMode(AsVehicle::e_gearModePark),
	m_gearIndex(-2),
	m_controlMode(AdsVehicleSafetyState::e_prohibited),
	m_blendHumanInputWithAds(false)
{
};

void HumanDriver::handle(Record& a_signal)
{
//	feLog("HumanDriver::handle layout \"%s\"\n",
//			a_signal.layout()->name().c_str());

	m_event.bind(a_signal.layout()->scope());
	if(m_event.check(a_signal))
	{
		handleEvent(a_signal);
		return;
	}

	if(m_asAdsSafetyState.bind(a_signal))
	{
		m_controlMode=AdsVehicleSafetyState::ControlMode(
				m_asAdsSafetyState.controlMode(a_signal));
		return;
	}

	if(!m_asVehicle.bind(a_signal))
	{
		return;
	}

	const Real deltaTime=m_asVehicle.deltaTime(a_signal);

	const Real incrementingThrottleMax=2.0;
	const Real incrementingBrakeMax=2.0;
	const Real incrementingSteeringMax=1.0;

	if(m_centeringSteering>0.0)
	{
		if(fabs(m_steering)<1e-3)
		{
			m_steering=0.0;
			m_incrementingSteering=0.0;
		}
		else if(m_steering>0.0)
		{
			m_incrementingSteering=fe::maximum(
					m_incrementingSteering-m_centeringSteering*deltaTime,
					-incrementingSteeringMax);
			m_steering=fe::clamp(m_steering+m_incrementingSteering*deltaTime,
					Real(0),Real(1));
		}
		else
		{
			m_incrementingSteering=fe::minimum(
					m_incrementingSteering+m_centeringSteering*deltaTime,
					incrementingSteeringMax);
			m_steering=fe::clamp(m_steering+m_incrementingSteering*deltaTime,
					Real(-1),Real(0));
		}
	}
	else
	{
		m_incrementingSteering=fe::clamp(
				m_incrementingSteering+m_rampingSteering*deltaTime,
				-incrementingSteeringMax,incrementingSteeringMax);
		m_steering=fe::clamp(m_steering+m_incrementingSteering*deltaTime,
				Real(-1),Real(1));
	}

	if(m_centeringThrottle>0.0)
	{
		if(fabs(m_throttle)<1e-3)
		{
			m_throttle=0.0;
			m_incrementingThrottle=0.0;
		}
		else if(m_throttle>0.0)
		{
			m_incrementingThrottle=fe::maximum(
					m_incrementingThrottle-m_centeringThrottle*deltaTime,
					-incrementingThrottleMax);
			m_throttle=fe::clamp(m_throttle+m_incrementingThrottle*deltaTime,
					Real(0),Real(1));
		}
		else
		{
			m_incrementingThrottle=fe::minimum(
					m_incrementingThrottle+m_centeringThrottle*deltaTime,
					incrementingThrottleMax);
			m_throttle=fe::clamp(m_throttle+m_incrementingThrottle*deltaTime,
					Real(-1),Real(0));
		}
	}
	else
	{
		m_incrementingThrottle=fe::clamp(
				m_incrementingThrottle+m_rampingThrottle*deltaTime,
				-incrementingThrottleMax,incrementingThrottleMax);

		m_throttle=fe::clamp(m_throttle+m_incrementingThrottle*deltaTime,
				Real(-1),Real(1));
	}

	if(m_centeringBrake>0.0)
	{
		if(m_brake<1e-3)
		{
			m_brake=0.0;
			m_incrementingBrake=0.0;
		}
		else
		{
			m_incrementingBrake=fe::maximum(
					m_incrementingBrake-m_centeringBrake*deltaTime,
					-incrementingBrakeMax);
			m_brake=fe::clamp(m_brake+m_incrementingBrake*deltaTime,
					Real(0),Real(1));
		}
	}
	else
	{
		m_incrementingBrake=fe::clamp(
				m_incrementingBrake+m_rampingBrake*deltaTime,
				-incrementingBrakeMax,incrementingBrakeMax);

		m_brake=fe::clamp(m_brake+m_incrementingBrake*deltaTime,
				Real(0),Real(1));
	}

//	feLog("throttle %.3f incr %.3f ramp %.3f center %.3f\n",
//			m_throttle,m_incrementingThrottle,m_rampingThrottle,
//			m_centeringThrottle);
//	feLog("brake %.3f incr %.3f ramp %.3f center %.3f\n",
//			m_brake,m_incrementingBrake,m_rampingBrake,
//			m_centeringBrake);
//	feLog("steer %.3f incr %.3f ramp %.3f center %.3f\n",
//			m_steering,m_incrementingSteering,m_rampingSteering,
//			m_centeringSteering);

	if(m_controlMode==AdsVehicleSafetyState::e_allowed)
	{
		//* ADS on
		//* NOTE human override can force controls

		const Real inc=deltaTime;
		const Real threshold=0.01;

		const BWORD detectThrottle=(fabs(m_throttle)>threshold);
		const BWORD detectBrake=(fabs(m_brake)>threshold);
		const BWORD detectSteering=(fabs(m_steering)>threshold);

		const Real incThrottle=(detectThrottle)? inc: -inc;
		m_weightThrottle=fe::maximum(Real(0),fe::minimum(Real(1),
				m_weightThrottle+incThrottle));

		const Real incBrake=(detectBrake)? inc: -inc;
		m_weightBrake=fe::maximum(Real(0),fe::minimum(Real(1),
				m_weightBrake+incBrake));

		const Real incSteering=detectSteering? inc: -inc;
		m_weightSteering=fe::maximum(Real(0),fe::minimum(Real(1),
				m_weightSteering+incSteering));

		m_asVehicle.throttleInput(a_signal)=
				m_weightThrottle*m_throttle+
				(1.0-m_weightThrottle)*m_asVehicle.throttleInput(a_signal);
		m_asVehicle.brakeInput(a_signal)=
				m_weightBrake*m_brake+
				(1.0-m_weightBrake)*m_asVehicle.brakeInput(a_signal);
		m_asVehicle.steeringFInput(a_signal)=
				m_weightSteering*m_steering+
				(1.0-m_weightSteering)*m_asVehicle.steeringFInput(a_signal);

		m_asVehicle.clutchInput(a_signal)=m_clutch;

		//* NOTE ADS may need to set gear values, so don't clobber those here
		//* copy the values in case it switches back to human mode
		m_gearMode=AsVehicle::GearMode(m_asVehicle.gearModeInput(a_signal));
		m_gearIndex=m_asVehicle.gearIndexInput(a_signal);
	}
	else
	{
		m_weightThrottle=0.0;
		m_weightBrake=0.0;
		m_weightSteering=0.0;

		m_asVehicle.throttleInput(a_signal)=m_throttle;
		m_asVehicle.brakeInput(a_signal)=m_brake;
		m_asVehicle.clutchInput(a_signal)=m_clutch;
		m_asVehicle.steeringFInput(a_signal)=m_steering;

		m_asVehicle.gearModeInput(a_signal)=m_gearMode;
		m_asVehicle.gearIndexInput(a_signal)=m_gearIndex;
	}

	m_asVehicle.hornInput(a_signal)=m_horn;

#if FALSE
	feLog("HumanDriver::handle AsVehicle\n");
	feLog("  steering  %.3f\n",m_asVehicle.steeringFInput(a_signal));
	feLog("  throttle  %.3f\n",m_asVehicle.throttleInput(a_signal));
	feLog("  brake     %.3f\n",m_asVehicle.brakeInput(a_signal));
	feLog("  clutch    %.3f\n",m_asVehicle.clutchInput(a_signal));
	feLog("  gearMode  %d\n",m_asVehicle.gearModeInput(a_signal));
	feLog("  gearIndex %d\n",m_asVehicle.gearIndexInput(a_signal));
	feLog("  manual    %d\n",m_manualShifting);
#endif
}

void HumanDriver::handleEvent(Record& record)
{
	m_event.bind(record);

	if(m_controlMode==AdsVehicleSafetyState::e_allowed 	&&
		!m_blendHumanInputWithAds 						&&
		m_event.isKeyboard())
	{
		//* NOTE Not able to override ADS input on Simulator.
		//*      until the Window Handler for Keyboard is fixed
		//*      regarding the Window Context
		return;
	}

//	feLog("HumanDriver::handleEvent %s\n",c_print(m_event));

	if(m_event.isPoll())
	{
		return;
	}

	if(m_event.isKeyPress(WindowEvent::Item('[')))
	{
		m_gearIndex--;
	}
	if(m_event.isKeyPress(WindowEvent::Item(']')))
	{
		m_gearIndex++;
	}
	if(m_event.isKeyPress(WindowEvent::Item('=')))
	{
		m_manualShifting= !m_manualShifting;
	}

	if(m_event.isKeyboard())
	{
		const BWORD pressDown=
				(m_event.isKeyPress(WindowEvent::Item('s')) ||
				m_event.isKeyPress(WindowEvent::Item('l')) ||
				m_event.isKeyPress(WindowEvent::e_keyCursorDown));
		const BWORD pressUp=
				(m_event.isKeyPress(WindowEvent::Item('w')) ||
				m_event.isKeyPress(WindowEvent::Item('p')) ||
				m_event.isKeyPress(WindowEvent::e_keyCursorUp));
		const BWORD pressLeft=
				(m_event.isKeyPress(WindowEvent::Item('a')) ||
				m_event.isKeyPress(WindowEvent::e_keyCursorLeft));
		const BWORD pressRight=
				(m_event.isKeyPress(WindowEvent::Item('d')) ||
				m_event.isKeyPress(WindowEvent::e_keyCursorRight));

		const BWORD releaseDown=
				(m_event.isKeyRelease(WindowEvent::Item('s')) ||
				m_event.isKeyRelease(WindowEvent::Item('l')) ||
				m_event.isKeyRelease(WindowEvent::e_keyCursorDown));
		const BWORD releaseUp=
				(m_event.isKeyRelease(WindowEvent::Item('w')) ||
				m_event.isKeyRelease(WindowEvent::Item('p')) ||
				m_event.isKeyRelease(WindowEvent::e_keyCursorUp));
		const BWORD releaseLeft=
				(m_event.isKeyRelease(WindowEvent::Item('a')) ||
				m_event.isKeyRelease(WindowEvent::e_keyCursorLeft));
		const BWORD releaseRight=
				(m_event.isKeyRelease(WindowEvent::Item('d')) ||
				m_event.isKeyRelease(WindowEvent::e_keyCursorRight));

		if(m_event.state2()==WindowEvent::e_state2NoRelease)
		{
			//* NOTE console keyboard events require console focus

			m_incrementingThrottle=0.0;
			m_incrementingBrake=0.0;
			m_incrementingSteering=0.0;
			m_rampingThrottle=0.0;
			m_rampingBrake=0.0;
			m_rampingSteering=0.0;
			m_centeringSteering=0.0;
			m_centeringThrottle=0.0;
			m_centeringBrake=0.0;

			const Real incThrottle(0.2);
			const Real incBrake(0.2);

			if(pressDown)
			{
				m_throttle=0.0;
				m_brake=fe::minimum(Real(1),m_brake+incBrake);
			}

			if(pressUp)
			{
				m_throttle=fe::minimum(Real(1),m_throttle+incThrottle);
				m_brake=0.0;
			}

			const Real incSteer(0.2);

			if(pressLeft)
			{
				m_steering=fe::maximum(Real(-1),m_steering-incSteer);
			}

			if(pressRight)
			{
				m_steering=fe::minimum(Real(1),m_steering+incSteer);
			}
		}
		else
		{
			const Real rampThrottle(4);
			const Real rampBrake(4);

			if(pressUp)
			{
				m_rampingThrottle=rampThrottle;
				m_incrementingThrottle=0.0;
				m_centeringThrottle=0.0;
			}

			if(pressDown)
			{
				m_rampingBrake=rampBrake;
				m_incrementingBrake=0.0;
				m_centeringBrake=0.0;
			}

			if(releaseUp)
			{
				m_rampingThrottle=0.0;
				m_incrementingThrottle=0.0;
				m_centeringThrottle=rampThrottle;
			}

			if(releaseDown)
			{
				m_rampingBrake=0.0;
				m_incrementingBrake=0.0;
				m_centeringBrake=rampBrake;
			}

			const Real rampSteer(4);
			const Real rampCenter(2.0+2.0*fabs(m_steering));
			const Real rampReversal(4.0+4.0*fabs(m_steering));

			if(pressLeft)
			{
				if(m_steering>0.0)
				{
					m_rampingSteering= -rampReversal;
				}
				else
				{
					m_rampingSteering= -rampSteer;
				}
				m_incrementingSteering=0.0;
				m_centeringSteering=0.0;
			}

			if(pressRight)
			{
				if(m_steering<0.0)
				{
					m_rampingSteering=rampReversal;
				}
				else
				{
					m_rampingSteering=rampSteer;
				}
				m_incrementingSteering=0.0;
				m_centeringSteering=0.0;
			}

			if(releaseLeft || releaseRight)
			{
				m_rampingSteering=0.0;
				m_incrementingSteering=0.0;
				m_centeringSteering=rampCenter;
			}
		}
	}

	if(m_event.source() == WindowEvent::Source::e_sourceJoy0 ||
		m_event.source() == WindowEvent::Source::e_sourceWheel0 ||
		m_event.source() == WindowEvent::Source::e_sourceWheel1)
	{
		m_incrementingThrottle=0.0;
		m_incrementingBrake=0.0;
		m_incrementingSteering=0.0;
		m_rampingThrottle=0.0;
		m_rampingBrake=0.0;
		m_rampingSteering=0.0;
		m_centeringSteering=0.0;
		m_centeringThrottle=0.0;
		m_centeringBrake=0.0;

		if(m_event.item() == WindowEvent::Item::e_joyRightUp)
		{
			m_horn=m_event.state();
		}

		if(m_event.source() == WindowEvent::Source::e_sourceJoy0)
		{
			if(m_event.item() ==
					WindowEvent::Item::e_joyLeftStickX)
			{
				//* adjusted steering (-1 to 1)

				m_steering = m_event.state()/32768.0;

				//* non-linear
				m_steering=powf(fabs(m_steering),1.5)*
						(m_steering<0.0? -1.0: 1.0);
			}
			else if(m_event.item() ==
					WindowEvent::Item::e_joyRightStickY)
			{
				const Real value=m_event.state()/32768.0;
				if(value >= 0.0)
				{
					//* throttle (0 to 1)
					m_throttle = m_event.state()/32768.0;
					m_brake = 0.0;
				}
				else
				{
					//* brake (0 to 1)
					m_brake = -m_event.state()/32768.0;
					m_throttle = 0.0;
				}
			}
			else if(m_event.item() ==
					WindowEvent::Item::e_joyRightRight)
			{
				m_throttle = m_event.state()? 1.0: 0.0;
			}
			else if(m_event.item() ==
					WindowEvent::Item::e_joyRightDown)
			{
				m_brake = m_event.state()? 1.0: 0.0;
			}
			else if(m_event.item() ==
					WindowEvent::Item::e_joyRightLeft)
			{
				m_clutch = m_event.state()? 1.0: 0.0;
			}
			else if(m_event.state())
			{
				if(m_event.item() ==
						WindowEvent::Item::e_joyLeftHigh)
				{
					m_gearIndex--;
				}
				else if(m_event.item() ==
						WindowEvent::Item::e_joyRightHigh)
				{
					m_gearIndex++;
				}
				else if(m_event.item() ==
						WindowEvent::Item::e_joyRightDepress)
				{
					m_manualShifting= !m_manualShifting;
				}
			}
		}
		else if(m_event.source() == WindowEvent::Source::e_sourceWheel0 ||
			m_event.source() == WindowEvent::Source::e_sourceWheel1)
		{
			if(m_event.item() ==
					WindowEvent::Item::e_joyLeftStickX)
			{
				//* direct steering (-1 to 1)
				m_steering = m_event.state()/32768.0;
			}
			else if(m_event.item() ==
					WindowEvent::Item::e_joyRightStickX)
			{
				//* throttle (0 to 1)
				m_throttle = 0.5 * (m_event.state()/32768.0 + 1.0);
			}
			else if(m_event.item() ==
					WindowEvent::Item::e_joyLeftStickY)
			{
				//* clutch (0 to 1)
				m_clutch = 0.5 * (m_event.state()/32768.0 + 1.0);
			}
			else if(m_event.item() ==
					WindowEvent::Item::e_joyRightStickY)
			{
				//* brake (0 to 1)
				m_brake = 0.5 * (m_event.state()/32768.0 + 1.0);
			}
		}
	}

	//* TODO gear count param
	const I32 maxGearIndex=m_manualShifting? 6: 1;

	m_gearIndex=fe::clamp(m_gearIndex,-2,maxGearIndex);

	switch(m_gearIndex)
	{
		case -2:
			m_gearMode=AsVehicle::e_gearModePark;
			break;
		case -1:
			m_gearMode=AsVehicle::e_gearModeReverse;
			break;
		case 0:
			m_gearMode=AsVehicle::e_gearModeNeutral;
			break;
		default:
			m_gearMode=AsVehicle::e_gearModeDrive;
			break;
	}
}

}
