/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

namespace hive
{

class FE_DL_EXPORT FileWatcherDaemonI : virtual public fe::Component,
                                        public fe::CastableAs<FileWatcherDaemonI>
{
public:
    // updateDelay: How often to check for changes.
    virtual void start(const unsigned int updateDelay=1000) = 0;

    virtual void stop() = 0;

    virtual void watchFile(const String &file, FileChangeHandlerI* handler) = 0;

    virtual void watchDirectory(const String &dir, FileChangeHandlerI *handler) = 0;
};

} // namespace hive
