/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

using namespace fe;

namespace hive
{

class FE_DL_EXPORT FileWatcherDaemon : virtual public FileWatcherDaemonI
{
public:
    FileWatcherDaemon();
    ~FileWatcherDaemon();

    void start(const unsigned int updateDelay = 1000) override;
    
    void stop() override;

    void watchFile(const String &file, FileChangeHandlerI *handler) override;

    void watchDirectory(const String &dir, FileChangeHandlerI *handler) override;

private:
    // Adaptor from SimpleFileWatcher handlers to FE FileChange handlers 
    class DirectoryListener : public FW::FileWatchListener
    {
    public:
        void addHandler(const String &filename, FileChangeHandlerI* feFileChangeHandler);
        void handleFileAction(FW::WatchID watchid, const FW::String &dir, const FW::String &filename,
                              FW::Action action);

    private:
        std::map<String, FileChangeHandlerI*> m_handlers;
    };

    // The Thread Task that keeps updating the filechange event state.
    class FileWatcherTask : public Thread::Functor
    {
    public:
        virtual void operate(void);

        FW::FileWatcher m_fileWatcher;
        Poison m_poison;
        unsigned int m_updateDelay = 0;

        void addListener(const String &directory, DirectoryListener *watcher);

    private:
        // The listeners have to be added from INSIDE the running thread for the callbacks
        // to work properly in windows. We use a buffer to pass the new listeners to the thread.
        std::map<String, DirectoryListener*> m_listenerBuffer;
    };

    std::map<String, DirectoryListener*> m_directoryListeners;
    FileWatcherTask m_fileWatcherTask;
    Thread *m_pFileWatcherThread;
};

} // namespace hive
