# Hot Reload

The HotReload module provides a Daemon for hotreloading dynamic libraries built by forge.

## Dependencies

Requires jsoncpp.

Linux installation:

```bash
sudo apt install libjsoncpp-dev
```

Windows installation:

```bash
python3 build.py vcpkg download
```

## Usage

Make sure you add the hotreload product to the product that you are building!

Now you can simply create and start the HotReload Daemon right after you create the registry.

```c++
#include "hotreload/hotreload.h"

// Create registry.
fe::sp<fe::Master> spMaster(new fe::Master);
fe::sp<fe::Registry> spRegistry = spMaster->registry();
spRegistry->manage("feAutoLoadDL");

// Create hot reload daemon.
// NOTE: Make sure that your hotreload daemon component does not get destructed 
// while your application is running! Very important!!! Watch your scopes!
fe::sp<hive::HotReloadDaemonI> spHotReloadDaemon = spRegistry->create("*.HotReloadJsonDaemon");
FEASSERT(spHotReloadDaemon.isValid());

// Start the daemon. 
spHotReloadDaemon->start(); // You can specify how often you want to daemon to update in the arguments. Default is 1000ms.
```

Now that you have your application running with the daemon started, you can make changes to your library implementation code and simply build it! Your forge output log should inform you with "hotreload mode detected" and perform a hotreload build of your changes. Once a hotreload is performed, the next time you use `Registry::create()` to create your component it will load and use the newly built library. 

Please note that the old library does not get unloaded and your old implementation can continue to run just fine. Only the newly created components will use the new library. This means you have the freedom to use hotreload in a few creative ways. You could for example set it up to to destruct the old component and create a new one in its place, or you could choose to create a second component and have them run side by side for comparison.

## Caveats

#### Interface Encapsulation

You can only hotreload components if your are referencing them using an interface. If you are directly using component implenetation symbols in your code instead of the interface symbols (like you should be), the hotreload will not work. 

```c++
// GOOD, can be hotreloaded.
fe::sp<hive::HotReloadTestComponentI> spTestComponent;
spTestComponent = spRegistry->create("*.HotReloadTestComponent");

// BAD, can't be hotreloaded. Accessing implementation symbols directly.
fe::sp<hive::HotReloadTestComponent> spTestComponent;
spTestComponent = spRegistry->create("*.HotReloadTestComponent");
```

#### Core Libraries

You cannot hotreload anything that the HotReload Daemon itself depends upon. That means you can't reload any core libraries. Forge will give you a warning if you accidentaly modify one of them and trigger a hotreload build.

#### FE_COPY_BINARIES_TO_PATH Not Supported

The `FE_COPY_BINARIES_TO_PATH` flag is not supported. When `FE_COPY_BINARIES_TO_PATH` is used you end up with multiple copies of the binaries everywhere so forge can't know which ones to check for open dlhandles to go into hotreload mode.

If you need to your binaries to be moved somewhere else you should use `FE_LIB_PATH` in combination with `FE_BINARY_SUFFIX`. This will build FE directly into the directory you want and perform hotreloading there. Using `FE_COMPACT_API_CODEGEN` for your `FE_BINARY_SUFFIX` is also recommended. 


