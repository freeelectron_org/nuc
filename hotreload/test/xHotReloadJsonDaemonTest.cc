/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

// Test hotreloading the Registry by firing up the HotReloadJsonDaemon
// and adding a hotreload manifest file.

#include "hotreload/hotreload.h"
#include "testDL/hotreloadtest.h"

using namespace fe;
using namespace hive;

int main(int argc, char **argv)
{
    UNIT_START();

    const char* manifestFilepath = "test/FEhotreload.json";

    remove(manifestFilepath);

    sp<Master> spMaster(new fe::Master);
    sp<Registry> spRegistry = spMaster->registry();
    Result result = spRegistry->manage("feAutoLoadDL");

    {
        // Create hot reload daemon.
        sp<HotReloadDaemonI> hotreloadDaemon = spRegistry->create("*.HotReloadJsonDaemon");
        FEASSERT(hotreloadDaemon.isValid());
        hotreloadDaemon->setCustomManifestFilepath(manifestFilepath);
        hotreloadDaemon->start(0);

        // Allow file watcher thread to spool up to avoid race conditions for this unit test.
        // Not neccessary in typical applications.
        milliSleep(1);

        // Create first implementation of component.
        sp<HotReloadTestComponentI> testComponent1 = spRegistry->create("*.HotReloadTestComponent");
        FEASSERT(testComponent1.isValid());
        String string1 = testComponent1->getText().c_str();
        feLog("%s\n", string1.c_str());
        UNIT_TEST(string1 == "OUTPUT FROM DL NUMBER 1");

        // Write hotreload manifest to file.
        const std::string hotreloadManifest =
R"({
    "Modules": {
        "HotReloadTestDL": "HotReloadTestDL-2"
    }
})";
        std::ofstream file;
        file.open(manifestFilepath);
        file << hotreloadManifest;
        file.close();

        // The destruction of the daemon forces it to join the file watcher thread which ensures that it updates.
    }

    // Create second implementation of component. It should now be hotreloaded.
    sp<HotReloadTestComponentI> testComponent2 = spRegistry->create("*.HotReloadTestComponent");
    FEASSERT(testComponent2.isValid());
    String string2 = testComponent2->getText().c_str();
    feLog("%s\n", string2.c_str());
    UNIT_TEST(string2 == "OUTPUT FROM DL NUMBER 2");

    remove(manifestFilepath);

    UNIT_TRACK(2);
    UNIT_RETURN();
}
