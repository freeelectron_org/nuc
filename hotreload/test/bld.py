import sys
forge = sys.modules["forge"]

import os.path

def setup(module):

	# Test DLs
	testDL1SrcList = [
		"testDL/HotReloadTestComponent1",
		"testDL/hotreloadtest1.pmh",
		"testDL/HotReloadTestDL"
	]
	testDL2SrcList = [
		"testDL/HotReloadTestComponent2",
		"testDL/hotreloadtest2.pmh",
		"testDL/HotReloadTestDL-2"
	]
	module.DLL("HotReloadTestDL", testDL1SrcList)
	module.DLL("HotReloadTestDL-2", testDL2SrcList)
	module.DLL("HotReloadTestDL-3", testDL1SrcList)
	forge.deps([
		"HotReloadTestDLLib",
		"HotReloadTestDL-2Lib",
		"HotReloadTestDL-3Lib"
	], forge.corelibs)

	# Unit tests
	module.Exe("xHotReloadRegistryTest")
	module.Exe("xHotReloadJsonDaemonTest")
	module.Exe("xHotReloadJsonDaemonColdReloadTest")
	module.Exe("xHotReloadDemo")
	forge.deps([
		"xHotReloadRegistryTestExe",
		"xHotReloadJsonDaemonTestExe",
		"xHotReloadJsonDaemonColdReloadTestExe",
		"xHotReloadDemoExe"
	], forge.corelibs)

	# Autorun unit tests
	forge.tests += [
		("inspect.exe",	"HotReloadTestDL", None, None),
		("inspect.exe",	"HotReloadTestDL-2", None, None),
		("inspect.exe",	"HotReloadTestDL-3", None, None),
		("xHotReloadRegistryTest.exe", "", None, None),
		("xHotReloadJsonDaemonTest.exe", "", None, None),
		("xHotReloadJsonDaemonColdReloadTest.exe", "", None, None)
	]
