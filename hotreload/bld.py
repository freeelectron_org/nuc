import os
import sys
forge = sys.modules["forge"]


def setup(module):

	srcList = [
		"hotreload.pmh",
		"HotReloadDL",
		"HotReloadJsonDaemon"
	]

	dll = module.DLL("HotReloadDL", srcList)

	dll.linkmap = {}
	if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
		dll.linkmap["jsoncpp_libs"] = "jsoncpp.lib"
	else:
		dll.linkmap["jsoncpp_libs"] =  "-ljsoncpp"

	forge.deps(["HotReloadDLLib"], forge.corelibs)

	forge.tests += [
		("inspect.exe",	"HotReloadDL", None, None),
	]

	module.Module('test')

def auto(module):
	if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
		forge.linkmap["jsoncpp_libs"] = "jsoncpp.lib"
		test_file = """
#include "json/json.h"
int main(void)
{
	Json::Value json = new Json::Value();
}
\n"""

	else:
		forge.linkmap["jsoncpp_libs"] = "-ljsoncpp"
		test_file = """
#include "jsoncpp/json/json.h"
int main(void)
{
	Json::Value json = new Json::Value();
}
\n"""

	result = forge.cctest(test_file)

	forge.linkmap.pop('jsoncpp_libs', None)

	return result
