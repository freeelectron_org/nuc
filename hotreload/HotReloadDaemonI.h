/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

namespace hive
{

class FE_DL_EXPORT HotReloadDaemonI : virtual public fe::Component,
	public fe::CastableAs<HotReloadDaemonI>
{
public:
    // updateDelay: How often to check for the hotreload file, set as milliseconds of delay.
    virtual void start(const unsigned int updateDelay=1000) = 0;

    // Set the the file this daemon should be checking for. Not needed typically, mostly used for testing.
    virtual void setCustomManifestFilepath(const fe::String& manifestFilepath) = 0;
};

} // namespace hive
