/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#ifndef hive_socket_can
#define hive_socket_can

#include <virtualCan/can_data_types.h>

namespace hive
{

class FE_DL_EXPORT SocketCan : virtual public VirtualCanI
{
	public:
				SocketCan(void);
				~SocketCan(void);

		void	start(const std::string &CANDevice);
		int		receive(struct can_frame *frame);
		int		send(struct can_frame *frame);
		bool	isSamePayload(can_frame &frame_1, can_frame &frame_2);
		void	copyPayload(can_frame &frameSrc, can_frame &frameDst);
		void	registerSafetyHandler(void)									{}
		void	registerTelemetryHandler(void)								{}

	private:
		void	shutdown(void);
		void	unbind(void);

		int					m_sock;
		struct sockaddr_can	m_addr;
		struct ifreq		m_ifr;
		std::string			m_deviceName;
		bool				m_ready;
};

}
#endif
