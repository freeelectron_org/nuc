/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#ifndef virtual_can_message_parser_H
#define virtual_can_message_parser_H


namespace hive
{

class FE_DL_EXPORT CanMsgParser
{
public:
	uint32_t getCanMsgVal(const struct can_frame * msg, uint32_t offset,
				uint64_t mask);
	void setCanMsgVal(struct can_frame * msg, uint32_t offset,
				uint64_t mask, uint32_t val);
	virtual struct can_frame * getCanMsg() = 0;
};
}
#endif
