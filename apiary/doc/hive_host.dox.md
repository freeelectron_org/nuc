Using the HiveHost {#hive_host}
==================

Accessing the HiveHost {#hive_access}
======================

hive::HiveHost is handled as a singleton.
First access will create the single instance.
Further access will return the same instance.

Releasing a reference to the HiveHost will not destroy it,
unless that was the last reference.
If a new instance is created once again,
the previous HiveHost contents will not be restored.

To access the HiveHost, potentially creating it,
use its static `create()` call and place the result in a smart pointer.

```cpp
sp<HiveHost> spHiveHost(HiveHost::create());
```

Setting up Autoload {#hive_autoload}
===================

Often, the first thing to do with the HiveHost is to set up
module auto-loading.
If you later try to create a component of a type for which there
is not currently a factory,
the autoload subsystem will scan a manifest for a module that
can provide a compatible implementation and then load it automatically.

```cpp
sp<Master> spMaster=spHiveHost->master();
sp<Registry> spRegistry=spMaster->registry();

Result result=spRegistry->manage("feAutoLoadDL");
```

The fe::Master is [almost always] also a singleton that holds a lot
a general data for the process.
The fe::Registry manages loadable modules and creates components.

Next Step {#hive_host_next}
=========

A common use of the HiveHost to to create spaces,
each in the form of an fe::StateCatalog.

```cpp
sp<StateCatalog> spStateCatalog=spHiveHost->accessSpace("world");
```

For more, see @ref hive_state_catalog.
