/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "apiary/apiary.h"
#include "math/math.h"

#include <chrono>
#include <thread>

using namespace fe;
using namespace hive;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			sp<HiveHost> m_spHiveHost = HiveHost::create();
			UNIT_TEST(m_spHiveHost.isValid() == true);

			sp<Master> m_spMaster = m_spHiveHost->master();
			UNIT_TEST(m_spMaster.isValid() == true);

			sp<Registry> m_spRegistry = m_spMaster->registry();
			UNIT_TEST(m_spRegistry.isValid() == true);

			Result result = m_spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result) == true);

			String confLine("localhost:9002 role=client"
					" ioPriority=high transport=tcp");
			feLog("Hive configuration: %s\n", confLine.c_str());
			sp<StateCatalog> m_spStateCatalog =
					m_spHiveHost->accessSpace("world", "*.ZeroCatalog");
			UNIT_TEST(m_spStateCatalog.isValid() == true);

			result = m_spStateCatalog->configure(confLine);
			UNIT_TEST(successful(result) == true);

			result = m_spStateCatalog->start();
			UNIT_TEST(successful(result) == true);

			// necessary for reproducing (seems like in 0.8.4 is not necessary)
//			feLog("Waiting five seconds...\n");
//			std::this_thread::sleep_for(std::chrono::milliseconds(5000));

			UNIT_TEST(m_spStateCatalog->connected() == false);

			feLog("Stopping the catalog...\n");
			result = m_spStateCatalog->stop();

			feLog("The catalog is stopped.\n");
			UNIT_TEST(successful(result) == true);
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}

