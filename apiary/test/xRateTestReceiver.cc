/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "apiary/apiary.h"
#include "xCommonDataType.h"

#define FILTER_LENGTH 200
const int N = FILTER_LENGTH;

void rxMsgFunc();
double movingAverage(double x_in);

HiveHost_T hh_receiver;

//=================================================================
//                          MAIN ()
//=================================================================

int main(int argc,char** argv)
{
    std::cout << "hello" << std::endl;
	hh_receiver.m_spHiveHost = hive::HiveHost::create();
    if(hh_receiver.m_spHiveHost.isValid())
    {
        fe::sp<fe::Registry> spRegistry = hh_receiver.m_spHiveHost->master()->registry();
        spRegistry->manage("feAutoLoadDL");
        spRegistry->manage("hiveApiaryDL");
        assertVehicleData(hh_receiver.m_spHiveHost->master()->typeMaster());

        hh_receiver.m_spStateCatalog=
				hh_receiver.m_spHiveHost->accessSpace("world");
        hh_receiver.m_spStateCatalog->setState<fe::String>(
				"net:role","client");
        hh_receiver.m_spStateCatalog->setState<fe::String>(
				"net:address","127.0.0.1");
        hh_receiver.m_spStateCatalog->setState<I32>("net:port",9001);
        hh_receiver.m_spStateCatalog->start();

        hh_receiver.m_xferThred = new std::thread(&rxMsgFunc);
    }
    char c;
    std::cout << "Press any key to finish" << std::endl;
    std::cin >> c;
    hh_receiver.m_isRunnig = false;
    return 0;
}


//-----------------------------------------------------------------
// rxMsgFunc()
//-----------------------------------------------------------------
void rxMsgFunc()
{
    hh_receiver.m_isRunnig = true;
    hh_receiver.m_vehicleXform.frameNumber = 0;
    hh_receiver.m_frameRate = 50; // Hz

    long rateMicroSecs = 1000000*(1 / hh_receiver.m_frameRate);
    int pktCtr = 0;

    double FreqMin = 10000.0;
    double FreqMax = 0.0;

    static std::chrono::time_point<std::chrono::high_resolution_clock> lastTime = std::chrono::high_resolution_clock::now();
    bool ready = false;
    int p_ctr = 0;

    while(hh_receiver.m_isRunnig)
    {
        vehicleDataT xform;
        std::chrono::time_point<std::chrono::high_resolution_clock> thisTime = std::chrono::high_resolution_clock::now();
        do
        {
            hh_receiver.m_spStateCatalog->getState<vehicleDataT>("cars:0.transform",xform);
        } while (hh_receiver.m_frameNumber == xform.frameNumber);

        double deltaTime = std::chrono::duration<double>(thisTime - lastTime).count();
        hh_receiver.m_frameNumber = xform.frameNumber;
        lastTime = thisTime;
        double freq = 1/deltaTime;

        if(freq > 2000){continue;}

        if((freq < FreqMin) || (freq > FreqMax))
        {
            if(freq < FreqMin)
            {
                FreqMin = freq;
            }
            if(freq > FreqMax)
            {
                FreqMax = freq;
            }
        }
        double f_avg = movingAverage(freq);

        if(++p_ctr >= 400)
        {
            // Only print every 200 pkts.
            std::cout << "\r" << "Rate Min: " << FreqMin << "\t200-Sample Avg: " << f_avg << "\tRate Max: " << FreqMax;
            p_ctr = 0;
        }
    }
    hh_receiver.m_xferThred->join();
}

//-----------------------------------------------------------------
// movingAverage()
//-----------------------------------------------------------------
double movingAverage(double x_in)
{
    static double x_i[FILTER_LENGTH] = {0.0};
    static int n = 0;
    static int initCtr = 0;
    static double y_n = 0.0;
    static double y_n_1 = 0.0;

    x_i[n % N] = x_in / N;

    if(initCtr >= N)
    {
        y_n = y_n_1 + x_i[n % N] - x_i[(n-N-1)%N];
        y_n_1 = y_n;
        ++n;
        return y_n;
    }
    else
    {
        ++initCtr;
        y_n += x_in / N;
        y_n_1 = y_n;
        ++n;
        return y_n;
    }
}
