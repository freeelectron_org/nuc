/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "apiary/apiary.h"
#include "xCommonDataType.h"

void txMsgFunc();
HiveHost_T hh_sender;

//=================================================================
//                          MAIN ()
//=================================================================

int main(int argc,char** argv)
{
	hh_sender.m_spHiveHost = hive::HiveHost::create();
    if(hh_sender.m_spHiveHost.isValid())
    {
        fe::sp<fe::Registry> spRegistry = hh_sender.m_spHiveHost->master()->registry();
        spRegistry->manage("feAutoLoadDL");
        spRegistry->manage("hiveApiaryDL");
        assertVehicleData(hh_sender.m_spHiveHost->master()->typeMaster());

        hh_sender.m_spStateCatalog=
				hh_sender.m_spHiveHost->accessSpace("world");
        hh_sender.m_spStateCatalog->setState<fe::String>("net:role","server");
        hh_sender.m_spStateCatalog->setState<I32>("net:port",9001);
        hh_sender.m_spStateCatalog->start();

        hh_sender.m_xferThred = new std::thread(&txMsgFunc);
    }
    char c;
    std::cout << "Press any key to finish" << std::endl;
    std::cin >> c;
    hh_sender.m_isRunnig = false;
    return 0;
}

//-----------------------------------------------------------------
// txMsgFunc()
//-----------------------------------------------------------------
void txMsgFunc()
{
    static int testDurCtr = 0;
    hh_sender.m_isRunnig = true;
    hh_sender.m_vehicleXform.frameNumber = 0;
    hh_sender.m_frameRate = 50; // Hz

    long rateMicroSecs = 1000000*(1 / hh_sender.m_frameRate);
    int pktCtr = 0;

    while(hh_sender.m_isRunnig == true)
    {
        //Send
        hh_sender.m_spStateCatalog->setState<vehicleDataT>("cars:0.transform", hh_sender.m_vehicleXform);
        hh_sender.m_spStateCatalog->flush();

        hh_sender.m_vehicleXform.frameNumber = (hh_sender.m_vehicleXform.frameNumber + 1) % 1000;

/*
        if(++pktCtr == 500)
        {
            hh_sender.m_frameRate += 10;
            rateMicroSecs = 1000000*(1 / hh_sender.m_frameRate);
            std::cout << "\r" << "Intended Frame Rate Hz: " << hh_sender.m_frameRate;
            pktCtr = 0;
        }
        std::this_thread::sleep_for(std::chrono::microseconds(rateMicroSecs));
*/
        if(++testDurCtr > 10000){
            hh_sender.m_isRunnig = false;
        }
    }
    std::cout << "Test completed" << std::endl;
    hh_sender.m_xferThred->join();
}
