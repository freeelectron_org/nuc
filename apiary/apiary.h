/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __apiary_apiary_h__
#define __apiary_apiary_h__

#include "networkhost/networkhost.h"

#ifdef MODULE_apiary
#define HIVE_APIARY_PORT FE_DL_EXPORT
#else
#define HIVE_APIARY_PORT FE_DL_IMPORT
#endif

#include "apiary/HiveHost.h"

#endif /* __apiary_apiary_h__ */
