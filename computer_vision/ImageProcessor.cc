/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include <computer_vision/computer_vision.pmh>

namespace hive
{
//-----------------------------------------------------------------
//
//-----------------------------------------------------------------

ImageProcessor::ImageProcessor()
    :m_kernelSzGaussianBlur(5,5), m_cannyLowThreshold(50), 
     m_cannyLowThresholdTimesRatio(150)
{
    left_flag = false;
    right_flag = false;
};


//-----------------------------------------------------------------
//
//-----------------------------------------------------------------

void ImageProcessor::canny(cv::Mat & srcImg, cv::Mat & dstImg, cv::Size gaussBlur)
{
    cv::Mat grayImg;
    cv::cvtColor(srcImg, grayImg, cv::COLOR_BGR2GRAY);
    cv::Mat blurImg;
    cv::GaussianBlur(grayImg, blurImg, gaussBlur, 0);
    cv::Canny(blurImg, dstImg, m_cannyLowThreshold, m_cannyLowThresholdTimesRatio);		
}


//-----------------------------------------------------------------
//
//-----------------------------------------------------------------

void ImageProcessor::regionOfInterestLeft(cv::Mat & srcImg, cv::Mat & dstImg)
{
    int height = srcImg.rows;
    int width = srcImg.cols;

    cv::Mat maskImg = cv::Mat::zeros(srcImg.size(), srcImg.type());
    cv:: Point points[4] = { cv::Point(0, height),			// bottom_left
                                cv::Point(0, height/2+10),		// top_left
                                cv::Point(width, height/2+10),	// top_right
                                cv::Point(width/2, height)		// bottom_right
                            };
    cv::fillConvexPoly(maskImg, points, 4, cv::Scalar(255,0,0));
    cv::bitwise_and(srcImg, maskImg, dstImg);		
}


//-----------------------------------------------------------------
//
//-----------------------------------------------------------------

void ImageProcessor::regionOfInterestRight(cv::Mat & srcImg, cv::Mat & dstImg)
{
    int height = srcImg.rows;
    int width = srcImg.cols;

    cv::Mat maskImg = cv::Mat::zeros(srcImg.size(), srcImg.type());
    cv:: Point points[4] = { cv::Point(width/2, height),            // bottom_left
                                cv::Point(0, height/2+10),	        // top_left
                                cv::Point(width, height/2+10),	    // top_right
                                cv::Point(width, height)		    // bottom_right
                            };
    cv::fillConvexPoly(maskImg, points, 4, cv::Scalar(255,0,0));
    cv::bitwise_and(srcImg, maskImg, dstImg);		
}

//-----------------------------------------------------------------
// ImageProcessor::regionOfInterest()
//-----------------------------------------------------------------
void ImageProcessor::regionOfInterest(cv::Mat & srcImg, cv::Mat & dstImg, cv::Point * maskPoly, int numMaskVertices)
{
    //int height = srcImg.rows;
    //int width = srcImg.cols;

    cv::Mat maskImg = cv::Mat::zeros(srcImg.size(), srcImg.type());
    cv::fillConvexPoly(maskImg, maskPoly, numMaskVertices, cv::Scalar(255,0,0));
    cv::bitwise_and(srcImg, maskImg, dstImg);		
}
//-----------------------------------------------------------------
//
//-----------------------------------------------------------------

void ImageProcessor::Hough(cv::Mat & srcImg, std::vector<cv::Vec4i> & lines, HoughParam hp)
{
    static double pi = 3.1415926535897932384626433832795;// cv::CV_PI;
    cv::HoughLinesP(srcImg, lines, hp.rho, pi/180.0, hp.threshold, hp.minLineLen, hp.maxLineGap);
}


//-----------------------------------------------------------------
//
//-----------------------------------------------------------------

void ImageProcessor::AvgSlopeIntercept(cv::Mat & srcImg, 
                        std::vector<cv::Vec4i> & lines,
                        std::vector<cv::Vec4i> & right_lines, 
                        std::vector<cv::Vec4i> & left_lines)
{
    size_t j = 0;
    
    // Two points in the line.
    cv::Point p0;
    cv::Point p1;
    
    double slope_thresh = 0.8;
    std::vector<double> slopes;
    std::vector<cv::Vec4i> selected_lines;
    

    for(cv::Vec4i line : lines)
    {
        p0 = cv::Point(line[0], line[1]);
        p1 = cv::Point(line[2], line[3]);

        // Basic algebra: slope = (y1 - y0)/(x1 - x0)
        double slope = ((double)(p1.y) - (double)(p0.y))/((double)(p1.x) - (double)(p0.x) + 0.00001);

        // If the slope is too horizontal, discard the line
        // If not, save them  and their respective slope
        if (std::abs(slope) > slope_thresh) 
        {
            slopes.push_back(slope);
            selected_lines.push_back(line);
        }
    }
    
    // Split the lines into right and left lines
    double img_center = (double)((srcImg.cols / 2));
    while (j < selected_lines.size()) 
    {
        p0 = cv::Point(selected_lines[j][0], selected_lines[j][1]);
        p1 = cv::Point(selected_lines[j][2], selected_lines[j][3]);

        // Condition to classify line as left side or right side
        if (slopes[j] > 0 && p1.x > img_center && p0.x > img_center) 
        {
            right_lines.push_back(selected_lines[j]);
            right_flag = true;
        } 
        else if (slopes[j] < 0 && p1.x < img_center && p0.x < img_center) 
        {
            left_lines.push_back(selected_lines[j]);
            left_flag = true;
        }
        j++;
    }
            
}


//-----------------------------------------------------------------
//
//-----------------------------------------------------------------

void ImageProcessor::drawLines(cv::Mat & srcImg, std::vector<cv::Vec4i> & lines)
{
    for (size_t i=0; i<lines.size(); i++) 
    {
        cv::Vec4i l = lines[i];
        cv::line(srcImg, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(255, 0, 0), 3, cv::LineTypes::LINE_AA);
    }
}


//-----------------------------------------------------------------
// ImageProcessor::makeCoordinates()
//-----------------------------------------------------------------

void ImageProcessor::makeCoordinates(cv::Mat srcImg, LineParameters & mb, cv::Vec4i & dstLine)
{
    double slope = mb._slope;
    double intercept = mb._intercept;
    int y1 = srcImg.rows;

    int y2 = int( y1*(3.0/5.0)); // Start at the bottom of the image and go 3/5 of the way "up"
    int x1 = int((y1 - intercept)/slope);

    int x2 = int((y2 - intercept)/slope);
    dstLine[0] = x1;
    dstLine[1] = y1;
    dstLine[2] = x2;
    dstLine[3] = y2;
}

//-----------------------------------------------------------------
//
//-----------------------------------------------------------------

void ImageProcessor::average_slope_intercept(cv::Mat & image, 
                                            std::vector<cv::Vec4i> & lines,						   
                                            cv::Vec4i & right_line, 
						                    cv::Vec4i & left_line)
{
    std::vector<LineParameters> left_fit;
    std::vector<LineParameters> right_fit;


    LineParameters tmpP;
    for(cv::Vec4i line : lines)
    {        
        lineFit(line, tmpP);

        double slope = tmpP._slope;
        //double intercept = tmpP._intercept;

        if (slope < 0) {
            left_fit.push_back(tmpP);
        }
        else {
            right_fit.push_back(tmpP);        
        }
    }

    LineParameters left_fit_average = averageParams(left_fit);
    LineParameters right_fit_average = averageParams(right_fit);

    makeCoordinates(image, left_fit_average, left_line);
    makeCoordinates(image, right_fit_average, right_line);  
}



//-----------------------------------------------------------------
// ImageProcessor::lineFit()
//-----------------------------------------------------------------

void ImageProcessor::lineFit(cv::Vec4i & line, LineParameters & si)
{
    double x1 = line[0];
    double y1 = line[1];
    double x2 = line[2];
    double y2 = line[3];

    si._slope = (y2-y1)/(x2-x1);
    si._intercept = y1 - si._slope * x1; 
}


//-----------------------------------------------------------------
// ImageProcessor::averageParams()
//-----------------------------------------------------------------

LineParameters ImageProcessor::averageParams(std::vector<LineParameters> & paramVec)
{
    LineParameters retVal;
    retVal._slope = 0;
    retVal._intercept = 0;
    
    for (LineParameters param : paramVec)
    {
        retVal._slope += param._slope;
        retVal._intercept += param._intercept;    
    }
    retVal._slope = retVal._slope / paramVec.size();
    retVal._intercept = retVal._intercept / paramVec.size();
    
    return retVal;
}


}
