/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#ifndef __computer_vision_ImageProcessor_h__
#define __computer_vision_ImageProcessor_h__

namespace hive
{

class FE_DL_EXPORT ImageProcessor : virtual public ComputerVisionI
{
public:
	ImageProcessor();
	~ImageProcessor(){};

	void canny(cv::Mat & srcImg, cv::Mat & dstImg, cv::Size gaussBlur);
	
	void regionOfInterestLeft(cv::Mat & srcImg, cv::Mat & dstImg);
	void regionOfInterestRight(cv::Mat & srcImg, cv::Mat & dstImg);
	void regionOfInterest(cv::Mat & srcImg, cv::Mat & dstImg, cv::Point * maskPoly, int numMaskVertices);
	
	void Hough(cv::Mat & srcImg, std::vector<cv::Vec4i> & lines, HoughParam hp);
	void AvgSlopeIntercept(cv::Mat & srcImg, 
	    				   std::vector<cv::Vec4i> & lines,
						   std::vector<cv::Vec4i> & right_lines, 
						   std::vector<cv::Vec4i> & left_lines);
    void average_slope_intercept(cv::Mat & image, 
                             std::vector<cv::Vec4i> & lines,						   
                             cv::Vec4i & right_line, 
						     cv::Vec4i & left_line);
	void drawLines(cv::Mat & srcImg, std::vector<cv::Vec4i> & lines);

private:
    void makeCoordinates(cv::Mat srcImg, LineParameters & mb, cv::Vec4i & dstLine);
    void lineFit(cv::Vec4i & line, LineParameters & si);
    LineParameters averageParams(std::vector<LineParameters> & paramVec);
	cv::Size m_kernelSzGaussianBlur;
	const int m_cannyLowThreshold;
	const int m_cannyLowThresholdTimesRatio;
	bool left_flag;
	bool right_flag;
};

}

#endif
