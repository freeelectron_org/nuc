/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

//=============================================================================
// File name: asio_client.cpp
//
// Objective: Main start file to the project code name Horaizon's Test Platform
//            (mimics a perception module).
//            This project serves as the test platform for the Sensor Simulation
//            platform.
//
// Requires: openCV 3.4.1
//=============================================================================

#include "sensors/sensors.h"

#include <opencv2/opencv.hpp>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <thread>
#include <time.h>
#include <stdio.h>



using namespace std;
using namespace hive;

struct TestHelper_T
{
	std::thread 		_receiverThread;
	SensorSubscriberI * _camera;
};

TestHelper_T	gSensor;

//-----------------------------------------------------------------
//							MAIN
//-----------------------------------------------------------------
int main(int argc,char** argv)
{
	//cout.setf(ios::hex, ios::basefield);
	cout << "      . .      " << endl;
	cout << "    .     .    " << endl;
	cout << "  .         .  " << endl;
	cout << " |  __   __  | " << endl;
	cout << " |           | " << endl;
	cout << "  .         .  " << endl;
	cout << "    .     .    " << endl;
	cout << "      . .      " << endl;

	cv::namedWindow("front_center", cv::WINDOW_NORMAL);
	cv::resizeWindow("front_center", 256, 144);
	cv::moveWindow("front_center", 800, 200);

	// start a thread to pull the data.
	gSensor._camera = new CameraSubscriber();
	gSensor._camera->start();

	fe::Array<U8> data;

	bool running = true;
	while(running)
	{
		gSensor._camera->listen(data);
		feLog("got data; size: %d\n");
		if(data.size() > 20)
		{	
					
			//std::vector<unsigned char> camData(  dataP->data, dataP->data + dataP->numBytes);
			std::vector<short> s;
			s.reserve(data.size() / 2);
			for (size_t idx = 0; idx < data.size(); idx += 2) 
			{
    			s.push_back(data[idx] & 0xff + static_cast<unsigned>(data[idx+1]) << 8);
			}
			//cv::Mat image = cv::imdecode(camData, cv::IMREAD_UNCHANGED);
			cv::Mat image(144, 256, CV_8UC4, s.data());

			if (image.data != NULL)
			{
				/*
				switch ((SensorID_t)dataP->_type)
				{
				case eCameraFrontCenter:
					cv::imshow("front_center", image);
					cv::waitKey(1);
					break;
				case eCameraFrontLeft:
					cv::imshow("front_left", image);
					cv::waitKey(1);
					break;
				case eCameraFrontRight:
					cv::imshow("front_right", image);
					cv::waitKey(1);
					break;
				default:
					break;
				}
				*/
				//cv::imshow("front_center", image);
				//cv::waitKey(1);
			}
			else
			{
				cout << "data sz:" << data.size() << endl;				
			}												
		}
		else
		{
			Sleep(2);
			running = false;
		}
	}
	
	cv::destroyAllWindows();
	cout << "Done" << endl;
}



/*
	cv::namedWindow("front_left", cv::WINDOW_NORMAL);
	cv::resizeWindow("front_left", 256, 144);
	cv::moveWindow("front_left", 500, 200);

	cv::namedWindow("front_right", cv::WINDOW_NORMAL);
	cv::resizeWindow("front_right", 256, 144);
	cv::moveWindow("front_right", 1100, 200);
*/
