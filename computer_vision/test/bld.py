import os.path
import sys
import string
forge = sys.modules["forge"]

# opencv on Windows:
# install vcpkg in your FE_WINDOWS_LOCAL
# see https://github.com/Microsoft/vcpkg/
# vcpkg.exe install opencv:x86-windows-static-md
# vcpkg.exe install opencv:x64-windows-static-md

def setup(module):
	deplibs = forge.corelibs + [
				"fePluginLib",
				"fexSignalLib",
				"hiveApiaryDLLib",
				"hiveSensorsDLLib",
				"hiveSensorPublishSubscribeDLLib",
				"hiveComputerVisionDLLib"
				]

	tests = [
			'xCvTest',
			'xSimpleClient'
			]

	for t in tests:
		exe = module.Exe(t)
		if forge.fe_os == "FE_LINUX":
			exe.linkmap = { "stdthread": "-lpthread" }
		forge.deps([t + "Exe"], deplibs)








