/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

// cd unreal-devbot\Plugins\FreeElectron\Source\ThirdParty\FreeElectronLibrary\FE

#include "computer_vision/computer_vision.h"
#include "math/math.h"
#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


//-----------------------------------------------------------------
// Global functions.

void stillImage(std::string fileName);


// Run like this, for example:
// xCvTest.exe c:\\local\\test_image.jpg




//=================================================================
//							MAIN()
//=================================================================
int main(int argc,char** argv)
{
	for (int i = 0; i < argc; ++i) {
        std::cout << argv[i] << "\n";
	}
	if(argc >= 2) {
		stillImage(argv[1]);
	}
	return 0;
}

//-----------------------------------------------------------------
//
//-----------------------------------------------------------------
void stillImage(std::string fileName)
{
    fe::sp<hive::HiveHost> spHiveHost(hive::HiveHost::create());
    fe::sp<fe::Master> spMaster=spHiveHost->master();
	fe::sp<fe::Registry> spRegistry=spMaster->registry();
   	spRegistry->manage("feAutoLoadDL");
    spRegistry->manage("hiveApiaryDL");

	fe::sp<hive::ComputerVisionI>  isp;
	isp = spRegistry->create("ComputerVisionI.ImageProcessor");

	cv::namedWindow("still image");
	//cv::Mat img = cv::imread("c:\\local\\test_image.jpg");
    cv::Mat img = cv::imread(fileName.c_str());

	// Canny			
	cv::Size kernelSzGaussianBlur1(3,3);
	cv::Mat new_image1 = cv::Mat::zeros( img.size(), img.type() );	
	isp->canny(img, new_image1, kernelSzGaussianBlur1);
	cv:: Point points[3] = { cv::Point(200, new_image1.rows),	// bottom_left
				cv::Point(1100, new_image1.rows),				// botom_right
				cv::Point(550,250)								// top
				};			
	isp->regionOfInterest(new_image1, new_image1, points, 3);

	hive::HoughParam hp1;
	hp1.rho = 2;
	hp1.threshold = 100;
	hp1.minLineLen = 40;
	hp1.maxLineGap = 5;
	std::vector<cv::Vec4i> lines;
	
	isp->Hough(new_image1, lines, hp1);
	//isp.drawLines(img, lines);
	
	// Draw Lines
	
	std::vector<cv::Vec4i> right_lines, left_lines;
	cv::Vec4i right_line, left_line;
	isp->average_slope_intercept(new_image1, lines, right_line, left_line);
	
    right_lines.push_back(right_line);
    left_lines.push_back(left_line);
    isp->drawLines(img, right_lines);
	isp->drawLines(img, left_lines);
	
	cv::imshow("still image", img);
	cv::waitKey(0);

}

