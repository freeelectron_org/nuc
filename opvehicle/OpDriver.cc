/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "opvehicle/opvehicle.pmh"

#define FE_OPD_DEBUG		FALSE

//*	TODO consider PID
//*	https://en.wikipedia.org/wiki/PID_controller#:~:text=A%20proportional%E2%80%93integral%E2%80%93derivative%20controller,applications%20requiring%20continuously%20modulated%20control
//*	https://www.youtube.com/watch?v=_bWvXn4ilrY&t=23s

using namespace fe;
using namespace fe::ext;

namespace hive
{

OpDriver::OpDriver(void):
	m_controlMode(AdsVehicleSafetyState::e_prohibited)
{
}

OpDriver::~OpDriver(void)
{
}

void OpDriver::initialize(void)
{
	m_spSurfaceAccessibleLua=registry()->create("*.SurfaceAccessibleLua");

	catalog<String>("paths_fbx")="";
	catalog<String>("paths_fbx","IO")="input";

	catalog<String>("paths_lua")="";
	catalog<String>("paths_lua","IO")="input";

	catalog<String>("paths_json_full")="";
	catalog<String>("paths_json_full","IO")="input";

	catalog<String>("paths_usd")="";
	catalog<String>("paths_usd","IO")="input";

	catalog<SpatialVector>("track_coordinates")=SpatialVector(0,0,0);
	catalog<String>("track_coordinates","IO")="input";

	catalog<String>("vehicleID")="";
	catalog<String>("vehicleID","IO")="input";

	catalog<Real>("deltaTime")=0.01;
	catalog<String>("deltaTime","IO")="input";

	catalog<SpatialVector>("location")=SpatialVector(0,0,0);
	catalog<String>("location","IO")="input";

	catalog<SpatialVector>("direction")=SpatialVector(0,0,0);
	catalog<String>("direction","IO")="input";

	catalog<Real>("aggressionScale")=1.0;
	catalog<String>("aggressionScale","IO")="input";

	catalog<Real>("steering")=0.0;
	catalog<String>("steering","IO")="output";

	catalog<Real>("throttle")=0.0;
	catalog<String>("throttle","IO")="output";

	catalog<Real>("brake")=0.0;
	catalog<String>("brake","IO")="output";

	catalog<SpatialVector>("target0")=SpatialVector(0,0,0);
	catalog<String>("target0","IO")="output";

	catalog<SpatialVector>("target1")=SpatialVector(0,0,0);
	catalog<String>("target1","IO")="output";

	m_spHiveHost = HiveHost::create();
	if(m_spHiveHost.isValid())
	{
		m_spStateCatalog=m_spHiveHost->accessSpace("world");
	}

	catalog< sp<Component> >("hive")=registry()->create("*.NetDispatch");
	catalog<String>("hive","IO")="input";
}

void OpDriver::handle(Record& a_signal)
{
#if FE_OPD_DEBUG
	feLog("OpDriver::handle layout \"%s\"\n",
			a_signal.layout()->name().c_str());
#endif

	if(m_spStateCatalog.isNull())
	{
		feLog("OpDriver::handle no StateCatalog\n");
		return;
	}

	sp<Scope> spScope=a_signal.layout()->scope();
	if(m_spScope!=spScope)
	{
		m_asVehicle.bind(spScope);
		m_asAdsSafetyState.bind(spScope);
		m_spScope=spScope;
	}

	if(m_asAdsSafetyState.check(a_signal))
	{
		m_driveMode=AdsVehicleSafetyState::DriveMode(
				m_asAdsSafetyState.driveMode(a_signal));
		m_controlMode=AdsVehicleSafetyState::ControlMode(
				m_asAdsSafetyState.controlMode(a_signal));
		m_safetyGear=m_asAdsSafetyState.gear(a_signal);
		return;
	}

	if(m_asVehicle.check(a_signal))
	{
		switch(m_safetyGear)
		{
			case AdsVehicleSafetyState::e_neutral:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeNeutral;
				break;
			case AdsVehicleSafetyState::e_drive:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeDrive;
				break;
			case AdsVehicleSafetyState::e_reverse:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeReverse;
				break;
			case AdsVehicleSafetyState::e_park:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModePark;
				break;
			default:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeUnknown;
				break;
		}

		m_asVehicle.gearIndexInput(a_signal)=1;	//* TODO manual gearing

		if(m_controlMode==AdsVehicleSafetyState::e_prohibited)
		{
			//* ADS prohibited (don't affect controls)
			return;
		}

		const Real deltaTime=m_asVehicle.deltaTime(a_signal);

		Real steering(0);
		Real throttle(0);
		Real brake(0);
		SpatialVector target0(0,0,0);
		SpatialVector target1(0,0,0);

		if(m_spSurfaceAccessibleLua.isValid())
		{
			const String vehicleID=
					m_asVehicle.vehicleID(a_signal);
			const String prefixN="vehicles."+vehicleID+".";

			const String inputPrefix="vehicles."+vehicleID+
					".driver.opdriver:input.";
			const String pattern="vehicles\\."+vehicleID+
					"\\.driver\\.opdriver:input\\..*";

			Array<String> stateKeys;
			m_spStateCatalog->getStateKeys(stateKeys,pattern);
			const I32 keyCount=stateKeys.size();
			for(I32 keyIndex=0;keyIndex<keyCount;keyIndex++)
			{
				const String& stateKey=stateKeys[keyIndex];
				const String key=stateKey.prechop(inputPrefix);

				String typeName;
				String value;
				Result result=m_spStateCatalog->getState(stateKey,"value",
						typeName,value);

				catalogSet(key,"value",typeName,value);
				catalog<String>(key,"IO")="input";

				//* TODO delete inputs that disappear from state
			}

			String paths_lua;
			Result result=
					m_spStateCatalog->getState<String>("paths.lua",paths_lua);

			String script;
			result=m_spStateCatalog->getState<String>(
					prefixN+"driver.opdriver:script",script);

			String path=System::findFile(script,paths_lua);
			if(path.empty())
			{
				feLog("OpDriver::handle"
						" failed to find \"%s\" using paths \"%s\"\n",
						script.c_str(),paths_lua.c_str());
				return;
			}

			String paths_fbx;
			result=m_spStateCatalog->getState<String>("paths.fbx",paths_fbx);

			String paths_json_full;
			result=m_spStateCatalog->getState<String>("paths.json_full",
					paths_json_full);

			String paths_usd;
			result=m_spStateCatalog->getState<String>("paths.usd",paths_usd);

			Vector3d track_coordinates;	//* in LLA
			result=m_spStateCatalog->getState<Vector3d>(
					"track.coordinates",track_coordinates);

			String fullname=path+"/"+script;

			const SpatialTransform chassisTransform=
					m_asVehicle.chassisTransform(a_signal);
			const SpatialVector location=chassisTransform.translation();
			const SpatialVector direction=chassisTransform.direction();

			catalog<String>("paths_fbx")=paths_fbx;
			catalog<String>("paths_lua")=paths_lua;
			catalog<String>("paths_json_full")=paths_json_full;
			catalog<String>("paths_usd")=paths_usd;
			catalog<SpatialVector>("track_coordinates")=track_coordinates;
			catalog<String>("vehicleID")=vehicleID;
			catalog<Real>("deltaTime")=deltaTime;
			catalog<SpatialVector>("location")=location;
			catalog<SpatialVector>("direction")=direction;

#if FALSE
			const Real aggressionScale=
					catalogOrDefault<Real>("aggressionScale",1.0);

			String options;
			options.sPrintf("deltaTime=%.6G"
					" location=%.6G,%.6G,%.6G"
					" direction=%.6G,%.6G,%.6G"
					" aggressionScale=%.6G",
					deltaTime,
					location[0],location[1],location[2],
					direction[0],direction[1],direction[2],
					aggressionScale);

			catalog<String>("options")=options;
#endif

#if FE_OPD_DEBUG
			feLog("OpDriver::handle load \"%s\"\n",fullname.c_str());
#endif

			sp<Catalog> spSettings(this);
			m_spSurfaceAccessibleLua->load(fullname,spSettings);

			steering=catalog<Real>("steering");
			throttle=catalog<Real>("throttle");
			brake=catalog<Real>("brake");
			target0=catalog<SpatialVector>("target0");
			target1=catalog<SpatialVector>("target1");
		}

		m_asVehicle.steeringFInput(a_signal)=steering;
		m_asVehicle.adsTarget0(a_signal)=target0;
		m_asVehicle.adsTarget1(a_signal)=target1;

		if(m_driveMode==AdsVehicleSafetyState::e_stop)
		{
			//* ADS controlled stop
			m_asVehicle.throttleInput(a_signal)=0.0;
			m_asVehicle.brakeInput(a_signal)=1.0;
		}
		else
		{
			m_asVehicle.throttleInput(a_signal)=throttle;
			m_asVehicle.brakeInput(a_signal)=brake;
		}

#if FE_OPD_DEBUG
		feLog("OpDriver::handle AsVehicle\n");
		feLog("  steering %.6G\n",m_asVehicle.steeringFInput(a_signal));
		feLog("  throttle %.6G\n",m_asVehicle.throttleInput(a_signal));
		feLog("  brake    %.6G\n",m_asVehicle.brakeInput(a_signal));
#endif

		return;
	}
}

}
