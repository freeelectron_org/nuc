/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef hive_OpDriver_h
#define hive_OpDriver_h

namespace hive
{

/**************************************************************************//**
    @brief Vehicle Driver Handler to drive using a Lua script

	@ingroup opvehicle
*//***************************************************************************/
class FE_DL_EXPORT OpDriver:
	public fe::Catalog,
	virtual public fe::ext::HandlerI,
	public fe::Initialize<OpDriver>
{
	public:
				OpDriver(void);
virtual			~OpDriver(void);

		void	initialize(void);

virtual	void	handle(fe::Record& a_signal);

	private:

		fe::sp<fe::Scope>					m_spScope;
		AsAdsSafetyState					m_asAdsSafetyState;
		AsVehicle							m_asVehicle;

		AdsVehicleSafetyState::DriveMode	m_driveMode;
		AdsVehicleSafetyState::ControlMode	m_controlMode;
		I32									m_safetyGear;

		fe::sp<fe::ext::SurfaceAccessibleI>	m_spSurfaceAccessibleLua;

		fe::sp<fe::StateCatalog>			m_spStateCatalog;
		fe::sp<HiveHost>					m_spHiveHost;
};

}
#endif
