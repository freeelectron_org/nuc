/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_ode_RigidBodyOde_h__
#define __rigid_body_engine_ode_RigidBodyOde_h__

namespace hive
{

class RigidBodyOde:
	virtual public RigidBodyI,
	public fe::CastableAs<RigidBodyOde>
{
public:
	RigidBodyOde();
	~RigidBodyOde();

	// for general rigid body (inertia already computed)
	void					specify(const fe::Real mass,
									const fe::SpatialVector& position,
									const fe::SpatialMatrix& rotation,
									const fe::SpatialMatrix& inertia,
									dWorldID worldID,
									const fe::sp<EngineCommonI> spCommon,
									bool bDynamic = true);
	// for spheres
	void					specify(const fe::Real mass,
									const fe::SpatialVector& position,
									const fe::Real radius,
									dWorldID worldID,
									const fe::sp<EngineCommonI> spCommon,
									bool bDynamic = true);

	void					setMass(const fe::Real mass);
	fe::Real				getMass(void) const;
	void					setPosition(const fe::SpatialVector &pos);
	fe::SpatialVector		getPosition(void) const;
	void					setRotation(const fe::SpatialMatrix &rotation);
	fe::SpatialMatrix		getRotation(void) const;
	void					setTransform(const fe::SpatialTransform &transform);
	fe::SpatialTransform	getTransform(void) const;

	void					applyForce(const fe::SpatialVector &force,
									const fe::SpatialVector &pos);
	void					applyTorque(const fe::SpatialVector &torque);
	void 					setVelocity(const fe::SpatialVector &vel);
	fe::SpatialVector		getVelocity(const fe::SpatialVector &pos) const;
	void 					setAngularVelocity(const fe::SpatialVector &angVel);
	fe::SpatialVector		getAngularVelocity(void) const;
	void					translate(const fe::SpatialVector &displ);

	// Needed by RigidBodyJoint
	dWorldID				getWorldID() const { return m_worldID; }
	dBodyID					getBodyID() const { return m_bodyID; }

	void 					setLinearDamping(const Real damping);
	void 					setAngularDamping(const Real damping);

private:
	dBodyID					m_bodyID;
	dWorldID				m_worldID;
	sp<EngineCommonI>		m_spCommon;
	SpatialMatrix			m_eigenRotation;
	SpatialMatrix			m_eigenRotationInverse;

};

} /* namespace hive */

#endif /* __rigid_body_engine_ode_RigidBodyOde_h__ */
