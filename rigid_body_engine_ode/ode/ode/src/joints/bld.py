import sys
import os
forge = sys.modules["forge"]

def setup(module):
	srcList = [	"amotor.cpp",
				"ball.cpp",
				"contact.cpp",
				"dball.cpp",
				"dhinge.cpp",
				"fixed.cpp",
				"hinge.cpp",
				"hinge2.cpp",
				"joint.cpp",
				"lmotor.cpp",
				"null.cpp",
				"piston.cpp",
				"plane2d.cpp",
				"pr.cpp",
				"pu.cpp",
				"slider.cpp",
				"transmission.cpp",
				"universal.cpp"]

	lib = module.Lib( "odeJoints", srcList )

	module.includemap = { 'ode' : os.path.join(module.modPath,'..','..','..','include'),
						'src' : os.path.join(module.modPath,'..'),
						'odeopcode' : os.path.join(module.modPath,'..','..','..','OPCODE'),
						'ou' : os.path.join(module.modPath,'..','..','..','ou','include'),
						'ccd' : os.path.join(module.modPath,'..','..','..','libccd','src'),
						'ccdcustom' : os.path.join(module.modPath,'..','..','..','libccd','src','custom')}

	module.cppmap = { 'visibility' : "" }
