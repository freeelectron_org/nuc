import sys
import os
forge = sys.modules["forge"]

def setup(module):
	srcList = [	"IceAABB.cpp",
				"IceContainer.cpp",
				"IceHPoint.cpp",
				"IceIndexedTriangle.cpp",
				"IceMatrix3x3.cpp",
				"IceMatrix4x4.cpp",
				"IceOBB.cpp",
				"IcePlane.cpp",
				"IcePoint.cpp",
				"IceRandom.cpp",
				"IceRay.cpp",
				"IceRevisitedRadix.cpp",
				"IceSegment.cpp",
				"IceTriangle.cpp",
				"IceUtils.cpp"
				]

	lib = module.Lib( "odeIce", srcList )

	module.includemap = { 'ode' : os.path.join(module.modPath,'..','..','include'),
						'OPCODE' : os.path.join(module.modPath,'..') }
#						'ou' : os.path.join(module.modPath,'..','..','..','ou','include'),
#						'ccd' : os.path.join(module.modPath,'..','..','..','libccd','src'),
#						'ccdcustom' : os.path.join(module.modPath,'..','..','..','libccd','src','custom')}

	module.cppmap = { 'visibility' : "" }
