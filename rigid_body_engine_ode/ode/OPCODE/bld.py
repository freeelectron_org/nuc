import sys
import os
forge = sys.modules["forge"]

def setup(module):
	ice = module.Module('Ice')
	module.AddDep(ice)

	srcList = [	"OPC_AABBCollider.cpp",
				"OPC_AABBTree.cpp",
				"OPC_BaseModel.cpp",
				"OPC_Collider.cpp",
				"OPC_Common.cpp",
				"OPC_HybridModel.cpp",
				"OPC_LSSCollider.cpp",
				"OPC_MeshInterface.cpp",
				"OPC_Model.cpp",
				"OPC_OBBCollider.cpp",
				"OPC_OptimizedTree.cpp",
				"OPC_Picking.cpp",
				"OPC_PlanesCollider.cpp",
				"OPC_RayCollider.cpp",
				"OPC_SphereCollider.cpp",
				"OPC_TreeBuilders.cpp",
				"OPC_TreeCollider.cpp",
				"OPC_VolumeCollider.cpp",
				"Opcode.cpp",
				"StdAfx.cpp"
				]

	deplibs = forge.basiclibs + [ "odeIceLib" ]

	lib = module.Lib( "odeOpCode", srcList )

	module.includemap = { 'ode' : os.path.join(module.modPath,'..','include'),
						'Ice' : os.path.join(module.modPath)}
#						'ou' : os.path.join(module.modPath,'..','..','ou','include'),
#						'ccd' : os.path.join(module.modPath,'..','..','libccd','src'),
#						'ccdcustom' : os.path.join(module.modPath,'..','..','libccd','src','custom')}

	module.cppmap = { 'visibility' : "" }
