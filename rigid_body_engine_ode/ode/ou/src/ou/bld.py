import sys
import os
forge = sys.modules["forge"]

def setup(module):	
	srcList = [	"atomic.cpp",
				"customization.cpp",
				"malloc.cpp"]

	lib = module.Lib( "odeOu", srcList )

	module.includemap = { 'ode' : os.path.join(module.modPath,'..','..','..','include'),
						'ou' : os.path.join(module.modPath,'..','..','include') }

	module.cppmap = { 'visibility' : "" }
