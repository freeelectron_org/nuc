import sys
import os
forge = sys.modules["forge"]

def setup(module):	
	srcList = [	"alloc.c",
				"ccd.c",
				"mpr.c",
				"polytope.c",
				"support.c",
				"vec3.c"]

	lib = module.Lib( "odeCcd", srcList )

	module.includemap = { 'ode' : os.path.join(module.modPath,'..','..','include'),
						'ccd' : module.modPath,
						'ccdcustom' : os.path.join(module.modPath,'custom')
				}

	module.cppmap = { 'visibility' : "" }
