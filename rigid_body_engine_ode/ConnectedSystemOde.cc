/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine_ode.pmh"

namespace hive {

void ConnectedSystemOde::initialize()
{
	// Below object is singleton, so will only be instantiated once
	m_spCommon = registry()->create("EngineCommonI.*.*.ode");
	m_worldID = dWorldCreate();

	// Default values for world constants
	SpatialVector gravity(0, 0, 0);
	setGravity(gravity);
	setGlobalERP(0.9);
	setGlobalCFM(0.000001);
	setGlobalLinearDamping(0.0005);
	setGlobalAngularDamping(0.0005);
}

/// General case - requires a rotation as well as inertia matrix
sp<RigidBodyI> ConnectedSystemOde::createRigidBody(const Real mass, const SpatialVector& position, const SpatialMatrix& rotation,
	const SpatialMatrix& inertia, bool bDynamic)
{
	sp<RigidBodyOde> spBodyOde(registry()->create("RigidBodyI.*.*.ode"));
	spBodyOde->specify(mass, position, rotation, inertia, m_worldID, m_spCommon, bDynamic);
	spBodyOde->setLinearDamping(m_linearDamping);
	spBodyOde->setAngularDamping(m_angularDamping);

	sp<RigidBodyI> spBody = spBodyOde;
	m_rigidBodyList.push_back(spBody);
	return spBody;
}

/// Special case for spherical objects - rotation is always setIdentity()
sp<RigidBodyI> ConnectedSystemOde::createRigidBody(const Real mass, const SpatialVector& position, const Real radius, bool bDynamic)
{
	sp<RigidBodyOde> spBodyOde(registry()->create("RigidBodyI.*.*.ode"));
	spBodyOde->specify(mass, position, radius, m_worldID, m_spCommon, bDynamic);
	spBodyOde->setLinearDamping(m_linearDamping);
	spBodyOde->setAngularDamping(m_angularDamping);

	sp<RigidBodyI> spBody = spBodyOde;
	m_rigidBodyList.push_back(spBody);
	return spBody;
}

sp<JointI> ConnectedSystemOde::createHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector& pivotInA, const SpatialVector& pivotInB,
	const SpatialVector& axisInA, const SpatialVector& axisInB)
{
	sp<JointOde> spJointOde(registry()->create("JointI.*.*.ode"));
	spJointOde->specifyHingeJoint(bodyA, bodyB, pivotInA, pivotInB, axisInA, axisInB, m_spCommon);

	sp<JointI> spJoint = spJointOde;
	m_jointList.push_back(spJoint);
	return spJoint;
}

sp<JointI> ConnectedSystemOde::createBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector& pivotInA, const SpatialVector& pivotInB)
{
	sp<JointOde> spJointOde(registry()->create("JointI.*.*.ode"));
	spJointOde->specifyBallJoint(bodyA, bodyB, pivotInA, pivotInB, m_spCommon);

	sp<JointI> spJoint = spJointOde;
	m_jointList.push_back(spJoint);
	return spJoint;
}

sp<JointI> ConnectedSystemOde::createHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector& pivotPos, const SpatialVector& axisDir)
{
	sp<JointOde> spJointOde(registry()->create("JointI.*.*.ode"));
	spJointOde->specifyHingeJoint(bodyA, bodyB, pivotPos, axisDir, m_spCommon);

	sp<JointI> spJoint = spJointOde;
	m_jointList.push_back(spJoint);
	return spJoint;
}

sp<JointI> ConnectedSystemOde::createBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector& pivotPos)
{
	sp<JointOde> spJointOde(registry()->create("JointI.*.*.ode"));
	spJointOde->specifyBallJoint(bodyA, bodyB, pivotPos, m_spCommon);

	sp<JointI> spJoint = spJointOde;
	m_jointList.push_back(spJoint);
	return spJoint;
}

sp<SpringDamperI> ConnectedSystemOde::createSpringDamper(
	const sp<RigidBodyI> bodyA, const SpatialVector pointA,
	const sp<RigidBodyI> bodyB, const SpatialVector pointB)
{
	sp<SpringDamperI> spSpringDamper(registry()->create("SpringDamperI.SpringDamperTest.hive"));
	spSpringDamper->setUp(bodyA, pointA, bodyB, pointB);

	m_springDamperList.push_back(spSpringDamper);
	return spSpringDamper;
}

sp<SpringDamperI> ConnectedSystemOde::createSpringDamper(const sp<RigidBodyI> bodyA, const SpatialVector pointA,
	const sp<RigidBodyI> bodyB, const SpatialVector pointB,
	const Real a_springRate, const Real a_dampingConstant,
	const Real a_restLength)
{
	sp<SpringDamperTest> spSpringDamperTest(registry()->create("SpringDamperI.SpringDamperTest.hive"));
	spSpringDamperTest->setUp(bodyA, pointA, bodyB, pointB, a_springRate, a_dampingConstant, a_restLength);

	sp<SpringDamperI> spSpringDamper = spSpringDamperTest;
	m_springDamperList.push_back(spSpringDamper);
	return spSpringDamper;
}

void ConnectedSystemOde::step(Real dt)
{
	for (uint32_t i = 0; i < m_springDamperList.size(); i++)
	{
		m_springDamperList[i]->step();
	}

	dReal ddt = dReal(dt);
	dWorldStep(m_worldID, ddt);
}

void ConnectedSystemOde::setGravity(const SpatialVector& gravity)
{
	dWorldSetGravity(m_worldID, dReal(gravity[0]), dReal(gravity[1]), dReal(gravity[2]));
}

void ConnectedSystemOde::setGlobalCFM(const Real cfm)
{
	dWorldSetCFM(m_worldID, dReal(cfm));
}

void ConnectedSystemOde::setGlobalERP(const Real erp)
{
	dWorldSetERP(m_worldID, dReal(erp));
}

void ConnectedSystemOde::setGlobalLinearDamping(const Real damping)
{
	dWorldSetLinearDamping(m_worldID, dReal(damping));
	m_linearDamping = damping;
	for (uint32_t i = 0; i < m_rigidBodyList.size(); i++)
	{
		m_rigidBodyList[i]->setLinearDamping(damping);
	}
}

void ConnectedSystemOde::setGlobalAngularDamping(const Real damping)
{
	dWorldSetAngularDamping(m_worldID, dReal(damping));
	m_angularDamping = damping;
	for (uint32_t i = 0; i < m_rigidBodyList.size(); i++)
	{
		m_rigidBodyList[i]->setAngularDamping(damping);
	}
}

std::vector<sp<RigidBodyI>>	ConnectedSystemOde::getRigidBodies()
{
	return m_rigidBodyList;
}

}
