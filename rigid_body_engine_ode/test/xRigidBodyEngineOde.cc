/** @file */

#include <rigid_body_engine/rigid_body_engine.h>

using namespace fe;
using namespace hive;

int main(int argc, char** argv)
{
	UNIT_START();

	BWORD complete = FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry = spMaster->registry();

		Result result = spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));

		{
			// Test creation
			sp<JointI> spJointI(spRegistry->create("JointI.*.*.ode"));
			sp<RigidBodyI> spRigidBodyI(
					spRegistry->create("RigidBodyI.*.*.ode"));
			sp<ConnectedSystemI> spConnectedSystemI(
					spRegistry->create("ConnectedSystemI.ConnectedSystemOde"));
			if (!spJointI.isValid() || !spRigidBodyI.isValid() ||
					!spConnectedSystemI.isValid())
			{
				feX(argv[0], "couldn't create components");
			}

			// Test functionality
			SpatialVector sv = { 1, 1, 1 };
			SpatialVector sv2 = { 0, 0, 0 };
			sp<RigidBodyI> spRB1 =
					spConnectedSystemI->createRigidBody(1, sv, 1);
			sp<RigidBodyI> spRB2 =
					spConnectedSystemI->createRigidBody(9, sv2, 0.5);
			sp<JointI> spJ =
					spConnectedSystemI->createBallJoint(spRB1, spRB2, sv, sv2);

			feLog("done\n");
		}

		complete = TRUE;
	}
	catch (Exception & e) { e.log(); }
	catch (...) { feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(3);
	UNIT_RETURN();
}
