/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_ode_JointOde_h__
#define __rigid_body_engine_ode_JointOde_h__

using namespace fe;

namespace hive
{

class FE_DL_EXPORT JointOde:
	virtual public JointI,
	public fe::CastableAs<JointOde>
{
public:
	JointOde();
	~JointOde();

	/// These take the positions (and orientation, for Hinge joint) in coordinates relative to each of the rigid bodies.
	void		specifyBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
						const SpatialVector pivotInA, const SpatialVector pivotInB,
						const sp<EngineCommonOde> spCommon);
	void		specifyHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
						const SpatialVector pivotInA, const SpatialVector pivotInB,
						const SpatialVector axisInA, const SpatialVector axisInB,
						const sp<EngineCommonOde> spCommon);

	/// These take the positions (and orientation, for Hinge joint) in world coordinates.
	void		specifyBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
						const SpatialVector pivotPos, const sp<EngineCommonOde> spCommon);
	void		specifyHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
						const SpatialVector pivotPos, const SpatialVector axisDir, const sp<EngineCommonOde> spCommon);


	void		limitRotation(Real angleLo, Real angleHi);
	void		setSpring(Real timestep, Real stiffness, Real damping);

	void		setCFM(Real cfm);
	void		setStopCFM(Real cfm);
	void		setStopERP(Real erp);
	void		setParam(int param, Real value);

private:
	dJointID			m_jointID;
	String 				m_jointType;
	dWorldID			m_worldID;
	sp<EngineCommonOde>	m_spCommon;
};

} /* namespace hive */

#endif /* __rigid_body_engine_ode_JointOde_h__ */
