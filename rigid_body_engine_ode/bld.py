import sys
import os.path

forge = sys.modules["forge"]

def prerequisites():
	return [ "eigen", "rigid_body_engine" ]

def setup(module):
	ode = module.Module( 'ode' )
	module.AddDep(ode)

	srcList = [	"rigid_body_engine_ode.pmh",
				"EngineCommonOde",
				"JointOde",
				"RigidBodyOde",
				"ConnectedSystemOde",
				"rigid_body_engine_odeDL" ]

	dll = module.DLL( "hiveRigidBodyEngineOdeDL", srcList )

	deplibs = forge.basiclibs + [ "fexGeometryDLLib",
								  "fexDataToolLib",
								  "feDataLib",
								  "fePluginLib",
								  "feMathLib",
								  "hiveRigidBodyEngineDLLib",
								  "odeMainLib",
								  "odeJointsLib",
								  "odeOuLib",
								  "odeOpCodeLib",
								  "odeIceLib",
								  "odeCcdLib"]

	forge.deps( ["hiveRigidBodyEngineOdeDLLib"], deplibs )

	module.includemap = { 'ode' : os.path.join(module.modPath,'ode','include') }

	module.Module('test')
