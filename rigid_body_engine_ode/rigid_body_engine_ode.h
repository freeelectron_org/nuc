/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_ode_h__
#define __rigid_body_engine_ode_h__

#define dDOUBLE

#include "fe/plugin.h"
#include "fe/data.h"
#include "ode/ode.h"
#include "eigen3/Eigen/Eigenvalues"
//#include "eigen3/Eigen/src/Core/Matrix.h"

#include "rigid_body_engine/rigid_body_engine.h"

#endif // __rigid_body_engine_ode_h__
