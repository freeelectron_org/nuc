/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "apiary/apiary.h"
#include "UdpClient.h"

using namespace fe;
using namespace fe::ext;

namespace hive
{

extern sp<Master> gMaster;

//-----------------------------------------------------------------
UdpClient::UdpClient() : m_isInit(false), m_readEnabled(true)
{
}

//-----------------------------------------------------------------
bool UdpClient::init(const std::string &topic, const char *IPaddress, const int port, const int bufferSize)
{
	if(m_isInit || !m_readEnabled)
	{
		return true;
	}

	feLog("UdpClient::init using address %s:%d\n", IPaddress, port);

	m_readEnabled = false; // Disable reading less init is successful

	if(!m_messageIn.isValid())
	{
		if(!gMaster.isValid())
		{
			gMaster = new Master();
		}
		sp<Registry> spRegistry = gMaster->registry();

		Result result = spRegistry->manage("hiveMessageDL");
		if(!successful(result))
		{
			feLog("UdpClient::init: Error loading hiveMessageDL\n");
			return false;
		}

		m_messageIn = spRegistry->create("MessageI.MessageUDP");

		if(!m_messageIn.isValid())
		{
			feLog("UdpClient::init: create failed\n");
			return false;
		}

		m_isInit = m_messageIn->postInit();

		if(!m_isInit)
		{
			feLog("UdpClient::init: postInit failed\n");
			return false;
		}
	}

	m_isInit = m_messageIn->bind(IPaddress, port);

	if(!m_isInit)
	{
		feLog("UdpClient::init bind failed\n");
		return false;
	}

	m_isInit = m_messageIn->setReceiveBufferSize(bufferSize);

	if(!m_isInit)
	{
		feLog("UdpClient::init setReceiveBufferSize failed\n");
		return false;
	}

	m_readEnabled = true; // Enable reading

	return m_isInit;
}

//-----------------------------------------------------------------
void UdpClient::shutdown()
{
	if(m_messageIn.isValid() && m_isInit)
	{
		m_messageIn->shutdown();
		m_isInit = false;
	}
}

//-----------------------------------------------------------------
int UdpClient::readMessage(std::vector<char> &message )
{
	if(!m_readEnabled || !m_isInit)
	{
		return 0;
	}

	m_msg.dataLen = 0;

	if( m_messageIn->recvFrom(&m_msg) && m_msg.dataLen != 0)
	{
		message.resize(m_msg.dataLen);
		memcpy(message.data(), m_msg.data, m_msg.dataLen );
	}

	return m_msg.dataLen;
}

//------------------------------------------------------------------------------
int UdpClient::readMessage(std::vector<char> &message,
							const long seconds,
							const long uSeconds )
{
	if(!m_readEnabled || !m_isInit)
	{
		return 0;
	}

	m_msg.dataLen = 0;

	if(m_messageIn->recvFrom(&m_msg, seconds, uSeconds) && m_msg.dataLen != 0)
	{
		message.resize(m_msg.dataLen);
		memcpy(message.data(), m_msg.data, m_msg.dataLen );
	}

	return m_msg.dataLen;
}

} // namespace hive
