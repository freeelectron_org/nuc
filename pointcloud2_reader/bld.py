import os
import sys
import string
forge = sys.modules["forge"]

# zeromq on Windows:
# install vcpkg in your forge.windows_local
# see https://github.com/Microsoft/vcpkg/
# vcpkg.exe install zeromq:x86-windows-static
# vcpkg.exe install zeromq:x64-windows-static

def setup(module):
	srcList = [	"UdpClient",
				"ZeroMQSubscriber",
				"PointCloud2",
				"PointCloud2Reader",
				"PointCloud2Reader.pmh",
				"PointCloud2ReaderDL" ]

	dll = module.DLL( "PointCloud2ReaderDL", srcList )

	deplibs = forge.corelibs + [
				"fexSignalLib",
				"fexNetworkDLLib" ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [ "fexDataToolLib" ]

	forge.objlists['pointcloud2reader'] = module.SrcToObj(srcList)

	forge.deps( ["PointCloud2ReaderDLLib"], deplibs )

def auto(module):
	if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
		forge.linkmap["zmq_libs"] = "libzmq-mt-s-4_3_3.lib Iphlpapi.lib"
	else:
		forge.linkmap["zmq_libs"] = "-lzmq"
