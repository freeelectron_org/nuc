/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include <stdint.h>
#include "SimpleBinStream.h"
#include "PointCloud2I.h"

namespace PointCloud2
{

//------------------------------------------------------------------------------
enum PointFieldDataType
{
    INT8    = 1,
    UINT8   = 2,
    INT16   = 3,
    UINT16  = 4,
    INT32   = 5,
    UINT32  = 6,
    FLOAT32 = 7,
    FLOAT64 = 8
};

//------------------------------------------------------------------------------
//  Standard metadata for higher-level stamped data types.
//  This is generally used to communicate timestamped data
//  in a particular coordinate frame.
//------------------------------------------------------------------------------
struct MsgHeader
{
    uint32_t mSequence;     // sequence ID: consecutively increasing ID
    struct TimeStamp        // Two-integer timestamp that is expressed as:
    {
        uint64_t sec;       //  - stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
        uint64_t nsec;      //  - stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    } mTimeStamp;

    std::string mFrameId;   // Frame this data is associated with

    void Serialize(simple::mem_ostream<std::false_type> &outputStream);
    void Deserialize(simple::mem_istream<std::false_type> &inputStream);
};

//------------------------------------------------------------------------------
struct PointField
{
    std::string mName;  // Name of field
    uint32_t mOffset;   // Offset from start of point struct
    uint8_t mDataType;  // Datatype enumeration, see above
    uint32_t mCount;    // How many elements in the field

    void Serialize(simple::mem_ostream<std::false_type> &outputStream);
    void Deserialize(simple::mem_istream<std::false_type> &inputStream);
};

//------------------------------------------------------------------------------
struct CloudPoint : public CloudPointI
{
    void Serialize(simple::mem_ostream<std::false_type> &outputStream);
    void Deserialize(simple::mem_istream<std::false_type> &inputStream);
};

//------------------------------------------------------------------------------
struct PointCloud2Lidar
{
    MsgHeader mHeader;

    // 2D structure of the point cloud. If the cloud is unordered, height is
    // 1 and width is the length of the point cloud.
    uint32_t mHeight = 1;
    uint32_t mWidth;

    PointField mFields[4] = { {"x", 0, (uint8_t)PointFieldDataType::FLOAT32, 1},
                              {"y", 4, (uint8_t)PointFieldDataType::FLOAT32, 1},
                              {"z", 8, (uint8_t)PointFieldDataType::FLOAT32, 1},
                              {"intensity", 12, (uint8_t)PointFieldDataType::UINT8, 1} };
    bool mIsBigendian = false;  // Is this data bigendian?
    uint32_t mPointStep = 13;   // Length of a point in bytes
    uint32_t mRowStep;          // Length of a row in bytes
    CloudPoint *mCloudData = nullptr;     // Actual point data, size is (mRowStep*mHeight)
    bool mIsDense = true;

    PointCloud2Lidar();
    ~PointCloud2Lidar();

    void Serialize(simple::mem_ostream<std::false_type> &outputStream);
    void Deserialize(simple::mem_istream<std::false_type> &inputStream);
};

} // namespace PointCloud2
