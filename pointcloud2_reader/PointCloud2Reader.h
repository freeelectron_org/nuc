/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include "apiary/apiary.h"
#include "PointCloud2.h"

#include "PointCloudSubscriberI.h"
#include "PointCloud2ReaderI.h"

namespace hive
{

//------------------------------------------------------------------------------
class FE_DL_EXPORT PointCloud2Reader : virtual public PointCloud2ReaderI
{
    public:
        PointCloud2Reader();
        ~PointCloud2Reader();

        virtual bool init(const char *receiveIPAddress,
						  const int receivePort,
						  const bool useUDP) override;

		virtual void shutdown() override;

        virtual void read(PointCloud2::CloudPointI* cloudPoints,
						  int &numPoints, long seconds,
						  long uSeconds) override;

    private:
        PointCloudSubscriberI *m_subscriber;
        std::vector<char> m_buffer;
};

} // namespace hive
