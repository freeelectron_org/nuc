/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include "apiary/apiary.h"
#include "PointCloud2I.h"

#include <string.h>

namespace hive
{

//------------------------------------------------------------------------------
class FE_DL_EXPORT PointCloud2ReaderI : virtual public fe::Component,
										public fe::CastableAs<PointCloud2ReaderI>
{
    public:
        virtual bool init(const char *receiveIPAddess,
						  const int receivePort,
						  const bool useUDP) = 0;

        virtual void read(PointCloud2::CloudPointI* cloudPoints,
						  int &numPoints, long seconds, long uSeconds) = 0;
		virtual void shutdown() = 0;
};

} // namespace hive
