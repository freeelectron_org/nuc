/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include "PointCloudSubscriberI.h"
#include "message/message.h"

namespace hive
{

//------------------------------------------------------------------------------
class UdpClient : public PointCloudSubscriberI
{
public:
	UdpClient();
	virtual ~UdpClient() {};

	virtual bool init(const std::string &topic, const char *IPaddress,
					  const int port, const int bufferSize) override;

	virtual void shutdown() override;

	virtual int readMessage(std::vector<char> &message) override;

	virtual int readMessage(std::vector<char> &message,
							const long seconds,
							const long uSeconds) override;

protected:

	bool m_readEnabled;
	bool m_isInit;
	fe::ext::Messagegram m_msg;

	fe::sp<fe::ext::MessageI> m_messageIn;
};

} // namespace hive
