/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_dynamics.pmh"

using namespace fe;
using namespace fe::ext;

#define HIVE_SAMPLER_VERBOSE	TRUE

namespace hive
{

TrackSampler::TrackSampler(void)
{
}

TrackSampler::~TrackSampler(void)
{
}

void TrackSampler::initialize(void)
{
	m_spHiveHost=HiveHost::create();
	if(m_spHiveHost.isNull())
	{
		feLog("TrackSampler::handle no HiveHost\n");
		return;
	}

	m_spStateCatalog=m_spHiveHost->accessSpace("world");
}

void TrackSampler::handle(fe::Record& a_signal)
{
#if HIVE_SAMPLER_VERBOSE
	feLog("TrackSampler::handle layout \"%s\"\n",
			a_signal.layout()->name().c_str());
#endif

	if(!m_asVehicle.bind(a_signal))
	{
		feLog("TrackSampler::handle not AsVehicle\n");
		return;
	}

	const F64 deltaTime=m_asVehicle.deltaTime(a_signal);
	if(deltaTime<=0.0)
	{
		return;
	}

	sp<SurfaceI> spSurfaceTrack=
			sp<SurfaceI>(m_asVehicle.surfaceTrack(a_signal));
	if(spSurfaceTrack.isNull())
	{
#if HIVE_SAMPLER_VERBOSE
		feLog("TrackSampler::handle no track surface\n");
#endif
		return;
	}

	if(m_spImageI.isNull())
	{
		m_spImageI=registry()->create("ImageI");
		if(m_spImageI.isValid())
		{
			//* HACK could be multiple paths; presumes image with USD files
			String usdPath;
			Result result=m_spStateCatalog->getState<String>(
					"paths.usd",usdPath);

			String texturefilename;
			result=m_spStateCatalog->getState<String>(
					"track.textures.0.filename",texturefilename);

			const String fullName=usdPath+"/"+texturefilename;
			feLog("LOAD \"%s\"\n",fullName.c_str());

			const I32 imageID=
					m_spImageI->loadSelect(usdPath+"/"+texturefilename);
#if HIVE_SAMPLER_VERBOSE
			feLog("TrackSampler::handle imageID %d\n",imageID);
#endif

			if(imageID<0)
			{
				feLog("TrackSampler::handle"
						" failed to load texture \"%s\"\n",
						texturefilename.c_str());
			}
		}
	}

	if(m_spImageI.isNull())
	{
#if HIVE_SAMPLER_VERBOSE
		feLog("TrackSampler::handle no texture\n");
#endif
		return;
	}

	const Matrix3x4d xformChassis=m_asVehicle.chassisTransform(a_signal);

	//* TODO each wheel
	const Real maxDistance(4);
	sp<SurfaceI::ImpactI> spImpact=
			spSurfaceTrack.asConst()->nearestPoint(
			xformChassis.translation(),maxDistance);

	const Vector2 uv=spImpact->uv();
#if HIVE_SAMPLER_VERBOSE
	feLog("TrackSampler::handle uv %s\n",c_print(uv));
#endif

	ImageI::Format format=m_spImageI->format();
#if HIVE_SAMPLER_VERBOSE
	feLog("TrackSampler::handle format %d\n",format);
#endif

	if(format!=ImageI::e_rgb)
	{
#if HIVE_SAMPLER_VERBOSE
		feLog("TrackSampler::handle not RGB\n");
#endif
		return;
	}

	const U32 imageWidth=m_spImageI->width();
	const U32 imageHeight=m_spImageI->height();

	const U32 ix=uv[0]*(imageWidth-1);
	const U32 iy=uv[1]*(imageHeight-1);

	const U8* pData=(U8*)m_spImageI->raw();
	const U8* pBytes=&pData[(iy*imageWidth+ix)*3];

	feLog("TrackSampler::handle bytes %d %d %d\n",
			pBytes[0],pBytes[1],pBytes[2]);
}

}
