/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef hive_locomotion_h
#define hive_locomotion_h

namespace hive
{

class FE_DL_EXPORT Locomotion:
	virtual public fe::ext::HandlerI,
	public fe::Initialize<Locomotion>
{
public:
				Locomotion(void);
virtual			~Locomotion(void)											{}

		void	initialize(void);

virtual	void	bind(fe::sp<fe::ext::SignalerI> spSignalerI,
					fe::sp<fe::Layout> spLayout)							{}
virtual	void	handle(fe::Record& a_signal);

private:
		void	tick(float_t a_deltaTime);

	fe::sp<HiveHost>			m_spHiveHost;
	fe::sp<fe::StateCatalog>	m_spStateCatalog;
	BWORD						m_started;

	AsObstacle					m_asObstacle;
};

} // namespace hive

#endif
