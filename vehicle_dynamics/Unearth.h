/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef hive_unearth_h
#define hive_unearth_h

namespace hive
{

class FE_DL_EXPORT Unearth:
	virtual public fe::ext::HandlerI,
	public fe::Initialize<Unearth>
{
public:
				Unearth(void);
virtual			~Unearth(void)												{}

		void	initialize(void);

virtual	void	bind(fe::sp<fe::ext::SignalerI> spSignalerI,
					fe::sp<fe::Layout> spLayout)							{}
virtual	void	handle(fe::Record& a_signal);

private:

	fe::sp<HiveHost>			m_spHiveHost;
	fe::sp<fe::StateCatalog>	m_spStateCatalog;

	AsVehicle					m_asVehicle;
};

} // namespace hive

#endif
