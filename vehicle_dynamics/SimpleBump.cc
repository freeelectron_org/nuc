/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_dynamics.pmh"

using namespace fe;
using namespace fe::ext;

#define HIVE_BUMP_VERBOSE	FALSE

namespace hive
{

SimpleBump::SimpleBump(void):
	m_started(FALSE)
{
	setIdentity(m_chassis);
}

void SimpleBump::initialize(void)
{
	m_spHiveHost=HiveHost::create();
	if(m_spHiveHost.isNull())
	{
		feLog("SimpleBump::handle no HiveHost\n");
		return;
	}

	m_spStateCatalog=m_spHiveHost->accessSpace("world");
}

void SimpleBump::handle(fe::Record& a_signal)
{
	if(m_spStateCatalog.isNull())
	{
		feLog("SimpleBump::handle no StateCatalog\n");
		return;
	}

	if(!m_asVehicle.bind(a_signal))
	{
		feLog("SimpleBump::handle not AsVehicle\n");
		return;
	}

	if(!m_started)
	{
		m_vehicleID=m_asVehicle.vehicleID(a_signal);

		const String prefixN="vehicles."+m_vehicleID+".";

		m_spStateCatalog->getState<Real>(prefixN+"mass",m_mass);

		m_started=TRUE;
	}

	m_chassis=m_asVehicle.chassisTransform(a_signal);
	if(magnitude(m_chassis.direction())<0.99)
	{
		feLog("SimpleBump::handle chassis transform not set\n");
		return;
	}

	m_velocity=m_asVehicle.centerOfMassVelocity(a_signal);
	m_vehicleID=m_asVehicle.vehicleID(a_signal);

	const Real deltaTime=m_asVehicle.deltaTime(a_signal);
	tick(deltaTime);

	m_asVehicle.impulse(a_signal)=m_impulse;
	m_asVehicle.angularImpulse(a_signal)=m_angularImpulse;

//	m_asVehicle.adsTarget0(a_signal)=transformVector(m_chassis,m_pivot);
}

void SimpleBump::tick(float_t a_deltaTime)
{
	//* NOTE DB2 bounds
	const Box3d alignedBox(
			Vector3d(-2.3,-0.95,0.0),
			Vector3d(4.75,1.9,1.0));
	const Real radius(2.6);

	set(m_impulse);
	set(m_angularImpulse);

	Array<String> keys;
	m_spStateCatalog->getStateKeys(keys,"vehicles\\.[^.]*\\.mass$");

	Matrix3x4d invChassis;
	invert(invChassis,m_chassis);

	const Vector3d localVelocity=
			rotateVector(invChassis,m_velocity);

#if HIVE_BUMP_VERBOSE
	feLog("SimpleBump::tick vel %s local %s\n",
			c_print(m_velocity),c_print(localVelocity));
#endif

	const I32 keyCount=keys.size();
	for(I32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		String key=keys[keyIndex];
		key.parse("",".");
		const String otherID=key.parse("",".");

		if(otherID==m_vehicleID)
		{
			continue;
		}

		const String prefixOther="vehicles."+otherID+".";

		//* NOTE other car transformed relative to local car

		SpatialTransform otherXform;
		Result result=m_spStateCatalog->getState<SpatialTransform>(
				prefixOther+m_asVehicle.chassisTransform.name(),otherXform);
		if(failure(result))
		{
			continue;
		}

		Matrix3x4d otherChassis(otherXform);

		const Vector3d otherLocation=transformVector(invChassis,
				Vector3d(otherChassis.translation()));
//		const Vector3d otherDirection=
//				rotateVector(invChassis,otherChassis.direction());

		const F64 distance=magnitude(otherLocation);

#if HIVE_BUMP_VERBOSE
		feLog("SimpleBump::tick"
				" vehicleID \"%s\" otherID %d/%d \"%s\" distance %.3f\n",
				m_vehicleID.c_str(),keyIndex,keyCount,otherID.c_str(),
				distance);
#endif

		if(distance>2.0*radius)
		{
			continue;
		}

		SpatialVector otherVelocityWorld;
		m_spStateCatalog->getState<SpatialVector>(prefixOther+"velocity",
				otherVelocityWorld);
		const Vector3d otherVelocity=
				rotateVector(invChassis,Vector3d(otherVelocityWorld));

		Real otherMass(0);
		m_spStateCatalog->getState<Real>(prefixOther+"mass",otherMass);

#if HIVE_BUMP_VERBOSE
		feLog("SimpleBump::tick other loc %s vel %s\n",
				c_print(otherLocation),c_print(otherVelocity));
#endif

		Matrix3x4d invOther;
		invert(invOther,otherChassis);

		Matrix3x4d otherToLocal=otherChassis*invChassis;
		Matrix3x4d localToOther=m_chassis*invOther;

		Vector3d pivotSum(0.0,0.0,0.0);
		F64 weightSum(0.0);

		Vector3d corners[4];
		set(corners[0],alignedBox[0],alignedBox[1],0.5);
		set(corners[1],alignedBox[0]+width(alignedBox),alignedBox[1],0.5);
		set(corners[2],alignedBox[0],alignedBox[1]+height(alignedBox),0.5);
		set(corners[3],corners[1][0],corners[2][1],0.5);

		//* other car's points inside our bounds
		for(I32 pass=0;pass<2;pass++)
		{
			const Matrix3x4d& rConversion=pass? localToOther: otherToLocal;
			for(I32 index=0;index<4;index++)
			{
				const Vector3d localPoint=
						transformVector(rConversion,corners[index]);
				if(!intersecting(alignedBox,localPoint))
				{
					continue;
				}

				F64 maxDepth(0);
				maxDepth=fe::maximum(maxDepth,localPoint[0]-corners[0][0]);
				maxDepth=fe::maximum(maxDepth,corners[1][0]-localPoint[0]);
				maxDepth=fe::maximum(maxDepth,localPoint[1]-corners[0][1]);
				maxDepth=fe::maximum(maxDepth,corners[2][1]-localPoint[1]);

				pivotSum+=(pass? corners[index]: localPoint)*maxDepth;
				weightSum+=maxDepth;
#if HIVE_BUMP_VERBOSE
				feLog("SimpleBump::tick hit %d %d  %s  %.3f\n",
						pass,index,c_print(localPoint),maxDepth);
#endif
			}
		}

		if(weightSum<1e-6)
		{
#if HIVE_BUMP_VERBOSE
			feLog("SimpleBump::tick no intersections\n");
#endif
			continue;
		}

		const Vector3d pivot=pivotSum/weightSum;

		const Vector3d towards=unitSafe(pivot-otherLocation);

		const Vector3d localVelocityTowards=
				towards*dot(towards,localVelocity);
//		const Vector3d localVelocityAside=
//				localVelocity-localVelocityTowards;

		const Vector3d otherVelocityTowards=
				towards*dot(towards,otherVelocity);
		const Vector3d otherVelocityAside=
				otherVelocity-otherVelocityTowards;

		const F64 diffVelocityTowardsMag=
				dot(towards,otherVelocity-localVelocity);

#if HIVE_BUMP_VERBOSE
		feLog("SimpleBump::tick towards %s velT %s velA %s\n",
				c_print(towards),
				c_print(otherVelocityTowards),c_print(otherVelocityAside));
#endif

		if(diffVelocityTowardsMag>0.0)
		{
			const F64 restitution(0.8);

			// v1' = (m1-m2)/(m1+m2)*v1 + (2*m2)/(m1+m2)*v2

			const F64 sumMass=m_mass+otherMass;
			const Vector3d newVelocityTowards=
					localVelocityTowards*(m_mass-otherMass)/sumMass+
					otherVelocityTowards*(2.0*otherMass)/sumMass;

			m_impulse+=restitution*m_mass*rotateVector(m_chassis,
					newVelocityTowards-localVelocityTowards);

			//* HACK
			const Vector3d arm0= -towards*radius;
			const Vector3d arm1=arm0+otherVelocityAside;

			const F64 twist=cross(unitSafe(arm0),unitSafe(arm1))[2];
			m_angularImpulse*=Quaterniond(twist,e_zAxis);

#if HIVE_BUMP_VERBOSE
			feLog("SimpleBump::tick m_impulse %s local %s\n",
					c_print(m_impulse),
					c_print(newVelocityTowards-localVelocityTowards));
			feLog("SimpleBump::tick m_angularImpulse %s twist %.6G\n",
					c_print(m_angularImpulse),twist);
#endif

			m_pivot=pivot;
		}
	}
}

}
