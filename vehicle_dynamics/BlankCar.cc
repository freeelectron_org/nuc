/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_dynamics.pmh"

using namespace fe;
using namespace fe::ext;

namespace hive
{

BlankCar::BlankCar()
{
	m_throttle = 0;
	m_brake = 0;
	m_steering = 0;

	setIdentity(m_chassis);
	setIdentity(m_wheels[0]);
	setIdentity(m_wheels[1]);
	setIdentity(m_wheels[2]);
	setIdentity(m_wheels[3]);

	set(m_centerOfMass);
	set(m_centerOfMassVelocity);
	set(m_angularVelocity);
	setIdentity(m_axlesRearTransform);
}

void BlankCar::initialize(void)
{
	m_spSurfaceTrack=registry()->create("*.SurfaceTrack");
}

void BlankCar::handle(fe::Record& a_signal)
{
	if(!m_asVehicle.bind(a_signal))
	{
		feLog("BlankCar::handle not AsVehicle\n");
		return;
	}

	m_throttle=m_asVehicle.throttleInput(a_signal);
	m_brake=m_asVehicle.brakeInput(a_signal);
	m_steering=m_asVehicle.steeringFInput(a_signal);

	const Real deltaTime=m_asVehicle.deltaTime(a_signal);
	tick(deltaTime);

	m_asVehicle.chassisTransform(a_signal)=m_chassis;
	m_asVehicle.wheelFLTransform(a_signal)=m_wheels[0];
	m_asVehicle.wheelFRTransform(a_signal)=m_wheels[1];
	m_asVehicle.wheelRLTransform(a_signal)=m_wheels[2];
	m_asVehicle.wheelRRTransform(a_signal)=m_wheels[3];

	m_asVehicle.centerOfMassOffset(a_signal)=m_centerOfMass;
	m_asVehicle.centerOfMassVelocity(a_signal)=m_centerOfMassVelocity;
	m_asVehicle.centerOfMassAngularVelocity(a_signal)=m_angularVelocity;
	m_asVehicle.axlesRearTransform(a_signal)=m_axlesRearTransform;
}

void BlankCar::tick(float_t a_deltaTime)
{
	//* TODO do physics

//	m_throttle
//	m_brake
//	m_steering

//	sp<SurfaceI::ImpactI> spImpact=
//			m_spSurfaceTrack->nearestPoint(fromVector3);

	setIdentity(m_chassis);
	setIdentity(m_wheels[0]);
	setIdentity(m_wheels[1]);
	setIdentity(m_wheels[2]);
	setIdentity(m_wheels[3]);

	set(m_centerOfMass);
	set(m_centerOfMassVelocity);
	set(m_angularVelocity);
	setIdentity(m_axlesRearTransform);
}

}
