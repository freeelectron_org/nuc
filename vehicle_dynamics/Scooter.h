/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef scooter_vehicle_dynamics_H
#define scooter_vehicle_dynamics_H

namespace hive
{

class AsScooter
	: public fe::ext::AsNamed, public fe::Initialize<AsScooter>
{
	public:
		AsConstruct(AsScooter);
		void initialize(void)
		{
			add(FL,				FE_USE("scooter:tire:FL"));
			add(FR,				FE_USE("scooter:tire:FR"));
			add(RL,				FE_USE("scooter:tire:RL"));
			add(RR,				FE_USE("scooter:tire:RR"));
		}
		fe::Accessor<Record>			FL;
		fe::Accessor<Record>			FR;
		fe::Accessor<Record>			RL;
		fe::Accessor<Record>			RR;
};

class FE_DL_EXPORT Scooter:
	virtual public fe::ext::HandlerI,
	public Initialize<Scooter>
{
public:
				Scooter(void);
virtual			~Scooter(void);

		void	initialize(void);

virtual	void	bind(fe::sp<fe::ext::SignalerI> spSignalerI,
					fe::sp<fe::Layout> spLayout)							{}
virtual	void	handle(fe::Record& a_signal);

private:
		void	tick(Real a_deltaTime);
static	F64		compassModulus(F64 a_heading,F64 a_minimum);

		void	errorSound(void);

	fe::sp<HiveHost>			m_spHiveHost;
	fe::sp<fe::StateCatalog>	m_spStateCatalog;
	BWORD						m_started;
	String						m_vehicleID;
	BWORD						m_useFreeBody;
	BWORD						m_useWeightTransfer;
	BWORD						m_useTireI;

	fe::Matrix3x4d				m_lastDeltaTransform;
	fe::Matrix3x4d				m_lastChassis;
	fe::Matrix3x4d				m_chassis;
	fe::Matrix3x4d				m_lastwheels[4];
	fe::Matrix3x4d				m_wheels[4];

	Real						m_mass;
	Real						m_mass_moi_z;
	Real						m_massRatioFront;
	Real						m_motorTorqueMaxFL;
	Real						m_motorTorqueMaxFR;
	Real						m_motorTorqueMaxRL;
	Real						m_motorTorqueMaxRR;
	Real						m_motorPowerLimitFL;
	Real						m_motorPowerLimitFR;
	Real						m_motorPowerLimitRL;
	Real						m_motorPowerLimitRR;
	Real						m_motorGearRatioFL;
	Real						m_motorGearRatioFR;
	Real						m_motorGearRatioRL;
	Real						m_motorGearRatioRR;
	Real						m_steeringAngleMaxF;
	fe::Vector3d				m_centerOfMassOffset;
	fe::Vector3d				m_wheelOffsetFL;
	fe::Vector3d				m_wheelOffsetFR;
	fe::Vector3d				m_wheelOffsetRL;
	fe::Vector3d				m_wheelOffsetRR;
	Real						m_tireStiffnessFL;
	Real						m_tireStiffnessFR;
	Real						m_tireStiffnessRL;
	Real						m_tireStiffnessRR;
	Real						m_tireFrictionCoeffFL;
	Real						m_tireFrictionCoeffFR;
	Real						m_tireFrictionCoeffRL;
	Real						m_tireFrictionCoeffRR;
	Real						m_dragRotational;
	Real						m_dragRotationalFade;
	Real						m_accelerationScale;
	Real						m_decelerationScale;
	Real						m_gLateralLimit;

	F64							m_speed;
	F64							m_headingCCW;
	F64							m_travelingCCWFront;
	F64							m_travelingCCWRear;
	F64							m_pitchDown;
	F64							m_bankRight;
	F64							m_steeringLeft;
	F64							m_throttle;
	F64							m_brake;
	AsVehicle::GearMode			m_gearMode;
	F64							m_powerScale;
	F64							m_deltaPitch;
	F64							m_deltaBank;
	F64							m_deltaZ;
	F64							m_zAngularVelocity;
	F64							m_tireSpin[4];
	fe::Vector3d				m_location;
	fe::Vector3d				m_velocity;
	fe::Vector3d				m_acceleration;
	fe::Vector3d				m_lastVelocity;
	fe::Vector3d				m_groundNormal[4];

	fe::Vector3d				m_impulse;
	fe::Quaterniond				m_angularImpulse;

	fe::Vector3d				m_probe;

	fe::sp<fe::ext::SurfaceI>	m_spSurfaceTrack;

	AsVehicle					m_asVehicle;

	fe::sp<fe::ext::ListenerI>	m_spListenerI;
	fe::sp<fe::ext::VoiceI>		m_spVoiceError;
	fe::sp<fe::ext::AudioI>		m_spAudioI;

	F64							m_instantaneousTorqueRL;
	F64							m_instantaneousTorqueRR;

	Record						m_r_tire[4];
	fe::ext::AsTireLive			m_asTireLive;
	Real						m_tireStiffnessZ[4];
	fe::sp<fe::ext::TireI>		m_spTire[4];
	F64							m_wheelAngularVelocity[4];
	F64							m_wheelInertia[4];
	Real						m_smoothed;
	Real						m_brakePeak;
	Real						m_wheelDrag;
	Real						m_frequencyHint;
};

} // namespace hive

#endif
