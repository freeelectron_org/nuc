/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_dynamics.pmh"

using namespace fe;
using namespace fe::ext;

#define HIVE_SCOOTER_VERBOSE		FALSE
#define HIVE_SCOOTER_AUDIO			FALSE
#define HIVE_SCOOTER_STOP_WHEN_SLOW	FALSE

namespace hive
{

Scooter::Scooter(void):
	m_started(FALSE),
	m_vehicleID("0"),
	m_useFreeBody(FALSE),
	m_useWeightTransfer(FALSE),
	m_useTireI(FALSE),
	m_mass(1000),
	m_massRatioFront(0.5),
	m_motorTorqueMaxFL(100),
	m_motorTorqueMaxFR(100),
	m_motorTorqueMaxRL(100),
	m_motorTorqueMaxRR(100),
	m_motorPowerLimitFL(100e3),
	m_motorPowerLimitFR(100e3),
	m_motorPowerLimitRL(100e3),
	m_motorPowerLimitRR(100e3),
	m_motorGearRatioFL(1),
	m_motorGearRatioFR(1),
	m_motorGearRatioRL(1),
	m_motorGearRatioRR(1),
	m_steeringAngleMaxF(0.349),
	m_centerOfMassOffset(0,0,0.5),
	m_wheelOffsetFL(1.5,0.75,0.4),
	m_wheelOffsetFR(1.5,-0.75,0.4),
	m_wheelOffsetRL(-1.5,0.75,0.4),
	m_wheelOffsetRR(-1.5,-0.75,0.4),
	m_tireStiffnessFL(10),
	m_tireStiffnessFR(10),
	m_tireStiffnessRL(10),
	m_tireStiffnessRR(10),
	m_tireFrictionCoeffFL(1.0),
	m_tireFrictionCoeffFR(1.0),
	m_tireFrictionCoeffRL(1.0),
	m_tireFrictionCoeffRR(1.0),
	m_dragRotational(2.0),		//* 0+
	m_dragRotationalFade(1.0),	//* 0 to 1 (reduce drag at speed)
	m_accelerationScale(1.0),
	m_decelerationScale(1.0),
	m_gLateralLimit(1.0),
	m_speed(0),
	m_headingCCW(0),
	m_travelingCCWFront(0),
	m_travelingCCWRear(0),
	m_pitchDown(0),
	m_bankRight(0),
	m_steeringLeft(0),
	m_throttle(0),
	m_brake(0),
	m_gearMode(AsVehicle::e_gearModeNeutral),
	m_deltaPitch(0),
	m_deltaBank(0),
	m_deltaZ(0),
	m_zAngularVelocity(0),
	m_location(0,0,0),
	m_velocity(0,0,0),
	m_acceleration(0,0,0),
	m_lastVelocity(0,0,0),
	m_probe(0,0,0),
	m_instantaneousTorqueRL(0.0),
	m_instantaneousTorqueRR(0.0),
	m_brakePeak(5000.0),
	m_wheelDrag(0.0),
	m_frequencyHint(400.0)
{
	setIdentity(m_lastDeltaTransform);
	setIdentity(m_lastChassis);
	setIdentity(m_chassis);
	setIdentity(m_lastwheels[0]);
	setIdentity(m_lastwheels[1]);
	setIdentity(m_lastwheels[2]);
	setIdentity(m_lastwheels[3]);
	setIdentity(m_wheels[0]);
	setIdentity(m_wheels[1]);
	setIdentity(m_wheels[2]);
	setIdentity(m_wheels[3]);

	set(m_groundNormal[0],0,0,1);
	set(m_groundNormal[1],0,0,1);
	set(m_groundNormal[2],0,0,1);
	set(m_groundNormal[3],0,0,1);

	m_tireSpin[0]=0.0;
	m_tireSpin[1]=0.0;
	m_tireSpin[2]=0.0;
	m_tireSpin[3]=0.0;

	m_travelingCCWFront=m_headingCCW;
	m_travelingCCWRear=m_headingCCW;
}

Scooter::~Scooter(void)
{
}

void Scooter::initialize(void)
{
//~	m_spSurfaceTrack=registry()->create("*.SurfaceTrack");

	m_spHiveHost=HiveHost::create();
	if(m_spHiveHost.isNull())
	{
		feLog("Scooter::handle no HiveHost\n");
		return;
	}

	m_spStateCatalog=m_spHiveHost->accessSpace("world");

#if HIVE_SCOOTER_AUDIO
	sp<Registry> spRegistry=registry();
	m_spListenerI=spRegistry->create("ListenerI");
	m_spVoiceError=spRegistry->create("VoiceI");
	m_spAudioI=spRegistry->create("AudioI");
#endif

	if(m_spListenerI.isNull() || m_spAudioI.isNull() ||
			m_spVoiceError.isNull())
	{
		m_spListenerI=NULL;
		m_spVoiceError=NULL;
		m_spAudioI=NULL;
	}

	if(m_spAudioI.isValid())
	{
		String wavPath=
			m_spHiveHost->master()->catalog()->catalog<String>("path:media")+
			"/wav";

		String path;

		path=wavPath+"/hammeronce.wav";
		Result result=m_spAudioI->load("hammer",path);
		if(fe::failure(result))
		{
			feLog("SurfacePuppet::cache failed to load pop sound\n");
		}
	}

	if(m_spVoiceError.isValid())
	{
		m_spVoiceError->setReferenceDistance(30.0f);
		m_spVoiceError->setVolume(0.05f);
	}

}

void Scooter::handle(fe::Record& a_signal)
{
#if HIVE_SCOOTER_VERBOSE
	feLog("Scooter::handle layout \"%s\"\n",
			a_signal.layout()->name().c_str());
#endif

	if(m_spStateCatalog.isNull())
	{
		feLog("Scooter::handle no StateCatalog\n");
		return;
	}

	if(!m_asVehicle.bind(a_signal))
	{
		feLog("Scooter::handle not AsVehicle\n");
		return;
	}

	sp<Registry> spRegistry=registry();
	sp<Master> spMaster=spRegistry->master();

	const BWORD starting=!m_started;
	if(!m_started)
	{
		m_vehicleID=m_asVehicle.vehicleID(a_signal);

		const String prefixN="vehicles."+m_vehicleID+".";

		fe::Vector3d startLocation;
		Result result=m_spStateCatalog->getState<Vector3d>(
				prefixN+"start.location",startLocation);
		m_location=startLocation;

		Real startHeading;
		result=m_spStateCatalog->getState<Real>(
				prefixN+"start.heading",startHeading);
		m_headingCCW=startHeading;

		result=m_spStateCatalog->getState<Real>(
				prefixN+"mass",m_mass);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"mass.moi.z",m_mass_moi_z);

		result=m_spStateCatalog->getState<Real>(
				prefixN+"mass.ratioFront",m_massRatioFront);

		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.FL.torqueMax",m_motorTorqueMaxFL);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.FR.torqueMax",m_motorTorqueMaxFR);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.RL.torqueMax",m_motorTorqueMaxRL);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.RR.torqueMax",m_motorTorqueMaxRR);

		m_asVehicle.motorTorqueMaxRL(a_signal)=m_motorTorqueMaxRL;
		m_asVehicle.motorTorqueMaxRR(a_signal)=m_motorTorqueMaxRR;

		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.FL.powerLimit",m_motorPowerLimitFL);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.FR.powerLimit",m_motorPowerLimitFR);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.RL.powerLimit",m_motorPowerLimitRL);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.RR.powerLimit",m_motorPowerLimitRR);

		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.FL.gearRatio",m_motorGearRatioFL);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.FR.gearRatio",m_motorGearRatioFR);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.RL.gearRatio",m_motorGearRatioRL);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"motors.RR.gearRatio",m_motorGearRatioRR);

		result=m_spStateCatalog->getState<Real>(
				prefixN+"steerings.F.angleMax",m_steeringAngleMaxF);

		SpatialVector offset;
		result=m_spStateCatalog->getState<SpatialVector>(
				prefixN+"mass.center.offset",offset);
		m_centerOfMassOffset=offset;
		result=m_spStateCatalog->getState<SpatialVector>(
				prefixN+"wheels.FL.offset",offset);
		m_wheelOffsetFL=offset;
		result=m_spStateCatalog->getState<SpatialVector>(
				prefixN+"wheels.FR.offset",offset);
		m_wheelOffsetFR=offset;
		result=m_spStateCatalog->getState<SpatialVector>(
				prefixN+"wheels.RL.offset",offset);
		m_wheelOffsetRL=offset;
		result=m_spStateCatalog->getState<SpatialVector>(
				prefixN+"wheels.RR.offset",offset);
		m_wheelOffsetRR=offset;

		result=m_spStateCatalog->getState<bool>(
				prefixN+"dynamics.scooter:useFreeBody",m_useFreeBody);
		result=m_spStateCatalog->getState<bool>(
				prefixN+"dynamics.scooter:useWeightTransfer",
				m_useWeightTransfer);
		result=m_spStateCatalog->getState<bool>(
				prefixN+"dynamics.scooter:useTireI",m_useTireI);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"dynamics.scooter:drag.rotational",
				m_dragRotational);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"dynamics.scooter:drag.rotational.fade",
				m_dragRotationalFade);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"dynamics.scooter:NFB.accelerationScale",
				m_accelerationScale);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"dynamics.scooter:NFB.decelerationScale",
				m_decelerationScale);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"dynamics.scooter:NFB.gLateralLimit",m_gLateralLimit);

		result=m_spStateCatalog->getState<Real>(
				prefixN+"tires.FL.stiffness",m_tireStiffnessFL);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"tires.FR.stiffness",m_tireStiffnessFR);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"tires.RL.stiffness",m_tireStiffnessRL);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"tires.RR.stiffness",m_tireStiffnessRR);

		result=m_spStateCatalog->getState<Real>(
				prefixN+"tires.FL.frictionCoefficient",m_tireFrictionCoeffFL);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"tires.FR.frictionCoefficient",m_tireFrictionCoeffFR);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"tires.RL.frictionCoefficient",m_tireFrictionCoeffRL);
		result=m_spStateCatalog->getState<Real>(
				prefixN+"tires.RR.frictionCoefficient",m_tireFrictionCoeffRR);

		Real startSpeed=0;
		result=m_spStateCatalog->getState<Real>(
				prefixN+"N.start.speed",startSpeed);
		setIdentity(m_chassis);
		rotate(m_chassis, m_headingCCW+(90.0*degToRad), e_zAxis);
		m_velocity=startSpeed*m_chassis.direction();

#if HIVE_SCOOTER_VERBOSE
		feLog("location %s\n",c_print(m_location));
		feLog("heading %.4f\n",m_headingCCW);
		feLog("mass %.4f\n",m_mass);
		feLog("mass.moi.z %.4f\n",m_mass_moi_z);
		feLog("mass.ratioFront %.4f\n",m_massRatioFront);
		feLog("mass.center.offset %s\n",c_print(m_centerOfMassOffset));
		feLog("FL.torqueMax %.4f\n",m_motorTorqueMaxFL);
		feLog("FR.torqueMax %.4f\n",m_motorTorqueMaxFR);
		feLog("RL.torqueMax %.4f\n",m_motorTorqueMaxRL);
		feLog("RR.torqueMax %.4f\n",m_motorTorqueMaxRR);
		feLog("FL.powerLimit %.4f\n",m_motorPowerLimitFL);
		feLog("FR.powerLimit %.4f\n",m_motorPowerLimitFR);
		feLog("RL.powerLimit %.4f\n",m_motorPowerLimitRL);
		feLog("RR.powerLimit %.4f\n",m_motorPowerLimitRR);
		feLog("FL.gearRatio %.4f\n",m_motorGearRatioFL);
		feLog("FR.gearRatio %.4f\n",m_motorGearRatioFR);
		feLog("RL.gearRatio %.4f\n",m_motorGearRatioRL);
		feLog("RR.gearRatio %.4f\n",m_motorGearRatioRR);
		feLog("steerings.F.angleMax %.4f\n",m_steeringAngleMaxF);
		feLog("FL.offset %s\n",c_print(m_wheelOffsetFL));
		feLog("FR.offset %s\n",c_print(m_wheelOffsetFR));
		feLog("RL.offset %s\n",c_print(m_wheelOffsetRL));
		feLog("RR.offset %s\n",c_print(m_wheelOffsetRR));
		feLog("FL.stiffness %.4f\n",m_tireStiffnessFL);
		feLog("FR.stiffness %.4f\n",m_tireStiffnessFR);
		feLog("RL.stiffness %.4f\n",m_tireStiffnessRL);
		feLog("RR.stiffness %.4f\n",m_tireStiffnessRR);
		feLog("FL.frictionCoefficient %.4f\n",m_tireFrictionCoeffFL);
		feLog("FR.frictionCoefficient %.4f\n",m_tireFrictionCoeffFR);
		feLog("RL.frictionCoefficient %.4f\n",m_tireFrictionCoeffRL);
		feLog("RR.frictionCoefficient %.4f\n",m_tireFrictionCoeffRR);
		feLog("useFreeBody %d\n",m_useFreeBody);
		feLog("useWeightTransfer %d\n",m_useWeightTransfer);
		feLog("useTireI %d\n",m_useWeightTransfer);
#endif

		sp<RecordGroup> rg_dataset =
				m_spHiveHost->master()->catalog()->catalog<sp<RecordGroup> >(
				"dataset");
		if(rg_dataset.isNull())
		{
			feLog("Scooter::initialize dataset not available\n");
			m_useTireI=FALSE;
		}
		else if(rg_dataset->scope().isNull())
		{
			feLog("Scooter::initialize dataset is empty\n");
			m_useTireI=FALSE;
		}

		//* provoke autoload, if neccessary
		spRegistry->create("TireI");

		if(m_useTireI)
		{
			const BWORD hasTireI=
					spMaster->typeMaster()->lookupType< sp<TireI> >().isValid();
			if(!hasTireI)
			{
				feLog("Scooter::initialize TireI not available\n");
				m_useTireI=FALSE;
			}
		}

		if(m_useTireI)
		{
			sp<Scope> spScope = rg_dataset->scope();
			AsDataset asDataset(spScope);
			enforce<AsTire,AsTireLive>(rg_dataset);
			enforce<AsTire,AsContactLive>(rg_dataset);
			AsTireLive asTireLive(spScope);
			m_asTireLive.bind(spScope);

			RecordDictionary<AsTireModel> rd_models(rg_dataset);
			AsTireModel asTireModel; asTireModel.bind(spScope);

			std::vector<Record>	scooters;
			AsScooter asScooter;
			asScooter.filter(scooters, rg_dataset);
			Accessor<Real> zStiffness(spScope,"tire:z:stiffness");

			for(I32 m=0;m<4;m++)
			{
				m_wheelAngularVelocity[m] = 0.0;
				m_wheelInertia[m] = 2.0;
			}

			if(scooters.size() > 0) // only care about first scooter
			{
				feLog("Scooter::handle using TireI\n");

				m_r_tire[0] = asScooter.FL(scooters[0]);
				m_r_tire[1] = asScooter.FR(scooters[0]);
				m_r_tire[2] = asScooter.RL(scooters[0]);
				m_r_tire[3] = asScooter.RR(scooters[0]);

				// override dataset radii
				asTireLive.radius(m_r_tire[0]) = m_wheelOffsetFL[2];
				asTireLive.radius(m_r_tire[1]) = m_wheelOffsetFR[2];
				asTireLive.radius(m_r_tire[2]) = m_wheelOffsetRL[2];
				asTireLive.radius(m_r_tire[3]) = m_wheelOffsetRR[2];

				for(unsigned int i = 0; i < 4; i++)
				{
					Record r_model = rd_models[asTireLive.model(m_r_tire[i])];
					sp<TireI> spTire(
						registry()->create(asTireModel.component(r_model)));

					m_tireStiffnessZ[i] = zStiffness(r_model);

					if(spTire.isValid())
					{
						spTire->compile(m_r_tire[i], r_model, rg_dataset);
						asTireLive.spTireI(m_r_tire[i]) = spTire;
						m_spTire[i] = spTire; // direct sp as optimization
						// default to no effective contact:
						asTireLive.contact_radius(m_r_tire[i]) = 1.0e12;
						asTireLive.inclination(m_r_tire[i]) = 0.0;
					}
					else
					{
						feLog("Scooter::handle failed to instance a TireI\n");
						m_useTireI = false;
						break;
					}
				}
			}
			else
			{
				feLog("Scooter::handle disabling TireI option\n");
				m_useTireI = false;
			}
		}

		m_started=TRUE;
	}

#if TRUE
	m_spSurfaceTrack=sp<SurfaceI>(m_asVehicle.surfaceTrack(a_signal));
#else
	if(m_spSurfaceTrack.isNull())
	{
		m_spSurfaceTrack=registry()->create("*.SurfacePlane");
	}
#endif
	if(m_spSurfaceTrack.isNull())
	{
		feLog("Scooter::handle null track\n");
		return;
	}

	const F64 steeringFInput=m_asVehicle.steeringFInput(a_signal);
	m_steeringLeft= -m_steeringAngleMaxF*steeringFInput;

	m_throttle=m_asVehicle.throttleInput(a_signal);
	m_brake=m_asVehicle.brakeInput(a_signal);
	m_gearMode=AsVehicle::GearMode(m_asVehicle.gearModeInput(a_signal));
	m_powerScale=m_asVehicle.powerScale(a_signal);

	if(m_gearMode==AsVehicle::e_gearModePark)
	{
		m_brake=1.0;
	}

	const F64 deltaTime=m_asVehicle.deltaTime(a_signal);

#if HIVE_SCOOTER_VERBOSE
	feLog("Scooter::handle"
			" steering %.3f throttle %.3f brake %.3f gearMode %d speed %.3f\n",
			m_steeringLeft,m_throttle,m_brake,m_gearMode,m_speed);
	feLog("Scooter::handle deltaTime %.3f\n",deltaTime);
#endif

	if(deltaTime<=0.0)
	{
		return;
	}

	//* update state if chassisTransform changes externally
	const Matrix3x4d signalChassis=m_asVehicle.chassisTransform(a_signal);
	if(!starting && !equivalent(signalChassis,m_chassis,1e-3))
	{
		const Eulerd hpb=signalChassis;
		const F64 signalBank=hpb[0];
		const F64 signalPitch=hpb[1];
		const F64 signalHeading=compassModulus(hpb[2]-(90.0*degToRad),0.0);

#if TRUE
		feLog("Scooter::handle external chassis change\n");
		for(I32 m=0;m<12;m++)
		{
			feLog("  %14.8f -> %14.8f\n",
					m_chassis(m%3,m/3),signalChassis(m%3,m/3));
		}

		feLog("  bank %.6G -> %.6G\n",m_bankRight,signalBank);
		feLog("  pitch %.6G -> %.6G\n",m_pitchDown,signalPitch);
		feLog("  heading %.6G -> %.6G\n",m_headingCCW,signalHeading);
#endif

		m_chassis=signalChassis;
		m_location=signalChassis.translation();
		m_bankRight=signalBank;
		m_pitchDown=signalPitch;
		m_headingCCW=signalHeading;

		set(m_lastVelocity);
		set(m_velocity);
		m_zAngularVelocity=0.0;
	}

	m_impulse=m_asVehicle.impulse(a_signal);
	m_angularImpulse=m_asVehicle.angularImpulse(a_signal);
	set(m_asVehicle.impulse(a_signal));
	set(m_asVehicle.angularImpulse(a_signal));

	m_asVehicle.frontSteerAngle(a_signal)=m_steeringLeft;
	m_asVehicle.integratedDeltaTime(a_signal) += deltaTime;
	tick(deltaTime);

#if FALSE
	if(m_vehicleID=="0")
	{
		static F64 totalTime(0);
		totalTime+=deltaTime;
		feLog("deltaTime %7.3f totalTime %7.3f\n",deltaTime,totalTime);
	}
#endif

	m_asVehicle.chassisTransform(a_signal)=m_chassis;
	m_asVehicle.wheelFLTransform(a_signal)=m_wheels[0];
	m_asVehicle.wheelFRTransform(a_signal)=m_wheels[1];
	m_asVehicle.wheelRLTransform(a_signal)=m_wheels[2];
	m_asVehicle.wheelRRTransform(a_signal)=m_wheels[3];

	m_asVehicle.heading(a_signal)=m_headingCCW;
	m_asVehicle.speed(a_signal)=m_speed;

	const F64 tireRadius[4]=
	{
		m_wheelOffsetFL[2],
		m_wheelOffsetFR[2],
		m_wheelOffsetRL[2],
		m_wheelOffsetRR[2]
	};

	if(!m_useTireI)
	{
		for(I32 m=0;m<4;m++)
		{
			const F64 distance=magnitude(m_wheels[m].translation()-
					m_lastwheels[m].translation());

			//* circum = 2*pi*radius => angle = distance / radius
			m_wheelAngularVelocity[m]=
				distance*(1.0/tireRadius[m])*(1.0/deltaTime);
		}
	}

	m_asVehicle.wheelFLAngularVelocity(a_signal)=m_wheelAngularVelocity[0];
	m_asVehicle.wheelFRAngularVelocity(a_signal)=m_wheelAngularVelocity[1];
	m_asVehicle.wheelRLAngularVelocity(a_signal)=m_wheelAngularVelocity[2];
	m_asVehicle.wheelRRAngularVelocity(a_signal)=m_wheelAngularVelocity[3];

	m_asVehicle.motorTorqueRL(a_signal)=m_instantaneousTorqueRL;
	m_asVehicle.motorTorqueRR(a_signal)=m_instantaneousTorqueRR;

#if FALSE
	feLog("wheelFLAngularVelocity %.6G\n",
			m_asVehicle.wheelFLAngularVelocity(a_signal));
	feLog("wheelFRAngularVelocity %.6G\n",
			m_asVehicle.wheelFRAngularVelocity(a_signal));
	feLog("wheelRLAngularVelocity %.6G\n",
			m_asVehicle.wheelRLAngularVelocity(a_signal));
	feLog("wheelRRAngularVelocity %.6G\n",
			m_asVehicle.wheelRRAngularVelocity(a_signal));
#endif

	//* HACK wild guess
	const Vector3d centerOfMass(0.0,0.0,
			0.25*(tireRadius[0]+tireRadius[1]+tireRadius[2]+tireRadius[3]));

	Matrix3x4d invLastChassis;
	invert(invLastChassis,m_lastChassis);
	const Matrix3x4d deltaTransform(m_chassis*invLastChassis);

#if FALSE
	if(m_asVehicle.vehicleID(a_signal)=="1")
	{
		const Real distance=magnitude(deltaTransform.translation());
		const Real measuredSpeed=distance/deltaTime;
		feLog("ds %.4f dt %.4f sp %.4f vel %.4f\n",
				distance,deltaTime,measuredSpeed,magnitude(m_velocity));
	}
#endif

	const Quaterniond deltaRotation=
			Quaterniond(deltaTransform)*(1.0/deltaTime);
//	const Matrix3x4d xformDeltaRot(deltaRotation);
//	const Eulerd angularVelocity(xformDeltaRot);
	const Eulerd angularVelocity(deltaRotation);

	Matrix3x4d invLastDeltaTransform;
	invert(invLastDeltaTransform,m_lastDeltaTransform);
	const Matrix3x4d deltaDeltaTransform(
			deltaTransform*invLastDeltaTransform);
	m_lastDeltaTransform=deltaTransform;

	const Quaterniond deltaDeltaRotation=
			Quaterniond(deltaDeltaTransform)*(1.0/deltaTime);
//	const Matrix3x4d xformDeltaDeltaRot(deltaDeltaRotation);
//	const Eulerd angularAcceleration(xformDeltaDeltaRot);
	const Eulerd angularAcceleration(deltaDeltaRotation);

	SpatialTransform axlesRearTransform;
//	interpolate(axlesRearTransform,m_wheels[2],m_wheels[3],0.5);
	makeFrameNormalY(axlesRearTransform,
			0.5*(m_wheels[2].translation()+m_wheels[3].translation()),
			m_chassis.direction(),
			unitSafe(m_wheels[2].translation()-m_wheels[3].translation()));

	m_asVehicle.centerOfMassOffset(a_signal)=centerOfMass;
//	m_asVehicle.centerOfMassVelocity(a_signal)=
//			(m_chassis.translation()-m_lastChassis.translation())/deltaTime;
	m_asVehicle.centerOfMassVelocity(a_signal)=m_velocity;
	m_asVehicle.centerOfMassAcceleration(a_signal)=
//		(m_velocity-m_lastVelocity)/deltaTime;
		m_acceleration;
	m_asVehicle.centerOfMassAngularVelocity(a_signal)=angularVelocity;
	m_asVehicle.centerOfMassAngularAcceleration(a_signal)=angularAcceleration;
	m_asVehicle.axlesRearTransform(a_signal)=axlesRearTransform;

	// Set OSS variable
	const float longitudinal = dot(m_velocity,m_chassis.direction());
	const float laterial = -dot(m_velocity,m_chassis.left());
	const float slipAngle =
			atan2(laterial, longitudinal) * 180.0f / (float)M_PI;

	m_asVehicle.longitudinalVelocityOss(a_signal) = longitudinal;
	m_asVehicle.lateralVelocityOss(a_signal) = laterial;
	m_asVehicle.slipAngle(a_signal) = slipAngle;

	//* arbitrary test values
	m_probe[0]= -dot(m_acceleration,m_chassis.left());
	m_probe[1]=dot(m_acceleration,m_chassis.direction());
	m_probe[2]=0.0;
	m_asVehicle.probe(a_signal) = m_probe;

#if FALSE
	feLog("centerOfMassVelocity %s\n",
			c_print(m_asVehicle.centerOfMassVelocity(a_signal)));
	feLog("centerOfMassAcceleration %s\n",
			c_print(m_asVehicle.centerOfMassAcceleration(a_signal)));
	feLog("centerOfMassAngularVelocity %s\n",
			c_print(m_asVehicle.centerOfMassAngularVelocity(a_signal)));
	feLog("centerOfMassAngularAcceleration %s\n",
			c_print(m_asVehicle.centerOfMassAngularAcceleration(a_signal)));
	feLog("chassisTransform\n%s\n",
			c_print(m_asVehicle.chassisTransform(a_signal)));
	feLog("wheelRLTransform\n%s\n",
			c_print(m_asVehicle.wheelRLTransform(a_signal)));
	feLog("wheelRRTransform\n%s\n",
			c_print(m_asVehicle.wheelRRTransform(a_signal)));
	feLog("axlesRearTransform\n%s\n",
			c_print(m_asVehicle.axlesRearTransform(a_signal)));
#endif

	m_lastVelocity=m_velocity;
}

// static
F64 Scooter::compassModulus(F64 a_heading,F64 a_minimum)
{
	F64 heading = fmod(a_heading - a_minimum, 2.0*fe::pi) ;
	if(heading < 0)
	{
		heading += 2.0*fe::pi;
	}
	return heading + a_minimum;
}

void Scooter::errorSound(void)
{
	if(m_spVoiceError.isValid())
	{
		m_spVoiceError->stop();
		m_spVoiceError->queue("hammer");
		m_spVoiceError->play();
	}
}

void Scooter::tick(Real a_deltaTime)
{
	if(a_deltaTime<=0.0)
	{
		return;
	}

	const BWORD reverse=(dot(m_chassis.direction(),m_velocity)<0.0);

	//* NOTE z angular velocity affects tire forces
	const F64 carSlipAngleFront=
			compassModulus(m_headingCCW-m_travelingCCWFront,-fe::pi);
	const F64 carSlipAngleRear=
			compassModulus(m_headingCCW-m_travelingCCWRear,-fe::pi);

	F64 carSlipAngleReversibleFront=carSlipAngleFront;
	if(carSlipAngleReversibleFront<-0.5*fe::pi)
	{
		carSlipAngleReversibleFront+=fe::pi;
	}
	if(carSlipAngleReversibleFront>0.5*fe::pi)
	{
		carSlipAngleReversibleFront-=fe::pi;
	}

	F64 carSlipAngleReversibleRear=carSlipAngleRear;
	if(carSlipAngleReversibleRear<-0.5*fe::pi)
	{
		carSlipAngleReversibleRear+=fe::pi;
	}
	if(carSlipAngleReversibleRear>0.5*fe::pi)
	{
		carSlipAngleReversibleRear-=fe::pi;
	}

#if HIVE_SCOOTER_VERBOSE
	const F64 tireSlipAngle=carSlipAngleReversibleFront+m_steeringLeft;

	feLog("Scooter::tick heading %.4f traveling %.4f %.4f reverse %d\n",
			m_headingCCW/degToRad,m_travelingCCWFront/degToRad,
			m_travelingCCWRear/degToRad,reverse);
	feLog("Scooter::tick"
			" m_steeringLeft %.3f carSlipAngleFront %.3f %.3f Rear %.3f %.3f"
			" tireSlipAngle %.3f\n",
			m_steeringLeft/degToRad,
			carSlipAngleFront/degToRad,
			carSlipAngleReversibleFront/degToRad,
			carSlipAngleRear/degToRad,
			carSlipAngleReversibleRear/degToRad,
			tireSlipAngle/degToRad);
#endif

	const F64 tireRadiusFL=m_wheelOffsetFL[2];
	const F64 tireRadiusFR=m_wheelOffsetFR[2];
	const F64 tireRadiusRL=m_wheelOffsetRL[2];
	const F64 tireRadiusRR=m_wheelOffsetRR[2];

	//* HACK braking should be an independent effect
	const F64 accelerator =
			(m_gearMode==AsVehicle::e_gearModeDrive ||
			m_gearMode==AsVehicle::e_gearModeManual)? m_throttle:
			((m_gearMode==AsVehicle::e_gearModeReverse)? -m_throttle: F64(0));

	F64 stepDistance(0);

	//* RC max acceleration ~1.1g
	//* RC max speed ~300 kph = 83 m/s

	//* tire COF appears to be about 0.9 to 1.8
	//* normalized tire stiffness appears to be about 5 to 30

	const F64 wheelbase=0.5*
			(m_wheelOffsetFL[0]+m_wheelOffsetFR[0]-
			m_wheelOffsetRL[0]-m_wheelOffsetRR[0]);

	if(m_useFreeBody)
	{
#if FALSE
		//* TODO param
		const F64 torqueStall(0.45);
		const F64 torquePeak(1.0);
		const F64 speedPeak(68.0);
		const F64 speedSync(80.0);
		const F64 torqueFraction=(m_speed<speedPeak)?
				torqueStall+(torquePeak-torqueStall)*m_speed/speedPeak:
				fe::maximum(F64(0),
				torquePeak*(speedSync-m_speed)/(speedSync-speedPeak));
#else
		const F64 torqueFraction(1);
#endif

		const Vector3d zDir(0,0,1);
		const Vector3d directionCar=m_chassis.direction();
		const Vector3d left=unitSafe(cross(zDir,directionCar));

		F64 weightFL=9.81*0.5*m_mass*m_massRatioFront;
		F64 weightFR=weightFL;
		F64 weightRL=9.81*0.5*m_mass*(1.0-m_massRatioFront);
		F64 weightRR=weightRL;

		if(m_useWeightTransfer)
		{
			// F_delta = (F_g * CG_height) / wheelbase
			const F64 cgHeight=m_centerOfMassOffset[2];
			const F64 trackwidth=0.5*
					(m_wheelOffsetFL[1]+m_wheelOffsetRL[1]-
					m_wheelOffsetFR[1]-m_wheelOffsetRR[1]);

			Vector3d localAcceleration;
			Matrix3x4d invChassis;
			invert(invChassis,m_chassis);
			rotateVector(invChassis,m_acceleration,localAcceleration);

			const Vector3d gForce= -localAcceleration*m_mass;

			const F64 deltaForceLong=gForce[0]*cgHeight/wheelbase;
			const F64 deltaForceLat=gForce[1]*cgHeight/trackwidth;

			F64 gWeight[4];
			gWeight[0]=deltaForceLong+deltaForceLat;
			gWeight[1]=deltaForceLong-deltaForceLat;
			gWeight[2]= -deltaForceLong+deltaForceLat;
			gWeight[3]= -deltaForceLong-deltaForceLat;

			weightFL+=gWeight[0];
			weightFR+=gWeight[1];
			weightRL+=gWeight[2];
			weightRR+=gWeight[3];

#if HIVE_SCOOTER_VERBOSE
			feLog("localAcceleration %6.1f %6.1f %6.1f\n",
					localAcceleration[0],localAcceleration[1],
					localAcceleration[2]);
			feLog("gWeight F %8.1f %8.1f\n",gWeight[0],gWeight[1]);
			feLog("gWeight R %8.1f %8.1f\n",gWeight[2],gWeight[3]);
			feLog("sum %6.3f\n",gWeight[0]+gWeight[1]+gWeight[2]+gWeight[3]);
			feLog("weight F  %8.1f %8.1f\n",weightFL,weightFR);
			feLog("weight R  %8.1f %8.1f\n",weightRL,weightRR);
#endif
		}

		const F64 forceNormalFL(weightFL*m_groundNormal[0][2]);
		const F64 forceNormalFR(weightFR*m_groundNormal[1][2]);
		const F64 forceNormalRL(weightRL*m_groundNormal[2][2]);
		const F64 forceNormalRR(weightRR*m_groundNormal[3][2]);

		const F64 forceNormal(
				forceNormalFL+forceNormalFR+
				forceNormalRL+forceNormalRR);

#if HIVE_SCOOTER_VERBOSE
		feLog("Scooter::tick forceNormal FL %.3f FR %.3f RL %.3f RR %.3f\n",
				forceNormalFL,forceNormalFR,forceNormalRL,forceNormalRR);
		feLog("  groundNormal[0] %s\n",c_print(m_groundNormal[0]));
		feLog("  groundNormal[1] %s\n",c_print(m_groundNormal[1]));
		feLog("  groundNormal[2] %s\n",c_print(m_groundNormal[2]));
		feLog("  groundNormal[3] %s\n",c_print(m_groundNormal[3]));
#endif

		const Vector3d forceSlopeFL(
				weightFL*m_groundNormal[0][0],
				weightFL*m_groundNormal[0][1],0);
		const Vector3d forceSlopeFR(
				weightFR*m_groundNormal[1][0],
				weightFR*m_groundNormal[1][1],0);
		const Vector3d forceSlopeRL(
				weightRL*m_groundNormal[2][0],
				weightRL*m_groundNormal[2][1],0);
		const Vector3d forceSlopeRR(
				weightRR*m_groundNormal[3][0],
				weightRR*m_groundNormal[3][1],0);

		const F64 forceSlopeDirFL=dot(forceSlopeFL,directionCar);
		const F64 forceSlopeDirFR=dot(forceSlopeFR,directionCar);
		const F64 forceSlopeDirRL=dot(forceSlopeRL,directionCar);
		const F64 forceSlopeDirRR=dot(forceSlopeRR,directionCar);

		const F64 forceSlopeLeftFL=dot(forceSlopeFL,left);
		const F64 forceSlopeLeftFR=dot(forceSlopeFR,left);
		const F64 forceSlopeLeftRL=dot(forceSlopeRL,left);
		const F64 forceSlopeLeftRR=dot(forceSlopeRR,left);

		const F64 tireStiffnessFrontWeighted(
				weightFL*m_tireStiffnessFL+weightFR*m_tireStiffnessFR);
		const F64 tireStiffnessRearWeighted(
				weightRL*m_tireStiffnessRL+weightRR*m_tireStiffnessRR);

		const F64 tireForceLimitFront(
				forceNormalFL*m_tireFrictionCoeffFL+
				forceNormalFR*m_tireFrictionCoeffFR);
		const F64 tireForceLimitRear(
				forceNormalRL*m_tireFrictionCoeffRL+
				forceNormalRR*m_tireFrictionCoeffRR);

		const F64 speedReferenceDrag(0.5);
		const F64 speedReferenceGrip(2);
		const F64 speedReferenceZAng(8);
		const F64 speedReferenceTurn(30);

		Matrix3x4d rotateSteering;
		setIdentity(rotateSteering);
		rotate(rotateSteering,m_steeringLeft,e_zAxis);

		const Vector3d directionSteer=
				rotateVector(rotateSteering,directionCar);

//~		const F64 dragViscous(0.13);
//~		m_velocity*=fe::maximum(F64(0.5),
//~				F64(1.0 - dragViscous * a_deltaTime));

		if(!m_useTireI)
		{
			const F64 zAngularDragTanh=m_dragRotationalFade*
				tanh(2.0 * magnitude(m_velocity) / speedReferenceZAng);

			const F64 rotationalScale(
				1.0 - m_dragRotational*a_deltaTime * (1.0-zAngularDragTanh));
			m_zAngularVelocity*=fe::maximum(F64(0.5),rotationalScale);
		}

		const F64 leftForceSteering=tireStiffnessFrontWeighted*m_steeringLeft*
				(reverse? -1.0: 1.0);
		F64 leftForceFront=
				tireStiffnessFrontWeighted*carSlipAngleReversibleFront*
				(reverse? -1.0: 1.0);
		F64 leftForceRear=
				tireStiffnessRearWeighted*carSlipAngleReversibleRear*
				(reverse? -1.0: 1.0);

		leftForceFront+=forceSlopeLeftFL+forceSlopeLeftFR;
		leftForceRear+=forceSlopeLeftRL+forceSlopeLeftRR;

		const F64 lateralScale=
				tanh(2.0 * magnitude(m_velocity) / speedReferenceGrip);

#if HIVE_SCOOTER_VERBOSE
		feLog("Scooter::tick lateral front %.4f rear %.4f scale %.4f\n",
				leftForceFront,leftForceRear,lateralScale);
#endif

		leftForceFront+=leftForceSteering*cos(m_steeringLeft);

		leftForceFront*=lateralScale;
		leftForceRear*=lateralScale;

		Vector3d tireForceFront(0,0,0);
		Vector3d tireForceRear(0,0,0);
		Real carMzfromTires = 0.0;

		if(m_useTireI)
		{
			leftForceFront = 0.0;
			leftForceRear = 0.0;

			//* `local space tire solve'
			Matrix3x4d invChassis;
			invert(invChassis,m_chassis);

			Matrix3x4d turnedChassis=m_chassis;
			rotate(turnedChassis,m_steeringLeft,e_zAxis);
			Matrix3x4d invTurnedChassis;
			invert(invTurnedChassis,turnedChassis);

			SpatialVector localWheelVelocityFront=
					rotateVector(invTurnedChassis,m_velocity);
			SpatialVector localWheelVelocityRear=
					rotateVector(invChassis,m_velocity);

			F64 tireRadius[4];
			tireRadius[0]=m_wheelOffsetFL[2];
			tireRadius[1]=m_wheelOffsetFR[2];
			tireRadius[2]=m_wheelOffsetRL[2];
			tireRadius[3]=m_wheelOffsetRR[2];

			localWheelVelocityFront[1] += m_zAngularVelocity * 0.5 * wheelbase;
			localWheelVelocityRear[1] -= m_zAngularVelocity * 0.5 * wheelbase;
			localWheelVelocityFront[2] = 0.0;
			localWheelVelocityRear[2] = 0.0;

			m_spTire[0]->setVelocity(localWheelVelocityFront);
			m_spTire[1]->setVelocity(localWheelVelocityFront);
			m_spTire[2]->setVelocity(localWheelVelocityRear);
			m_spTire[3]->setVelocity(localWheelVelocityRear);

			const Real steeringVelocity(0);

			Vector3d localForce[4];

			Real subReal = a_deltaTime * m_frequencyHint;
			if(subReal < 1.0) // at least 1
				{ subReal = 1.0; }
			else if(subReal < 2.0) // no rounding down to 1
				{ subReal = 2.0; }
			I32 subSteps = (I32)(subReal);
			const Real subDeltaTime=a_deltaTime/Real(subSteps);
			for(I32 subStep=0;subStep<subSteps;subStep++)
			{
				for(I32 m=0;m<4;m++)
				{
					m_spTire[m]->setAngularVelocity(
						t_moa_v3(0.0,m_wheelAngularVelocity[m],
							steeringVelocity));
				}

				const Real inclination(0);
				m_spTire[0]->setContact( tireRadius[0]
					- (forceNormalFL/m_tireStiffnessZ[0]), inclination);
				m_spTire[1]->setContact( tireRadius[1]
					- (forceNormalFR/m_tireStiffnessZ[1]), inclination);
				m_spTire[2]->setContact( tireRadius[2]
					- (forceNormalRL/m_tireStiffnessZ[2]), inclination);
				m_spTire[3]->setContact( tireRadius[3]
					- (forceNormalRR/m_tireStiffnessZ[3]), inclination);

				for(I32 m=0;m<4;m++)
				{
					m_spTire[m]->step(subDeltaTime);
					localForce[m]=m_spTire[m]->getForce();
					// do not care what tire says about Fz:
					localForce[m][2] = 0.0;

					// apply tire forces
					m_wheelAngularVelocity[m] += (subDeltaTime * tireRadius[m]
						* -localForce[m][0]) / m_wheelInertia[m];
					// per wheel rotational drag
					m_wheelAngularVelocity[m] -= subDeltaTime
						* m_wheelAngularVelocity[m]
						* m_wheelDrag / m_wheelInertia[m];
				}

				// throttle
				m_wheelAngularVelocity[0]+=subDeltaTime*accelerator
					* m_motorTorqueMaxFL*m_motorGearRatioFL / m_wheelInertia[0];
				m_wheelAngularVelocity[1]+=subDeltaTime*accelerator
					* m_motorTorqueMaxFR*m_motorGearRatioFR / m_wheelInertia[1];
				m_wheelAngularVelocity[2]+=subDeltaTime*accelerator
					* m_motorTorqueMaxRL*m_motorGearRatioRL / m_wheelInertia[2];
				m_wheelAngularVelocity[3]+=subDeltaTime*accelerator
					* m_motorTorqueMaxRR*m_motorGearRatioRR / m_wheelInertia[3];

				// brakes
				for(I32 m=0;m<4;m++)
				{
					Real delta = -subDeltaTime
						* ((m_wheelAngularVelocity[m]>0.0) ? m_brake: -m_brake)
						* m_brakePeak / m_wheelInertia[m];
					if(fabs(delta) > fabs(m_wheelAngularVelocity[m]))
					{
						delta *= fabs(m_wheelAngularVelocity[m]) / fabs(delta);
					}
					m_wheelAngularVelocity[m] += delta;
				}

			}


			const Vector3d localForceFront=localForce[0]+localForce[1];
			const Vector3d localForceRear=localForce[2]+localForce[3];

			carMzfromTires = (localForceFront[1]-localForceRear[1])
				* 0.5 * wheelbase;

			tireForceFront = rotateVector(turnedChassis,localForceFront);
			tireForceRear = rotateVector(m_chassis,localForceRear);
		}
		else
		{
			//* turning steering wheel resists longitudinal motion
			tireForceFront=lateralScale*
					-leftForceSteering*sin(m_steeringLeft)*directionCar;
		}

		// power = torque * (rad/s)
		// torque_limited_by_power_limit = power_limit / (rad/s)
		// kg*m^2/s^2 = W / 1/s
		// W = kg*m^2/s^3

		const F64 pi2=fe::pi*2.0;
		const F64 circumferenceFL=pi2*tireRadiusFL;
		const F64 circumferenceFR=pi2*tireRadiusFR;
		const F64 circumferenceRL=pi2*tireRadiusRL;
		const F64 circumferenceRR=pi2*tireRadiusRR;

		const F64 rpsFL=pi2*m_speed/circumferenceFL*m_motorGearRatioFL;
		const F64 rpsFR=pi2*m_speed/circumferenceFR*m_motorGearRatioFR;
		const F64 rpsRL=pi2*m_speed/circumferenceRL*m_motorGearRatioRL;
		const F64 rpsRR=pi2*m_speed/circumferenceRR*m_motorGearRatioRR;

		const F64 torqueLimitFL=m_powerScale*m_motorPowerLimitFL/(rpsFL+1e-6);
		const F64 torqueLimitFR=m_powerScale*m_motorPowerLimitFR/(rpsFR+1e-6);
		const F64 torqueLimitRL=m_powerScale*m_motorPowerLimitRL/(rpsRL+1e-6);
		const F64 torqueLimitRR=m_powerScale*m_motorPowerLimitRR/(rpsRR+1e-6);

		const F64 unitTorque=torqueFraction*accelerator;
		F64 torqueFL=m_motorTorqueMaxFL*unitTorque;
		F64 torqueFR=m_motorTorqueMaxFR*unitTorque;
		F64 torqueRL=m_motorTorqueMaxRL*unitTorque;
		F64 torqueRR=m_motorTorqueMaxRR*unitTorque;

/***
		example: 75kW at 100 kph
		100 kph = 27 m/s
		cir = 2*pi*r = 2.29 m
		rps = 2*pi * speed/cir * gear = 2*pi * 27/2.29 * 6.25 = 463 radians/s
		torque limit = power limit / rps = 75000/463 = 161 W*s (kg*m^2/s^2)
		vs torque max = 200 kg*m^2/s^2
*/

#if HIVE_SCOOTER_VERBOSE
		feLog("Scooter::tick"
				" m_motorTorqueMaxRL %.1f m_motorPowerLimitRL %.1f\n",
				m_motorTorqueMaxRL,m_motorPowerLimitRL);
		feLog("Scooter::tick"
				" rpsRL %.1f torqueLimitRL %.1f vs %.1f speed %.1f\n",
				rpsRL,torqueLimitRL,torqueRL,m_speed);
#endif

		//* limit torque by power
		torqueFL=fe::minimum(torqueFL,torqueLimitFL);
		torqueFR=fe::minimum(torqueFR,torqueLimitFR);
		torqueRL=fe::minimum(torqueRL,torqueLimitRL);
		torqueRR=fe::minimum(torqueRR,torqueLimitRR);

		Vector3d forceThrottleFront=directionSteer*
				(torqueFL*m_motorGearRatioFL/tireRadiusFL+
				torqueFR*m_motorGearRatioFR/tireRadiusFR);
		Vector3d forceThrottleRear=directionCar*
				(torqueRL*m_motorGearRatioRL/tireRadiusRL+
				torqueRR*m_motorGearRatioRR/tireRadiusRR);

		//* TODO brakes should be some fixed scale (not based on gravity)
		Vector3d forceBrakeFront=directionSteer*
				tireForceLimitFront*(reverse? m_brake: -m_brake);
		Vector3d forceBrakeRear=directionCar*
				tireForceLimitRear*(reverse? m_brake: -m_brake);

		if(m_useTireI)
		{
			forceThrottleFront = Vector3d(0.0,0.0,0.0);
			forceThrottleRear = Vector3d(0.0,0.0,0.0);
			forceBrakeFront = Vector3d(0.0,0.0,0.0);
			forceBrakeRear = Vector3d(0.0,0.0,0.0);
		}

		forceBrakeFront*=lateralScale;
		forceBrakeRear*=lateralScale;

		Vector3d forceFront=forceThrottleFront+forceBrakeFront+
				leftForceFront*left+
				tireForceFront+
				(forceSlopeDirFL+forceSlopeDirFR)*directionCar;
		Vector3d forceRear=forceThrottleRear+forceBrakeRear+
				leftForceRear*left+
				tireForceRear+
				(forceSlopeDirRL+forceSlopeDirRR)*directionCar;

		const F64 forceFrontMag=magnitude(forceFront);
		const F64 forceRearMag=magnitude(forceRear);

		if(!m_useTireI)
		{
			if(forceFrontMag>tireForceLimitFront)
			{
				const F64 scalar=tireForceLimitFront/forceFrontMag;
				leftForceFront*=scalar;
				forceFront*=scalar;

				//* no noise when going very slow
				if(lateralScale>0.5)
				{
					errorSound();
				}
			}
			if(forceRearMag>tireForceLimitRear)
			{
				const F64 scalar=tireForceLimitRear/forceRearMag;
				leftForceRear*=scalar;
				forceRear*=scalar;
			}
		}

		//* DB2:		AeroCoefficient = 0.7024 [N * (sec/m)^2]
		//*				F_rolling_resistance = 349.8894 [N]
		//*				front surface = 1.225 m^2
		//* N = kg m/s^2

		//* Cd*A of street car about 0.3 to 0.8, commonly around 0.56
		//* p (density of air) about 1.2
		//* FD = 1/2 Cd p A v^2
		//* FD ~= 0.4 v^2

#if FALSE
		//* TODO param
		const F64 facingArea=1.0;	//* m^2
		const F64 airDensity=1.2;	//* 1.1 to 1.4 kg/m^3
		const F64 coeffDrag=0.75;	//* 0.35 street to 0.75 racing w/ downforce

		const F64 dragConstant=0.01;
		const F64 dragLinear=0.01;
		const F64 dragSquared=0.5*coeffDrag*airDensity*facingArea;
#else
		const F64 dragConstant=0.03075;
		const F64 dragLinear=0.0;
		const F64 dragSquared=0.7024;	//* 1/2 Cd p A
#endif

		const Vector3d forceDragConstant=
				-unitSafe(m_velocity)*
				(dragConstant*forceNormal*
				tanh(2.0 * m_speed / speedReferenceDrag));

		const Vector3d forceDragLinear=
				-dragLinear*m_velocity;

		const Vector3d forceDragSquared=
				-dragSquared*magnitude(m_velocity)*m_velocity;

		const Vector3d forceDrag=
				forceDragConstant+forceDragLinear+forceDragSquared;

#if HIVE_SCOOTER_VERBOSE
		feLog("Scooter::tick torqueFraction %.4f\n",torqueFraction);
		feLog("Scooter::tick forceSlopeDir %.4f %.4f %.4f %.4f\n",
				forceSlopeDirFL,forceSlopeDirFR,
				forceSlopeDirRL,forceSlopeDirRR);
		feLog("Scooter::tick leftForceFront %s\n",
				c_print(leftForceFront));
		feLog("Scooter::tick leftForceRear %s\n",
				c_print(leftForceRear));
		feLog("Scooter::tick forceThrottleFront %s\n",
				c_print(forceThrottleFront));
		feLog("Scooter::tick forceThrottleRear %s\n",
				c_print(forceThrottleRear));
		feLog("Scooter::tick forceBrakeFront %s\n",
				c_print(forceBrakeFront));
		feLog("Scooter::tick forceBrakeRear %s\n",
				c_print(forceBrakeRear));
		feLog("Scooter::tick forceFront %.4f -> %s\n",
				forceFrontMag,c_print(forceFront));
		feLog("Scooter::tick forceRear %.4f -> %s\n",
				forceRearMag,c_print(forceRear));
		feLog("Scooter::tick forceDrag %.4f -> %s\n",
				magnitude(forceDrag),c_print(forceDrag));
		feLog("Scooter::tick old velocity %.4f\n",
				magnitude(m_velocity));
		feLog("Scooter::tick old acceleration %.4f\n",
				magnitude(m_acceleration));
#endif

		m_acceleration=(forceFront+forceRear+forceDrag)/m_mass;
		m_velocity += m_acceleration*a_deltaTime;

#if HIVE_SCOOTER_STOP_WHEN_SLOW
//		feLog("Scooter::tick acc %.3f vel %.3f\n",
//				magnitude(m_acceleration),magnitude(m_velocity));
		if(magnitude(m_acceleration)<1.0 && magnitude(m_velocity)<0.1 &&
				m_brake>0.1)
		{
			set(m_acceleration);
			set(m_velocity);
		}
#endif

		//* TEST
		m_velocity += m_impulse/m_mass;

		//* NOTE old speed
		const F64 speedScaleLateral=tanh(2.0 * m_speed / speedReferenceGrip);

#if HIVE_SCOOTER_VERBOSE
		feLog("Scooter::tick old speed %.4f scaleLat %.4f\n",
				m_speed,speedScaleLateral);
#endif

		const Vector3d velocityForward=
				dot(m_velocity,directionCar)*directionCar;
		const Vector3d velocityLateral=m_velocity-velocityForward;
		if(!m_useTireI)
		{
			m_velocity=velocityForward+velocityLateral*speedScaleLateral;
		}

		const Vector3d stepForward=m_velocity*a_deltaTime;
		m_location += stepForward;
		m_location[2] += m_deltaZ;

		// F_eff = F_dynamic * tanh(2.0 * w / w0);

		//* NOTE new speed
		m_speed=magnitude(m_velocity);
		const F64 speedScaleRotational=
				tanh(2.0 * m_speed / speedReferenceTurn);
#if FALSE
		const F64 accelerationScaleTurn(1.0/m_mass);
		F64 zAngularAcceleration=
				accelerationScaleTurn*speedScaleRotational*
				(leftForceFront-leftForceRear);
#else

		const F64 wheelOffsetFront=0.5*(m_wheelOffsetFL[0]+m_wheelOffsetFR[0]);
		const F64 wheelOffsetRear=0.5*(m_wheelOffsetRL[0]+m_wheelOffsetRR[0]);
		const F64 wheelArmFront=wheelOffsetFront-m_centerOfMassOffset[0];
		const F64 wheelArmRear= -wheelOffsetRear+m_centerOfMassOffset[0];

		const F64 zTorqueFront=leftForceFront*wheelArmFront;
		const F64 zTorqueRear=leftForceRear*wheelArmRear;

		F64 zAngularAcceleration=speedScaleRotational*
				(zTorqueFront-zTorqueRear)/m_mass_moi_z;

//		feLog("Scooter::tick torque front %.4f rear %.4f\n",
//				zTorqueFront,zTorqueRear);
#endif

		if(m_useTireI)
		{
			//* TODO consider using m_mass_moi_z
			Real guess_carMOI = m_mass * 0.5 * wheelbase;

			zAngularAcceleration = carMzfromTires / guess_carMOI;
		}

#if HIVE_SCOOTER_VERBOSE
		feLog("Scooter::tick new speed %.4f scaleRot %.4f\n",
				m_speed,speedScaleRotational);
		feLog("Scooter::tick ang %.4f acc %.4f\n",
				m_zAngularVelocity,zAngularAcceleration);
#endif

		m_zAngularVelocity+=zAngularAcceleration*a_deltaTime;

		if(!m_useTireI)
		{
			//* TEST
			const F64 impulseScale(1.0);
			const F64 angularImpulseZ=Eulerd(m_angularImpulse)[2];

			m_zAngularVelocity+=impulseScale*angularImpulseZ;
		}

		m_headingCCW=compassModulus(
				m_headingCCW+m_zAngularVelocity*a_deltaTime,0.0);

		const Vector3d velocityFront=
				m_velocity+wheelArmFront*m_zAngularVelocity*left;
		const Vector3d velocityRear=
				m_velocity-wheelArmRear*m_zAngularVelocity*left;

		m_travelingCCWFront=compassModulus(
				atan2(-velocityFront[0],velocityFront[1]),0.0);
		m_travelingCCWRear=compassModulus(
				atan2(-velocityRear[0],velocityRear[1]),0.0);

#if HIVE_SCOOTER_VERBOSE
		feLog("Scooter::tick acc %s (%.3f)\n",
				c_print(m_acceleration),magnitude(m_acceleration));
		feLog("Scooter::tick vel %s (%.3f)\n",
				c_print(m_velocity),magnitude(m_velocity));
		feLog("Scooter::tick loc %s\n",c_print(m_location));
		feLog("Scooter::tick travelingCCW %.3f %.3f headingCCW %.3f\n",
				m_travelingCCWFront,m_travelingCCWRear,m_headingCCW);
#endif

		stepDistance=dot(stepForward,directionCar);

		// Instantaneous Torque Meas.----------------------------------------
		m_instantaneousTorqueRL=accelerator*torqueFraction*m_motorTorqueMaxRL;
		m_instantaneousTorqueRR=accelerator*torqueFraction*m_motorTorqueMaxRR;
	}
	else
	{
		//* Curvature Threshold

		//* TODO param
//~		const F64 accelerationScale(11);
//~		const F64 decelerationScale(30);
//~		const F64 gLateralLimit(0.8);

		//* tan(steering_angle) = wheelbase / radius = wheelbase * curvature
		//* RC: curvature = tan(20 deg) / wheelbase = 0.36 / 2.8 = 0.13
//~		const F64 curvature= -0.13*m_steering;
		const F64 wheelbase=0.5*
				(m_wheelOffsetFL[0]+m_wheelOffsetFR[0]-
				m_wheelOffsetRL[0]-m_wheelOffsetRR[0]);
		const F64 steeringFInput= -m_steeringLeft/m_steeringAngleMaxF;
		const F64 curvature= -tan(m_steeringAngleMaxF)/wheelbase*steeringFInput;

		//* tweak
		const F64 dragFactor=fe::minimum(2.0,0.13+4.0*fabs(carSlipAngleFront));

		m_speed*=fe::maximum(F64(0.5), F64(1.0 - dragFactor * a_deltaTime));

		m_speed += m_accelerationScale * accelerator * a_deltaTime;

		const F64 braking=m_decelerationScale * m_brake * a_deltaTime;
		if(m_speed>0.0)
		{
			m_speed=fe::maximum(F64(0),m_speed-braking);
		}
		else
		{
			m_speed=fe::minimum(F64(0),m_speed+braking);
		}

		const F64 curvatureMax=m_gLateralLimit*9.81/(m_speed*m_speed);
#if HIVE_SCOOTER_VERBOSE
		feLog("Scooter::tick speed %.4f curvature %.4f max %.4f\n",
				m_speed,curvature,curvatureMax);
#endif

		const Vector3d advance(m_speed*a_deltaTime,0.0,m_deltaZ);

		m_headingCCW += curvature * advance[0];

		const F64 adjustMax=curvatureMax*advance[0];
		F64 adjustLeft=
				compassModulus(m_headingCCW-m_travelingCCWFront,-fe::pi);
		if(fabs(adjustLeft)>adjustMax)
		{
			adjustLeft=copysign(adjustMax,adjustLeft);
		}

		//* NOTE when slipping, m_travelingCCWFront lags behind m_headingCCW
		m_travelingCCWFront=
				compassModulus(m_travelingCCWFront + adjustLeft,0.0);
		m_travelingCCWRear=m_travelingCCWFront;
		m_headingCCW=compassModulus(m_headingCCW,0.0);
#if HIVE_SCOOTER_VERBOSE
		feLog("Scooter::tick"
				" m_headingCCW %.4f m_travelingCCWFront %.4f"
				" adjustLeft %.4f\n",
				m_headingCCW,m_travelingCCWFront,adjustLeft);
#endif

		Matrix3x4d rotation;
		setIdentity(rotation);
		rotate(rotation,0.5*fe::pi+m_travelingCCWFront,e_zAxis);
		Vector3d stepForward;
		rotateVector(rotation,advance,stepForward);

		m_location += stepForward;
		m_velocity=stepForward/a_deltaTime;
		m_acceleration=(m_velocity-m_lastVelocity)/a_deltaTime;

#if HIVE_SCOOTER_VERBOSE
		const Vector3d measuredAcceleration=
				(m_velocity-m_lastVelocity)*(1.0/a_deltaTime);
		const F64 gForward=
				dot(measuredAcceleration,m_chassis.direction())/9.81;

		const F64 vForward=dot(m_velocity,m_chassis.direction());
		const F64 gLateral=vForward*vForward*curvature/9.81;

		feLog("Scooter::tick acc forward %.2fg lateral %.2fg\n",
				gForward,gLateral);
#endif

		stepDistance=advance[0];
	}

	m_pitchDown += m_deltaPitch;
	m_bankRight += m_deltaBank;

	const F64 deltaFade =
			fe::maximum(F64(0.5), F64(1.0 - 12.0 * a_deltaTime));
	m_deltaZ *= deltaFade;
	m_deltaPitch *= deltaFade;
	m_deltaBank *= deltaFade;

	m_lastChassis=m_chassis;
	m_lastwheels[0]=m_wheels[0];
	m_lastwheels[1]=m_wheels[1];
	m_lastwheels[2]=m_wheels[2];
	m_lastwheels[3]=m_wheels[3];

	//* NOTE per ISO, world is x-east, car is x-forward

	setIdentity(m_chassis);
	rotate(m_chassis, m_headingCCW+(90.0*degToRad), e_zAxis);
	rotate(m_chassis, m_pitchDown, e_yAxis);
	rotate(m_chassis, m_bankRight, e_xAxis);
	m_chassis.translation() = m_location;

	//* local space -> world space
	const Vector3d tireCenterFL=transformVector(m_chassis,
			m_wheelOffsetFL);
	const Vector3d tireCenterFR=transformVector(m_chassis,
			m_wheelOffsetFR);
	const Vector3d tireCenterRL=transformVector(m_chassis,
			m_wheelOffsetRL);
	const Vector3d tireCenterRR=transformVector(m_chassis,
			m_wheelOffsetRR);

	const Vector3d tireBottomFL=transformVector(m_chassis,
			m_wheelOffsetFL-Vector3d(0,0,tireRadiusFL));
	const Vector3d tireBottomFR=transformVector(m_chassis,
			m_wheelOffsetFR-Vector3d(0,0,tireRadiusFR));
	const Vector3d tireBottomRL=transformVector(m_chassis,
			m_wheelOffsetRL-Vector3d(0,0,tireRadiusRL));
	const Vector3d tireBottomRR=transformVector(m_chassis,
			m_wheelOffsetRR-Vector3d(0,0,tireRadiusRR));

	Matrix3x4d rotateSteering;
	setIdentity(rotateSteering);
	rotate(rotateSteering,m_steeringLeft,e_zAxis);

	//* angle = distance/radius
	if(m_useTireI)
	{
		for(I32 m=0;m<4;m++)
		{
			m_tireSpin[m] += m_wheelAngularVelocity[m]*a_deltaTime;
		}
	}
	else
	{
		m_tireSpin[0]+=stepDistance/tireRadiusFL;
		m_tireSpin[1]+=stepDistance/tireRadiusFR;
		m_tireSpin[2]+=stepDistance/tireRadiusRL;
		m_tireSpin[3]+=stepDistance/tireRadiusRR;
	}

	Matrix3x4d spinTire;
	for(I32 m=0;m<4;m++)
	{
		setIdentity(spinTire);
		rotate(spinTire,m_tireSpin[m],e_yAxis);

		m_wheels[m]=(m<2)?
				spinTire*rotateSteering*m_chassis:
				spinTire*m_chassis;
	}

	m_wheels[0].translation() = tireCenterFL;
	m_wheels[1].translation() = tireCenterFR;
	m_wheels[2].translation() = tireCenterRL;
	m_wheels[3].translation() = tireCenterRR;

#if FALSE
	feLog("m_chassis\n%s\n",c_print(m_chassis));
	for(I32 m=0;m<4;m++)
	{
		feLog("m_wheels[%d]\n%s\n",m,c_print(m_wheels[m]));
	}
#endif

	Vector3d searchOffset(0.0,0.0,2.0);
	const F64 maxDistance(8);

	//* NOTE use const to make sure we stay thread safe while searching track
	sp<SurfaceI::ImpactI> spImpactFL=m_spSurfaceTrack.asConst()->nearestPoint(
			tireBottomFL+searchOffset,maxDistance);
	sp<SurfaceI::ImpactI> spImpactFR=m_spSurfaceTrack.asConst()->nearestPoint(
			tireBottomFR+searchOffset,maxDistance);
	sp<SurfaceI::ImpactI> spImpactRL=m_spSurfaceTrack.asConst()->nearestPoint(
			tireBottomRL+searchOffset,maxDistance);
	sp<SurfaceI::ImpactI> spImpactRR=m_spSurfaceTrack.asConst()->nearestPoint(
			tireBottomRR+searchOffset,maxDistance);

	const F64 zGain		= fe::minimum(F64(0.5),F64(15.0 * a_deltaTime));
	const F64 pitchGain	= fe::minimum(F64(0.05),F64(0.6 * a_deltaTime));
	const F64 bankGain	= fe::minimum(F64(0.1),F64(1.2 * a_deltaTime));

	const F64 zLimit	= 1.0;
	const F64 strutPlay	= 0.1;

	if(spImpactFL.isValid())
	{
		m_groundNormal[0]=spImpactFL->normal();
		F64 above = tireBottomFL[2]-spImpactFL->intersection()[2];
		above = fe::clamp(above,-zLimit,zLimit);
		m_deltaZ -= zGain * above;
		m_deltaPitch += pitchGain * above;
		m_deltaBank -= bankGain * above;

		const F64 raise = fe::clamp(-above,-strutPlay,strutPlay);
		m_wheels[0].translation()[2] += raise;
	}
	else
	{
		set(m_groundNormal[0],0,0,1);
		m_deltaZ -= zGain*zLimit;
	}
	if(spImpactFR.isValid())
	{
		m_groundNormal[1]=spImpactFR->normal();
		F64 above = tireBottomFR[2] - spImpactFR->intersection()[2];
		above = fe::clamp(above,-zLimit,zLimit);
		m_deltaZ -= zGain * above;
		m_deltaPitch += pitchGain * above;
		m_deltaBank += bankGain * above;

		const F64 raise = fe::clamp(-above,-strutPlay,strutPlay);
		m_wheels[1].translation()[2] += raise;
	}
	else
	{
		set(m_groundNormal[1],0,0,1);
		m_deltaZ -= zGain*zLimit;
	}
	if(spImpactRL.isValid())
	{
		m_groundNormal[2]=spImpactRL->normal();
		F64 above = tireBottomRL[2]-spImpactRL->intersection()[2];
		above = fe::clamp(above,-zLimit,zLimit);
		m_deltaZ -= zGain * above;
		m_deltaPitch -= pitchGain * above;
		m_deltaBank -= bankGain * above;

		const F64 raise = fe::clamp(-above,-strutPlay,strutPlay);
		m_wheels[2].translation()[2] += raise;
	}
	else
	{
		set(m_groundNormal[2],0,0,1);
		m_deltaZ -= zGain*zLimit;
	}
	if(spImpactRR.isValid())
	{
		m_groundNormal[3]=spImpactRR->normal();
		F64 above = tireBottomRR[2]-spImpactRR->intersection()[2];
		above = fe::clamp(above,-zLimit,zLimit);
		m_deltaZ -= zGain*above;
		m_deltaPitch -= pitchGain*above;
		m_deltaBank += bankGain*above;

		const F64 raise = fe::clamp(-above,-strutPlay,strutPlay);
		m_wheels[3].translation()[2] += raise;
	}
	else
	{
		set(m_groundNormal[3],0,0,1);
		m_deltaZ -= zGain*zLimit;
	}
}

}
