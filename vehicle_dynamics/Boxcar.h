/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_dynamics_Boxcar_h__
#define __vehicle_dynamics_Boxcar_h__

#define BOXCAR_MAX_STEER_DEGREES 20
#define BOXCAR_LENGTH 5
#define BOXCAR_WIDTH 2
#define BOXCAR_HEIGHT 1.5
#define BOXCAR_WHEEL_RADIUS 0.5
#define BOXCAR_MASS 1000

namespace hive
{

/*
	This car is a 5m long x 2m wide x 1.5m tall, 1000 kg (approx car size) box
	Wheel radius is 0.5m
*/

class FE_DL_EXPORT Boxcar:
	virtual public fe::ext::HandlerI,
	public fe::Initialize<Boxcar>
{
public:
				Boxcar(void) {}
virtual			~Boxcar(void) {}

		void	initialize();


virtual	void	bind(fe::sp<fe::ext::SignalerI> spSignalerI,
						fe::sp<fe::Layout> spLayout)						{}
virtual	void	handle(fe::Record& a_signal);

private:
	void							tick(float_t a_deltaTime);

									// 0 = FL; 1 = FR; 2 = RL; 3 = RR.
	fe::SpatialTransform			getWheelTransform(uint8_t idx);
	fe::SpatialVector				m_wheelOffsets[4];

	fe::sp<fe::ext::SurfaceI>		m_spSurfaceTrack;
	fe::sp<hive::ConnectedSystemI>	m_spConnectedSystem;
	fe::sp<hive::RigidBodyI>		m_theBox;

									// -1 to 1 --> -20 to 20 degrees
	float_t							m_steering;

	float_t							m_throttle;
	float_t							m_brake;

	hive::AsVehicle					m_asVehicle;

	fe::SpatialVector				m_previousPosition;
	float_t							m_displacement;
	float_t							m_steerAngle;
	float_t							m_cumulativeTimeStep;
};

} /* namespace hive */

#endif /* __vehicle_dynamics_Boxcar_h__ */
