/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include <math.h>
#include <chrono>
#include "LiDARRaytraceSim.h"

namespace hive
{

//-----------------------------------------------------------------
void LiDARRaytraceSim::ConfigScan( float minRange, float maxRange,
                                   float horizontalMin, float horizontalMax, int step,
                                   float verticalMin, float verticalMax, int channels,
                                   int deviceID )
{
    mLidarConfig.mDistanseMin     = minRange;
    mLidarConfig.mDistanseMax     = maxRange;
    mLidarConfig.mFOV_HorizontMin = horizontalMin;
    mLidarConfig.mFOV_HorizontMax = horizontalMax;
    mLidarConfig.mStep            = step;
    mLidarConfig.mFOV_VerticalMin = verticalMin;
    mLidarConfig.mFOV_VerticalMax = verticalMax;
    mLidarConfig.mChannels        = channels;
    mLidarConfig.mDeviceID        = deviceID;
}

//-----------------------------------------------------------------
void LiDARRaytraceSim::ConfigOutput( const char *sendIPaddress, int sendPort, int receivePort )
{
    mLidarConfig.mSendIPaddress = sendIPaddress;
    mLidarConfig.mSendPort      = sendPort;
    mLidarConfig.mReceivePort   = receivePort;
}

//-----------------------------------------------------------------
void LiDARRaytraceSim::ConfigDebug( bool drawTracedRays, bool drawNotTracedRays, bool createFileDump )
{
    mLidarConfig.mDrawTracedRays    = drawTracedRays;
    mLidarConfig.mDrawNotTracedRays = drawNotTracedRays;
    mLidarConfig.mCreateFileDump    = createFileDump;
}

//-----------------------------------------------------------------
void LiDARRaytraceSim::Init()
{
    mLidarPoints = new LidarRaytracePoint[mLidarConfig.mChannels * mLidarConfig.mStep];

    int p = 0;
    for (int i = 0; i < mLidarConfig.mChannels; i++)
    {
        float verticalAngle = mLidarConfig.mFOV_VerticalMin + (mLidarConfig.mFOV_VerticalMax - mLidarConfig.mFOV_VerticalMin) / (float)mLidarConfig.mChannels * (float)i;
        for (int j = 0; j < mLidarConfig.mStep; j++)
        {
            // Data need to do raycasts
            mLidarPoints[p].horzAngle = mLidarConfig.mFOV_HorizontMin + (mLidarConfig.mFOV_HorizontMax - mLidarConfig.mFOV_HorizontMin) / (float)mLidarConfig.mStep * (float)j;
            mLidarPoints[p].vertAngle = verticalAngle;
            mLidarPoints[p].minRange  = mLidarConfig.mDistanseMin;
            mLidarPoints[p].maxRange  = mLidarConfig.mDistanseMax;

            // Data need for LiDAR coms
            mLidarPoints[p].layer = mLidarConfig.mChannels - i - 1;
            p++;
        }
    }

    mLidarPublisher.Advertise( mLidarConfig.mSendIPaddress, mLidarConfig.mSendPort );
}

//-----------------------------------------------------------------
void LiDARRaytraceSim::shutdown()
{
	mLidarPublisher.shutdown();
}

//-----------------------------------------------------------------
void LiDARRaytraceSim::SendScan()
{
    mLidarPublisher.mScan.points.resize(mLidarConfig.mChannels * mLidarConfig.mStep);

	mLidarPublisher.mScan.device_start_time = GetTimestamp();

    for (int i = 0; i < (mLidarConfig.mChannels * mLidarConfig.mStep); i++)
    {
    	ScanPoint& pt = mLidarPublisher.mScan.points[i];
        pt.layer = mLidarPoints[i].layer;

        if( mLidarPoints[i].hasBlock )
        {
			pt.properties = ScanPoint::Properties::Valid;

		    pt.coords.x =  mLidarPoints[i].relativeHitPosition.x;
		    pt.coords.y = -mLidarPoints[i].relativeHitPosition.y;
		    pt.coords.z =  mLidarPoints[i].relativeHitPosition.z;

            if( mLidarConfig.mEnabledGroundFilter &&
				mLidarPoints[i].distanceToGround > mLidarConfig.mDistanceToGround )
			{
				pt.properties = ScanPoint::Properties::None;
			}
        }
        else
        {
    		pt.properties = ScanPoint::Properties::None;

    		pt.coords.x = 0;
		    pt.coords.y = 0;
	    	pt.coords.z = 0;
        }
    }

	mLidarPublisher.Publish();
}

//-----------------------------------------------------------------
std::int64_t LiDARRaytraceSim::GetTimestamp()
{
	auto t = std::chrono::system_clock::now().time_since_epoch();
	std::int64_t timestamp = std::chrono::duration_cast< std::chrono::milliseconds >(t).count();

	return timestamp;
}

} // namespace hive

