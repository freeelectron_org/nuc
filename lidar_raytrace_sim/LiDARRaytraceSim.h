/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include "LiDARRaytraceSimI.h"
#include "LiDARRaytraceSimConfig.h"
#include "UDPLiDARPublisher.h"

namespace hive
{

//-----------------------------------------------------------------
class FE_DL_EXPORT LiDARRaytraceSim : virtual public LiDARRaytraceSimI
{
    private:
        LidarRaytracePoint *mLidarPoints;
        UDPLiDARPublisher   mLidarPublisher;
        LiDARRaytraceSimConfig mLidarConfig;

        std::int64_t GetTimestamp();

    public:
        void Init() override;
		void shutdown() override;

        virtual void ConfigScan( float minRange, float maxRange,
                                 float horizontalMin, float horizontalMax, int step,
                                 float verticalMin, float verticalMax, int channels,
                                 int deviceID ) override;
        void ConfigOutput( const char * sendIPaddress, int sendPort, int receivePort ) override;
        void ConfigDebug( bool drawTracedRays, bool drawNotTracedRays, bool createFileDump ) override;

        int GetPointCount() override { return (mLidarConfig.mChannels * mLidarConfig.mStep);  }
        LidarRaytracePoint* GetLidarPoints() override { return mLidarPoints; }
        void SendScan() override;

        bool GetDisplayTracePoints() override { return mLidarConfig.mDrawTracedRays; }
};

} // namespace hive
