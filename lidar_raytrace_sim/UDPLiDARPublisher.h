/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include <stdint.h>
#include <vector>
#include <tuple>

#include "message/message.h"

namespace hive
{

/// \cond unofficial
//-----------------------------------------------------------------
class Pose2d final
{
public:
  Pose2d() noexcept = default;

  Pose2d(float x, float y, float yaw) noexcept
    : m_x(x)
    , m_y(y)
    , m_yaw(yaw)
  {}

  float x() const noexcept { return m_x; }
  void setX(float x) noexcept { m_x = x; }
  float y() const noexcept { return m_y; }
  void setY(float y) noexcept { m_y = y; }
  float yaw() const noexcept { return m_yaw; }
  void setYaw(float yaw) noexcept { m_yaw = yaw; }

  bool operator == (Pose2d const & other) const noexcept {
    return std::tie(m_x, m_y, m_yaw) == std::tie(other.m_x, other.m_y, other.m_yaw);
  }

  bool operator != (Pose2d const & other) const noexcept { return !(*this == other); }

  void Write(std::ostream& out);

private:
  float m_x = 0, m_y = 0;
  float m_yaw = 0;
};


#pragma pack(push, 1)
//-----------------------------------------------------------------
struct ScanPoint
{
	/**
	Properties associated with scan point.

	\warning All the properties represent results of vendor-specific classification.
	*/
	enum Properties : std::uint8_t {
		None = 0x0, ///< No properties associated with this point.
		Valid = 0x1, ///< This point is valid in some vendor-specific sense. For example, \em invalid points may be the ones that were classfied as dirt on LiDAR's glass surface, any kind of precipitation, or even road surface.
		Transparent = 0x2, ///< This point is considered to be transparent.
		Reflector = 0x4 ///< This point is reflective enough to be counted as a reflector.
	};

	struct {
		float x, y, z;
	} coords; ///< Cartesian coordinates of this point, in meters.

  std::uint8_t device_id = 0; ///< ID of device this point is produced by.
  std::uint8_t layer = 0; ///< Index of horizontal layer this point belongs to.
  std::uint8_t echo = 0; ///< Echo index of this point.
  std::uint16_t properties = Properties::None; ///< \link ScanPoint::Properties Properties\endlink of this point.

  /// \cond unofficial

  bool operator == (ScanPoint const & other) const noexcept
  {
    return std::tie(device_id, layer, echo, properties) ==
      std::tie(other.device_id, other.layer, other.echo, other.properties) &&
      std::abs(coords.x - other.coords.x) <= 1e-5f &&
      std::abs(coords.y - other.coords.y) <= 1e-5f &&
      std::abs(coords.z - other.coords.z) <= 1e-5f;
  }

  bool operator != (ScanPoint const & other) const noexcept { return !(*this == other); }

  /// \endcond
};
#pragma pack(pop)

static_assert(sizeof(ScanPoint) == 17, "ScanPoint has unexpected size");


#pragma pack(push, 1)
//-----------------------------------------------------------------
//  Scan header used for sending data via some transport layer.
//-----------------------------------------------------------------
struct scanHeader
{
  std::uint8_t generation = 0; ///< Generation of data format.
	/**
    Number of devices in this scan.

    Value 0 means there's only one flat list of points without any grouping by device ID.
  */
	std::uint8_t devices_count = 0;
	std::uint16_t reserved = 0; ///< Reserved for future use.
	std::uint32_t length = 0;   ///< Length of the upcoming block.
};
#pragma pack(pop)

static_assert(sizeof(scanHeader) == 8, "scanHeader has unexpected size");


//-----------------------------------------------------------------
//  This struct represents scan of LiDAR.
//
//  It consists of ID of device, points, and several time-related fields
//  (steady clocks timestamp, system clocks timestamp, start time of scan,
//  and duration of scan).
//-----------------------------------------------------------------
struct Scan
{
  std::vector<ScanPoint> points; ///< Points of this scan.

  /**
    Steady clock's timestamp, in milliseconds.
   */
  std::int64_t steady_timestamp = 0;
  /**
    System clock's timestamp, in milliseconds.
   */
  std::int64_t system_timestamp = 0;
  /**
    Start time of scan regarding device's clocks, in milliseconds.
   */
  std::int64_t device_start_time = 0;
  /**
    Duration of scan regarding device's clocks, in milliseconds.
   */
  std::int64_t device_duration = 0;

  /**
    Origin device ID.

    Note: By default device ID should be positive number and each point in a scan
    has its \link ScanPoint::device_id device_id\endlink field set to this value.
    For legacy reasons, if device ID appears to be equal to 0, then each point in
    this  scan has its own \link ScanPoint::device_id device_id\endlink field set
    to an ID of appropriate device.
   */
  std::uint16_t device_id = 0;

  /**
    Origin device's pose.
   */
  Pose2d device_pose;

  /**
    Removes any points from this scan and resets its timestamp to 0.
   */
  void clear() noexcept
  {
    points.clear();
    steady_timestamp = 0;
    system_timestamp = 0;
    device_start_time = 0;
    device_duration = 0;
    device_id = 0;
    device_pose = Pose2d();
  }

  bool operator == (Scan const & other) const noexcept
  {
    return std::tie(
      steady_timestamp, system_timestamp,
      device_start_time, device_duration,
      device_id, device_pose, points
    ) == std::tie(
      other.steady_timestamp, other.system_timestamp,
      other.device_start_time, other.device_duration,
      other.device_id, other.device_pose, other.points
    );
  }

  bool operator != (Scan const & other) const noexcept { return !(*this == other); }

  std::size_t Write(std::ostream& out);

  private:
    void Write(std::ostream& out, void const* data, std::size_t size);
    void Read(std::istream& in, void* data, std::size_t size);
};


//-----------------------------------------------------------------
struct UDPLiDARPublisher
{
  UDPLiDARPublisher() : mIsInit(false), mSendEnabled(true)
  {}

	void Advertise(const std::string IPaddress, const int port);
	void shutdown();
	void Publish();
	bool IsAdvertised()	{ return (mMessageOut.isValid() && mIsInit);	}

	Scan mScan;

  protected:
    std::string mIPaddress;
    int mPort;

    bool mIsInit;
    bool mSendEnabled;

    fe::sp<fe::ext::MessageI> mMessageOut;
};

} // namespace hive

