/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_viewer_VehicleOscilloscope_h__
#define __vehicle_viewer_VehicleOscilloscope_h__

namespace hive
{

class FE_DL_EXPORT VehicleOscilloscope:
	virtual public fe::ext::HandlerI,
	public fe::Initialize<VehicleOscilloscope>
{
public:
				VehicleOscilloscope(void);
virtual			~VehicleOscilloscope(void)									{}

		void	initialize(void);

virtual	void	handle(fe::Record& a_signal);

private:

	AsVehicle							m_asVehicle;

	fe::sp<fe::ext::QuickViewerI>		m_spQuickViewerI;
	fe::sp<fe::ext::ImguiHandlerRecord>	m_spImguiHandlerRecord;
	fe::sp<fe::RecordGroup>				m_spRecordGroup;
};

} // namespace __vehicle_viewer_VehicleOscilloscope_h__

#endif
