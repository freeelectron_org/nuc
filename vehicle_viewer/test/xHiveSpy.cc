/** @file */

#include "imgui/imgui.pmh"
#include "viewer/viewer.h"
#include "apiary/apiary.h"

using namespace fe;
using namespace fe::ext;
using namespace hive;

int main(int argc,char** argv)
{
	UNIT_START();
	BWORD complete=FALSE;

	try
	{
		{
			sp<HiveHost> spHiveHost(HiveHost::create());
			FEASSERT(spHiveHost.isValid());

			sp<Master> spMaster(new Master);
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			String implementation="ConnectedCatalog";
			if(argc>1)
			{
				implementation=argv[1];
			}
			feLog("implementation \"%s\"\n",implementation.c_str());

			sp<StateCatalog> spStateCatalog=
					spHiveHost->accessSpace("world",implementation);
			FEASSERT(spStateCatalog.isValid());

			spStateCatalog->setState<String>("net:role","client");
//			spStateCatalog->setState<String>("net:transport", "tcp");
//			spStateCatalog->setState<String>("net:address","127.0.0.1");
			spStateCatalog->setState<I32>("net:port",7890);

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));

			feLog("StateCatalog started\n");

			sp<QuickViewerI> spQuickViewerI(
					spRegistry->create("QuickViewerI"));
			if(!spQuickViewerI.isValid())
			{
				feX(argv[0], "couldn't create QuickViewerI");
			}

			spQuickViewerI->open();

			sp<DrawI> spDrawI=spQuickViewerI->getDrawI();
			if(spDrawI.isNull())
			{
				feX(argv[0], "couldn't get draw interface");
			}

			sp<ImguiHandlerCatalog> spImguiHandlerCatalog=
					spRegistry->create("*.ImguiHandlerCatalog");
			if(spImguiHandlerCatalog.isNull())
			{
				feX("could not create ImguiHandlerCatalog");
			}

			spImguiHandlerCatalog->bind(spDrawI);
			spImguiHandlerCatalog->bind(sp<Catalog>(spStateCatalog));

			spQuickViewerI->insertDrawHandler(spImguiHandlerCatalog);
			spQuickViewerI->insertEventHandler(spImguiHandlerCatalog);

			if(successful(result))
			{
				spStateCatalog->waitForConnection();

				spStateCatalog->setState<Real>("nanoDelay",10e6);
				spStateCatalog->flush();

				String netID;
				spStateCatalog->getState<String>("net:id",netID);
				feLog("net:id \"%s\"\n",netID.c_str());

				I32 flushCount=0;
				I32 serial=0;
				I32 spins=0;
				bool running=true;
				while(running)
				{
#if FALSE
					const I32 lastSerial=serial;
					sp<StateCatalog::Snapshot> spSnapshot;
					while(serial==lastSerial)
					{
						result=spStateCatalog->snapshot(spSnapshot);
						flushCount=spSnapshot->flushCount();
						serial=spSnapshot->serial();
					}

//					feLog("Snapshot %d\n",spSnapshot->serial());
//					spSnapshot->dump();
//					spSnapshot->dumpUpdates();

					spSnapshot->getState<bool>("running",running);

					//* TODO ImguiHandlerSnapshot

					spQuickViewerI->run(1);
#else

					{
						StateCatalog::Atomic atomic(spStateCatalog,flushCount);
						result=atomic.getState<bool>("running",running);

//						SpatialTransform xform;
//						setIdentity(xform);
//
//						result=atomic.getState<SpatialTransform>(
//								"vehicles.0.chassis.transform",xform);
//						feLog("xform\n%s\n",c_print(xform));

						spQuickViewerI->run(1);
					}

					milliSleep(100);
#endif
				}

				spStateCatalog->stop();
			}

			spHiveHost=NULL;
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
