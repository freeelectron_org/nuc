/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef vehicle_viewer__H
#define vehicle_viewer__H

#include <stdint.h>
#include <thread>

#include "fe/plugin.h"
#include "math/math.h"
#include "signal/signal.h"
#include "apiary/apiary.h"

#endif
